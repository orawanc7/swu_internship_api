<?php

namespace App\Apis;
use App\Repositories\AmphurRepository;
use App\Entities\ItsCProvince;
use App\Entities\ItsCAmphur;

class AmphurController extends BaseController {
    public function list() {    
        $rep = new AmphurRepository();
        $result = $rep->list();
        
        $data = array();        
        foreach ($result as $row) {
            $data[] = array(                
                "amphurId" => $row['amphurId'],
                "amphurNameTh" => $row['amphurNameTh'],
                "amphurNameEn" => $row['amphurNameEn'],
                "provinceId" => $row['provinceId'],
                "provinceNameTh" => $row['provinceNameTh'],
                "provinceNameEn" => $row['provinceNameEn'],
                "activeFlag" => $row['activeFlag'],
            );            
        }
                
        $alldata = array (
            'data' => $data
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }

    public function listByProvince($param) {
        $provinceId = $param['provinceId'];            
        
        $rep = new AmphurRepository();
        $result = $rep->listByProvince($provinceId);
        
        $data = array();        
        foreach ($result as $row) {
            $data[] = array(                
                "amphurId" => $row['amphurId'],
                "amphurNameTh" => $row['amphurNameTh'],
                "amphurNameEn" => $row['amphurNameEn'],
                "provinceId" => $row['provinceId'],
                "provinceNameTh" => $row['provinceNameTh'],
                "provinceNameEn" => $row['provinceNameEn'],
                "activeFlag" => $row['activeFlag'],
            );            
        }
                
        $alldata = array (
            'data' => $data
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }

    public function get($param) {    
        $amphurId = $param['amphurId'];        

        $rep = new AmphurRepository();
        $result = $rep->get($amphurId);

        $data = array();
        if ($result!=null) {
            $data = array(                
                "amphurId" => $result['amphurId'],
                "amphurNameTh" => $result['amphurNameTh'],
                "amphurNameEn" => $result['amphurNameEn'],
                "provinceId" => $result['provinceId'],
                "provinceNameTh" => $result['provinceNameTh'],
                "provinceNameEn" => $result['provinceNameEn'],
                "activeFlag" => $result['activeFlag'],   
            );            
        }
                
        $alldata = array (
            'data' => $data
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);        
    }

    public function save() {    
        $amphurId = $_REQUEST['amphurId'];        
        $amphurNameTh = $_REQUEST['amphurNameTh'];        
        $amphurNameEn = $_REQUEST['amphurNameEn'];
        $provinceId = $_REQUEST['provinceId'];                        
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";

        $province = new ItsCProvince();
        $province->setProvinceId($provinceId);
        
        $amphur = new ItsCAmphur();                
        $amphur->setAmphurId($amphurId);
        $amphur->setAmphurNameTh($amphurNameTh);
        $amphur->setAmphurNameEn($amphurNameEn);        
        $amphur->setActiveFlag($activeFlag);
        $amphur->setProvince($province);

        $result = array();
        try {
            $rep = new AmphurRepository();
            $result = $rep->save($amphurId,$amphur);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function delete($param) {    
        $amphurId = $param['amphurId'];
               
        $result = array();
        try {
            $rep = new AmphurRepository();
            $result = $rep->delete($amphurId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
}

