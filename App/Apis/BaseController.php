<?php

namespace App\Apis;

class BaseController
{
    public function __construct() { 
        header("Access-Control-Allow-Origin: *");        
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Max-Age: 1000");        
        header("Access-Control-Allow-Headers: X-Requested-With, Content-Type, Origin, Cache-Control, Pragma, Authorization, Accept, Accept-Encoding, X-Mashape-Authorization,X-Auth-Token,X-File-Name,multipart/form-data
");
        // header("Access-Control-Allow-Methods: PUT, HEAD,POST, GET, OPTIONS, DELETE,OPTIONS");
        header("Access-Control-Allow-Methods: HEAD, GET, PUT, POST, DELETE, OPTIONS");
    }
}
