<?php

namespace App\Apis;
use App\Repositories\StdMasterRepository;
use App\Repositories\PersonMasterRepository;

class ColorController extends BaseController {
    public function change($param) {    
        $color = $param['color'];
        
        $_SESSION['COLOR'] = $color;


        //Update Theme
        if ($_SESSION['TYPE']=='STUDENT') {
            $rep = new StdMasterRepository();
            $data = $rep->saveTheme($_SESSION['ID'],$color);
        } else {
            $rep = new PersonMasterRepository();
            $data = $rep->saveTheme($_SESSION['ID'],$color);
        }
                
    }    
}

