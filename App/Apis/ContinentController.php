<?php

namespace App\Apis;
use App\Repositories\ContinentRepository;
use App\Entities\ItsCContinent;

class ContinentController extends BaseController {
    public function list() {    
        
        $rep = new ContinentRepository();
        $result = $rep->list();
                               
        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }

    public function get($param) {    
        $continentId = $param['continentId'];        

        $rep = new ContinentRepository();
        $result = $rep->get($continentId);
       
        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);        
    }

    public function save() {    
        $continentId = $_REQUEST['continentId'];        
        $continentNameTh = $_REQUEST['continentNameTh'];        
        $continentNameEn = $_REQUEST['continentNameEn'];                
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";
        
        $continent = new ItsCContinent();        
        $continent->setContinentId($continentId);
        $continent->setContinentNameTh($continentNameTh);
        $continent->setContinentNameEn($continentNameEn);        
        $continent->setActiveFlag($activeFlag);

        $result = array();
        try {
            $rep = new ContinentRepository();
            $result = $rep->save($continentId,$continent);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function delete($param) {    
        $continentId = $param['continentId'];
               
        $result = array();
        try {
            $rep = new ContinentRepository();
            $result = $rep->delete($continentId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
}

