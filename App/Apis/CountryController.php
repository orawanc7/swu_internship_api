<?php

namespace App\Apis;
use App\Repositories\CountryRepository;

class CountryController extends BaseController {
    public function list() {    
        
        $rep = new CountryRepository();
        $result = $rep->list();
                        
        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }
}