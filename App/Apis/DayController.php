<?php

namespace App\Apis;

use App\Repositories\DayRepository;

class DayController extends BaseController
{
    public function list()
    {

        $rep = new DayRepository();
        $result = $rep->list();

        $alldata = array(
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($alldata);
    }
}
