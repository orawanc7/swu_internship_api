<?php

namespace App\Apis;
use App\Repositories\DistrictRepository;
use App\Entities\ItsCProvince;
use App\Entities\ItsCAmphur;
use App\Entities\ItsCDistrict;

class DistrictController extends BaseController {
    public function list() {    
        $rep = new DistrictRepository();
        $result = $rep->list();
        
        $data = array();        
        foreach ($result as $row) {
            $data[] = array(                
                "districtId" => $row['districtId'],
                "districtNameTh" => $row['districtNameTh'],
                "districtNameEn" => $row['districtNameEn'],
                "amphurId" => $row['amphurId'],
                "amphurNameTh" => $row['amphurNameTh'],
                "amphurNameEn" => $row['amphurNameEn'],
                /** **/
                "personId" => $row['personId'],
                "waashing" => $row['personNameTh'],
                "amphurNameTh" => $row['amphurNameTh'],
                "amphurNameEn" => $row['amphurNameEn'],
                /** **/
                "activeFlag" => $row['activeFlag'],
            );            
        }
                
        $alldata = array (
            'data' => $data
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }

    public function listByAmphur($param) {
        $amphurId = $param['amphurId'];            
        
        $rep = new DistrictRepository();
        $result = $rep->listByAmphur($amphurId);
        
        $data = array();        
        foreach ($result as $row) {
            $data[] = array(                
                "districtId" => $row['districtId'],
                "districtNameTh" => $row['districtNameTh'],
                "districtNameEn" => $row['districtNameEn'],
                "amphurId" => $row['amphurId'],
                "amphurNameTh" => $row['amphurNameTh'],
                "amphurNameEn" => $row['amphurNameEn'],
                "activeFlag" => $row['activeFlag'],
            );            
        }
                
        $alldata = array (
            'data' => $data
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }

    public function get($param) {            
        $districtId = $param['districtId'];        

        $rep = new DistrictRepository();
        $result = $rep->get($districtId);

        $data = array();
        if ($result!=null) {
            $data = array(                
                "districtId" => $result['districtId'],
                "districtNameTh" => $result['districtNameTh'],
                "districtNameEn" => $result['districtNameEn'],
                "amphurId" => $result['amphurId'],
                "amphurNameTh" => $result['amphurNameTh'],
                "amphurNameEn" => $result['amphurNameEn'],
                "activeFlag" => $result['activeFlag'],   
            );            
        }
                
        $alldata = array (
            'data' => $data
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);        
    }

    public function save() {    
        $districtId = $_REQUEST['districtId'];        
        $districtNameTh = $_REQUEST['districtNameTh'];        
        $districtNameEn = $_REQUEST['districtNameEn'];
        $amphurId = $_REQUEST['amphurId'];                        
        $provinceId = $_REQUEST['provinceId'];                        
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";

        $province = new ItsCProvince();
        $province->setProvinceId($provinceId);

        $amphur = new ItsCAmphur();
        $amphur->setAmphurId($amphurId);
        
        $district = new ItsCDistrict();                
        $district->setDistrictId($districtId);
        $district->setDistrictNameTh($districtNameTh);
        $district->setDistrictNameEn($districtNameEn);        
        $district->setActiveFlag($activeFlag);
        $district->setProvince($province);
        $district->setAmphur($amphur);

        $result = array();
        try {
            $rep = new DistrictRepository();
            $result = $rep->save($districtId,$district);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );            
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function delete($param) {    
        $districtId = $param['districtId'];
               
        $result = array();
        try {
            $rep = new DistrictRepository();
            $result = $rep->delete($districtId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
}

