<?php

namespace App\Apis;
use App\Repositories\DocRepository;
use App\Entities\ItsCDocMaster;

class DocController extends BaseController {
    public function list() {    
        
        $rep = new DocRepository();
        $result = $rep->list();

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);                
    }
    public function get($param) {    
        $docId = \htmlspecialchars($param['docId']);    

        $rep = new DocRepository();
        $result = $rep->get($docId);

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);          
    }

    public function save() {    
        $docId = htmlspecialchars($_REQUEST['docId']);        
        $docTypeId = htmlspecialchars($_REQUEST['docTypeId']);        
        $docNameTh = htmlspecialchars($_REQUEST['docNameTh']);        
        $docNameEn = htmlspecialchars($_REQUEST['docNameEn']);
        $docCdTh = htmlspecialchars($_REQUEST['docCdTh']);        
        $docCdEn = htmlspecialchars($_REQUEST['docCdEn']);
        $remark = htmlspecialchars($_REQUEST['remark']);
        $docFinalFlag = isset($_REQUEST['docFinalFlag'])?$_REQUEST['docFinalFlag']:"N";
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";
                
        $data = [
            'docId' => $docId,
            'docTypeId' => $docTypeId,
            'docNameTh' => $docNameTh,
            'docNameEn' => $docNameEn,
            'docCdTh' => $docCdTh,
            'docCdEn' => $docCdEn,
            'docFinalFlag' => $docFinalFlag,
            'remark' => $remark,
            'activeFlag' => $activeFlag
        ];

        $result = array();
        try {
            $rep = new DocRepository();
            $result = $rep->save($docId,$data);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function delete($param) {    
        $docId = htmlspecialchars($param['docId']);    
               
        $result = array();
        try {
            $rep = new DocRepository();
            $result = $rep->delete($docId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
}