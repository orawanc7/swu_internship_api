<?php

namespace App\Apis;
use App\Repositories\DocDownloadRepository;
use App\Entities\ItsDocDownload;
use App\Classes\Response;

class DocDownloadController extends BaseController {
    public function getAll() {    
        
        $rep = new DocDownloadRepository();
        $result = $rep->list();

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);                
    }
    public function getActive() {    
        
        $response = new Response();
        try {
            $rep = new DocDownloadRepository();
            $result = $rep->getActive();

            $data = [];
            foreach ($result as $r) {
                $info = $this->getFileInfo($r);

                $url = '';
                $fileSize = 0;

                if ($info!=null) {
                    $url = $info['url'];
                    $fileSize = number_format(filesize($info['file']) / 1048576, 2) . ' MB';
                    $data [] = [
                        'docGroupDownloadId' => $r['docGroupDownloadId'],
                        'docGroupDownloadNameTh' => $r["docGroupDownloadNameTh"],
                        'docDownloadNameTh' => $r["docDownloadNameTh"],
                        'fileUrl' => $url,
                        'fileSize' => $fileSize,
                        'fileExtension' => $r["fileExtension"],
                    ];
                }                                
            }

            $response->setData($data);
            $response->setStatus(true);
        } catch (Exception $e) {
            $response->setMessage($e->getMessage());
        }

        return $response->json();        
    }
    public function get($param) {    
        $response = new Response();
        try {
            $docDownloadId = \htmlspecialchars($param['docDownloadId']);    

            $rep = new DocDownloadRepository();
            $result = $rep->get($docDownloadId);

            if ($result!=null) {
                $info = $this->getFileInfo($result);
                
                if ($info!=null) {
                    $resultFile = [
                        'fileUrl' => $info['url']
                    ];

                    $result = array_merge($result, $resultFile);
                } 
            }

            $response->setData($result);
            $response->setStatus(true);            
        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }       
        return $response->json();  
    }

    private function getFileInfo ($result) {
        //\var_dump($result); exit;
        $downloadPath = getenv('DOWNLOAD_PATH');
        $fileExtension = $result['fileExtension'];
            
        $filePath = $downloadPath . '/' . $result['docDownloadId'] . '.' . $fileExtension;

        if (\file_exists($filePath)) {
            return [
                'file' => $filePath,
                'url' => 'DocDownload/download/' . $result['docDownloadId']
            ];                        
        }
        return null;
    }

    public function save() {    
        $docDownloadId = htmlspecialchars($_REQUEST['docDownloadId']);        
        $docGroupDownloadId = htmlspecialchars($_REQUEST['docGroupDownloadId']);        
        $docDownloadNameTh = htmlspecialchars($_REQUEST['docDownloadNameTh']);        
        $docDownloadNameEn = htmlspecialchars($_REQUEST['docDownloadNameEn']);
        $seqid = isset($_REQUEST['seqId'])?$_REQUEST['seqId']:0;        
        $remark = htmlspecialchars($_REQUEST['remark']);
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";
                
        $data = [
            'docDownloadId' => $docDownloadId,
            'docGroupDownloadId' => $docGroupDownloadId,
            'docDownloadNameTh' => $docDownloadNameTh,
            'docDownloadNameEn' => $docDownloadNameEn,
            'remark' => $remark,
            'activeFlag' => $activeFlag
        ];

        $result = array();
        try {
            $rep = new DocDownloadRepository();
            $result = $rep->save($docDownloadId,$data);

            if (!empty($_FILES)) {
                $fileExtension = $this->uploadFile($docDownloadId,$_FILES);

                $rep->saveFileExtension($docDownloadId,$fileExtension);
            }            

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function uploadFile ($id,$file) {
        $downloadPath = getenv('DOWNLOAD_PATH');            
        $filePath = $file['file']['name'];            
        
        $ext = strtolower(pathinfo($filePath, PATHINFO_EXTENSION));

        $uploadFilePath = $downloadPath . "/" . $id . "." . $ext;
        

        if (file_exists($uploadFilePath)) {
            unlink($uploadFilePath);
        }
       // echo  $downloadPath;exit;
        move_uploaded_file($_FILES['file']['tmp_name'], $uploadFilePath);

        return $ext;
    }

    public function delete($param) {    
        $docDownloadId = htmlspecialchars($param['docDownloadId']);    
               
        $result = array();
        try {
            $rep = new DocDownloadRepository();
            $result = $rep->delete($docDownloadId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function download ( $param ) {                
        $docDownloadId = \htmlspecialchars($param['docDownloadId']);            

        $downloadPath = getenv('DOWNLOAD_PATH');

        $rep = new DocDownloadRepository();
        $result = $rep->get($docDownloadId);

        if ($result!=null) {
            
            $fileExtension = $result['fileExtension'];
            $fileName = $result['docDownloadId'] ;
            $filePath = $downloadPath . '/' . $fileName . '.' . $fileExtension;

            $fileNameDownload = $result['docDownloadNameTh'] . '.' . $fileExtension;            

            if (\file_exists($filePath)) {
                header("Cache-Control: public");
                header("Content-Description: File Transfer");
                if ($fileExtension == "pdf") {
                    header('Content-type: application/pdf');
                } else {
                    header('Content-Type: application/octet-stream');
                }
                header("Content-Disposition: attachment; filename=$fileNameDownload");
                header("Content-Transfer-Encoding: binary");

                readfile($filePath);
            }
        }
    }
}