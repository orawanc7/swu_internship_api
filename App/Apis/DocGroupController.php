<?php

namespace App\Apis;
use App\Repositories\DocDownloadRepository;
use App\Entities\ItsCDocDownload;

class DocDownloadController extends BaseController {
    public function list() {    
        
        $rep = new DocDownloadRepository();
        $result = $rep->list();

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);                
    }
    public function get($param) {    
        $docDownloadId = \htmlspecialchars($param['docDownloadId']);    

        $rep = new DocDownloadRepository();
        $result = $rep->get($docDownloadId);

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);          
    }

    public function save() {    
        $docDownloadId = htmlspecialchars($_REQUEST['docDownloadId']);        
        $docGroupDownloadId = htmlspecialchars($_REQUEST['docGroupDownloadId']);        
        $docDownloadNameTh = htmlspecialchars($_REQUEST['docDownloadNameTh']);        
        $docDownloadNameEn = htmlspecialchars($_REQUEST['docDownloadNameEn']);        
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";
                
        $data = [
            'docDownloadId' => $docDownloadId,
            'docGroupDownloadId' => $docGroupDownloadId,
            'docDownloadNameTh' => $docDownloadNameTh,
            'docDownloadNameEn' => $docDownloadNameEn,            
            'activeFlag' => $activeFlag
        ];

        $result = array();
        try {
            $rep = new DocDownloadRepository();
            $result = $rep->save($docDownloadId,$data);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function delete($param) {    
        $docDownloadId = htmlspecialchars($param['docDownloadId']);    
               
        $result = array();
        try {
            $rep = new DocDownloadRepository();
            $result = $rep->delete($docDownloadId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
}