<?php

namespace App\Apis;
use App\Repositories\DocGroupDownloadRepository;
use App\Entities\ItsDocGroupDownload;

class DocGroupDownloadController extends BaseController {
    public function list() {    
        
        $rep = new DocGroupDownloadRepository();
        $result = $rep->list();

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);                
    }
    public function get($param) {    
        $docGroupDownloadId = \htmlspecialchars($param['docGroupDownloadId']);    

        $rep = new DocGroupDownloadRepository();
        $result = $rep->get($docGroupDownloadId);

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);          
    }

    public function save() {    
        $docGroupDownloadId = htmlspecialchars($_REQUEST['docGroupDownloadId']);        
        $docGroupDownloadNameTh = htmlspecialchars($_REQUEST['docGroupDownloadNameTh']);        
        $docGroupDownloadNameEn = htmlspecialchars($_REQUEST['docGroupDownloadNameEn']);     
        $seqid = isset($_REQUEST['seqId'])?$_REQUEST['seqId']:0;        
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";
        
        $docGroupDownload = new ItsDocGroupDownload();        
        $docGroupDownload->setDocGroupDownloadId($docGroupDownloadId);
        $docGroupDownload->setDocGroupDownloadNameTh($docGroupDownloadNameTh);
        $docGroupDownload->setDocGroupDownloadNameEn($docGroupDownloadNameEn);             
        $docGroupDownload->setSeqId($seqid);             
        $docGroupDownload->setActiveFlag($activeFlag);                

        $result = array();
        try {
            $rep = new DocGroupDownloadRepository();
            $result = $rep->save($docGroupDownloadId,$docGroupDownload);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function delete($param) {    
        $docGroupDownloadId = htmlspecialchars($param['docGroupDownloadId']);    
               
        $result = array();
        try {
            $rep = new DocGroupDownloadRepository();
            $result = $rep->delete($docGroupDownloadId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
}