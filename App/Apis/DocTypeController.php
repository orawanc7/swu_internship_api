<?php

namespace App\Apis;
use App\Repositories\DocTypeRepository;
use App\Entities\ItsCDocType;

class DocTypeController extends BaseController {
    public function list() {                    
        $rep = new DocTypeRepository();
        $result = $rep->list();

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);                
    }
    public function get($param) {    
        $docTypeId = \htmlspecialchars($param['docTypeId']);    

        $rep = new DocTypeRepository();
        $result = $rep->get($docTypeId);

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);          
    }

    public function save() {    
        $docTypeId = htmlspecialchars($_REQUEST['docTypeId']);        
        $docTypeTh = htmlspecialchars($_REQUEST['docTypeTh']);        
        $docTypeEn = htmlspecialchars($_REQUEST['docTypeEn']);
        $docFormType = htmlspecialchars($_REQUEST['docTypeEn']);        
        $remark = htmlspecialchars($_REQUEST['remark']);
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";
        $docScreenFlag = isset($_REQUEST['docScreenFlag'])?$_REQUEST['docScreenFlag']:"F"; /* F,S*/
        
        $docType = new ItsCDocType();        
        $docType->setDocTypeId($docTypeId);
        $docType->setDocTypeTh($docTypeTh);
        $docType->setDocTypeEn($docTypeEn);  
        $docType->setRemark($remark);      
        $docType->setActiveFlag($activeFlag);        
        $docType->setDocScreenFlag($docScreenFlag);

        $result = array();
        try {
            $rep = new DocTypeRepository();
            $result = $rep->save($docTypeId,$docType);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function delete($param) {    
        $docTypeId = htmlspecialchars($param['docTypeId']);    
               
        $result = array();
        try {
            $rep = new DocTypeRepository();
            $result = $rep->delete($docTypeId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
}