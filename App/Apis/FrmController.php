<?php

namespace App\Apis;

use App\Repositories\FrmRepository;

class FrmController extends BaseController
{
    public function getAll() {    
        
        $rep = new FrmRepository();
        $result = $rep->getAll();

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);                
    }
    
    public function get($param) {    
        $frmHdrId = \htmlspecialchars($param['frmHdrId']);    

        $rep = new FrmRepository();
        $result = $rep->get($frmHdrId);

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);          
    }

    public function getTemplate($param) {    
        $frmHdrId = \htmlspecialchars($param['frmHdrId']);    

        $rep = new FrmRepository();
        $result = $rep->getTemplate($frmHdrId);

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);          
    }

    public function save() {    
        $frmHdrId = htmlspecialchars($_REQUEST['frmHdrId']);        
        $frmNameTh = htmlspecialchars($_REQUEST['frmNameTh']);        
        $frmNameEn = htmlspecialchars($_REQUEST['frmNameEn']);
        $docId = htmlspecialchars($_REQUEST['docId']);        
        $remark = htmlspecialchars($_REQUEST['remark']);
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";        
        
        
        $data = [
            'frmHdrId' => $frmHdrId,
            'frmNameTh' => $frmNameTh,
            'frmNameEn' => $frmNameEn,
            'docId' => $docId,
            'remark' => $remark,
            'activeFlag' => $activeFlag
        ];

        $result = array();
        try {
            $rep = new FrmRepository();
            $result = $rep->save($frmHdrId,$data);            

            $result = array (
                "status"=>true,
                "id" => $result
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function saveTemplate() {    
        $frmHdrId = htmlspecialchars($_REQUEST['frmHdrId']);        
        $frmTemplate = $_REQUEST['frmTemplate'];

        $result = array();
        try {
            $rep = new FrmRepository();
            $result = $rep->saveTemplate($frmHdrId,$frmTemplate);            

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function delete($param) {    
        $frmHdrId = htmlspecialchars($param['frmHdrId']);    
               
        $result = array();
        try {
            $rep = new FrmRepository();
            $result = $rep->delete($frmHdrId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
}
