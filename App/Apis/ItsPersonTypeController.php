<?php

namespace App\Apis;

use App\Repositories\ItsPersonTypeRepository;
use App\Classes\Response;

class ItsPersonTypeController extends BaseController
{
    
    public function getMentorByItsPersonId($param)
    {
        $response = new Response();
        try {
            $itsPersonId = $param['itsPersonId'];

            $rep = new ItsPersonTypeRepository();
            $data = $rep->getMentorByItsPersonId($itsPersonId);

            $response->setStatus(true);
            $response->setData($data);
        } catch(Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json();
    }    
}
