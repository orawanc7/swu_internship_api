<?php

namespace App\Apis;
use App\Repositories\LearningGroupRepository;
use App\Entities\ItsCLearningGroup;

class LearningGroupController extends BaseController {
    public function list() {    
        
        $rep = new LearningGroupRepository();
        $result = $rep->list();
        
        $data = array();        
        foreach ($result as $row) {
            $data[] = array(                
                "learningGroupId" => $row['learningGroupId'],
                "learningGroupNameTh" => $row['learningGroupNameTh'],
                "learningGroupNameEn" => $row['learningGroupNameEn'],
                "activeFlag" => $row['activeFlag'],
            );            
        }
                
        $alldata = array (
            'data' => $data
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }

    public function get($param) {    
        $learningGroupId = $param['learningGroupId'];        

        $rep = new LearningGroupRepository();
        $result = $rep->get($learningGroupId);

        $data = array();
        if ($result!=null) {
            $data = array(                
                "learningGroupId" => $result['learningGroupId'],
                "learningGroupNameTh" => $result['learningGroupNameTh'],
                "learningGroupNameEn" => $result['learningGroupNameEn'],
                "activeFlag" => $result['activeFlag'],
                "remark"=>$result['remark']
            );            
        }
                
        $alldata = array (
            'data' => $data
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);        
    }

    public function save() {    
        $learningGroupId = $_REQUEST['learningGroupId'];        
        $learningGroupNameTh = $_REQUEST['learningGroupNameTh'];        
        $learningGroupNameEn = $_REQUEST['learningGroupNameEn'];        
        $remark = $_REQUEST['remark'];        
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";
        
        $learningGroup = new ItsCLearningGroup();        
        $learningGroup->setLearningGroupId($learningGroupId);
        $learningGroup->setLearningGroupNameTh($learningGroupNameTh);
        $learningGroup->setLearningGroupNameEn($learningGroupNameEn);
        $learningGroup->setRemark($remark);
        $learningGroup->setActiveFlag($activeFlag);

        $result = array();
        try {
            $rep = new LearningGroupRepository();
            $result = $rep->save($learningGroupId,$learningGroup);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function delete($param) {    
        $learningGroupId = $param['learningGroupId'];
               
        $result = array();
        try {
            $rep = new LearningGroupRepository();
            $result = $rep->delete($learningGroupId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
}

