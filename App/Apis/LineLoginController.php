<?php

namespace App\Apis;

class LineLoginController extends BaseController {        
    public function index() {        
        $url = "https://access.line.me/oauth2/v2.1/authorize?" .
                 "response_type=code&" .
                 "client_id=1564278694&" .
                 "redirect_uri=" . urlencode('http://localhost/internship_portal/api/linelogin/result') . "&" .
                 "state=1234&" .
                 "scope=profile"
                 ;

        header ('Location: ' . $url);
    }   

    public function result () {
        /*
            Array ( [code] => 5o3P7n4hyxItAQhj2fme [state] => 1234 )
        */
        $url = 'https://api.line.me/oauth2/v2.1/token';
        $accesstoken = $_GET['code'];

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded'
        ));

        $data = 
        "grant_type=authorization_code&" .
        "code=" . $accesstoken . "&"  .
        "redirect_uri=" . urlencode('http://localhost/internship_portal/api/linelogin/result') . "&" .
        "client_id=1564278694&" .
        "client_secret=153d2511c5edfa77cd841ea29358d874";
        
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);        
        
        /*
            {"access_token":"eyJhbGciOiJIUzI1NiJ9.UKWGW_6124VnoRMDVmaKcgmvhbUQ13Y2xASCfZNi81D3aw-ENVeRBrEPYNxUFKzrU9XS2uIffR9oaDT_4zx9Ar3cPSkqTRq0hAQrs7Uz9U_aELVwan_BUyCI6kATN14K94s-QyWqYLGCrfANV38p5gWVaCa_2bgTINyw2pUzVzU.2_CdsLwvl4YSEZ8rQ9WsuZibDvWP_mFY7GPPZBg_Bm8","token_type":"Bearer","refresh_token":"m8545eZgoGDg81rIyNev","expires_in":2592000,"scope":"profile"}
        */
        $response = curl_exec($ch);
        print_r($response);
    }
}