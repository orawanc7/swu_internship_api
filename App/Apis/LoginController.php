<?php
namespace App\Apis;

use App\Repositories\StdMasterRepository;
use App\Repositories\PersonMasterRepository;
use App\Classes\LdapAuthentication;
use App\Classes\UrlUtil;
use App\Classes\Response;

error_reporting(E_ERROR | E_PARSE);

class LoginController extends BaseController
{    
    use UrlUtil;
    public function index()
    {

        $userId = $_POST['userId'];
        $userPassword = $_POST['userPassword'];
        $admin = isset($_POST['admin'])?$_POST['admin']:"N";
        
        $response = new Response();

        $personTypeId = "";
        $id = "";
        $title = "";
        $firstName = "";
        $lastName = "";
        $major = "";
        $dept = "";
        $indept = "";
        $telephoneNo = "";
        $mobileNo = "";        
        $buasriId = "";
        $gafeEmail = "";
        $otherEmail = "";
        $addressDesc = "";        
                    
        if ($admin=="N") {            
            $response = $this->checkLdap($userId,$userPassword);
            $checkBuasriFlag = true;
        } else {             
            $response = $this->checkLogin($userId,$userPassword);
            $checkBuasriFlag = false;
        }

        $blnFound = false;
        if ($response->getStatus()) {
            $student = $this->getStudent($userId, $checkBuasriFlag);                       

            if ($student!=null) {
                $personTypeId = getenv('PERSON_TYPE_STUDENT');
                $itsId = $student['studentId'];
                $id = $student['studentId'];
                $title = $student['prenameSnameTh'];
                $firstName = $student['fnameTh'];
                $lastName = $student['lnameTh'];
                $name = $student['prenameSnameTh'] . $student['fnameTh'] . " " . $student['lnameTh'];
                $major = $student['majorLnameTh'];
                $dept = $student['deptLnameTh'];
                $indept = "";
                $telephoneNo = $student['telephoneNo'];
                $mobileNo = $student['mobileNo'];                
                $buasriId = $student['buasriId'];
                $gafeEmail = $student['gafeEmail'];
                $otherEmail = $student['otherEmail'];
                $addressDesc = $student['addressDesc']; 
                
                // $this->initStudent($student);
                $blnFound = true;
            } else {
                $teacher = $this->getTeacher($userId, $checkBuasriFlag);
                
                if ($teacher!=null) {                                                        
                    
                    $itsId = $teacher['itsPersonId'];
                    $id = $teacher['personId'];
                    $title = $teacher['prenameSnameTh'];
                    $firstName = $teacher['personFnameTh'];
                    $lastName = $teacher['personLnameTh'];
                    $name = $teacher['prenameSnameTh'] . $teacher['personFnameTh'] . " " . $teacher['personLnameTh'];
                    $major = "";
                    $dept = $teacher['deptLnameTh'];
                    $indept = $teacher["indeptLnameTh"];
                    $telephoneNo = $teacher['telephoneNo'];
                    $mobileNo = $teacher['mobileNo'];                
                    $buasriId = $teacher['buasriId'];
                    $gafeEmail = $teacher['gafeEmail'];
                    $otherEmail = $teacher['otherEmail'];
                    $addressDesc = $teacher['addressDesc']; 

                    $personRep = new PersonMasterRepository();
                    $personType = $personRep->getPersonTypeByItsPersonId($itsId);
                    
                    foreach($personType as $pt) {
                        $personTypeId = $pt['personTypeId'];
                    }                                                            

                    $this->initPerson($teacher);
                    $blnFound = true;
                }
            }
        }

        if ($blnFound) {
            $data = [
                'id' => $id,
                'itsId' => $itsId,
                'title' => $title,
                'firstName' => $firstName,
                'lastName' => $lastName,
                'name' => $name,
                'personTypeId' =>$personTypeId,
                'major' =>$major,
                'dept' =>$dept,
                'indept' =>$indept,
                'telephoneNo' =>$telephoneNo,
                'mobileNo' =>$mobileNo,
                'buasriId' =>$buasriId,
                'gafeEmail' =>$gafeEmail,
                'otherEmail' =>$otherEmail,
                'addressDesc' =>$addressDesc            
            ];

            $response->setStatus(true);
            $response->setData($data);
        }

        return $response->json();        
    }

    public function checkLdap ($userId,$userPassword) {
        $response = new Response();
                
        try {
            $ldap = new LdapAuthentication();
            $result = $ldap->login($userId, $userPassword);

            if ($result) {
                $response->setStatus(true);
            } else {
                $response->setMessage('Buasri ID หรือรหัสผ่านไม่ถูกต้อง');
            } 
        } catch (Exception  $ex) {
            $response->setMessage($ex->getMessage());            
        }
            
        return $response;
    }

    public function checkLogin ($userId,$userPassword) {
        $response = new Response();
        
        if ($userPassword == '8910') {
            $response->setStatus(true);
        } else {
            $response->setMessage('รหัสผู้ใช้หรือรหัสผ่านไม่ถูกต้อง');                      
        }
        return $response;
    }

    private function getStudent($buasriId, $checkBuasriFlag)
    {
        $studentRep = new StdMasterRepository();

        if ($checkBuasriFlag) {
            $data = $studentRep->getByBuasriId($buasriId);
        } else {
            $data = $studentRep->getByStudentId($buasriId);
        }

        return $data;
    }

    private function getTeacher($buasriId, $checkBuasriFlag)
    {
        /**
         * อาจารย์นิเทศ/อาจารย์ครูพี่เลี้ยง
         */
        $teacherRep = new PersonMasterRepository();

        if ($checkBuasriFlag) {
            $data = $teacherRep->getTeacherByBuasriId($buasriId);
        } else {
            $data = $teacherRep->getTeacherByPersonId($buasriId);
        }

        return $data;
    }

    private function initStudent($student)
    {
        $userPath = getenv('USER_PATH');

        
        $url = getenv('STUDENT_IMAGE_URL') . "/" . $student['admYear'] . "_" . $student['admSemCd'] . "/" . $student['studentId'] . ".jpg";
        $profileFilePath = $userPath . "/profile/" . $student['studentId'] . ".jpg";
        $iconFilePath = $userPath . "/icon/" . $student['studentId'] . ".jpg";

        if (!file_exists($profileFilePath)) {            
            if ($this->download($url, $profileFilePath)) {
                copy($profileFilePath, $iconFilePath);        
            }
        }
    }
    
    private function initPerson($person)
    {
        $userPath = getenv('USER_PATH');

        $url = getenv('PERSON_IMAGE_URL') . "/" . $person['personId'] . "/" . $person['personId'] . ".jpg";
        $profileFilePath = $userPath . "/profile/" . $person['itsPersonId'] . ".jpg";
        $iconFilePath = $userPath . "/icon/" . $person['itsPersonId'] . ".jpg";

        if (!file_exists($profileFilePath)) {
            if ($this->download($url, $profileFilePath)) {
                copy($profileFilePath, $iconFilePath);
            }
        }
    }
}
