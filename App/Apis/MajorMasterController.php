<?php

namespace App\Apis;
use App\Repositories\MajorMasterRepository;
use App\Entities\ItsCMajorMaster;

class MajorMasterController extends BaseController {
    public function list() {    
        
        $rep = new MajorMasterRepository();
        $result = $rep->list();
        
        $data = array();        
        foreach ($result as $row) {
            $data[] = array(                
                "itsMajorId" => $row['itsMajorId'],
                "itsMajorSnameTh" => $row['itsMajorSnameTh'],
                "itsMajorSnameEn" => $row['itsMajorSnameEn'],
                "itsMajorLnameTh" => $row['itsMajorLnameTh'],
                "itsMajorLnameEn" => $row['itsMajorLnameEn'],
                "majorCd" => $row['majorCd'],
                "majorLnameTh" => $row['itsMajorLnameTh'],
                "majorLnameEn" => $row['majorLnameEn'],
                "activeFlag" => $row['activeFlag'],
            );            
        }
                
        $alldata = array (
            'data' => $data
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }

    public function get($param) {    
        $itsMajorId = $param['itsMajorId'];        

        $rep = new MajorMasterRepository();
        $result = $rep->get($itsMajorId);

        $data = array();
        if ($result!=null) {
            $data[] = array(                
                "itsMajorId" => $row['itsMajorId'],
                "itsMajorSnameTh" => $row['itsMajorSnameTh'],
                "itsMajorSnameEn" => $row['itsMajorSnameEn'],
                "itsMajorLnameTh" => $row['itsMajorLnameTh'],
                "itsMajorLnameEn" => $row['itsMajorLnameEn'],
                "majorCd" => $row['majorCd'],
                "majorLnameTh" => $row['itsMajorLnameTh'],
                "majorLnameEn" => $row['majorLnameEn'],
                "activeFlag" => $row['activeFlag'],
            );
        }
                
        $alldata = array (
            'data' => $data
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);        
    }

    public function save() {    
        $itsMajorId = $_REQUEST['itsMajorId'];        
        $itsMajorSnameTh = $_REQUEST['itsMajorSnameTh'];        
        $itsMajorSnameEn = $_REQUEST['itsMajorSnameEn'];                
        $itsMajorLnameTh = $_REQUEST['itsMajorLnameTh'];        
        $itsMajorLnameEn = $_REQUEST['itsMajorLnameEn'];                
        $majorCd = $_REQUEST['majorCd'];                
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";
        
        $itsMajor = new ItsCMajorMaster();        
        $itsMajor->setItsMajorId($itsMajorId);
        $itsMajor->setItsMajorSnameTh($itsMajorSnameTh);
        $itsMajor->setItsMajorSnameEn($itsMajorSnameEn);        
        $itsMajor->setItsMajorLnameTh($itsMajorLnameTh);
        $itsMajor->setItsMajorLnameEn($itsMajorLnameEn);        
        $itsMajor->setActiveFlag($activeFlag);

        $result = array();
        try {
            $rep = new MajorMasterRepository();
            $result = $rep->save($itsMajorId,$itsMajor);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function delete($param) {    
        $itsMajorId = $param['itsMajorId'];
               
        $result = array();
        try {
            $rep = new MajorMasterRepository();
            $result = $rep->delete($itsMajorId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
}
