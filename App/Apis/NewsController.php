<?php

namespace App\Apis;
use App\Repositories\NewsRepository;
use App\Entities\ItsNews;
use App\Entities\ItsNewsReceiver;

class NewsController extends BaseController {
    public function listBySenderReceiver($param) {    
        $senderId = $param['senderId'];        
        $receiverId = $param['receiverId']; 

        $rep = new NewsRepository();
        $result = $rep->listBySenderReceiver($senderId,$receiverId);        

        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);                
    }    

    public function listByReceiver($param) {    
        $receiverId = $param['receiverId'];                

        $rep = new NewsRepository();
        $result = $rep->listByReceiver($receiverId);        

        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);                
    }    

    public function listBySender($param) {    
        $senderId = $param['senderId'];                

        $rep = new NewsRepository();
        $result = $rep->listBySender($senderId);        

        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);                
    }    

    public function saveTeacher () {
        $itsPersonId = $_REQUEST['itsPersonId'];
        $newsTitleNameTh = $_REQUEST['newsTitleNameTh'];
        $newsDescTh = $_REQUEST['newsDescTh'];
        
        $news = new ItsNews();
        $news->setSendBy($itsPersonId);
        $news->setNewsTitleNameTh($newsTitleNameTh);
        $news->setNewsDescTh($newsDescTh);
        
        $receivers = array();
        foreach($_REQUEST['receiverId'] as $receiverId) {
            $receiver = new ItsNewsReceiver();
            $receiver->setReceiverId($receiverId);
            $receivers[] =  $receiver;
        }

        $result = array();
        try {
            $rep = new NewsRepository();
            $rep->saveTeacher($news,$receivers);

            $result = array(
                "status" => true
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);        
    }

    public function delete($param)
    {
        $itsNewsId = $param['itsNewsId'];

        $result = array();
        try {
            $rep = new NewsRepository();
            $result = $rep->delete($itsNewsId);
            
            $result = array(
                "status" => $result
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }

    public function deleteByReceiver($param)
    {
        $newsReceiverId = $param['newsReceiverId'];

        $result = array();
        try {
            $rep = new NewsRepository();
            $result = $rep->deleteByReceiver($newsReceiverId);
            
            $result = array(
                "status" => $result
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }
}