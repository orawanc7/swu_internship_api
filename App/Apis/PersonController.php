<?php

namespace App\Apis;

use App\Repositories\PersonMasterRepository;
use App\Classes\Response;

class PersonController extends BaseController
{
    public function get($param)
    {
        $response = new Response();
        try {

            $itsPersonId = $param['itsPersonId'];

            $rep = new PersonMasterRepository();
            $data = $rep->get($itsPersonId);

            $response->setStatus(true);
            $response->setData($data);

        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json();   
    }

    public function getExternalByName($param)
    {
        $name = $_REQUEST['name'];

        $rep = new PersonMasterRepository();
        $data = $rep->getExternalByName($name);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($data);
    }

    public function getExternalByDeptAndName($param)
    {
        $response = new Response();
        try {
            $deptCd = $_REQUEST['deptCd'];
            $name = $_REQUEST['name'];

            $rep = new PersonMasterRepository();
            $data = $rep->getExternalByDeptAndName($deptCd, $name);

            $response->setStatus(true);
            $response->setData($data);

        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json();   
    }

    public function save()
    {
        $itsRoundId = $_REQUEST['itsRoundId'];
        $itsPersonId = $_REQUEST['itsPersonId'];
        $itsPersonTypeId = $_REQUEST['itsPersonTypeId'];
        $personTypeId = $_REQUEST['personTypeId'];

        $prenameIntThCd = $_REQUEST['prenameIntThCd'];
        $personFnameTh = trim($_REQUEST['personFnameTh']);
        $personLnameTh = trim($_REQUEST['personLnameTh']);
        $mobileNo = trim($_REQUEST['mobileNo']);
        $otherEmail = trim($_REQUEST['otherEmail']);
        $deptCd = trim($_REQUEST['deptCd']);

        $data = array(
            'itsRoundId' => $itsRoundId,
            'itsPersonId' => $itsPersonId,
            'deptCd' => $deptCd,
            'prenameIntThCd' => $prenameIntThCd,
            'personFnameTh' => $personFnameTh,
            'personLnameTh' => $personLnameTh,
            'mobileNo' => $mobileNo,
            'otherEmail' => $otherEmail
        );

        $result = array();
        try {
            $rep = new SchdStdMentorRepository();
            $result = $rep->save($itsPersonTypeId, $data);

            $result = array(
                "status" => true
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }

    public function delete($param)
    {
        $stdMentorId = $param['stdMentorId'];

        $result = array();
        try {
            $rep = new SchdStdMentorRepository();
            $result = $rep->delete($stdMentorId);

            $result = array(
                "status" => true
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }
}
