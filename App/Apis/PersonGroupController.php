<?php

namespace App\Apis;
use App\Repositories\PersonGroupRepository;
use App\Entities\ItsCPersonGroup;

class PersonGroupController extends BaseController {
    public function list() {    
        
        $rep = new PersonGroupRepository();
        $result = $rep->list();

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);                
    }
    public function get($param) {    
        $personGroupId = \htmlspecialchars($param['personGroupId']);    

        $rep = new PersonGroupRepository();
        $result = $rep->get($personGroupId);

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);          
    }

    public function save() {    
        $personGroupId = htmlspecialchars($_REQUEST['personGroupId']);        
        $personGroupNameTh = htmlspecialchars($_REQUEST['personGroupNameTh']);        
        $personGroupNameEn = htmlspecialchars($_REQUEST['personGroupNameEn']);
        $remark = htmlspecialchars($_REQUEST['remark']);
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";
        
        $personGroup = new ItsCPersonGroup();        
        $personGroup->setPersonGroupId($personGroupId);
        $personGroup->setPersonGroupNameTh($personGroupNameTh);
        $personGroup->setPersonGroupNameEn($personGroupNameEn);  
        $personGroup->setRemark($remark);      
        $personGroup->setActiveFlag($activeFlag);                

        $result = array();
        try {
            $rep = new PersonGroupRepository();
            $result = $rep->save($personGroupId,$personGroup);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function delete($param) {    
        $personGroupId = htmlspecialchars($param['personGroupId']);    
               
        $result = array();
        try {
            $rep = new PersonGroupRepository();
            $result = $rep->delete($personGroupId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
}