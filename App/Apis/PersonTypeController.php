<?php

namespace App\Apis;
use App\Repositories\PersonTypeRepository;
use App\Entities\ItsCPersonType;

class PersonTypeController extends BaseController {
    public function list() {    
        
        $rep = new PersonTypeRepository();
        $result = $rep->list();

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);                
    }
    public function get($param) {    
        $personTypeId = \htmlspecialchars($param['personTypeId']);    

        $rep = new PersonTypeRepository();
        $result = $rep->get($personTypeId);

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);          
    }

    public function save() {    
        $personTypeId = htmlspecialchars($_REQUEST['personTypeId']);        
        $personGroupId = htmlspecialchars($_REQUEST['personGroupId']);        
        $personTypeNameTh = htmlspecialchars($_REQUEST['personTypeNameTh']);        
        $personTypeNameEn = htmlspecialchars($_REQUEST['personTypeNameEn']);
        $remark = htmlspecialchars($_REQUEST['remark']);
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";
                
        $data = [
            'personTypeId' => $personTypeId,
            'personGroupId' => $personGroupId,
            'personTypeNameTh' => $personTypeNameTh,
            'personTypeNameEn' => $personTypeNameEn,
            'remark' => $remark,
            'activeFlag' => $activeFlag
        ];

        $result = array();
        try {
            $rep = new PersonTypeRepository();
            $result = $rep->save($personTypeId,$data);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function delete($param) {    
        $personTypeId = htmlspecialchars($param['personTypeId']);    
               
        $result = array();
        try {
            $rep = new PersonTypeRepository();
            $result = $rep->delete($personTypeId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
}