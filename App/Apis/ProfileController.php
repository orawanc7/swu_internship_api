<?php

namespace App\Apis;

use App\Repositories\StdMasterRepository;
use App\Repositories\ItsPersonRepository;
use App\Entities\ItsStdMaster;
use App\Entities\ItsPersonMaster;
use App\Repositories\PersonMasterRepository;
use App\Classes\Response;

class ProfileController extends BaseController
{
    public function getImageProfile($param) {
        $itsUserId = $param['itsUserId'];
        $path = getenv('USER_PATH');

        $filePath = $path . "/profile/" . $itsUserId . ".jpg";        
        $defaultFilePath = $path . "/profile/" . "/user.jpg";
                
        if (file_exists($filePath)) {
            $fp = fopen($filePath, 'rb');
            $file = $filePath;
        } else {
            $fp = fopen($defaultFilePath, 'rb');
            $file = $defaultFilePath;
        }
        
        header("Content-Type: image/jpg");
        header("Content-Length: " . filesize($file));
        
        fpassthru($fp);
        exit;

    }

    public function getImageIcon($param) {
        $itsUserId = $param['itsUserId'];
        $path = getenv('USER_PATH');

        $filePath = $path . "/icon/" . $itsUserId . ".jpg";        
        $defaultFilePath = $path . "/icon/" . "/user.jpg";

        if (file_exists($filePath)) {
            $fp = fopen($filePath, 'rb');
            $file = $filePath;
        } else {
            $fp = fopen($defaultFilePath, 'rb');
            $file = $defaultFilePath;
        }
                
        
        
        header("Content-Type: image/jpg");
        header("Content-Length: " . filesize($file));
        
        fpassthru($fp);
        exit;
    }

    public function save()
    {
        $itsId = $_REQUEST['itsId'];
        $otherEmail = $_REQUEST['otherEmail'];
        $telephoneNo = $_REQUEST['telephoneNo'];
        $mobileNo = $_REQUEST['mobileNo'];
        $type = $_SESSION['TYPE'];

        $result = array();
        try {

            if ($type == "STUDENT") {
                $student = new ItsStdMaster();
                $student->setStudentId($itsId);
                $student->setOtherEmail($otherEmail);
                $student->setTelephoneNo($telephoneNo);
                $student->setMobileNo($mobileNo);

                $rep = new StdMasterRepository();
                $rep->save($student);
            } else {
                $person = new ItsPersonMaster();
                $person->setItsPersonId($itsId);
                $person->setOtherEmail($otherEmail);
                $person->setTelephoneNo($telephoneNo);
                $person->setMobileNo($mobileNo);

                $rep = new PersonMasterRepository();
                $rep->save($person);
            }

            $_SESSION['TELEPHONE_NO'] = $telephoneNo;
            $_SESSION['MOBILE_NO'] = $mobileNo;
            $_SESSION['OTHER_EMAIL'] = $otherEmail;

            $result = array(
                "status" => true
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }

    public function upload()
    {
        $response = new Response();
        try {
            $iconFilePath = "";
            $uploaddir = getenv('USER_PATH');

            $itsId = $_REQUEST['itsId'];            
            $filePath = $_FILES['file']['name'];
            $ext = pathinfo($filePath, PATHINFO_EXTENSION);

            $uploadFilePath = $uploaddir . "/icon/" . $itsId . "." . $ext;            

            if (file_exists($uploadFilePath)) {
                unlink($uploadFilePath);
            }
            move_uploaded_file($_FILES['file']['tmp_name'], $uploadFilePath);

            $iconFilePath = $uploadFilePath;
            $mt = mime_content_type($uploadFilePath);
            if ($mt == "image/png") {
                $resource = imagecreatefrompng($uploadFilePath);
                $newUploadFilePath = $uploaddir . "/icon/" . $itsId . ".jpg";
                $out = imagejpeg($resource, $newUploadFilePath);
                imagedestroy($resource);

                unlink($uploadFilePath);

                $iconFilePath = $newUploadFilePath;
            }

            $profileFilePath = $uploaddir . "/profile/" . $itsId . ".jpg";
            copy($iconFilePath, $profileFilePath);

            $response->setStatus(true);
        } catch (\Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json();
    }

    public function createIcon()
    {
        $x = $_REQUEST['x'];
        $y = $_REQUEST['y'];
        $width = $_REQUEST['width'];
        $height = $_REQUEST['height'];

        $strProfileId = $_SESSION['ITS_ID'];
        $sourceImagedir = WWW_PATH . 'images/users/profiles';
        $sourceImageFile = $sourceImagedir . "/" . $strProfileId . ".png";

        $targetImagedir = WWW_PATH . 'images/users/icons';
        $targetImageFile = $targetImagedir . "/" . $strProfileId . ".png";

        $resource = imagecreatefrompng($sourceImageFile);
        $crop_array = array(
            'x' => $x,
            'y' => $y,
            'width' => $width,
            'height' => $height
        );

        $out = imagecrop($resource, $crop_array);

        $out = imagepng($out, $targetImageFile);

        imagedestroy($resource);
    }
}
