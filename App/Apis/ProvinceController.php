<?php

namespace App\Apis;
use App\Repositories\ProvinceRepository;

class ProvinceController extends BaseController {
    public function list() {    
        
        $rep = new ProvinceRepository();
        $result = $rep->list();

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);                
    }
    public function get($param) {           
       
        $provinceId = \htmlspecialchars($param['provinceId']);    

        $rep = new ProvinceRepository();
        $result = $rep->get($provinceId);

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);          
    }
    public function getParentProvince($param) {  
        $outdata = array();  
        $provinceId = \htmlspecialchars($param['provinceId']); 
        $rep = new ProvinceRepository();
        $result = $rep->get($provinceId);
        foreach( $result as $d) {
            if (($d['id']%100)==0) {
                $outdata[]=$d; 
            } 
        } 

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);          
    }

    public function save() {    
        $provinceId = htmlspecialchars($_REQUEST['provinceId']);                  
        $provinceNameTh = htmlspecialchars($_REQUEST['provinceNameTh']);        
        $provinceNameEn = htmlspecialchars($_REQUEST['provinceNameEn']);
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";
                
        $data = [
            'provinceId' => $provinceId,
            'provinceNameTh' => $provinceNameTh,
            'provinceNameEn' => $provinceNameEn,
            'activeFlag' => $activeFlag
        ];

        $result = array();
        try {
            $rep = new ProvinceRepository();
            $result = $rep->save($provinceId,$data);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function delete($param) {    
        $provinceId = htmlspecialchars($param['provinceId']);    
               
        $result = array();
        try {
            $rep = new ProvinceRepository();
            $result = $rep->delete($provinceId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
}