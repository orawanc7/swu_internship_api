<?php

namespace App\Apis;
use App\Repositories\RegionRepository;

class RegionController extends BaseController {
    public function list() {    
        
        $rep = new RegionRepository();
        $result = $rep->list();
        
        $data = array();        
        foreach ($result as $row) {
            $data[] = array(                
                "regionId" => $row['regionId'],
                "regionNameTh" => $row['regionNameTh'],
                "regionNameEn" => $row['regionNameEn'],
                "activeFlag" => $row['activeFlag']
            );            
        }
                
        $alldata = array (
            'data' => $data
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }
}