<?php

namespace App\Apis;

use App\Repositories\SchdActivityRepository;
use App\Repositories\SchdActivityPtypeRepository;

use App\Classes\DateUtil;

class SchdActivityController extends BaseController
{
    use DateUtil;

    public function getByRoundActivitySetNo($param) {    
        $roundId = \htmlspecialchars($param['roundId']);    
        $activitySetNo = \htmlspecialchars($param['activitySetNo']);    

        $rep = new SchdActivityRepository();
        $result = $rep->getByRoundActivitySetNo($roundId,$activitySetNo);

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);          
    }
    

    public function get($param) {    
        $activityId = \htmlspecialchars($param['activityId']);    

        $rep = new SchdActivityRepository();
        $result = $rep->get($activityId);

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);          
    }

    public function getActivityAndPType($param) {    
        $activityId = \htmlspecialchars($param['activityId']);    

        $repActivity = new SchdActivityRepository();
        $resultActivity = $repActivity->get($activityId);

        $repPType = new SchdActivityPtypeRepository();
        $resultPType = $repPType->getByActivity($activityId);        

        $alldata = array (
            'data' => array (
                'activity' => $resultActivity,
                'pType' => $resultPType
            )
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);          
    }


    public function save() {    
        $activityId = htmlspecialchars($_REQUEST['activityId']);        
        $roundId = htmlspecialchars($_REQUEST['roundId']);        
        $courseCd = htmlspecialchars($_REQUEST['courseCd']);        
        $activitySeqId = htmlspecialchars($_REQUEST['activitySeqId']);        
        $activityNameTh = htmlspecialchars($_REQUEST['activityNameTh']);        
        $activityNameEn = htmlspecialchars($_REQUEST['activityNameEn']);        
        $startDate = htmlspecialchars($_REQUEST['startDate']);        
        $endDate = htmlspecialchars($_REQUEST['endDate']);        
        $docId = htmlspecialchars($_REQUEST['docId']);
        $refActivityId = htmlspecialchars($_REQUEST['refActivityId']);
        $activityFlag = isset($_REQUEST['activityFlag'])?$_REQUEST['activityFlag']:"A";        
        $remark = htmlspecialchars($_REQUEST['remark']);
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";    
        $personTypeId = isset($_REQUEST['personTypeId'])?$_REQUEST['personTypeId']:"";    

        $startDate = $this->toDate($startDate);
        $endDate = $this->toDate($endDate);
        
        $data = [
            'activityId' => $activityId,
            'roundId' => $roundId,
            'courseCd' => $courseCd,
            'activitySeqId' => $activitySeqId,
            'activityNameTh' => $activityNameTh,
            'activityNameEn' => $activityNameEn,
            'startDate' => new \DateTime( $startDate),
            'endDate' => new \DateTime( $endDate),
            'docId' => $docId,
            'refActivityId' => $refActivityId,
            'activityFlag' => $activityFlag,
            'remark' => $remark,
            'activeFlag' => $activeFlag,
            'personTypeId' => $personTypeId
        ];

        $result = array();
        try {
            
            $rep = new SchdActivityRepository();
            $result = $rep->save($activityId,$data);            

            $result = array (
                "status"=>true,
                "id" => $result
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function saveParent() {    
        $activityId = htmlspecialchars($_REQUEST['activityId']);        
        $activitySeqId = htmlspecialchars($_REQUEST['activitySeqId']);        
        $refActivityId = htmlspecialchars($_REQUEST['refActivityId']);                        
        
        $data = [
            'activityId' => $activityId,
            'activitySeqId' => $activitySeqId,
            'refActivityId' => $refActivityId,            
        ];

        $result = array();
        try {
            $rep = new SchdActivityRepository();
            $result = $rep->saveParent($activityId,$data);            

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function delete($param) {    
        $activityId = htmlspecialchars($param['activityId']);    
               
        $result = array();
        try {
            $rep = new SchdActivityRepository();
            $result = $rep->delete($activityId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
        
}
