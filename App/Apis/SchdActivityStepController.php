<?php

namespace App\Apis;

use App\Repositories\SchdActivityStepRepository;

class SchdActivityStepController extends BaseController
{

    public function listByActivityStep($param)
    {
        $activityId = $param['activityId'];
        $stepSeq = $param['stepSeq'];

        $rep = new SchdActivityStepRepository();
        $data = $rep->listByActivityStep($activityId, $stepSeq);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($data);
    }
}
