<?php
namespace App\Apis;

use App\Repositories\SchdAttachRepository;
use App\Classes\Response;

class SchdAttachController extends BaseController
{
    private $uploaddir;

    public function __construct() {
        parent::__construct();
        $this->uploaddir = getenv('UPLOAD_FILE_PATH');
    }    
    
    public function getByRefDocId($param) {
        $response = new Response();
        try {
            $refDocId = $param['refDocId'];
            

            $rep = new SchdAttachRepository();
            $data = $rep->getByRefDocId($refDocId);

            $response->setStatus(true);
            $response->setData($data);
        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json();    
    }

    public function download($param)
    {
        $attachId = $param['attachId'];
        
        $rep = new SchdAttachRepository();
        $data = $rep->get($attachId);

        if ($data) {
            $downloadPath = $this->uploaddir . '/' . $data['filePath'];

            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            if (strtolower($data['fileExtension']) == "pdf") {
                header('Content-type: application/pdf');
            } else {
                header('Content-Type: application/octet-stream');
            }
            if (substr($data['fileName'], 0, 1) == ".") {
                $fileName = $data['fileId'] . $data['fileName'];
            } else {
                $fileName = $data['fileName'];
            }
            
            header("Content-Disposition: attachment; filename=$fileName");
            header("Content-Transfer-Encoding: binary");
            //read the file from disk
            readfile($downloadPath);
        }        
    }

    public function delete($param)
    {
        $response = new Response();

        $attachId = $param['attachId'];

        $result = array();
        try {
            $rep = new SchdAttachRepository();
            $result = $rep->delete($attachId);

            if ($result != null) {
                $file = $this->uploaddir . '/' . $result;
                if (file_exists($file)) {
                    unlink($file);
                }
            }

            $response->setStatus(true);
            $response->setData($data);
        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json();    
    }
}
