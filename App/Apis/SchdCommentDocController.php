<?php

namespace App\Apis;
use App\Repositories\SchdCommentDocRepository;
use App\Entities\ItsSchdCommentDoc;
use App\Entities\ItsSchdDocStatus;

class SchdCommentDocController extends BaseController {
    public function listByStatusSend($param) {    
        $itsStatusSendId = $param['itsStatusSendId'];        
        $seqId = isset($_REQUEST['seqId'])?$_REQUEST['seqId']:0;
        
        $rep = new SchdCommentDocRepository();
        $data = $rep->listByStatusSend($itsStatusSendId,$seqId);
                
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($data);        
    }

    public function save() {    
        $itsStatusSendId = $_REQUEST['comment-itsStatusSendId'];
        $post = $_REQUEST['post'];
        
        $doc = new ItsSchdDocStatus();
        $doc->setItsStatusSendId($itsStatusSendId);
        
        $comment = new ItsSchdCommentDoc();
        $comment->setPost($post);
        $comment->setItsStatusSend($doc);
        $comment->setCommentatorId($_SESSION['ITS_ID']);
        
        $rep = new SchdCommentDocRepository();
        $data = $rep->save($comment); 

        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($data);        
    }

    public function delete($param) {    
        $itsCommentDocId = $param['itsCommentDocId'];
               
        $result = array();
        try {
            $rep = new SchdCommentDocRepository();
            $result = $rep->delete($itsCommentDocId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
}