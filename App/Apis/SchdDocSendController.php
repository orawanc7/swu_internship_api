<?php

namespace App\Apis;

use App\Repositories\SchdDocSendRepository;

class SchdDocSendController extends BaseController
{
    public function listDocSendByItsStudentIdActivity($param)
    {
        $itsStudentId = $param['itsStudentId'];
        $activityId = $param['activityId'];
        $stdCourseId = isset($param['stdCourseId']) ? $param['stdCourseId'] : 0;
        $refDocId = isset($param['refDocId']) ? $param['refDocId'] : 0;

        $rep = new SchdDocSendRepository();
        $data = $rep->listDocSendByItsStudentIdActivity($itsStudentId, $activityId, $stdCourseId, $refDocId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($data);
    }

    public function getLastSendByUser()
    {
        $activityId = $_REQUEST['activityId'];
        $itsStudentId = $_REQUEST['itsStudentId'];
        $itsUserId = $_REQUEST['itsUserId'];
        $stdCourseId = $_REQUEST['stdCourseId'];
        $refDocId = $_REQUEST['refDocId'];

        $rep = new SchdDocSendRepository();
        $data = $rep->getLastSendByUser($activityId, $itsStudentId, $itsUserId, $stdCourseId,$refDocId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($data);
    }

    public function saveCheck()
    {
        $itsStatusSendId = $_REQUEST['itsStatusSendId'];
        $itsDocId = $_REQUEST['itsDocId'];
        $receiverRemark = $_REQUEST['receiverRemark'];
        $confirmFlag = $_REQUEST['confirmFlag'];

        $result = array();
        try {
            $rep = new SchdDocSendRepository();
            $status = $rep->saveCheck($itsStatusSendId, $itsDocId, $receiverRemark, $confirmFlag);

            $result = array(
                "status" => $status
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }

    public function cancelSend($param)
    {
        $itsStatusSendId = $param['itsStatusSendId'];
        $itsDocId = $param['itsDocId'];


        $result = array();
        try {
            $rep = new SchdDocSendRepository();
            $status = $rep->cancelSend($itsStatusSendId, $itsDocId);

            $result = array(
                "status" => $status
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }
}
