<?php

namespace App\Apis;

use App\Repositories\SchdDocStatusRepository;
use App\Classes\Response;

class SchdDocStatusController extends BaseController
{

    public function getByRoundActivitySetNoOwner()
    {
        $response = new Response();

        try {
            $roundId = $_REQUEST['roundId'];
            $activitySetNo = $_REQUEST['activitySetNo'];
            $personTypeId = $_REQUEST['personTypeId']; /* Owner */
            $itsUserId = $_REQUEST['itsUserId']; /* Owner */
            

            $rep = new SchdDocStatusRepository();
            $data = $rep->getByRoundActivitySetNoOwner($roundId,$activitySetNo,$personTypeId,$itsUserId);    

            $response->setStatus(true);
            $response->setData($data);
        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json();            
    }      
    
    public function getByRoundChecker()
    {
        $response = new Response();

        try {
            $roundId = $_REQUEST['roundId'];
            $personTypeId = $_REQUEST['personTypeId'];  /* Checker หมายถึง อาจารย์นิเทศ/ครูอาจารย์พี่เลี้ยง */
            $itsUserId = $_REQUEST['itsUserId']; /* Checker หมายถึง อาจารย์นิเทศ/ครูอาจารย์พี่เลี้ยง */
            

            $rep = new SchdDocStatusRepository();
            $data = $rep->getByRoundChecker($roundId,$personTypeId,$itsUserId);    

            $response->setStatus(true);
            $response->setData($data);
        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json();
            
    }  

    public function getByRoundNotOwnerCanViewer()
    {
        $response = new Response();

        try {
            $roundId = $_REQUEST['roundId'];
            $activitySetNo = $_REQUEST['activitySetNo'];
            $personTypeId = $_REQUEST['personTypeId']; 
            $itsUserId = $_REQUEST['itsUserId']; 
            

            $rep = new SchdDocStatusRepository();
            $data = $rep->getByRoundNotOwnerCanViewer($roundId,$activitySetNo,$personTypeId,$itsUserId);    

            $response->setStatus(true);
            $response->setData($data);
        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json();            
    }   

    public function send() {
        $response = new Response();
        try {
           
            
            $data = [
                'activityId' => $_REQUEST['activityId'],
                'docId' => $_REQUEST['docId'],            
                'refDocId' => $_REQUEST['refDocId'],
                'itsUserId' => $_REQUEST['itsUserId'],
                'personTypeId' => $_REQUEST['personTypeId'],
                'remark' => $_REQUEST['remark']
            ];
                      
            
            $rep = new SchdDocStatusRepository();
            $result = $rep->send($data);

            $response->setStatus(true);
            $response->setData($result);
        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }        
        return $response->json();
    }
}
