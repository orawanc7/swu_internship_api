<?php
namespace App\Apis;

use App\Repositories\FileRepository;
use App\Repositories\SchdFrmRepository;
use App\Classes\Response;

class SchdFrmController extends BaseController
{
    private $uploaddir;

    public function __construct() {
        parent::__construct();
        $this->uploaddir = getenv('UPLOAD_FILE_PATH');
    }       

    public function getByActivityOwner()
    {
        $response = new Response();

        try {
            $activityId = trim(htmlspecialchars($_REQUEST['activityId']));        
            $itsUserId = trim(htmlspecialchars($_REQUEST['itsUserId']));
            $personTypeId = trim(htmlspecialchars($_REQUEST['personTypeId'])); 
            
            $rep = new SchdFrmRepository();
            $result = $rep->getByActivityOwner($activityId,$itsUserId,$personTypeId);    
            if ($result) {
                $resultDtl = $rep->getDtlByFrmHdrId($result['frmHdrId']);
                $data = [
                    'hdr' => $result,
                    'dtl' => $resultDtl
                ];
            } else {
                $data = [
                    'hdr' => $result
                ];
            }
           

            $response->setStatus(true);
            $response->setData($data);
        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json();            
    }      

    public function save() {    
        $response = new Response();     
        
        
        $fields = ['frmHdrId','roundId','activityId','docId','itsUserId','personTypeId','file'];
        
        try {
            $frmHdrId = trim(htmlspecialchars($_REQUEST['frmHdrId']));        
            $roundId = trim(htmlspecialchars($_REQUEST['roundId']));        
            $activityId = trim(htmlspecialchars($_REQUEST['activityId']));        
            $docId = trim(htmlspecialchars($_REQUEST['docId']));
            $itsUserId = trim(htmlspecialchars($_REQUEST['itsUserId']));
            $personTypeId = trim(htmlspecialchars($_REQUEST['personTypeId'])); 

            $schdFrm = [
                'roundId' => $roundId,
                'activityId' => $activityId,
                'docId' => $docId,
                'itsUserId' => $itsUserId,
                'personTypeId' => $personTypeId,
            ];            

            $schdFrmDtl = [];
            foreach ($_REQUEST as $name=>$value) {
                if (!\in_array($name,$fields)) {
                    $schdFrmDtl [] = [
                        'question' => $name,
                        'answer' => $value
                    ];
                }                
            }

            $total = 0;
            if($_FILES['file']['name']!="") {
                $total = count($_FILES['file']['name']);
                $uploaddir = $this->uploaddir . "/" . $roundId;

                if (!file_exists($uploaddir)) {
                    mkdir($uploaddir);
                }
            }            

            $rep = new SchdFrmRepository();            
            $repFile = new FileRepository();            

            for ($idx = 0; $idx < $total; $idx++) {
                $filePath = $_FILES['file']['name'][$idx];                                                   
                
                $fileName = $filePath;
                $fileExt = pathinfo($filePath, PATHINFO_EXTENSION);
                $fileId = $repFile->getLastFileId($roundId);
                $filePath = $roundId . '/' . $fileId;

                $uploadpath = $uploaddir . '/' . $fileId;
                

                move_uploaded_file($_FILES['file']['tmp_name'][$idx], $uploadpath);
                
                $attach = [
                    'activityId' => $activityId,   
                    'docId' => $docId,
                    'fileId' => $fileId,
                    'fileName' => $fileName,
                    'fileExt' => $fileExt,
                    'filePath' => $filePath,
                    'attachType' => "F"
                ];
                                
            }               
            
            $frmHdrId = $rep->save($frmHdrId,$schdFrm, $schdFrmDtl, $attach);

            $response->setStatus(true);
            $response->setData($frmHdrId);
        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json(); 
    }
    
}
