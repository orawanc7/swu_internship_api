<?php

namespace App\Apis;
use App\Repositories\SchdMentorStdRepository;
use App\Classes\Response;

class SchdMentorStdController extends BaseController {
    public function getStudentByPersonRound() {    
        $response = new Response();
        try {
            $itsPersonId = $_REQUEST['itsPersonId'];
            $roundId = $_REQUEST['roundId'];

            $rep = new SchdMentorStdRepository();
            $data = $rep->getStudentByPersonRound($itsPersonId,$roundId);
                                
            $response->setStatus(true);
            $response->setData($data);
            
        } catch(Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json();
    }
     
    public function getPersonByItsStudent($param) {    
        $itsStudentId = $param['itsStudentId'];

        $response = new Response();
        try {
            
            $rep = new SchdMentorStdRepository();
            $data = $rep->getPersonByItsStudent($itsStudentId);
                                
            $response->setStatus(true);
            $response->setData($data);
            
        } catch(Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json();
    }
    
    public function getByItsStudentItsPersonType($param) {    
        $itsStudentId = $param['itsStudentId'];
        $itsPersonTypeId = $param['itsPersonTypeId'];

        $response = new Response();
        try {
            
            $rep = new SchdMentorStdRepository();
            $data = $rep->getByItsStudentItsPersonType($itsStudentId,$itsPersonTypeId);
                                
            $response->setStatus(true);
            $response->setData($data);
            
        } catch(Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json();
    }    

    public function get($param)
    {
        $mentorStdId = $param['mentorStdId'];

        $response = new Response();
        try {
            
            $rep = new SchdMentorStdRepository();
            $data = $rep->get($mentorStdId);    
                                
            $response->setStatus(true);
            $response->setData($data);
            
        } catch(Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json();
    }

    public function save()
    {
        $response = new Response();
        try {
            $mentorStdId = $_REQUEST['mentorStdId'];
            $roundId = $_REQUEST['roundId'];
            $itsStudentId = $_REQUEST['itsStudentId'];
            $itsPersonId = $_REQUEST['itsPersonId'];
            $itsPersonTypeId = $_REQUEST['itsPersonTypeId'];
            $prenameIntThCd = $_REQUEST['prenameIntThCd'];
            $personFnameTh = trim($_REQUEST['personFnameTh']);
            $personLnameTh = trim($_REQUEST['personLnameTh']);
            $mobileNo = trim($_REQUEST['mobileNo']);
            $otherEmail = trim($_REQUEST['otherEmail']);
            $deptCd = trim($_REQUEST['deptCd']);

            $data = array(
                'roundId' => $roundId,
                'itsStudentId' => $itsStudentId,
                'itsPersonId' => $itsPersonId,
                'deptCd' => $deptCd,
                'itsPersonTypeId' => $itsPersonTypeId,
                'prenameIntThCd' => $prenameIntThCd,
                'personFnameTh' => $personFnameTh,
                'personLnameTh' => $personLnameTh,
                'mobileNo' => $mobileNo,
                'otherEmail' => $otherEmail
            );

            $rep = new SchdMentorStdRepository();
            $result = $rep->save($mentorStdId, $data);

            $response->setStatus(true);
            $response->setData($result);
                 
        } catch(Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json();
    }

    public function delete($param)
    {
        $response = new Response();
        try {
        
            $mentorStdId = $param['mentorStdId'];

            $rep = new SchdMentorStdRepository();
            $result = $rep->delete($mentorStdId);

            $response->setStatus(true);
            
        } catch(Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json();        
    }
}