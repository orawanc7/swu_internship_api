<?php

namespace App\Apis;
use App\Repositories\SchdPersonStdRepository;
use App\Repositories\PersonMasterRepository;
use App\Classes\Response;

class SchdPersonStdController extends BaseController {
    public function getStudentByPersonRound() {    
        $response = new Response();
        try {
            $itsPersonId = $_REQUEST['itsPersonId'];
            $roundId = $_REQUEST['roundId'];

            $rep = new SchdPersonStdRepository();
            $data = $rep->getStudentByPersonRound($itsPersonId,$roundId);
                                
            $response->setStatus(true);
            $response->setData($data);
            
        } catch(Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json();
    }

    public function downloadDocStudent() {        
        $itsPersonId = $_REQUEST['itsPersonId'];
        $roundId = $_REQUEST['roundId'];

        $rep = new SchdPersonStdRepository();
        $result = $rep->getStudentByPersonRound($itsPersonId,$roundId);
        
        $repPerson = new PersonMasterRepository();
        $resultPerson = $repPerson->get($itsPersonId);
        $strPersonName = "";
        
        
        if ($resultPerson) {
            $strPersonName = $resultPerson['prenameSnameTh'] . $resultPerson['personFnameTh'] . ' ' . $resultPerson['personLnameTh'];
        }
        
        if ($result) {

            $tempFilePath = getenv('TEMP_FILE_PATH');
            $tempFilePathName = $tempFilePath . '/studentlist_' . $itsPersonId;
            
            $fileName = "stuentlist.docx";
            $templateFilePath = getenv('TEMPLATE_PATH') . '/teacher/studentlist.docx';
            

            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($templateFilePath);                                           
            
            $templateProcessor->cloneRow('seq',count($result));
            $i = 1;
            $semCd = "";
            $year = "";
            foreach ($result as $student) {                        
                
                $templateProcessor->setValue('seq#' . $i,  $i );
                $templateProcessor->setValue('student_name#' . $i, $student['prenameSnameTh'] . $student['fnameTh'] . ' ' . $student['lnameTh']);
                $templateProcessor->setValue('major_name#' . $i, $student['majorLnameTh']);                
                $templateProcessor->setValue('school_name#' . $i, $student['schoolNameTh']);                

                $year = $student['year'];
                $semCd = $student['semCd'];
                $i++;
            }                     
            
            $templateProcessor->replaceBookmark('year', $year);
            $templateProcessor->replaceBookmark('sem_cd', $semCd);
            $templateProcessor->replaceBookmark('teacher_name1', $strPersonName);
            $templateProcessor->replaceBookmark('teacher_name2', $strPersonName);
            
            
            

            $templateProcessor->saveAs($tempFilePathName);

            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header('Content-Type: application/octet-stream');
                        
            header("Content-Disposition: attachment; filename=$fileName");
            header("Content-Transfer-Encoding: binary");

            // read the file from disk
            readfile($tempFilePathName);

            unlink($tempFilePathName);
        }
        
    }        
}