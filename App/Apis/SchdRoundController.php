<?php

namespace App\Apis;

use App\Repositories\SchdRoundRepository;
use App\Entities\ItsSchdRound;
use App\Classes\DateUtil;

class SchdRoundController extends BaseController
{
    use DateUtil;

    public function getAll() {    
        
        $rep = new SchdRoundRepository();
        $result = $rep->getAll();

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);                
    }
    
    public function get($param) {    
        $roundId = \htmlspecialchars($param['roundId']);    

        $rep = new SchdRoundRepository();
        $result = $rep->get($roundId);

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);          
    }

    public function save() {    
        $roundId = htmlspecialchars($_REQUEST['roundId']);        
        $year = htmlspecialchars($_REQUEST['year']);        
        $semCd = htmlspecialchars($_REQUEST['semCd']);
        $roundNameTh = htmlspecialchars($_REQUEST['roundNameTh']);        
        $roundNameEn = htmlspecialchars($_REQUEST['roundNameEn']);        
        $startDate = htmlspecialchars($_REQUEST['startDate']);        
        $endDate = htmlspecialchars($_REQUEST['endDate']);        
        $remark = htmlspecialchars($_REQUEST['remark']);
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";        

        $activitySetNo = isset($_REQUEST['activitySetNo'])?$_REQUEST['activitySetNo']:"";        
       
        $startDate = $this->toDate($startDate);
        $endDate = $this->toDate($endDate);

        $arrActivitySet = [];

        if (count($activitySetNo)>0) {
            foreach ($activitySetNo as $c) {
                $arrActivitySet[] = $c;
            }
        }
                
        $data = [
            'roundId' => $roundId,
            'year' => $year,
            'semCd' => $semCd,
            'roundNameTh' => $roundNameTh,
            'roundNameEn' => $roundNameEn,
            'startDate' => new \DateTime( $startDate),
            'endDate' => new \DateTime( $endDate),
            'remark' => $remark,
            'activeFlag' => $activeFlag,
            'activitySetNo' => $arrActivitySet
        ];

        $result = array();
        try {
            $rep = new SchdRoundRepository();
            $result = $rep->save($roundId,$data);            

            $result = array (
                "status"=>true,
                "id" => $result
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function delete($param) {    
        $roundId = htmlspecialchars($param['roundId']);    
               
        $result = array();
        try {
            $rep = new SchdRoundRepository();
            $result = $rep->delete($roundId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
        
}
