<?php

namespace App\Apis;

use App\Repositories\SchdRoundCourseRepository;

class SchdRoundCourseController extends BaseController
{        
    public function getByRoundId($param) {    
        $roundId = \htmlspecialchars($param['roundId']);    

        $rep = new SchdRoundCourseRepository();
        $result = $rep->getByRoundId($roundId);

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);          
    }
   
}
