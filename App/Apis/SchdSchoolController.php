<?php

namespace App\Apis;
use App\Repositories\SchdSchoolRepository;

class SchdSchoolController extends BaseController {    

    public function getByRoundId($param) { 
        $roundId = \htmlspecialchars($param['roundId']);       
        
        $rep = new SchdSchoolRepository();
        $result = $rep->getByRoundId($roundId);

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);                
    }

    public function get($param) {    
        $schoolHdrId = \htmlspecialchars($param['schoolHdrId']);    

        $rep = new SchdSchoolRepository();
        $result = $rep->get($schoolHdrId);

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);          
    }

    public function save() {    
        $schoolHdrId = htmlspecialchars($_REQUEST['schoolHdrId']);                  
        $roundId = htmlspecialchars($_REQUEST['roundId']);        
        $schoolId = htmlspecialchars($_REQUEST['schoolId']);        
        $activeFlag = htmlspecialchars($_REQUEST['activeFlag']);  
        $remark = htmlspecialchars($_REQUEST['remark']);  
                
        $data = [
            'schoolHdrId' => $schoolHdrId,
            'roundId' => $roundId,
            'schoolId' => $schoolId,
            'activeFlag' => $activeFlag,
            'remark' => $remark,
        ];

        $result = array();
        try {
            $rep = new SchdSchoolRepository();
            $result = $rep->save($schoolHdrId,$data);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function delete($param) {    
        $schoolHdrId = htmlspecialchars($param['schoolHdrId']);    
               
        $result = array();
        try {
            $rep = new SchdSchoolRepository();
            $result = $rep->delete($schoolHdrId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
}

