<?php

namespace App\Apis;
use App\Repositories\SchdSeminarRepository;

class SchdSeminarController extends BaseController {    

    public function getSeminarByRoundAttendee() { 
        $roundId = \htmlspecialchars($_REQUEST['roundId']);       
        $activitySetNo = $_REQUEST['activitySetNo'];
        $personTypeId = \htmlspecialchars($_REQUEST['personTypeId']); 
        $attendeeId = \htmlspecialchars($_REQUEST['attendeeId']);         
        
        $rep = new SchdSeminarRepository();
        $result = $rep->getSeminarByRoundAttendee($roundId,$activitySetNo,$personTypeId,$attendeeId);

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);                
    }

    public function saveCheck() {    
        $activityId = htmlspecialchars($_REQUEST['activityId']);        
        $personTypeId = htmlspecialchars($_REQUEST['personTypeId']);        
        $attendeeId = htmlspecialchars($_REQUEST['attendeeId']);          
        $checkType = htmlspecialchars($_REQUEST['checkType']);          
                        
        $result = array();
        try {
            $rep = new SchdSeminarRepository();
            $result = $rep->saveCheck($checkType,$activityId,$personTypeId,$attendeeId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
}