<?php

namespace App\Apis;

use App\Repositories\SchdStdCourseRepository;
use App\Classes\Response;

class SchdStdCourseController extends BaseController
{
    public function get($param)
    {
        $response = new Response();
        try {
            $stdCourseId = $param['stdCourseId'];
            $rep = new SchdStdCourseRepository();
            $result = $rep->get($stdCourseId);

            $response->setStatus(true);
            $response->setData($result);
        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }
        
        return $response->json();
    }

    public function getByItsStudent($param)
    {
        $response = new Response();
        try {
            $itsStudentId = $param['itsStudentId'];

            $rep = new SchdStdCourseRepository();
            $result = $rep->getByItsStudent($itsStudentId);

            $response->setStatus(true);
            $response->setData($result);
        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }
        
        return $response->json();
    }
        

    public function save()
    {
        $response = new Response();
        try {

            $stdCourseId = $_REQUEST['stdCourseId'];

            $data = [
                'activityId' => $_REQUEST['activityId'],
                'itsStudentId' => $_REQUEST['itsStudentId'],            
                'itsCourseCd' => $_REQUEST['itsCourseCd'],
                'itsCourseName' => $_REQUEST['itsCourseName'],
                'levelClass' => $_REQUEST['levelClass'],
                'creditAmt' => $_REQUEST['creditAmt'],
                'minutesQty' => $_REQUEST['minutesQty'],
                'hourAmt' => $_REQUEST['hourAmt'],
                'remark' => $_REQUEST['remark']
            ];
                      
            $rep = new SchdStdCourseRepository();
            $result = $rep->save($stdCourseId, $data);

            $response->setStatus(true);
            $response->setData($result);
        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }        
        return $response->json();
    }

    public function delete($param)
    {
        $stdCourseId = $param['stdCourseId'];

        $result = array();
        try {
            $rep = new SchdStdCourseRepository();
            $result = $rep->delete($stdCourseId);

            $result = array(
                "status" => true
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }
}
