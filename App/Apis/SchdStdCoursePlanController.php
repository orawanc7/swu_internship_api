<?php

namespace App\Apis;

use App\Repositories\SchdStdCoursePlanRepository;
use App\Entities\ItsSchdSemYear;
use App\Entities\ItsSchdStdInfo;
use App\Entities\ItsSchdStdCoursePlan;
use App\Entities\ItsSchdStdCourse;
use App\Classes\DateUtil;

class SchdStdCoursePlanController extends BaseController
{
    use DateUtil;
    public function get($param)
    {
        $stdPlanId = $param['stdPlanId'];

        $rep = new SchdStdCoursePlanRepository();
        $result = $rep->get($stdPlanId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }

    public function listByItsStudentId($param)
    {
        $itsStudentId = $param['itsStudentId'];

        $rep = new SchdStdCoursePlanRepository();
        $result = $rep->listฺByItsStudentId($itsStudentId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }

    public function save()
    {
        $itsRoundId = $_REQUEST['itsRoundId'];
        $itsStudentId = $_REQUEST['itsStudentId'];
        $stdPlanId = $_REQUEST['stdPlanId'];
        $stdCourseId = $_REQUEST['stdCourseId'];
        $weekId = $_REQUEST['weekId'];
        $stdRemark = $_REQUEST['stdRemark'];

        $schdSemYear = new ItsSchdSemYear();
        $schdSemYear->setItsRoundId($itsRoundId);

        $schdStudentInfo = new ItsSchdStdInfo();
        $schdStudentInfo->setItsStudentId($itsStudentId);

        $schdStdCourse = new ItsSchdStdCourse();
        $schdStdCourse->setStdCourseId($stdCourseId);

        $schdStdPlan = new ItsSchdStdCoursePlan();
        $schdStdPlan->setStdPlanId($stdPlanId);
        $schdStdPlan->setItsRound($schdSemYear);
        $schdStdPlan->setItsStudent($schdStudentInfo);
        $schdStdPlan->setStdCourse($schdStdCourse);
        $schdStdPlan->setWeekId($weekId);
        $schdStdPlan->setStdRemark($stdRemark);

        $result = array();
        try {
            $rep = new SchdStdCoursePlanRepository();
            $data = $rep->save($stdPlanId, $schdStdPlan);

            $result = array(
                "status" => true,
                "stdPlanId" => $data
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }

    public function delete($param)
    {
        $stdTeachId = $param['stdTeachId'];

        $result = array();
        try {
            $rep = new SchdStdCoursePlanRepository();
            $result = $rep->delete($stdTeachId);

            $result = array(
                "status" => true
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }
}
