<?php

namespace App\Apis;

use App\Repositories\SchdStdInfoRepository;
use App\Repositories\SchdPersonStdRepository;
use App\Repositories\SchdMentorStdRepository;
use App\Classes\Response;

class SchdStdInfoController extends BaseController
{

    public function get($param)
    {
        $response = new Response();
        try {
            $itsStudentId = $param['itsStudentId'];        

            $rep = new SchdStdInfoRepository();
            $data = $rep->get($itsStudentId);

            $response->setStatus(true);
            $response->setData($data);
        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }
        return $response->json();
    }    

    public function getByStudentRound($param)
    {
        $studentId = $param['studentId'];
        $roundId = $param['roundId'];

        $rep = new SchdStdInfoRepository();
        $data = $rep->getByStudentRound($studentId,$roundId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($data);
    }    

    public function getRelateByItsStudent($param)
    {
        $response = new Response();
        try {
            $data = [];

            $itsStudentId = $param['itsStudentId'];                    

            $repPerson = new SchdPersonStdRepository();
            $dataPerson = $repPerson->getPersonByItsStudent($itsStudentId);                            

            if ($dataPerson) {
                foreach ($dataPerson as $d) {
                    $data [] = [
                        'itsId' => $d['itsPersonId'],
                        'itsName' => $d['prenameSnameTh'] . $d['personFnameTh'] . ' ' . $d['personLnameTh'],
                        'personTypeId' => $d['personTypeId'],
                        'personTypeNameTh' => $d['personTypeNameTh'],
                    ];
                }                
            }

            $repMentor = new SchdMentorStdRepository();
            $dataMentor = $repMentor->getPersonByItsStudent($itsStudentId);                            

            if ($dataMentor) {
                foreach ($dataMentor as $d) {
                    $data [] = [
                        'itsId' => $d['itsPersonId'],
                        'itsName' => $d['prenameSnameTh'] . $d['personFnameTh'] . ' ' . $d['personLnameTh'],
                        'personTypeId' => $d['personTypeId'],
                        'personTypeNameTh' => $d['personTypeNameTh'],
                    ];
                }                
            }
           
            $response->setStatus(true);
            $response->setData($data);
        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }

        return $response->json();
    }    
}
