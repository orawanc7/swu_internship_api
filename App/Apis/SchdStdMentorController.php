<?php

namespace App\Apis;

use App\Repositories\SchdStdMentorRepository;
use App\Entities\ItsSchdSemYear;
use App\Entities\ItsSchdStdInfo;
use App\Entities\ItsSchdStdMentor;
use App\Entities\ItsPersonMaster;

class SchdStdMentorController extends BaseController
{
    public function get($param)
    {
        $stdMentorId = $param['stdMentorId'];

        $rep = new SchdStdMentorRepository();
        $data = $rep->get($stdMentorId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($data);
    }

    public function getByItsStudentItsPersonType($param)
    {
        $itsStudentId = $param['itsStudentId'];
        $itsPersonTypeId = $param['itsPersonTypeId'];

        $rep = new SchdStdMentorRepository();
        $data = $rep->getByItsStudentItsPersonType($itsStudentId, $itsPersonTypeId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($data);
    }

    public function listByItsStudentId($param)
    {
        $itsStudentId = $param['itsStudentId'];
        $rep = new SchdStdMentorRepository();
        $data = $rep->listByItsStudentId($itsStudentId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($data);
    }

    public function save()
    {
        $stdMentorId = $_REQUEST['stdMentorId'];
        $itsRoundId = $_REQUEST['itsRoundId'];
        $itsStudentId = $_REQUEST['itsStudentId'];
        $itsPersonId = $_REQUEST['itsPersonId'];
        $itsPersonTypeId = $_REQUEST['itsPersonTypeId'];
        $prenameIntThCd = $_REQUEST['prenameIntThCd'];
        $personFnameTh = trim($_REQUEST['personFnameTh']);
        $personLnameTh = trim($_REQUEST['personLnameTh']);
        $mobileNo = trim($_REQUEST['mobileNo']);
        $otherEmail = trim($_REQUEST['otherEmail']);
        $deptCd = trim($_REQUEST['deptCd']);

        $data = array(
            'itsRoundId' => $itsRoundId,
            'itsStudentId' => $itsStudentId,
            'itsPersonId' => $itsPersonId,
            'deptCd' => $deptCd,
            'itsPersonTypeId' => $itsPersonTypeId,
            'prenameIntThCd' => $prenameIntThCd,
            'personFnameTh' => $personFnameTh,
            'personLnameTh' => $personLnameTh,
            'mobileNo' => $mobileNo,
            'otherEmail' => $otherEmail
        );

        $result = array();
        try {
            $rep = new SchdStdMentorRepository();
            $result = $rep->save($stdMentorId, $data);

            $result = array(
                "status" => true
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }

    public function delete($param)
    {
        $stdMentorId = $param['stdMentorId'];

        $result = array();
        try {
            $rep = new SchdStdMentorRepository();
            $result = $rep->delete($stdMentorId);

            $result = array(
                "status" => true
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }
}
