<?php

namespace App\Apis;

use App\Repositories\SchdStdPortfolioRepository;
use App\Repositories\SchdStdInfoRepository;
use App\Entities\ItsSchdSemYear;
use App\Entities\ItsSchdStdInfo;
use App\Entities\ItsSchdStdPortfolio;
use App\Entities\ItsCDocMaster;

class SchdStdPortfolioController extends BaseController
{
    public function get($param)
    {
        $itsPortfolioId = $param['itsPortfolioId'];

        $rep = new SchdStdPortfolioRepository();
        $result = $rep->get($itsPortfolioId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }

    public function getByItsStudentIdDocId($param)
    {
        $itsStudentId = $param['itsStudentId'];
        $docId = $param['docId'];

        $rep = new SchdStdPortfolioRepository();
        $result = $rep->getByItsStudentIdDocId($itsStudentId, $docId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }    

    public function download($param) {        
        $itsPortfolioId = $param['itsPortfolioId'];

        $rep = new SchdStdPortfolioRepository();
        $result = $rep->get($itsPortfolioId);
        

        if ($result) {
            $teacher = getenv('PERSON_TYPE_TEACHER');
            $mentor = getenv('PERSON_TYPE_MENTOR');
            $teacher_info = []; $mentor_info = [];
            $repRelate = new SchdStdInfoRepository();
            $resultTeacher = $repRelate->listRelateByItsStudentIdPersonType($result['itsStudentId'],$teacher);                        
            foreach ($resultTeacher as $t) {
                $teacher_info[] = $t;                
            }

            $resultMentor = $repRelate->listRelateByItsStudentIdPersonType($result['itsStudentId'],$mentor);
            foreach ($resultMentor as $m) {
                $mentor_info[] = $m;                
            }

            $tempFilePath = getenv('TEMP_FILE_PATH');
            $tempFilePathName = $tempFilePath . '/schdportfolio_' . $itsPortfolioId;
            $fileName = $result['studentId'] . ".docx";
            $templateFilePath = getenv('TEMPLATE_PATH') . '/doc/15/template.docx';

            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($templateFilePath);                   
            
            $templateProcessor->replaceBookmark('sem_cd', $result['semCd']);
            $templateProcessor->replaceBookmark('year', $result['year']);
            $templateProcessor->replaceBookmark('school_name', $result['schoolNameTh']);
            $templateProcessor->replaceBookmark('street', $result['street']);
            $templateProcessor->replaceBookmark('province_name', $result['provinceNameTh']);
            $templateProcessor->replaceBookmark('amphur_name', $result['amphurNameTh']);
            $templateProcessor->replaceBookmark('district_name', $result['districtNameTh']);
            
            if (count($teacher_info)>0) {
                for ($i=0;$i<count($teacher_info);$i++) {
                    $templateProcessor->replaceBookmark('teacher_name' . ($i+1), $teacher_info[$i]['prenameLnameTh'] . $teacher_info[$i]['personFnameTh'] . ' ' . $teacher_info[$i]['personLnameTh']);            
                }
            }
            

            if (count($mentor_info)>0) {
                $templateProcessor->replaceBookmark('mentor_name1' , $mentor_info[0]['prenameLnameTh'] . $mentor_info[0]['personFnameTh'] . ' ' . $mentor_info[0]['personLnameTh']);            
            }
            $templateProcessor->saveAs($tempFilePathName);

            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header('Content-Type: application/octet-stream');
                        
            header("Content-Disposition: attachment; filename=$fileName");
            header("Content-Transfer-Encoding: binary");

            // read the file from disk
            readfile($tempFilePathName);

            unlink($tempFilePathName);
        }
        
    }

    public function save()
    {
        $itsPortfolioId = $_REQUEST['itsPortfolioId'];
        $itsRoundId = $_REQUEST['itsRoundId'];
        $itsStudentId = $_REQUEST['itsStudentId'];
        $docId = $_REQUEST['docId'];
        $portfolioName = $_REQUEST['portfolioName'];        
        $remark = $_REQUEST['remark'];      

        $schdSemYear = new ItsSchdSemYear();
        $schdSemYear->setItsRoundId($itsRoundId);

        $schdStudentInfo = new ItsSchdStdInfo();
        $schdStudentInfo->setItsStudentId($itsStudentId);

        $doc = new ItsCDocMaster();
        $doc->setDocId($docId);

        $schdStdPortfolio = new ItsSchdStdPortfolio();
        $schdStdPortfolio->setItsPortFolioId($itsPortfolioId);
        $schdStdPortfolio->setItsRound($schdSemYear);
        $schdStdPortfolio->setItsStudent($schdStudentInfo);
        $schdStdPortfolio->setDoc($doc);
        $schdStdPortfolio->setPortfolioName($portfolioName);
        $schdStdPortfolio->setRemark($remark);
        
        $result = array();
        try {
            $rep = new SchdStdPortfolioRepository();
            $result = $rep->save($itsPortfolioId, $schdStdPortfolio);

            $result = array(
                "status" => true,
                "itsPortfolioId" => $result
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }
}
