<?php

namespace App\Apis;

use App\Repositories\SchdStdResearchRepository;
use App\Repositories\SchdStdInfoRepository;
use App\Entities\ItsSchdSemYear;
use App\Entities\ItsSchdStdInfo;
use App\Entities\ItsSchdStdResearch;
use App\Entities\ItsCDocMaster;

class SchdStdResearchController extends BaseController
{
    public function get($param)
    {
        $researchId = $param['researchId'];

        $rep = new SchdStdResearchRepository();
        $result = $rep->get($researchId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }

    public function getByItsStudentIdDocId($param)
    {
        $itsStudentId = $param['itsStudentId'];
        $docId = $param['docId'];

        $rep = new SchdStdResearchRepository();
        $result = $rep->getByItsStudentIdDocId($itsStudentId, $docId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }

    public function download($param) {        
        $researchId = $param['researchId'];

        $rep = new SchdStdResearchRepository();
        $result = $rep->get($researchId);
        

        if ($result) {
            $teacher = getenv('PERSON_TYPE_TEACHER');
            $mentor = getenv('PERSON_TYPE_MENTOR');
            $teacher_info = []; $mentor_info = [];
            $repRelate = new SchdStdInfoRepository();
            $resultTeacher = $repRelate->listRelateByItsStudentIdPersonType($result['itsStudentId'],$teacher);                        
            foreach ($resultTeacher as $t) {
                $teacher_info[] = $t;                
            }

            $resultMentor = $repRelate->listRelateByItsStudentIdPersonType($result['itsStudentId'],$mentor);
            foreach ($resultMentor as $m) {
                $mentor_info[] = $m;                
            }

            $template_no = 1;
            if (count($teacher_info)>1) {
                $template_no = 2;
            }
            
            
            $tempFilePath = getenv('TEMP_FILE_PATH');
            $tempFilePathName = $tempFilePath . '/schdresearch_' . $researchId;
            $fileName = $result['studentId'] . ".docx";
            $templateFilePath = getenv('TEMPLATE_PATH') . '/doc/' .  $result['docTypeId']  . '/' . $template_no . '.docx';

            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($templateFilePath);                   
            
            $templateProcessor->replaceBookmark('research_name', $result['researchNameTh']);
            $templateProcessor->replaceBookmark('student_id', $result['studentId']);                        
            $templateProcessor->replaceBookmark('student_name', $result['prenameLnameTh'] . $result['fnameTh'] . ' ' . $result['lnameTh']);                        
            $templateProcessor->replaceBookmark('sem_cd', $result['semCd']);                        
            $templateProcessor->replaceBookmark('year', $result['year']);
            $templateProcessor->replaceBookmark('major_name', $result['majorLnameTh']);            
            $templateProcessor->replaceBookmark('dean_name', $result['signPrenameTh'] . $result['signerFnameTh'] . ' ' . $result['signerLnameTh']);            
            $templateProcessor->replaceBookmark('dean_position_name', $result['positionNameTh']);            
                        
            if (count($teacher_info)>0) {
                for ($i=0;$i<count($teacher_info);$i++) {
                    $templateProcessor->replaceBookmark('teacher_name' . ($i+1), $teacher_info[$i]['prenameLnameTh'] . $teacher_info[$i]['personFnameTh'] . ' ' . $teacher_info[$i]['personLnameTh']);            
                }
            }
            

            if (count($mentor_info)>0) {
                $templateProcessor->replaceBookmark('mentor_name1' , $mentor_info[0]['prenameLnameTh'] . $mentor_info[0]['personFnameTh'] . ' ' . $mentor_info[0]['personLnameTh']);            
            }

            $templateProcessor->saveAs($tempFilePathName);

            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header('Content-Type: application/octet-stream');
                        
            header("Content-Disposition: attachment; filename=$fileName");
            header("Content-Transfer-Encoding: binary");

            // read the file from disk
            readfile($tempFilePathName);

            unlink($tempFilePathName);
        }
        
    }

    public function save()
    {
        $researchId = $_REQUEST['researchId'];
        $itsRoundId = $_REQUEST['itsRoundId'];
        $itsStudentId = $_REQUEST['itsStudentId'];
        $docId = $_REQUEST['docId'];
        $rearchNameTh = $_REQUEST['researchNameTh'];
        $rearchNameEn = $_REQUEST['researchNameEn'];
        $abstractTh = $_REQUEST['abstractTh'];
        $abstractEn = $_REQUEST['abstractEn'];
        $remark = $_REQUEST['remark'];

        $schdSemYear = new ItsSchdSemYear();
        $schdSemYear->setItsRoundId($itsRoundId);

        $schdStudentInfo = new ItsSchdStdInfo();
        $schdStudentInfo->setItsStudentId($itsStudentId);

        $doc = new ItsCDocMaster();
        $doc->setDocId($docId);

        $schdStdResearch = new ItsSchdStdResearch();
        $schdStdResearch->setResearchId($researchId);
        $schdStdResearch->setItsRound($schdSemYear);
        $schdStdResearch->setItsStudent($schdStudentInfo);
        $schdStdResearch->setDoc($doc);
        $schdStdResearch->setResearchNameTh($rearchNameTh);
        $schdStdResearch->setResearchNameEn($rearchNameEn);
        $schdStdResearch->setAbstractTh($abstractTh);
        $schdStdResearch->setAbstractEn($abstractEn);
        $schdStdResearch->setRemark($remark);

        $result = array();
        try {
            $rep = new SchdStdResearchRepository();
            $result = $rep->save($researchId, $schdStdResearch);

            $result = array(
                "status" => true,
                "researchId" => $result
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }
}
