<?php

namespace App\Apis;

use App\Repositories\SchdStdTeachRepository;
use App\Entities\ItsSchdSemYear;
use App\Entities\ItsSchdStdInfo;
use App\Entities\ItsSchdStdTeach;
use App\Entities\ItsSchdStdCourse;
use App\Classes\DateUtil;

class SchdStdTeachController extends BaseController
{
    use DateUtil;
    public function get($param)
    {
        $stdTeachId = $param['stdTeachId'];

        $rep = new SchdStdTeachRepository();
        $result = $rep->get($stdTeachId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }

    public function listByItsStudentId($param)
    {
        $itsStudentId = $param['itsStudentId'];

        $rep = new SchdStdTeachRepository();
        $result = $rep->listฺByItsStudentId($itsStudentId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }

    public function listByItsStudentIdStdCourse($param)
    {
        $itsStudentId = $param['itsStudentId'];
        $stdCourseId = $param['stdCourseId'];

        $rep = new SchdStdTeachRepository();
        $result = $rep->listByItsStudentIdStdCourse($itsStudentId, $stdCourseId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }

    public function listByStdCourse($param)
    {
        $stdCourseId = $param['stdCourseId'];

        $rep = new SchdStdTeachRepository();
        $result = $rep->listByStdCourse($stdCourseId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }

    public function save()
    {
        $itsRoundId = $_REQUEST['itsRoundId'];
        $itsStudentId = $_REQUEST['itsStudentId'];
        $stdTeachId = $_REQUEST['stdTeachId'];
        $stdCourseId = $_REQUEST['stdCourseId'];
        $teachDay = $_REQUEST['teachDay'];
        $teachSeqNo = $_REQUEST['teachSeqNo'];
        $startTime = $_REQUEST['startTime'];
        $endTime = $_REQUEST['endTime'];
        $teachHour = $_REQUEST['teachHour'];
        $teachAddrDesc = $_REQUEST['teachAddrDesc'];
        $stdRemark = $_REQUEST['stdRemark'];

        $schdSemYear = new ItsSchdSemYear();
        $schdSemYear->setItsRoundId($itsRoundId);

        $schdStudentInfo = new ItsSchdStdInfo();
        $schdStudentInfo->setItsStudentId($itsStudentId);

        $schdStdCourse = new ItsSchdStdCourse();
        $schdStdCourse->setStdCourseId($stdCourseId);

        $saveStartTime = $this->toTime($startTime);
        $saveEndTime = $this->toTime($endTime);

        $schdStdTeach = new ItsSchdStdTeach();
        $schdStdTeach->setStdTeachId($stdTeachId);
        $schdStdTeach->setItsRound($schdSemYear);
        $schdStdTeach->setItsStudent($schdStudentInfo);
        $schdStdTeach->setStdCourse($schdStdCourse);
        $schdStdTeach->setTeachDay($teachDay);
        $schdStdTeach->setTeachSeqNo($teachSeqNo);
        $schdStdTeach->setStartTime(new \DateTime($saveStartTime));
        $schdStdTeach->setEndTime(new \DateTime($saveEndTime));
        $schdStdTeach->setTeachHour($teachHour);
        $schdStdTeach->setTeachAddrDesc($teachAddrDesc);
        $schdStdTeach->setStdRemark($stdRemark);

        $result = array();
        try {
            $rep = new SchdStdTeachRepository();
            $result = $rep->save($stdTeachId, $schdStdTeach);

            $result = array(
                "status" => true
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }

    public function delete($param)
    {
        $stdTeachId = $param['stdTeachId'];

        $result = array();
        try {
            $rep = new SchdStdTeachRepository();
            $result = $rep->delete($stdTeachId);

            $result = array(
                "status" => true
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }
}
