<?php

namespace App\Apis;

use App\Entities\ItsSchdUploadHdr;
use App\Entities\ItsSchdSemYear;
use App\Repositories\FileRepository;
use App\Repositories\SchdUploadHdrRepository;
use App\Entities\ItsCDocMaster;

class SchdUploadController extends BaseController
{
    private $uploaddir;

    public function __construct()
    {
        parent::__construct();
        $this->uploaddir = getenv('UPLOAD_FILE_PATH');
    }

    public function download($param)
    {
        $fileHdrId = $param['fileHdrId'];

        $rep = new SchdUploadHdrRepository();
        $data = $rep->get($fileHdrId);

        if ($data) {
            $downloadPath = $this->uploaddir . '/' . $data['filePath'];

            header("Cache-Control: public");

            header("Content-Description: File Transfer");
            if (strtolower($data['fileExtension']) == "pdf") {
                header('Content-type: application/pdf');
            } else {
                header('Content-Type: application/octet-stream');
            }
            if (substr($data['fileName'], 0, 1) == ".") {
                $fileName = $data['fileId'] . $data['fileName'];
            } else {
                $fileName = $data['fileName'];
            }

            header("Content-Disposition: attachment; filename=$fileName");
            header("Content-Transfer-Encoding: binary");

            // read the file from disk
            readfile($downloadPath);
        }
    }

    public function listByDocCourseRefDocSender($param)
    {
        $docId = $param['docId'];
        $stdCourseId = $param['stdCourseId'];
        $refDocId = $param['refDocId'];
        $senderId = $param['senderId'];

        $rep = new SchdUploadHdrRepository();
        $data = $rep->listByDocCourseRefDocSender($docId, $stdCourseId, $refDocId, $senderId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($data);
    }

    public function listByDocCourseSender($param)
    {
        $docId = $param['docId'];
        $stdCourseId = $param['stdCourseId'];
        $senderId = $param['senderId'];

        $rep = new SchdUploadHdrRepository();
        $data = $rep->listByDocCourseSender($docId, $stdCourseId, $senderId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($data);
    }


    public function listByDocRefDocSender($param)
    {
        $docId = $param['docId'];
        $refDocId = $param['refDocId'];
        $senderId = $param['senderId'];

        $rep = new SchdUploadHdrRepository();
        $data = $rep->listByDocRefDocSender($docId, $refDocId, $senderId);

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($data);
    }

    public function upload()
    {
        $result = array();
        try {
            $itsRoundId = $_REQUEST['itsRoundId'];
            $docId = $_REQUEST['docId'];
            $stdCourseId = isset($_REQUEST['stdCourseId']) ? $_REQUEST['stdCourseId'] : '';
            $refDocId = isset($_REQUEST['refDocId']) ? $_REQUEST['refDocId'] : '';
            $senderId = isset($_REQUEST['senderId']) ? $_REQUEST['senderId'] : '';
            
            $total = count($_FILES['file']['name']);
            $uploaddir = $this->uploaddir . "/" . $itsRoundId;

            if (!file_exists($uploaddir)) {
                mkdir($uploaddir);
            }
            $repFile = new FileRepository();
            $repUpload = new SchdUploadHdrRepository();

            for ($idx = 0; $idx < $total; $idx++) {

                $filePath = $_FILES['file']['name'][$idx];
                //$fileName = basename($filePath);
                $fileName = $filePath;
                $fileExt = pathinfo($filePath, PATHINFO_EXTENSION);
                $fileId = $repFile->getLastFileId('UPLOAD', $itsRoundId);
                $filePath = $itsRoundId . '/' . $fileId;

                $uploadpath = $uploaddir . '/' . $fileId;

                move_uploaded_file($_FILES['file']['tmp_name'][$idx], $uploadpath);

                $round = new ItsSchdSemYear();
                $round->setItsRoundId($itsRoundId);

                $doc = new ItsCDocMaster();
                $doc->setDocId($docId);

                $uploadHdr = new ItsSchdUploadHdr();
                $uploadHdr->setItsRound($round);
                $uploadHdr->setDoc($doc);

                $uploadHdr->setFileId($fileId);
                $uploadHdr->setFileName($fileName);
                $uploadHdr->setFileExtenstion($fileExt);
                $uploadHdr->setFilePath($filePath);
                $uploadHdr->setUploadType('F');

                if (strlen($senderId) > 0) {
                    $uploadHdr->setSenderId($senderId);
                }

                if (strlen($stdCourseId) > 0) {
                    $uploadHdr->setStdCourseId($stdCourseId);
                }

                if (strlen($refDocId) > 0) {
                    $uploadHdr->setRefDocId($refDocId);
                }

                $repUpload->save($uploadHdr);
            }

            $result = array(
                "status" => true,
                "message" => ''
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }

    public function saveLink()
    {
        $result = array();
        try {
            $itsRoundId = $_REQUEST['itsRoundId'];
            $docId = $_REQUEST['docId'];
            $stdCourseId = isset($_REQUEST['stdCourseId']) ? $_REQUEST['stdCourseId'] : '';
            $refDocId = isset($_REQUEST['refDocId']) ? $_REQUEST['refDocId'] : '';
            $senderId = isset($_REQUEST['senderId']) ? $_REQUEST['senderId'] : '';
            $link_url = isset($_REQUEST['link_url']) ? $_REQUEST['link_url'] : '';

            $repUpload = new SchdUploadHdrRepository();

            $round = new ItsSchdSemYear();
            $round->setItsRoundId($itsRoundId);

            $doc = new ItsCDocMaster();
            $doc->setDocId($docId);

            $uploadHdr = new ItsSchdUploadHdr();
            $uploadHdr->setItsRound($round);
            $uploadHdr->setDoc($doc);

            $uploadHdr->setLinkUrl($link_url);
            $uploadHdr->setUploadType('L');

            if (strlen($senderId) > 0) {
                $uploadHdr->setSenderId($senderId);
            }

            if (strlen($stdCourseId) > 0) {
                $uploadHdr->setStdCourseId($stdCourseId);
            }

            if (strlen($refDocId) > 0) {
                $uploadHdr->setRefDocId($refDocId);
            }

            $repUpload->save($uploadHdr);


            $result = array(
                "status" => true,
                "message" => ''
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }

    public function delete($param)
    {
        $fileHdrId = $param['fileHdrId'];

        $result = array();
        try {
            $rep = new SchdUploadHdrRepository();
            $result = $rep->delete($fileHdrId);

            if ($result != null) {
                $file = $this->uploaddir . '/' . $result;
                if (file_exists($file)) {
                    unlink($file);
                }
            }

            $result = array(
                "status" => true
            );
        } catch (\Throwable $th) {
            $result = array(
                "status" => false,
                "message" => $th->getMessage()
            );
        }

        header('Content-Type: application/json;charset=utf-8');
        echo json_encode($result);
    }
}
