<?php

namespace App\Apis;
use App\Repositories\SchoolRepository;
use App\Entities\ItsCSchool;
use App\Classes\Response;

class SchoolController extends BaseController {
    public function list() {    
        
        $rep = new SchoolRepository();
        $result = $rep->list();

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);                
    }
    
    public function get($param) {    
        
        /*$schoolId = \htmlspecialchars($param['schoolId']);    

        $rep = new SchoolRepository();
        $result = $rep->get($schoolId);

        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);     */    
        $response = new Response();
        try {
            $schoolId = \htmlspecialchars($param['schoolId']);      

            $rep = new SchoolRepository();
            $result = $rep->get($schoolId);

            if ($result!=null) {
                $info = $this->getFileInfo($result);
                
                if ($info!=null) {
                    $resultFile = [
                        'fileUrl' => $info['url']
                    ];

                    $result = array_merge($result, $resultFile);
                } 
            }

            $response->setData($result);
            $response->setStatus(true);            
        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }       
        return $response->json();   
    }

    private function getFileInfo ($result) {
        $downloadPath = getenv('DOWNLOAD_PATH');
        $fileExtension = $result['pictureSchool'];
        //var_dump($fileExtension);  exit;
        $filePath = $downloadPath . '/' . $result['schoolId'] . '.' . $fileExtension;
        //var_dump( $filePath );  exit;
        if (\file_exists($filePath)) {
            return [
                'file' => $filePath,
                'url' => 'School/download/' . $result['schoolId']
            ];                        
        }
        return null;
    }

    public function save() {    
        $schoolId = htmlspecialchars($_REQUEST['schoolId']);   
        $schoolNameTh = htmlspecialchars($_REQUEST['schoolNameTh']);        
        $schoolNameEn = htmlspecialchars($_REQUEST['schoolNameEn']);
        $schoolSubId = htmlspecialchars($_REQUEST['schoolSubId']);
        $schoolTypeId = htmlspecialchars($_REQUEST['schoolTypeId']);
        $campusName = htmlspecialchars($_REQUEST['campusName']);
        $addressNo = htmlspecialchars($_REQUEST['addressNo']);
        $soi = htmlspecialchars($_REQUEST['soi']);
        $street = htmlspecialchars($_REQUEST['street']);
        $districtId =htmlspecialchars($_REQUEST['districtId']);
        $amphurId = htmlspecialchars($_REQUEST['amphurId']);
        $postcodeId = htmlspecialchars($_REQUEST['postcodeId']);
        $provinceId = htmlspecialchars($_REQUEST['provinceId']);
        $addressDesc = htmlspecialchars($_REQUEST['addressDesc']);
        $faxNo = htmlspecialchars($_REQUEST['faxNo']);
        $telephoneNo = htmlspecialchars($_REQUEST['telephoneNo']);
        $mobileNo = htmlspecialchars($_REQUEST['mobileNo']);
        $schoolGoogleMap = htmlspecialchars($_REQUEST['schoolGoogleMap']);
        $schoolWebsite = htmlspecialchars($_REQUEST['schoolWebsite']);
        $schoolEmail = htmlspecialchars($_REQUEST['schoolEmail']);
        $schoolFbLink = htmlspecialchars($_REQUEST['schoolFbLink']);
        //$pictureSchool = htmlspecialchars($_REQUEST['pictureSchool']);       
        $blacklistRemark = htmlspecialchars($_REQUEST['blacklistRemark']);        
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";
        $blacklistFlag = isset($_REQUEST['blacklistFlag'])?$_REQUEST['blacklistFlag']:"N";
                
        $data = [
            'schoolId' => $schoolId,            
            'schoolNameTh' => $schoolNameTh,
            'schoolNameEn' => $schoolNameEn,
            'schoolSubId' => $schoolSubId,
            'schoolTypeId' => $schoolTypeId,
            'campusName' => $campusName,
            'addressNo' => $addressNo,
            'soi' => $soi,
            'street' => $street,
            'districtId' => $districtId,
            'amphurId' => $amphurId,
            'postcodeId' => $postcodeId,     
            'provinceId' => $provinceId,
            'addressDesc' => $addressDesc,
            'faxNo' => $faxNo,
            'telephoneNo' => $telephoneNo,
            'mobileNo' => $mobileNo,
            'schoolGoogleMap' => $schoolGoogleMap,
            'schoolWebsite' => $schoolWebsite,
            'schoolEmail' => $schoolEmail,
            'schoolFbLink' => $schoolFbLink,
            //'pictureSchool' => $pictureSchool,
            'blacklistRemark' => $blacklistRemark,
            'blacklistFlag' => $blacklistFlag,
            'activeFlag' => $activeFlag
        ];

        $result = array();
        try {
            $rep = new SchoolRepository();
            $result = $rep->save($schoolId,$data);
            if (!empty($_FILES)) {
                $filePictureSchool = $this->uploadFile($schoolId,$_FILES);

                $rep->saveFilePictureSchool($schoolId,$filePictureSchool);
            }   

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function uploadFile ($id,$file) {
        $downloadPath = getenv('DOWNLOAD_PATH');            
        $filePath = $file['file']['name'];            
        
        $ext = strtolower(pathinfo($filePath, PATHINFO_EXTENSION));

        $uploadFilePath = $downloadPath . "/" . $id . "." . $ext;
        

        if (file_exists($uploadFilePath)) {
            unlink($uploadFilePath);
        }
       // echo  $downloadPath;exit;
        move_uploaded_file($_FILES['file']['tmp_name'], $uploadFilePath);

        return $ext;
    }

    public function delete($param) {    
        $schoolId = htmlspecialchars($param['schoolId']);    
               
        $result = array();
        try {
            $rep = new SchoolRepository();
            $result = $rep->delete($schoolId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
    public function download ( $param ) {                
        $schoolId = \htmlspecialchars($param['schoolId']);            

        $downloadPath = getenv('DOWNLOAD_PATH');

        $rep = new SchoolRepository();
        $result = $rep->get($schoolId);

        if ($result!=null) {
            
            $fileExtension = $result['pictureSchool'];
            $fileName = $result['schoolId'] ;
            $filePath = $downloadPath . '/' . $fileName . '.' . $fileExtension;

            $fileNameDownload = $result['schoolNameTh'] . '.' . $fileExtension;            

            if (\file_exists($filePath)) {
                header("Cache-Control: public");
                header("Content-Description: File Transfer");
                if ($fileExtension == "pdf") {
                    header('Content-type: application/pdf');
                } else {
                    header('Content-Type: application/octet-stream');
                }
                header("Content-Disposition: attachment; filename=$fileNameDownload");
                header("Content-Transfer-Encoding: binary");

                readfile($filePath);
            }
        }
    }
}

