<?php

namespace App\Apis;
use App\Repositories\SchoolSubRepository;
use App\Entities\ItsCSchoolSub;

class SchoolSubController extends BaseController {
    public function list() {    
        
        $rep = new SchoolSubRepository();
        $result = $rep->list();
        
        $data = array();        
        foreach ($result as $row) {
            $data[] = array(                
                "schoolSubId" => $row['schoolSubId'],
                "schoolSubNameTh" => $row['schoolSubNameTh'],
                "schoolSubNameEn" => $row['schoolSubNameEn'],
                "activeFlag" => $row['activeFlag'],
            );            
        }
                
        $alldata = array (
            'data' => $data
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }

    public function get($param) {    
        $schoolSubId = $param['schoolSubId'];        

        $rep = new SchoolSubRepository();
        $result = $rep->get($schoolSubId);

        $data = array();
        if ($result!=null) {
            $data = array(                
                "schoolSubId" => $result['schoolSubId'],
                "schoolSubNameTh" => $result['schoolSubNameTh'],
                "schoolSubNameEn" => $result['schoolSubNameEn'],
                "activeFlag" => $result['activeFlag']
            );            
        }
                
        $alldata = array (
            'data' => $data
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);        
    }

    public function save() {    
        $schoolSubId = $_REQUEST['schoolSubId'];        
        $schoolSubNameTh = $_REQUEST['schoolSubNameTh'];        
        $schoolSubNameEn = $_REQUEST['schoolSubNameEn'];                       
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";
        
        $schoolSub = new ItsCSchoolSub();        
        $schoolSub->setSchoolSubId($schoolSubId);
        $schoolSub->setSchoolSubNameTh($schoolSubNameTh);
        $schoolSub->setSchoolSubNameEn($schoolSubNameEn);        
        $schoolSub->setActiveFlag($activeFlag);

        $result = array();
        try {
            $rep = new SchoolSubRepository();
            $result = $rep->save($schoolSubId,$schoolSub);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function delete($param) {    
        $schoolSubId = $param['schoolSubId'];
               
        $result = array();
        try {
            $rep = new SchoolSubRepository();
            $result = $rep->delete($schoolSubId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
}

