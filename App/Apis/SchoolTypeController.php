<?php

namespace App\Apis;
use App\Repositories\SchoolTypeRepository;
use App\Entities\ItsCSchoolType;

class SchoolTypeController extends BaseController {
    public function list() {    
        
        $rep = new SchoolTypeRepository();
        $result = $rep->list();
        
        $data = array();        
        foreach ($result as $row) {
            $data[] = array(                
                "schoolTypeId" => $row['schoolTypeId'],
                "schoolTypeNameTh" => $row['schoolTypeNameTh'],
                "schoolTypeNameEn" => $row['schoolTypeNameEn'],
                "activeFlag" => $row['activeFlag'],
            );            
        }
                
        $alldata = array (
            'data' => $data
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }

    public function get($param) {    
        $schoolTypeId = $param['schoolTypeId'];        

        $rep = new SchoolTypeRepository();
        $result = $rep->get($schoolTypeId);

        $data = array();
        if ($result!=null) {
            $data = array(                
                "schoolTypeId" => $result['schoolTypeId'],
                "schoolTypeNameTh" => $result['schoolTypeNameTh'],
                "schoolTypeNameEn" => $result['schoolTypeNameEn'],
                "activeFlag" => $result['activeFlag']
            );            
        }
                
        $alldata = array (
            'data' => $data
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);        
    }

    public function save() {    
        $schoolTypeId = $_REQUEST['schoolTypeId'];        
        $schoolTypeNameTh = $_REQUEST['schoolTypeNameTh'];        
        $schoolTypeNameEn = $_REQUEST['schoolTypeNameEn'];                       
        $activeFlag = isset($_REQUEST['activeFlag'])?$_REQUEST['activeFlag']:"N";
        
        $schoolType = new ItsCSchoolType();        
        $schoolType->setSchoolTypeId($schoolTypeId);
        $schoolType->setSchoolTypeNameTh($schoolTypeNameTh);
        $schoolType->setSchoolTypeNameEn($schoolTypeNameEn);        
        $schoolType->setActiveFlag($activeFlag);

        $result = array();
        try {
            $rep = new SchoolTypeRepository();
            $result = $rep->save($schoolTypeId,$schoolType);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }

    public function delete($param) {    
        $schoolTypeId = $param['schoolTypeId'];
               
        $result = array();
        try {
            $rep = new SchoolTypeRepository();
            $result = $rep->delete($schoolTypeId);

            $result = array (
                "status"=>true
            );
        } catch (\Throwable $th) {
            $result = array (
                "status"=>false,
                "message"=> $th->getMessage()
            );
        }        
        
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($result);        
    }
}

