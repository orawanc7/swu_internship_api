<?php

namespace App\Apis;
use App\Repositories\SemRepository;

class SemController extends BaseController {
    public function list() {    
        
        $rep = new SemRepository();
        $result = $rep->list();
                        
        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }
}