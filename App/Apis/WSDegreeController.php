<?php

namespace App\Apis;
use App\Repositories\WSDegreeRepository;

class WSDegreeController extends BaseController {
    public function list() {    
        
        $rep = new WSDegreeRepository();
        $result = $rep->list();
                        
        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }
}