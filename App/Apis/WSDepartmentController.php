<?php

namespace App\Apis;
use App\Repositories\WSDepartmentRepository;

class WSDepartmentController extends BaseController {
    public function list() {    
        
        $rep = new WSDepartmentRepository();
        $result = $rep->list();
               
        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }
}