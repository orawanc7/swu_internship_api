<?php

namespace App\Apis;
use App\Repositories\WSLevelGroupRepository;

class WSLevelGroupController extends BaseController {
    public function list() {    
        
        $rep = new WSLevelGroupRepository();
        $result = $rep->list();
                        
        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }
}