<?php

namespace App\Apis;
use App\Repositories\WSMajorRepository;

class WSMajorController extends BaseController {
    public function list() {    
        
        $rep = new WSMajorRepository();
        $result = $rep->list();
                        
        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }
}