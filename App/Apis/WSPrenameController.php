<?php

namespace App\Apis;
use App\Repositories\WSPrenameRepository;

class WSPrenameController extends BaseController {
    public function list() {    
        
        $rep = new WSPrenameRepository();
        $result = $rep->list();
                        
        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }
}