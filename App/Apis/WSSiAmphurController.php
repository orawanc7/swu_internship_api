<?php

namespace App\Apis;
use App\Repositories\WSSiAmphurRepository;
use App\Classes\Response;

class WSSiAmphurController extends BaseController {
    public function list() {    
        
        $rep = new WSSiAmphurRepository();
        $result = $rep->list();
               
        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }
    public function listAmphurByProvince() {     
    	$response = new Response();
        try {
            $provinceId = \htmlspecialchars($_REQUEST['provinceId']);    

            $rep = new WSSiAmphurRepository();
            $result = $rep->listAmphurByProvince($provinceId);    
            $response->setData($result);
            $response->setStatus(true);            
        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }       
        return $response->json();  
    }
}