<?php

namespace App\Apis;
use App\Repositories\WSSiProvinceRepository;

class WSSiProvinceController extends BaseController {
    public function list() {    
        
        $rep = new WSSiProvinceRepository();
        $result = $rep->list();
               
        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }
}