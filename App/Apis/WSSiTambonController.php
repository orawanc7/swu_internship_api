<?php

namespace App\Apis;
use App\Repositories\WSSiTambonRepository;
use App\Classes\Response;
class WSSiTambonController extends BaseController {
    public function list() {    
        
        $rep = new WSSiTambonRepository();
        $result = $rep->list();
               
        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }
    public function listTambonByAmphur() {     
        $response = new Response();
        
        try {
            
            $amphurId = \htmlspecialchars($_REQUEST['amphurId']);    
            $provinceId = \htmlspecialchars($_REQUEST['provinceId']);    

            $rep = new WSSiTambonRepository();
            $result = $rep->listTambonByAmphur($amphurId, $provinceId);    
            $response->setData($result);
            $response->setStatus(true);            
        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }       
        return $response->json();  
    }
    public function getAmphurByProvince() {     
        $response = new Response();
        
        try {
            
            $amphurId = \htmlspecialchars($_REQUEST['amphurId']);    
            $provinceId = \htmlspecialchars($_REQUEST['provinceId']);    

            $rep = new WSSiTambonRepository();
            $result = $rep->listTambonByAmphur($amphurId, $provinceId);    
            $response->setData($result);
            $response->setStatus(true);            
        } catch (Exception $ex) {
            $response->setMessage($ex->getMessage());
        }       
        return $response->json();  
    }
}