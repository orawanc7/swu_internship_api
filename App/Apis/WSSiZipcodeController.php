<?php

namespace App\Apis;
use App\Repositories\WSSiZipcodeRepository;

class WSSiZipcodeController extends BaseController {
    public function list() {    
        
        $rep = new WSSiZipcodeRepository();
        $result = $rep->list();
               
        $alldata = array (
            'data' => $result
        );
        header('Content-Type: application/json;charset=utf-8');  
        echo json_encode($alldata);
    }
}