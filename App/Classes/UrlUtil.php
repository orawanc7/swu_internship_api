<?
namespace App\Classes;

trait UrlUtil
{
    public function exists($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);   
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($httpCode == 200) {
            return true;
        }
        return false;
    }

    public function download($url, $filePath)
    {
        $urlParse = parse_url($url);

        if($urlParse['scheme'] == 'https'){
            return $this->downloadHttps($url,$filePath);
        } else {
            return $this->downloadHttp($url,$filePath);
        }                        
    }

    public function downloadHttp($url,$filePath) {        
        try {

            if ($this->exists($url)) {
                $content = file_get_contents($url);

                if ($content) {
                    $fp = fopen($filePath, "w");
                    fwrite($fp, $content);
                    fclose($fp);
                    return true;
                }
            }
        } catch (Exception $e) { }        
        return false;
    }

    public function downloadHttps($url,$filePath) {        
        $host = $url;
        $port = 443;
        $fp = fsockopen($host,$port);

        if (!$fp) {
            // echo "$errstr ($errno)<br />\n";
            return false;
        } else {
            $content = "";
            while (!feof($fp)) {  //This looped forever
                $content .= fread($fp, 1024);
            }
            fclose($fp);
            echo $content;
        }
    }
}
