<?php

namespace App\Controllers\Admin;
use App\Repositories\ProvinceRepository;

class AmphurController {
    public function index($param) {
        $provinceId = $param['provinceId'];

        $rep = new ProvinceRepository();
        $result = $rep->get($provinceId);
        
        view('admin.master.amphur',
            [
                '_menu'=>'master',
                'province' => $result
            ]
        );
    }
}