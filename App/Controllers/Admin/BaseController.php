<?php

namespace App\Controllers\Admin;

class BaseController extends \App\Controllers\BaseController {
    public function __construct () {
        parent::__construct();

        if (!isset($_SESSION['ID'])) {
            header ('Location: ' . route(''));
            exit;
        }

        if ($_SESSION['TYPE']=='STUDENT') {
            header ('Location: ' . route('student'));
            exit;       
        }
    }
}    