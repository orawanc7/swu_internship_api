<?php

namespace App\Controllers\Admin;
use App\Repositories\AmphurRepository;

class DistrictController {
    public function index($param) {
        $amphurId = $param['amphurId'];

        $rep = new AmphurRepository();
        $result = $rep->get($amphurId);
        
        view('admin.master.district',[
                '_menu'=>'master',
                'amphur' => $result
            ]
        );
    }
}