<?php

namespace App\Controllers\Admin;
use App\Repositories\DocGroupRepository;

class DownloadController {
    public function index($param) {
        $docGroupId = $param['docGroupId'];

        $rep = new DocGroupRepository();
        $result = $rep->get($docGroupId);
        
        view('admin.publish.download',[
                '_menu'=>'publish',
                'docGroup' => $result
            ]
        );
    }
}