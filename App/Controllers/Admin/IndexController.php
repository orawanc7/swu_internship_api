<?php

namespace App\Controllers\Admin;

class IndexController extends BaseController {
    public function index() {
        view('admin.index');
    }
}