<?php

namespace App\Controllers\Admin;

class NewsController {
    public function index() {
        view('admin.publish.news',[
            '_menu'=>'publish',
        ]);
    }
}