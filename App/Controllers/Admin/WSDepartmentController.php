<?php

namespace App\Controllers\Admin;

class WSDepartmentController {
    public function index() {
        view('admin.masterregister.wsdepartment',
            ['_menu'=>'masterregister']
        );
    }
}