<?php

namespace App\Controllers;

class BaseController {
    function __construct () {        

        if (!isset($_SESSION['ID'])) {
            header ('Location: ' . route(''));
            exit;
        }
    }
}