<?php

namespace App\Controllers;
use App\Repositories\BaseRepository;

class DashboardController {
    public function index() {
        view('index');
    }
}