<?php

namespace App\Controllers;
use App\Repositories\BaseRepository;

class HomeController {
    public function index() {
        view('home');
    }

    public function profile() {
        view('profile');
    }
}