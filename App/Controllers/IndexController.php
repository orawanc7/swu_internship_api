<?php

namespace App\Controllers;

use App\Repositories\BaseRepository;

class IndexController
{
    public function index()
    {
        $strUserId = isset($_COOKIE['userId']) ? $_COOKIE['userId'] : "";
        $remember = "";

        if (strlen($strUserId) > 0) {
            $remember = "checked";
        }

        view('index', [
            'userId' => $strUserId,
            'remember' => $remember,
            'admin' => 0
        ]);
    }

    public function loginadmin()
    {
        $strUserId = isset($_COOKIE['userId']) ? $_COOKIE['userId'] : "";
        $remember = "";

        if (strlen($strUserId) > 0) {
            $remember = "checked";
        }

        view('loginindex', [
            'userId' => $strUserId,
            'remember' => $remember,
            'admin' => 1
        ]);
    }
}
