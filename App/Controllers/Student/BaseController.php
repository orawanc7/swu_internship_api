<?php

namespace App\Controllers\Student;

class BaseController {
    function __construct () {
        if (!isset($_SESSION['ID'])) {
            header ('Location: ' . route(''));
            exit;
        }

        if ($_SESSION['TYPE']!='STUDENT') {
            header ('Location: ' . route(''));
            exit;       
        }
    }
}    