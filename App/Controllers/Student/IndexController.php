<?php

namespace App\Controllers\Student;

use App\Repositories\SchdActivityRepository;
use App\Repositories\SchdActivityStepRepository;

class IndexController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        view('student.index');
    }

    public function activitylist($param)
    {
        $itsStudentId = $param['itsStudentId'];

        view('student.activitylist', [
            'itsStudentId' => $itsStudentId
        ]);
    }

    public function activity($param)
    {
        $activityId = $param['activityId'];
        $itsStudentId = $param['itsStudentId'];
        $itsStatusSendId = isset($param['itsStatusSendId']) ? $param['itsStatusSendId'] : 0;

        $rep = new SchdActivityRepository();
        $activity = $rep->get($activityId);

        /* เป็นคนทำกิจกรรมหรือเป็นคนดูกิจกรรม */
        $rep = new SchdActivityStepRepository();
        $activityStep = $rep->listByActivityStep($activityId, 1);
        $personType = getenv('PERSON_TYPE_STUDENT');
        
        $makerFlag = false;
        if ($activityStep) {            
            foreach ($activityStep as $step) {
                if ($step['senderTypeId'] == $personType) {
                    $makerFlag = true;
                    break;
                }
            }
        }

        if ($makerFlag) {
            view('student.activity', [
                'activityId' => $activityId,
                'itsStudentId' => $itsStudentId,
                'itsStatusSendId' => $itsStatusSendId,                
                'activity' => $activity
            ]);
        } else {
            view('student.activity2', [
                'activityId' => $activityId,
                'itsStudentId' => $itsStudentId,
                'itsStatusSendId' => $itsStatusSendId,                
                'activity' => $activity
            ]);            
        }
    }

    public function mentor($param)
    {
        $itsStudentId = $param['itsStudentId'];

        view('student.mentor', [
            'itsStudentId' => $itsStudentId
        ]);
    }

    public function executive($param)
    {
        $itsStudentId = $param['itsStudentId'];

        view('student.executive', [
            'itsStudentId' => $itsStudentId
        ]);
    }

    public function register($param)
    {
        $itsStudentId = $param['itsStudentId'];

        view('student.register', [
            'itsStudentId' => $itsStudentId
        ]);
    }
}
