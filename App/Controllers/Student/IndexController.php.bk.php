<?php

namespace App\Controllers\Student;
use App\Repositories\SchdActivityRepository;
use App\Repositories\SchdSemYearRepository;
use App\Repositories\SchdStdInfoRepository;
use App\Repositories\DocTypeRepository;

use App\Entities\ItsSchdSemYear;

class IndexController {
    public function index() {        
        

        $roundId = $_SESSION['ROUND_ID'];

        //Round Id Info
        $repSemYear = new SchdSemYearRepository();
        $resultSemYear = $repSemYear->get($roundId);

        $dataSemYear = array();
        if ($resultSemYear!=null) {
            $dataSemYear = array (
                "roundId" => $resultSemYear->getItsRoundId(),
                "year" => $resultSemYear->getYear(),
                "semCd" => $resultSemYear->getSemCd()
            );
        }

        //Student Id Info
        $repSchdStudent = new SchdStdInfoRepository();
        $resultSchdStudent = $repSchdStudent->getByRoundStudent($roundId,$_SESSION['ID']);
        
        $dataSchdStudent = array();
        if ($resultSchdStudent!=null) {
            $dataSchdStudent = array (
                "itsStudentId" => $resultSchdStudent->getItsStudentId(),
                "leaderFlag" => $resultSchdStudent->getLeaderFlag()
            );
        }               

        //Doc Type
        $repDocType = new DocTypeRepository();
        $resultDocType = $repDocType->list();

        $dataDocType = array();   
        $seqId = 0;     
        foreach($resultDocType as $docType) {        
            $dataDocType[] = array (   
                "seqId" => ++$seqId,             
                "docTypeTh" => $docType->getDocTypeTh()
            );            
        }

        
        $all = array (
            'studentId' => $_SESSION['ID'],
            'schdStudent' => $dataSchdStudent,
            'semYear' => $dataSemYear,
            'docTypes' => $dataDocType
        );

        view('student.index',$all);
    }

    public function activity($param) {
        $roundId = $param['id'];        

        //Activity        
        $repActivity = new SchdActivityRepository();
        $resultActivity = $repActivity->listByRoundId($roundId);

        $dataActivity = array();
        foreach ($resultActivity as $row) {
            $dataActivity[] = array(
                "activityId" => $row->getActivityId(),
                "activityNameTh" => $row->getActivityNameTh(),
                "startDate" => $row->getStartDate(),
                "endDate" => $row->getEndDate(),
                "docId" => $row->getDocId()
            );            
        }

        //Round Id Info
        $repSemYear = new SchdSemYearRepository();
        $resultSemYear = $repSemYear->get($roundId);

        $dataSemYear = array();
        if ($resultSemYear!=null) {
            $dataSemYear = array (
                "year" => $resultSemYear->getYear(),
                "semCd" => $resultSemYear->getSemCd()
            );
        }

        //Student Id Info
        $repSchdStudent = new SchdStdInfoRepository();
        $resultSchdStudent = $repSchdStudent->getByRoundStudent($roundId,$_SESSION['ID']);
        
        $dataSchdStudent = array();
        if ($resultSchdStudent!=null) {
            $dataSchdStudent = array (
                "itsStudentId" => $resultSchdStudent->getItsStudentId(),
                "leaderFlag" => $resultSchdStudent->getLeaderFlag()
            );
        }        
        
        $all = array (
            'schdstudent' => $dataSchdStudent,
            'activitys' => $dataActivity,
            'semYear' => $dataSemYear
        );

        view('student.activity',$all);
    }

    public function activitydetail($param) {
        $activityId = $param['id'];

        //Activity        
        $repActivity = new SchdActivityRepository();
        $resultActivity = $repActivity->get($activityId);

        $dataActivity = array();
        if ($resultActivity!=null) {
            
            $dataActivity = array(
                "activityId" => $resultActivity->getActivityId(),
                "activityNameTh" => $resultActivity->getActivityNameTh(),
                "startDate" => $resultActivity->getStartDate(),
                "endDate" => $resultActivity->getEndDate(),                
                "docId" => $resultActivity->getDocId(),
                "roundId" => $resultActivity->getItsRound()->getItsRoundId()
            );   
        }
        
        $all = array (
            'activity' => $dataActivity
        );

        view('student.activitydetail',$all);
    }

    public function activitydoc($param) {
        $itsStudentId = $param['id'];   
        
        //Student Id Info
        $repSchdStudent = new SchdStdInfoRepository();
        $resultSchdStudent = $repSchdStudent->get($itsStudentId);
        
        $dataSchdStudent = array();
        if ($resultSchdStudent!=null) {
            $dataSchdStudent = array (
                "year" => $resultSchdStudent['year'],
                "semCd" => $resultSchdStudent['semCd'],
                "studentId" => $resultSchdStudent['studentId'],
                "leaderFlag" => $resultSchdStudent['leaderFlag'],
                "itsStudentId" => $resultSchdStudent['itsStudentId']
            );
        }
       

        //Doc Type
        $repDocType = new DocTypeRepository();
        $resultDocType = $repDocType->list();

        $dataDocType = array();   
        $seqId = 0;     
        foreach($resultDocType as $docType) {        
            $dataDocType[] = array (   
                "seqId" => ++$seqId,             
                "docTypeTh" => $docType->getDocTypeTh()
            );            
        }

        $all = array (
            'schdStudent'=> $dataSchdStudent,
            'docTypes' => $dataDocType
        );
       
        view('student.activitydoc',$all);
    }
}