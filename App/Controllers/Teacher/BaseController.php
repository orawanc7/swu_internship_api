<?php

namespace App\Controllers\Teacher;

class BaseController {
    function __construct () {
        if (!isset($_SESSION['ID'])) {
            header ('Location: ' . route(''));
            exit;
        }

        if ($_SESSION['TYPE']!='TEACHER') {
            header ('Location: ' . route(''));
            exit;       
        }
    }
}    