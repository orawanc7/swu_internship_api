<?php

namespace App\Controllers\Teacher;

use App\Repositories\SchdSemYearRepository;
use App\Repositories\SchdStdInfoRepository;
use App\Repositories\SchdActivityRepository;
use App\Repositories\SchdActivityStepRepository;

class IndexController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        view('teacher.index');
    }

    public function activity($param)
    {
        $teacher = getenv('PERSON_TYPE_TEACHER');        

        $activityId = $param['activityId'];
        $itsStudentId = isset($param['itsStudentId']) ? $param['itsStudentId'] : null;
        $itsStatusSendId = isset($param['itsStatusSendId']) ? $param['itsStatusSendId'] : null;

        $rep = new SchdActivityRepository();
        $activity = $rep->get($activityId);


        $repStep = new SchdActivityStepRepository();
        $activityStep = $repStep->listByActivityStep($activityId ,1);

        $blnCheck = true;
        if ($activityStep) {
            if ($teacher == $activityStep[0]['senderTypeId']) {
                $blnCheck = false;
            }
        }
        
        if ($blnCheck) {
            view('teacher.activity', [
                'activityId' => $activityId,
                'itsStudentId' => $itsStudentId,
                'itsStatusSendId' => $itsStatusSendId,
                'activity' => $activity
            ]);
        } else {
            view('teacher.activity2', [
                'activityId' => $activityId,
                'itsStudentId' => $itsStudentId,
                'itsStatusSendId' => $itsStatusSendId,
                'activity' => $activity
            ]);
            
        }
              
    }

    public function activitylist($param)
    {
        $itsRoundId = $param['itsRoundId'];

        $rep = new SchdSemYearRepository();
        $round = $rep->get($itsRoundId);

        view('teacher.activitylist', [
            'itsRoundId' => $itsRoundId,
            'round' => $round
        ]);
    }

    public function student($param)
    {
        $strItsStudentId = $param['itsStudentId'];

        $rep = new SchdStdInfoRepository();
        $result = $rep->get($strItsStudentId);

        view('teacher.student', [
            'itsStudent' => $result
        ]);
    }
}
