<?php

namespace App\Controllers\Teacher;
use App\Repositories\PersonTypeRepository;
use App\Repositories\SchdPersonStdRepository;

class IndexController {
    public function index() {
        //Load All Person Type
        $repPersonStd = new SchdPersonStdRepository();
        $resultPersonStd = $repPersonStd->listRoundPersonTypeByPersonId($_SESSION['ID']);
        
        $dataPersonType = array();
        if ($resultPersonStd != null) {
            $itsPersonTypeId = "";
            $personTypeNameTh = "";
            $dataRound = array();           
            foreach ($resultPersonStd as $row) {
                if (($itsPersonTypeId != $row['itsPersonTypeId']) && (strlen($itsPersonTypeId)>0)) {
                    $dataPersonType[] = array (
                        "itsPersonTypeId" => $itsPersonTypeId,
                        "personTypeNameTh" => $personTypeNameTh ,
                        "round" => $dataRound
                    );
                    $dataRound = array();
                }
                $dataRound[] = array (
                    "itsRoundId" => $row['itsRoundId'],
                    "year" => $row['year'],
                    "semCd" => $row['semCd']
                );

                $itsPersonTypeId = $row['itsPersonTypeId'];
                $personTypeNameTh = $row['personTypeNameTh'];
            } //end foreach
            //Last Record
            $dataPersonType[] = array (
                "itsPersonTypeId" => $itsPersonTypeId,
                "personTypeNameTh" => $personTypeNameTh ,
                "rounds" => $dataRound
            );
        } 

        $all = array (
            'itsPersonId' => $_SESSION['ID'],
            'personTypes' => $dataPersonType
        );
        
        view('teacher.index',$all);        
    }

    public function activity() {
        view('teacher.activity');
    }

    public function activitydetail() {
        view('teacher.activitydetail');
    }
}