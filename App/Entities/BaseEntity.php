<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

class BaseEntity
{
    public function __construct()
    {
        $now = new \DateTime('NOW');
        $this->setLastUpdateDtm($now);
    }

    /**
     * @ORM\PrePersist()     
     */
    public function createDateTiime()
    {
        $now = new \DateTime('now');
        $this->setCreationDtm($now);
    }

}
