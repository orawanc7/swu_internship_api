<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsAttach
 *
 * @ORM\Table(name="ITS_ATTACH")
 * @ORM\Entity
 */
class ItsAttach
{
    /**
     * @var int
     *
     * @ORM\Column(name="FILE_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\SequenceGenerator(sequenceName="ITS_ATTACH_FILE_ID_seq", allocationSize=1, initialValue=1)
     */
    private $fileId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FILE_CATEGORY", type="string", length=20, nullable=true)
     */
    private $fileCategory;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FILE_NAME", type="string", length=300, nullable=true)
     */
    private $fileName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FILE_EXTENSION", type="string", length=20, nullable=true)
     */
    private $fileExtension;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FILE_PATH", type="string", length=100, nullable=true)
     */
    private $filePath;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=200, nullable=true)
     */
    private $remark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;



    /**
     * Get fileId.
     *
     * @return int
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * Set fileCategory.
     *
     * @param string|null $fileCategory
     *
     * @return ItsAttach
     */
    public function setFileCategory($fileCategory = null)
    {
        $this->fileCategory = $fileCategory;
    
        return $this;
    }

    /**
     * Get fileCategory.
     *
     * @return string|null
     */
    public function getFileCategory()
    {
        return $this->fileCategory;
    }

    /**
     * Set fileName.
     *
     * @param string|null $fileName
     *
     * @return ItsAttach
     */
    public function setFileName($fileName = null)
    {
        $this->fileName = $fileName;
    
        return $this;
    }

    /**
     * Get fileName.
     *
     * @return string|null
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set fileExtension.
     *
     * @param string|null $fileExtension
     *
     * @return ItsAttach
     */
    public function setFileExtension($fileExtension = null)
    {
        $this->fileExtension = $fileExtension;
    
        return $this;
    }

    /**
     * Get fileExtension.
     *
     * @return string|null
     */
    public function getFileExtension()
    {
        return $this->fileExtension;
    }

    /**
     * Set filePath.
     *
     * @param string|null $filePath
     *
     * @return ItsAttach
     */
    public function setFilePath($filePath = null)
    {
        $this->filePath = $filePath;
    
        return $this;
    }

    /**
     * Get filePath.
     *
     * @return string|null
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsAttach
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;
    
        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsAttach
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;
    
        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsAttach
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;
    
        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsAttach
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;
    
        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsAttach
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;
    
        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsAttach
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;
    
        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsAttach
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;
    
        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }
}
