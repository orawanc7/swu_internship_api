<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsCDocMaster
 *
 * @ORM\Table(name="ITS_C_DOC_MASTER", indexes={@ORM\Index(name="IDX_4AB6806FB06E494F", columns={"DOC_GROUP_ID"}), @ORM\Index(name="IDX_4AB6806F5696B213", columns={"DOC_TYPE_ID"})})
 * @ORM\Entity
 */
class ItsCDocMaster
{
    /**
     * @var int
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=false, options={"comment"="รหัสเอกสาร  (XX) (Running 2 หลัก)"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\SequenceGenerator(sequenceName="ITS_C_DOC_MASTER_DOC_ID_seq", allocationSize=1, initialValue=1)
     */
    private $docId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DOC_CD_TH", type="string", length=10, nullable=true, options={"comment"="รหัสเอกสารภาษาไทย"})
     */
    private $docCdTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DOC_CD_EN", type="string", length=10, nullable=true, options={"comment"="รหัสเอกสารภาษาอังกฤษ"})
     */
    private $docCdEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DOC_NAME_TH", type="string", length=200, nullable=true, options={"comment"="ชื่อเอกสารภาษาไทย"})
     */
    private $docNameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DOC_NAME_EN", type="string", length=200, nullable=true, options={"comment"="ชื่อเอกสารภาษาอังกฤษ"})
     */
    private $docNameEn;
    
    /**
     * @var string|null
     *
     * @ORM\Column(name="DOC_FINAL_FLAG", type="string", length=1, nullable=true, options={"comment"="ประเภทเอกสารที่ต้องส่งปลายภาค"})
     */
    private $docFinalFlag;
    
    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=400, nullable=true, options={"comment"="หมายเหตุ"})
     */
    private $remark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true, options={"comment"="สถานะการใช้งาน N:ไม่ใช้งาน, Y:ใช้งาน"})
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true, options={"comment"="วันที่เพิ่มข้อมูล"})
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true, options={"comment"="ผู้เพิ่มข้อมูล"})
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true, options={"comment"="วันที่แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true, options={"comment"="ผู้แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true, options={"comment"="ชื่อโปรแกรมที่ทำการแก้ไข"})
     */
    private $programCd;


    /**
     * @var \App\Entities\ItsCDocType
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\ItsCDocType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="DOC_TYPE_ID", referencedColumnName="DOC_TYPE_ID")
     * })
     */
    private $docType;

    /**
     * Set docId.
     *
     * @param string|null $docId
     *
     * @return ItsCDocMaster
     */
    public function setDocId($docId = null)
    {
        $this->docId = $docId;

        return $this;
    }

    /**
     * Get docId.
     *
     * @return int
     */
    public function getDocId()
    {
        return $this->docId;
    }

    /**
     * Set docCdTh.
     *
     * @param string|null $docCdTh
     *
     * @return ItsCDocMaster
     */
    public function setDocCdTh($docCdTh = null)
    {
        $this->docCdTh = $docCdTh;

        return $this;
    }

    /**
     * Get docCdTh.
     *
     * @return string|null
     */
    public function getDocCdTh()
    {
        return $this->docCdTh;
    }

    /**
     * Set docCdEn.
     *
     * @param string|null $docCdEn
     *
     * @return ItsCDocMaster
     */
    public function setDocCdEn($docCdEn = null)
    {
        $this->docCdEn = $docCdEn;

        return $this;
    }

    /**
     * Get docCdEn.
     *
     * @return string|null
     */
    public function getDocCdEn()
    {
        return $this->docCdEn;
    }

    /**
     * Set docNameTh.
     *
     * @param string|null $docNameTh
     *
     * @return ItsCDocMaster
     */
    public function setDocNameTh($docNameTh = null)
    {
        $this->docNameTh = $docNameTh;

        return $this;
    }

    /**
     * Get docNameTh.
     *
     * @return string|null
     */
    public function getDocNameTh()
    {
        return $this->docNameTh;
    }

    /**
     * Set docNameEn.
     *
     * @param string|null $docNameEn
     *
     * @return ItsCDocMaster
     */
    public function setDocNameEn($docNameEn = null)
    {
        $this->docNameEn = $docNameEn;

        return $this;
    }

    /**
     * Get docNameEn.
     *
     * @return string|null
     */
    public function getDocNameEn()
    {
        return $this->docNameEn;
    }    

    /**
     * Set docFinalFlag.
     *
     * @param string|null $docFinalFlag
     *
     * @return ItsCDocMaster
     */
    public function setDocFinalFlag($docFinalFlag = null)
    {
        $this->docFinalFlag = $docFinalFlag;

        return $this;
    }

    /**
     * Get docFinalFlag.
     *
     * @return string|null
     */
    public function getDocFinalFlag()
    {
        return $this->docFinalFlag;
    }    

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsCDocMaster
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsCDocMaster
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsCDocMaster
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsCDocMaster
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsCDocMaster
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsCDocMaster
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsCDocMaster
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }


    /**
     * Set docType.
     *
     * @param \App\Entities\ItsCDocType|null $docType
     *
     * @return ItsCDocMaster
     */
    public function setDocType(\App\Entities\ItsCDocType $docType = null)
    {
        $this->docType = $docType;

        return $this;
    }

    /**
     * Get docType.
     *
     * @return \App\Entities\ItsCDocType|null
     */
    public function getDocType()
    {
        return $this->docType;
    }
}
