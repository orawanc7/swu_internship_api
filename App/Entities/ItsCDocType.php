<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsCDocType
 *
 * @ORM\Table(name="ITS_C_DOC_TYPE")
 * @ORM\Entity
 */
class ItsCDocType
{
    /**
     * @var int
     *
     * @ORM\Column(name="DOC_TYPE_ID", type="integer", nullable=false, options={"comment"="รหัสประเภทเอกสาร"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\SequenceGenerator(sequenceName="ITS_C_DOC_TYPE_DOC_TYPE_ID_seq", allocationSize=1, initialValue=1)
     */
    private $docTypeId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DOC_TYPE_TH", type="string", length=200, nullable=true, options={"comment"="ชื่อประเภทเอกสารภาษาไทย"})
     */
    private $docTypeTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DOC_TYPE_EN", type="string", length=200, nullable=true, options={"comment"="ชื่อกลุ่มเอกสารภาษาอังกฤษ"})
     */
    private $docTypeEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true, options={"comment"="สถานะการใช้งาน N:ไม่ใช้งาน, Y:ใช้งาน"})
     */
    private $activeFlag;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DOC_SCREEN_FLAG", type="string", length=1, nullable=true, options={"comment"="ลักษณะการแสดง S=Screen,F=Form (Dynamic)"})
     */
    private $docScreenFlag;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=400, nullable=true, options={"comment"="หมายเหตุ"})
     */
    private $remark;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true, options={"comment"="วันที่เพิ่มข้อมูล"})
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true, options={"comment"="ผู้เพิ่มข้อมูล"})
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true, options={"comment"="วันที่แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true, options={"comment"="ผู้แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true, options={"comment"="ชื่อโปรแกรมที่ทำการแก้ไข"})
     */
    private $programCd;

    /**
     * Set docTypeId.
     *
     * @param string|null $docTypeId
     *
     * @return ItsCDocType
     */
    public function setDocTypeId($docTypeId = null)
    {
        $this->docTypeId = $docTypeId;

        return $this;
    }

    /**
     * Get docTypeId.
     *
     * @return int
     */
    public function getDocTypeId()
    {
        return $this->docTypeId;
    }

    /**
     * Set docTypeTh.
     *
     * @param string|null $docTypeTh
     *
     * @return ItsCDocType
     */
    public function setDocTypeTh($docTypeTh = null)
    {
        $this->docTypeTh = $docTypeTh;

        return $this;
    }

    /**
     * Get docTypeTh.
     *
     * @return string|null
     */
    public function getDocTypeTh()
    {
        return $this->docTypeTh;
    }

    /**
     * Set docTypeEn.
     *
     * @param string|null $docTypeEn
     *
     * @return ItsCDocType
     */
    public function setDocTypeEn($docTypeEn = null)
    {
        $this->docTypeEn = $docTypeEn;

        return $this;
    }

    /**
     * Get docTypeEn.
     *
     * @return string|null
     */
    public function getDocTypeEn()
    {
        return $this->docTypeEn;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsCDocType
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set docScreenFlag.
     *
     * @param string|null $docScreenFlag
     *
     * @return ItsCDocType
     */
    public function setDocScreenFlag($docScreenFlag = null)
    {
        $this->docScreenFlag = $docScreenFlag;

        return $this;
    }

    /**
     * Get docScreenFlag.
     *
     * @return string|null
     */
    public function getDocScreenFlag()
    {
        return $this->docScreenFlag;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsCDocType
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsCDocType
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsCDocType
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsCDocType
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsCDocType
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsCDocType
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }
}
