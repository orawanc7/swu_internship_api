<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks as HasLifecycleCallbacks;

/**
 * ItsCPersonGroup
 *
 * @ORM\Table(name="ITS_C_PERSON_GROUP")
 * @ORM\Entity
 * @HasLifecycleCallbacks 
 */
class ItsCPersonGroup extends BaseEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="PERSON_GROUP_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\SequenceGenerator(sequenceName="ITS_C_PERSON_GROUP_PERSON_GROU", allocationSize=1, initialValue=1)
     */
    private $personGroupId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_GROUP_NAME_TH", type="string", length=50, nullable=true)
     */
    private $personGroupNameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_GROUP_NAME_EN", type="string", length=50, nullable=true)
     */
    private $personGroupNameEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=200, nullable=true)
     */
    private $remark;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;


    /**
     * Set personGroupId.
     *
     * @param string|null $personGroupId
     *
     * @return ItsCPersonGroup
     */
    public function setPersonGroupId($personGroupId = null)
    {
        $this->personGroupId = $personGroupId;

        return $this;
    }

    /**
     * Get personGroupId.
     *
     * @return int
     */
    public function getPersonGroupId()
    {
        return $this->personGroupId;
    }

    /**
     * Set personGroupNameTh.
     *
     * @param string|null $personGroupNameTh
     *
     * @return ItsCPersonGroup
     */
    public function setPersonGroupNameTh($personGroupNameTh = null)
    {
        $this->personGroupNameTh = $personGroupNameTh;

        return $this;
    }

    /**
     * Get personGroupNameTh.
     *
     * @return string|null
     */
    public function getPersonGroupNameTh()
    {
        return $this->personGroupNameTh;
    }

    /**
     * Set personGroupNameEn.
     *
     * @param string|null $personGroupNameEn
     *
     * @return ItsCPersonGroup
     */
    public function setPersonGroupNameEn($personGroupNameEn = null)
    {
        $this->personGroupNameEn = $personGroupNameEn;

        return $this;
    }

    /**
     * Get personGroupNameEn.
     *
     * @return string|null
     */
    public function getPersonGroupNameEn()
    {
        return $this->personGroupNameEn;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsCPersonGroup
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsCPersonGroup
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsCPersonGroup
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsCPersonGroup
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsCPersonGroup
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsCPersonGroup
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsCPersonGroup
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }
}
