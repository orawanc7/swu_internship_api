<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks as HasLifecycleCallbacks;

/**
 * ItsCPersonType
 *
 * @ORM\Table(name="ITS_C_PERSON_TYPE", indexes={@ORM\Index(name="its_c_person_type_index1", columns={"PERSON_GROUP_ID"})})
 * @ORM\Entity
 * @HasLifecycleCallbacks 
 */
class ItsCPersonType extends BaseEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="PERSON_TYPE_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\SequenceGenerator(sequenceName="ITS_C_PERSON_TYPE_PERSON_TYPE_", allocationSize=1, initialValue=1)
     */
    private $personTypeId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_TYPE_NAME_TH", type="string", length=50, nullable=true)
     */
    private $personTypeNameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_TYPE_NAME_EN", type="string", length=50, nullable=true)
     */
    private $personTypeNameEn;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PERSON_GROUP_ID", type="integer", nullable=true)
     */
    private $personGroupId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=200, nullable=true)
     */
    private $remark;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;


    /**
     * Set personTypeId.
     *
     * @param string|null $personTypeId
     *
     * @return ItsCPersonType
     */
    public function setPersonTypeId($personTypeId = null)
    {
        $this->personTypeId = $personTypeId;

        return $this;
    }

    /**
     * Get personTypeId.
     *
     * @return int
     */
    public function getPersonTypeId()
    {
        return $this->personTypeId;
    }

    /**
     * Set personTypeNameTh.
     *
     * @param string|null $personTypeNameTh
     *
     * @return ItsCPersonType
     */
    public function setPersonTypeNameTh($personTypeNameTh = null)
    {
        $this->personTypeNameTh = $personTypeNameTh;

        return $this;
    }

    /**
     * Get personTypeNameTh.
     *
     * @return string|null
     */
    public function getPersonTypeNameTh()
    {
        return $this->personTypeNameTh;
    }

    /**
     * Set personTypeNameEn.
     *
     * @param string|null $personTypeNameEn
     *
     * @return ItsCPersonType
     */
    public function setPersonTypeNameEn($personTypeNameEn = null)
    {
        $this->personTypeNameEn = $personTypeNameEn;

        return $this;
    }

    /**
     * Get personTypeNameEn.
     *
     * @return string|null
     */
    public function getPersonTypeNameEn()
    {
        return $this->personTypeNameEn;
    }

    /**
     * Set personGroupId.
     *
     * @param int|null $personGroupId
     *
     * @return ItsCPersonType
     */
    public function setPersonGroupId($personGroupId = null)
    {
        $this->personGroupId = $personGroupId;

        return $this;
    }

    /**
     * Get personGroupId.
     *
     * @return int|null
     */
    public function getPersonGroupId()
    {
        return $this->personGroupId;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsCPersonType
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsCPersonType
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsCPersonType
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsCPersonType
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsCPersonType
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsCPersonType
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsCPersonType
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }
}
