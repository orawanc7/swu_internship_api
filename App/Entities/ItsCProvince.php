<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsCProvince
 *
 * @ORM\Table(name="ITS_C_PROVINCE")
 * @ORM\Entity
 */
class ItsCProvince
{
    /**
     * @var int
     *
     * @ORM\Column(name="PROVINCE_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ITS_C_PROVINCE_PROVINCE_ID_seq", allocationSize=1, initialValue=1)
     */
    private $provinceId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROVINCE_NAME_TH", type="string", length=150, nullable=true)
     */
    private $provinceNameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROVINCE_NAME_EN", type="string", length=150, nullable=true)
     */
    private $provinceNameEn;

    /**
     * @var int|null
     *
     * @ORM\Column(name="REGION_ID", type="integer", nullable=true)
     */
    private $regionId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;



    /**
     * Get provinceId.
     *
     * @return int
     */
    public function getProvinceId()
    {
        return $this->provinceId;
    }

    /**
     * Set provinceNameTh.
     *
     * @param string|null $provinceNameTh
     *
     * @return ItsCProvince
     */
    public function setProvinceNameTh($provinceNameTh = null)
    {
        $this->provinceNameTh = $provinceNameTh;

        return $this;
    }

    /**
     * Get provinceNameTh.
     *
     * @return string|null
     */
    public function getProvinceNameTh()
    {
        return $this->provinceNameTh;
    }

    /**
     * Set provinceNameEn.
     *
     * @param string|null $provinceNameEn
     *
     * @return ItsCProvince
     */
    public function setProvinceNameEn($provinceNameEn = null)
    {
        $this->provinceNameEn = $provinceNameEn;

        return $this;
    }

    /**
     * Get provinceNameEn.
     *
     * @return string|null
     */
    public function getProvinceNameEn()
    {
        return $this->provinceNameEn;
    }

    /**
     * Set regionId.
     *
     * @param int|null $regionId
     *
     * @return ItsCProvince
     */
    public function setRegionId($regionId = null)
    {
        $this->regionId = $regionId;

        return $this;
    }

    /**
     * Get regionId.
     *
     * @return int|null
     */
    public function getRegionId()
    {
        return $this->regionId;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsCProvince
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsCProvince
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsCProvince
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsCProvince
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsCProvince
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsCProvince
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }
}
