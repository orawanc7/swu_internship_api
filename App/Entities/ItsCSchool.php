<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks as HasLifecycleCallbacks;

/**
 * ItsCSchool
 *
 * @ORM\Table(name="ITS_C_SCHOOL")
 * @ORM\Entity
 *  @HasLifecycleCallbacks 
 */
class ItsCSchool extends BaseEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="SCHOOL_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\SequenceGenerator(sequenceName="ITS_C_SCHOOL_SCHOOL_ID_seq", allocationSize=1, initialValue=1)
     */
    private $schoolId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SCHOOL_NAME_TH", type="string", length=150, nullable=true)
     */
    private $schoolNameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SCHOOL_NAME_EN", type="string", length=150, nullable=true)
     */
    private $schoolNameEn;

    /**
     * @var int|null
     *
     * @ORM\Column(name="SCHOOL_SUB_ID", type="integer", nullable=true)
     */
    private $schoolSubId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="SCHOOL_TYPE_ID", type="integer", nullable=true)
     */
    private $schoolTypeId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CAMPUS_NAME", type="string", length=50, nullable=true)
     */
    private $campusName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ADDRESS_NO", type="string", length=10, nullable=true)
     */
    private $addressNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SOI", type="string", length=80, nullable=true)
     */
    private $soi;

    /**
     * @var string|null
     *
     * @ORM\Column(name="STREET", type="string", length=80, nullable=true)
     */
    private $street;

    /**
     * @var int|null
     *
     * @ORM\Column(name="DISTRICT_ID", type="integer", nullable=true)
     */
    private $districtId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="AMPHUR_ID", type="smallint", nullable=true)
     */
    private $amphurId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="POSTCODE_ID", type="smallint", nullable=true)
     */
    private $postcodeId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PROVINCE_ID", type="integer", nullable=true)
     */
    private $provinceId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ADDRESS_DESC", type="string", length=4000, nullable=true)
     */
    private $addressDesc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FAX_NO", type="string", length=50, nullable=true)
     */
    private $faxNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TELEPHONE_NO", type="string", length=50, nullable=true)
     */
    private $telephoneNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MOBILE_NO", type="string", length=50, nullable=true)
     */
    private $mobileNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SCHOOL_GOOGLE_MAP", type="string", length=250, nullable=true)
     */
    private $schoolGoogleMap;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SCHOOL_WEBSITE", type="string", length=4000, nullable=true)
     */
    private $schoolWebsite;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SCHOOL_EMAIL", type="string", length=50, nullable=true)
     */
    private $schoolEmail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SCHOOL_FB_LINK", type="string", length=4000, nullable=true)
     */
    private $schoolFbLink;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PICTURE_SCHOOL", type="string", length=50, nullable=true)
     */
    private $pictureSchool;

    /**
     * @var string|null
     *
     * @ORM\Column(name="BLACKLIST_FLAG", type="string", length=1, nullable=true)
     */
    private $blacklistFlag;

    /**
     * @var string|null
     *
     * @ORM\Column(name="BLACKLIST_REMARK", type="string", length=50, nullable=true)
     */
    private $blacklistRemark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;


    /**
     * Set schoolId.
     *
     * @param string|null $schoolId
     *
     * @return ItsCSchool
     */
    public function setSchoolId($schoolId = null)
    {
        $this->schoolId = $schoolId;

        return $this;
    }


    /**
     * Get schoolId.
     *
     * @return int
     */
    public function getSchoolId()
    {
        return $this->schoolId;
    }

    /**
     * Set schoolNameTh.
     *
     * @param string|null $schoolNameTh
     *
     * @return ItsCSchool
     */
    public function setSchoolNameTh($schoolNameTh = null)
    {
        $this->schoolNameTh = $schoolNameTh;

        return $this;
    }

    /**
     * Get schoolNameTh.
     *
     * @return string|null
     */
    public function getSchoolNameTh()
    {
        return $this->schoolNameTh;
    }

    /**
     * Set schoolNameEn.
     *
     * @param string|null $schoolNameEn
     *
     * @return ItsCSchool
     */
    public function setSchoolNameEn($schoolNameEn = null)
    {
        $this->schoolNameEn = $schoolNameEn;

        return $this;
    }

    /**
     * Get schoolNameEn.
     *
     * @return string|null
     */
    public function getSchoolNameEn()
    {
        return $this->schoolNameEn;
    }

    /**
     * Set schoolSubId.
     *
     * @param int|null $schoolSubId
     *
     * @return ItsCSchool
     */
    public function setSchoolSubId($schoolSubId = null)
    {
        $this->schoolSubId = $schoolSubId;

        return $this;
    }

    /**
     * Get schoolSubId.
     *
     * @return int|null
     */
    public function getSchoolSubId()
    {
        return $this->schoolSubId;
    }

    /**
     * Set schoolTypeId.
     *
     * @param int|null $schoolTypeId
     *
     * @return ItsCSchool
     */
    public function setSchoolTypeId($schoolTypeId = null)
    {
        $this->schoolTypeId = $schoolTypeId;

        return $this;
    }

    /**
     * Get schoolTypeId.
     *
     * @return int|null
     */
    public function getSchoolTypeId()
    {
        return $this->schoolTypeId;
    }

    /**
     * Set campusName.
     *
     * @param string|null $campusName
     *
     * @return ItsCSchool
     */
    public function setCampusName($campusName = null)
    {
        $this->campusName = $campusName;

        return $this;
    }

    /**
     * Get campusName.
     *
     * @return string|null
     */
    public function getCampusName()
    {
        return $this->campusName;
    }

    /**
     * Set addressNo.
     *
     * @param string|null $addressNo
     *
     * @return ItsCSchool
     */
    public function setAddressNo($addressNo = null)
    {
        $this->addressNo = $addressNo;

        return $this;
    }

    /**
     * Get addressNo.
     *
     * @return string|null
     */
    public function getAddressNo()
    {
        return $this->addressNo;
    }

    /**
     * Set soi.
     *
     * @param string|null $soi
     *
     * @return ItsCSchool
     */
    public function setSoi($soi = null)
    {
        $this->soi = $soi;

        return $this;
    }

    /**
     * Get soi.
     *
     * @return string|null
     */
    public function getSoi()
    {
        return $this->soi;
    }

    /**
     * Set street.
     *
     * @param string|null $street
     *
     * @return ItsCSchool
     */
    public function setStreet($street = null)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street.
     *
     * @return string|null
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set districtId.
     *
     * @param int|null $districtId
     *
     * @return ItsCSchool
     */
    public function setDistrictId($districtId = null)
    {
        $this->districtId = $districtId;

        return $this;
    }

    /**
     * Get districtId.
     *
     * @return int|null
     */
    public function getDistrictId()
    {
        return $this->districtId;
    }

    /**
     * Set amphurId.
     *
     * @param int|null $amphurId
     *
     * @return ItsCSchool
     */
    public function setAmphurId($amphurId = null)
    {
        $this->amphurId = $amphurId;

        return $this;
    }

    /**
     * Get amphurId.
     *
     * @return int|null
     */
    public function getAmphurId()
    {
        return $this->amphurId;
    }

    /**
     * Set postcodeId.
     *
     * @param int|null $postcodeId
     *
     * @return ItsCSchool
     */
    public function setPostcodeId($postcodeId = null)
    {
        $this->postcodeId = $postcodeId;

        return $this;
    }

    /**
     * Get postcodeId.
     *
     * @return int|null
     */
    public function getPostcodeId()
    {
        return $this->postcodeId;
    }

    /**
     * Set provinceId.
     *
     * @param int|null $provinceId
     *
     * @return ItsCSchool
     */
    public function setProvinceId($provinceId = null)
    {
        $this->provinceId = $provinceId;

        return $this;
    }

    /**
     * Get provinceId.
     *
     * @return int|null
     */
    public function getProvinceId()
    {
        return $this->provinceId;
    }

    /**
     * Set addressDesc.
     *
     * @param string|null $addressDesc
     *
     * @return ItsCSchool
     */
    public function setAddressDesc($addressDesc = null)
    {
        $this->addressDesc = $addressDesc;

        return $this;
    }

    /**
     * Get addressDesc.
     *
     * @return string|null
     */
    public function getAddressDesc()
    {
        return $this->addressDesc;
    }

    /**
     * Set faxNo.
     *
     * @param string|null $faxNo
     *
     * @return ItsCSchool
     */
    public function setFaxNo($faxNo = null)
    {
        $this->faxNo = $faxNo;

        return $this;
    }

    /**
     * Get faxNo.
     *
     * @return string|null
     */
    public function getFaxNo()
    {
        return $this->faxNo;
    }

    /**
     * Set telephoneNo.
     *
     * @param string|null $telephoneNo
     *
     * @return ItsCSchool
     */
    public function setTelephoneNo($telephoneNo = null)
    {
        $this->telephoneNo = $telephoneNo;

        return $this;
    }

    /**
     * Get telephoneNo.
     *
     * @return string|null
     */
    public function getTelephoneNo()
    {
        return $this->telephoneNo;
    }

    /**
     * Set mobileNo.
     *
     * @param string|null $mobileNo
     *
     * @return ItsCSchool
     */
    public function setMobileNo($mobileNo = null)
    {
        $this->mobileNo = $mobileNo;

        return $this;
    }

    /**
     * Get mobileNo.
     *
     * @return string|null
     */
    public function getMobileNo()
    {
        return $this->mobileNo;
    }

    /**
     * Set schoolGoogleMap.
     *
     * @param string|null $schoolGoogleMap
     *
     * @return ItsCSchool
     */
    public function setSchoolGoogleMap($schoolGoogleMap = null)
    {
        $this->schoolGoogleMap = $schoolGoogleMap;

        return $this;
    }

    /**
     * Get schoolGoogleMap.
     *
     * @return string|null
     */
    public function getSchoolGoogleMap()
    {
        return $this->schoolGoogleMap;
    }

    /**
     * Set schoolWebsite.
     *
     * @param string|null $schoolWebsite
     *
     * @return ItsCSchool
     */
    public function setSchoolWebsite($schoolWebsite = null)
    {
        $this->schoolWebsite = $schoolWebsite;

        return $this;
    }

    /**
     * Get schoolWebsite.
     *
     * @return string|null
     */
    public function getSchoolWebsite()
    {
        return $this->schoolWebsite;
    }

    /**
     * Set schoolEmail.
     *
     * @param string|null $schoolEmail
     *
     * @return ItsCSchool
     */
    public function setSchoolEmail($schoolEmail = null)
    {
        $this->schoolEmail = $schoolEmail;

        return $this;
    }

    /**
     * Get schoolEmail.
     *
     * @return string|null
     */
    public function getSchoolEmail()
    {
        return $this->schoolEmail;
    }

    /**
     * Set schoolFbLink.
     *
     * @param string|null $schoolFbLink
     *
     * @return ItsCSchool
     */
    public function setSchoolFbLink($schoolFbLink = null)
    {
        $this->schoolFbLink = $schoolFbLink;

        return $this;
    }

    /**
     * Get schoolFbLink.
     *
     * @return string|null
     */
    public function getSchoolFbLink()
    {
        return $this->schoolFbLink;
    }

    /**
     * Set pictureSchool.
     *
     * @param string|null $pictureSchool
     *
     * @return ItsCSchool
     */
    public function setPictureSchool($pictureSchool = null)
    {
        $this->pictureSchool = $pictureSchool;

        return $this;
    }

    /**
     * Get pictureSchool.
     *
     * @return string|null
     */
    public function getPictureSchool()
    {
        return $this->pictureSchool;
    }

    /**
     * Set blacklistFlag.
     *
     * @param string|null $blacklistFlag
     *
     * @return ItsCSchool
     */
    public function setBlacklistFlag($blacklistFlag = null)
    {
        $this->blacklistFlag = $blacklistFlag;

        return $this;
    }

    /**
     * Get blacklistFlag.
     *
     * @return string|null
     */
    public function getBlacklistFlag()
    {
        return $this->blacklistFlag;
    }

    /**
     * Set blacklistRemark.
     *
     * @param string|null $blacklistRemark
     *
     * @return ItsCSchool
     */
    public function setBlacklistRemark($blacklistRemark = null)
    {
        $this->blacklistRemark = $blacklistRemark;

        return $this;
    }

    /**
     * Get blacklistRemark.
     *
     * @return string|null
     */
    public function getBlacklistRemark()
    {
        return $this->blacklistRemark;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsCSchool
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsCSchool
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsCSchool
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsCSchool
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsCSchool
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsCSchool
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }
}
