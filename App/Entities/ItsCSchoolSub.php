<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsCSchoolSub
 *
 * @ORM\Table(name="ITS_C_SCHOOL_SUB")
 * @ORM\Entity
 */
class ItsCSchoolSub
{
    /**
     * @var int
     *
     * @ORM\Column(name="SCHOOL_SUB_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ITS_C_SCHOOL_SUB_SCHOOL_SUB_ID", allocationSize=1, initialValue=1)
     */
    private $schoolSubId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SCHOOL_SUB_NAME_TH", type="string", length=150, nullable=true)
     */
    private $schoolSubNameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SCHOOL_SUB_NAME_EN", type="string", length=150, nullable=true)
     */
    private $schoolSubNameEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;



    /**
     * Get schoolSubId.
     *
     * @return int
     */
    public function getSchoolSubId()
    {
        return $this->schoolSubId;
    }

    /**
     * Set schoolSubNameTh.
     *
     * @param string|null $schoolSubNameTh
     *
     * @return ItsCSchoolSub
     */
    public function setSchoolSubNameTh($schoolSubNameTh = null)
    {
        $this->schoolSubNameTh = $schoolSubNameTh;

        return $this;
    }

    /**
     * Get schoolSubNameTh.
     *
     * @return string|null
     */
    public function getSchoolSubNameTh()
    {
        return $this->schoolSubNameTh;
    }

    /**
     * Set schoolSubNameEn.
     *
     * @param string|null $schoolSubNameEn
     *
     * @return ItsCSchoolSub
     */
    public function setSchoolSubNameEn($schoolSubNameEn = null)
    {
        $this->schoolSubNameEn = $schoolSubNameEn;

        return $this;
    }

    /**
     * Get schoolSubNameEn.
     *
     * @return string|null
     */
    public function getSchoolSubNameEn()
    {
        return $this->schoolSubNameEn;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsCSchoolSub
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsCSchoolSub
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsCSchoolSub
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsCSchoolSub
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsCSchoolSub
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsCSchoolSub
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }
}
