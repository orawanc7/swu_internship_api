<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsCSchoolType
 *
 * @ORM\Table(name="ITS_C_SCHOOL_TYPE")
 * @ORM\Entity
 */
class ItsCSchoolType
{
    /**
     * @var int
     *
     * @ORM\Column(name="SCHOOL_TYPE_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ITS_C_SCHOOL_TYPE_SCHOOL_TYPE_", allocationSize=1, initialValue=1)
     */
    private $schoolTypeId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SCHOOL_TYPE_NAME_TH", type="string", length=150, nullable=true)
     */
    private $schoolTypeNameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SCHOOL_TYPE_NAME_EN", type="string", length=150, nullable=true)
     */
    private $schoolTypeNameEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;



    /**
     * Get schoolTypeId.
     *
     * @return int
     */
    public function getSchoolTypeId()
    {
        return $this->schoolTypeId;
    }

    /**
     * Set schoolTypeNameTh.
     *
     * @param string|null $schoolTypeNameTh
     *
     * @return ItsCSchoolType
     */
    public function setSchoolTypeNameTh($schoolTypeNameTh = null)
    {
        $this->schoolTypeNameTh = $schoolTypeNameTh;

        return $this;
    }

    /**
     * Get schoolTypeNameTh.
     *
     * @return string|null
     */
    public function getSchoolTypeNameTh()
    {
        return $this->schoolTypeNameTh;
    }

    /**
     * Set schoolTypeNameEn.
     *
     * @param string|null $schoolTypeNameEn
     *
     * @return ItsCSchoolType
     */
    public function setSchoolTypeNameEn($schoolTypeNameEn = null)
    {
        $this->schoolTypeNameEn = $schoolTypeNameEn;

        return $this;
    }

    /**
     * Get schoolTypeNameEn.
     *
     * @return string|null
     */
    public function getSchoolTypeNameEn()
    {
        return $this->schoolTypeNameEn;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsCSchoolType
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsCSchoolType
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsCSchoolType
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsCSchoolType
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsCSchoolType
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsCSchoolType
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }
}
