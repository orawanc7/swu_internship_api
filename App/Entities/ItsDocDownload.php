<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsDocDownload
 *
 * @ORM\Table(name="ITS_DOC_DOWNLOAD", indexes={@ORM\Index(name="its_doc_download_index1", columns={"DOC_GROUP_DOWNLOAD_ID"})})
 * @ORM\Entity
 */
class ItsDocDownload
{
    /**
     * @var int
     *
     * @ORM\Column(name="DOC_DOWNLOAD_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\SequenceGenerator(sequenceName="ITS_DOC_DOWNLOAD_DOC_DOWNLOAD_", allocationSize=1, initialValue=1)
     */
    private $docDownloadId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="DOC_GROUP_DOWNLOAD_ID", type="integer", nullable=true)
     */
    private $docGroupDownloadId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DOC_DOWNLOAD_NAME_TH", type="string", length=200, nullable=true)
     */
    private $docDownloadNameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DOC_DOWNLOAD_NAME_EN", type="string", length=200, nullable=true)
     */
    private $docDownloadNameEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=200, nullable=true)
     */
    private $remark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FILE_EXTENSION", type="string", length=20, nullable=true)
     */
    private $fileExtension;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="SEQ_ID", type="integer", nullable=true)
     */
    private $seqId;

    /**
     * Set docGroupDownloadId.
     *
     * @param int|null $docDownloadId
     *
     * @return ItsDocDownload
     */
    public function setDocDownloadId($docDownloadId = null)
    {
        $this->docDownloadId = $docDownloadId;

        return $this;
    }

    /**
     * Get docDownloadId.
     *
     * @return int
     */
    public function getDocDownloadId()
    {
        return $this->docDownloadId;
    }

    /**
     * Set docGroupDownloadId.
     *
     * @param int|null $docGroupDownloadId
     *
     * @return ItsDocDownload
     */
    public function setDocGroupDownloadId($docGroupDownloadId = null)
    {
        $this->docGroupDownloadId = $docGroupDownloadId;

        return $this;
    }

    /**
     * Get docGroupDownloadId.
     *
     * @return int|null
     */
    public function getDocGroupDownloadId()
    {
        return $this->docGroupDownloadId;
    }

    /**
     * Set docDownloadNameTh.
     *
     * @param string|null $docDownloadNameTh
     *
     * @return ItsDocDownload
     */
    public function setDocDownloadNameTh($docDownloadNameTh = null)
    {
        $this->docDownloadNameTh = $docDownloadNameTh;

        return $this;
    }

    /**
     * Get docDownloadNameTh.
     *
     * @return string|null
     */
    public function getDocDownloadNameTh()
    {
        return $this->docDownloadNameTh;
    }

    /**
     * Set docDownloadNameEn.
     *
     * @param string|null $docDownloadNameEn
     *
     * @return ItsDocDownload
     */
    public function setDocDownloadNameEn($docDownloadNameEn = null)
    {
        $this->docDownloadNameEn = $docDownloadNameEn;

        return $this;
    }

    /**
     * Get docDownloadNameEn.
     *
     * @return string|null
     */
    public function getDocDownloadNameEn()
    {
        return $this->docDownloadNameEn;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsDocDownload
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsDocDownload
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set fileExtension.
     *
     * @param string|null $fileExtension
     *
     * @return ItsDocDownload
     */
    public function setFileExtension($fileExtension = null)
    {
        $this->fileExtension = $fileExtension;

        return $this;
    }

    /**
     * Get fileExtension.
     *
     * @return string|null
     */
    public function getFileExtension()
    {
        return $this->fileExtension;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsDocDownload
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsDocDownload
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsDocDownload
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsDocDownload
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsDocDownload
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }

    /**
     * Set seqId.
     *
     * @param int|null $seqId
     *
     * @return ItsSchdActivity
     */
    public function setSeqId($seqId = null)
    {
        $this->seqId = $seqId;

        return $this;
    }

    /**
     * Get seqId.
     *
     * @return int|null
     */
    public function getSeqId()
    {
        return $this->seqId;
    }
    
}
