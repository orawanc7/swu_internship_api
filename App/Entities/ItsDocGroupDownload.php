<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsDocGroupDownload
 *
 * @ORM\Table(name="ITS_DOC_GROUP_DOWNLOAD")
 * @ORM\Entity
 */
class ItsDocGroupDownload
{
    /**
     * @var int
     *
     * @ORM\Column(name="DOC_GROUP_DOWNLOAD_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\SequenceGenerator(sequenceName="ITS_DOC_GROUP_DOWNLOAD_DOC_GRO", allocationSize=1, initialValue=1)
     */
    private $docGroupDownloadId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DOC_GROUP_DOWNLOAD_NAME_TH", type="string", length=200, nullable=true)
     */
    private $docGroupDownloadNameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DOC_GROUP_DOWNLOAD_NAME_EN", type="string", length=200, nullable=true)
     */
    private $docGroupDownloadNameEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="SEQ_ID", type="integer", nullable=true)
     */
    private $seqId;

    /**
     * Set docGroupDownloadId.
     *
     * @param string|null $docGroupDownloadId
     *
     * @return ItsDocGroupDownload
     */
    public function setDocGroupDownloadId($docGroupDownloadId = null)
    {
        $this->docGroupDownloadId = $docGroupDownloadId;

        return $this;
    }


    /**
     * Get docGroupDownloadId.
     *
     * @return int
     */
    public function getDocGroupDownloadId()
    {
        return $this->docGroupDownloadId;
    }

    /**
     * Set docGroupDownloadNameTh.
     *
     * @param string|null $docGroupDownloadNameTh
     *
     * @return ItsDocGroupDownload
     */
    public function setDocGroupDownloadNameTh($docGroupDownloadNameTh = null)
    {
        $this->docGroupDownloadNameTh = $docGroupDownloadNameTh;

        return $this;
    }

    /**
     * Get docGroupDownloadNameTh.
     *
     * @return string|null
     */
    public function getDocGroupDownloadNameTh()
    {
        return $this->docGroupDownloadNameTh;
    }

    /**
     * Set docGroupDownloadNameEn.
     *
     * @param string|null $docGroupDownloadNameEn
     *
     * @return ItsDocGroupDownload
     */
    public function setDocGroupDownloadNameEn($docGroupDownloadNameEn = null)
    {
        $this->docGroupDownloadNameEn = $docGroupDownloadNameEn;

        return $this;
    }

    /**
     * Get docGroupDownloadNameEn.
     *
     * @return string|null
     */
    public function getDocGroupDownloadNameEn()
    {
        return $this->docGroupDownloadNameEn;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsDocGroupDownload
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsDocGroupDownload
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsDocGroupDownload
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsDocGroupDownload
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsDocGroupDownload
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsDocGroupDownload
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }

    /**
     * Set seqId.
     *
     * @param int|null $seqId
     *
     * @return ItsSchdActivity
     */
    public function setSeqId($seqId = null)
    {
        $this->seqId = $seqId;

        return $this;
    }

    /**
     * Get seqId.
     *
     * @return int|null
     */
    public function getSeqId()
    {
        return $this->seqId;
    }
}
