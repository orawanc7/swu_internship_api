<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks as HasLifecycleCallbacks;

/**
 * ItsFrmHdr
 *
 * @ORM\Table(name="ITS_FRM_HDR")
 * @ORM\Entity
 * @HasLifecycleCallbacks 
 */
class ItsFrmHdr extends BaseEntity
{
    const _keyCd = 'FRM_HDR';

    /**
     * @var int
     *
     * @ORM\Column(name="FRM_HDR_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Classes\KeyGenerator")
     */
    private $frmHdrId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=true)
     */
    private $docId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FRM_NAME_TH", type="string", length=4000, nullable=true)
     */
    private $frmNameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FRM_NAME_EN", type="string", length=4000, nullable=true)
     */
    private $frmNameEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=400, nullable=true)
     */
    private $remark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FRM_TEMPLATE", type="text", nullable=true)
     */
    private $frmTemplate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;



    /**
     * Get frmHdrId.
     *
     * @return int
     */
    public function getFrmHdrId()
    {
        return $this->frmHdrId;
    }

    /**
     * Set docId.
     *
     * @param int|null $docId
     *
     * @return ItsFrmHdr
     */
    public function setDocId($docId = null)
    {
        $this->docId = $docId;

        return $this;
    }

    /**
     * Get docId.
     *
     * @return int|null
     */
    public function getDocId()
    {
        return $this->docId;
    }

    /**
     * Set frmNameTh.
     *
     * @param string|null $frmNameTh
     *
     * @return ItsFrmHdr
     */
    public function setFrmNameTh($frmNameTh = null)
    {
        $this->frmNameTh = $frmNameTh;

        return $this;
    }

    /**
     * Get frmNameTh.
     *
     * @return string|null
     */
    public function getFrmNameTh()
    {
        return $this->frmNameTh;
    }

    /**
     * Set frmNameEn.
     *
     * @param string|null $frmNameEn
     *
     * @return ItsFrmHdr
     */
    public function setFrmNameEn($frmNameEn = null)
    {
        $this->frmNameEn = $frmNameEn;

        return $this;
    }

    /**
     * Get frmNameEn.
     *
     * @return string|null
     */
    public function getFrmNameEn()
    {
        return $this->frmNameEn;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsFrmHdr
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set frmTemplate.
     *
     * @param string|null $frmTemplate
     *
     * @return ItsFrmHdr
     */
    public function setFrmTemplate($frmTemplate = null)
    {
        $this->frmTemplate = $frmTemplate;

        return $this;
    }

    /**
     * Get frmTemplate.
     *
     * @return string|null
     */
    public function getFrmTemplate()
    {
        return $this->frmTemplate;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsFrmHdr
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsFrmHdr
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsFrmHdr
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsFrmHdr
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsFrmHdr
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsFrmHdr
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }

    public function getKeyCd()
    {
        return self::_keyCd;
    }

    /**
     * Get keyGen.
     *
     * @return string|null
     */
    public function getKeyGen()
    {
        return "";
    }
}
