<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsKeyConfig
 *
 * @ORM\Table(name="ITS_KEY_CONFIG")
 * @ORM\Entity
 */
class ItsKeyConfig
{
    /**
     * @var string
     *
     * @ORM\Column(name="KEY_CD", type="string", length=20, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")    
     */
    private $keyCd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="RUNNING_NUMBER", type="integer", nullable=true)
     */
    private $runningNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="KEY_GEN_FLAG", type="string", length=1, nullable=true, options={"comment"="N=Running Only, Y=Include KEY GEN"})
     */
    private $keyGenFlag;

    /**
     * Set keyCd.
     *     
     * @param string|null $keyCd
     *
     * @return ItsKeyConfig
     */
    public function setKeyCd($keyCd = null)
    {
        $this->keyCd = $keyCd;

        return $this;
    }

    /**
     * Get keyCd.
     *
     * @return string
     */
    public function getKeyCd()
    {
        return $this->keyCd;
    }

    /**
     * Set runningNumber.
     *
     * @param int|null $runningNumber
     *
     * @return ItsKeyConfig
     */
    public function setRunningNumber($runningNumber = null)
    {
        $this->runningNumber = $runningNumber;

        return $this;
    }

    /**
     * Get runningNumber.
     *
     * @return int|null
     */
    public function getRunningNumber()
    {
        return $this->runningNumber;
    }

    /**
     * Set keyGenFlag.
     *
     * @param string|null $keyGenFlag
     *
     * @return ItsKeyConfig
     */
    public function setKeyGenFlag($keyGenFlag = null)
    {
        $this->keyGenFlag = $keyGenFlag;

        return $this;
    }

    /**
     * Get keyGenFlag.
     *
     * @return string|null
     */
    public function getKeyGenFlag()
    {
        return $this->keyGenFlag;
    }
}
