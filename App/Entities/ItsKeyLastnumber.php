<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsKeyLastnumber
 *
 * @ORM\Table(name="ITS_KEY_LASTNUMBER", indexes={@ORM\Index(name="IDX_554B30FE3F4E3CDF", columns={"KEY_CD"})})
 * @ORM\Entity
 */
class ItsKeyLastnumber
{
    /**
     * @var string
     *
     * @ORM\Column(name="KEY_GEN", type="string", length=20, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $keyGen;

    /**
     * @var int|null
     *
     * @ORM\Column(name="LAST_NO", type="integer", nullable=true)
     */
    private $lastNo;

    /**
     * @var \App\Entities\ItsKeyConfig
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="App\Entities\ItsKeyConfig")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="KEY_CD", referencedColumnName="KEY_CD")
     * })
     */
    private $keyCd;



    /**
     * Set keyGen.
     *
     * @param string $keyGen
     *
     * @return ItsKeyLastnumber
     */
    public function setKeyGen($keyGen)
    {
        $this->keyGen = $keyGen;

        return $this;
    }

    /**
     * Get keyGen.
     *
     * @return string
     */
    public function getKeyGen()
    {
        return $this->keyGen;
    }

    /**
     * Set lastNo.
     *
     * @param int|null $lastNo
     *
     * @return ItsKeyLastnumber
     */
    public function setLastNo($lastNo = null)
    {
        $this->lastNo = $lastNo;

        return $this;
    }

    /**
     * Get lastNo.
     *
     * @return int|null
     */
    public function getLastNo()
    {
        return $this->lastNo;
    }

    /**
     * Set keyCd.
     *
     * @param \App\Entities\ItsKeyConfig $keyCd
     *
     * @return ItsKeyLastnumber
     */
    public function setKeyCd(\App\Entities\ItsKeyConfig $keyCd)
    {
        $this->keyCd = $keyCd;

        return $this;
    }

    /**
     * Get keyCd.
     *
     * @return \App\Entities\ItsKeyConfig
     */
    public function getKeyCd()
    {
        return $this->keyCd;
    }
}
