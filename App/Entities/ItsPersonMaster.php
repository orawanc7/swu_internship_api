<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks as HasLifecycleCallbacks;

/**
 * ItsPersonMaster
 *
 * @ORM\Table(name="ITS_PERSON_MASTER", indexes={@ORM\Index(name="its_person_master_index1", columns={"PERSON_ID"})})
 * @ORM\Entity
 * @HasLifecycleCallbacks
 */
class ItsPersonMaster extends BaseEntity
{
    const _keyCd = 'PERSON_MASTER';

    /**
     * @var int
     *
     * @ORM\Column(name="ITS_PERSON_ID", type="integer", nullable=false, options={"comment"="รหัสบุคคลภายในระบบ ITS"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Classes\KeyGenerator")
     */
    private $itsPersonId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PERSON_ID", type="integer", nullable=true, options={"comment"="รหัสประจำตัวบุคคล"})
     */
    private $personId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="EXT_PERSON_FLAG", type="string", length=1, nullable=true, options={"comment"="Y : เป็นบุคคลภายนอกมศว N:เป็นบุคล มศว"})
     */
    private $extPersonFlag;

    /**
     * @var int|null
     *
     * @ORM\Column(name="DEPT_CD", type="integer", nullable=true, options={"comment"="รหัสหน่วยงานที่สังกัด มศว, โรงเรียน"})
     */
    private $deptCd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PRENAME_EXT_EN_CD", type="integer", nullable=true, options={"comment"="รหัสคำนำหน้าชื่อ ภาษาอังกฤษ"})
     */
    private $prenameExtEnCd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PRENAME_INT_EN_CD", type="integer", nullable=true, options={"comment"="รหัสคำนำหน้าชื่อ ภาษาอังกฤษ ที่ใช้ภายใน"})
     */
    private $prenameIntEnCd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PRENAME_EXT_TH_CD", type="integer", nullable=true, options={"comment"="รหัสคำนำหน้าชื่อ ภาษาไทย"})
     */
    private $prenameExtThCd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PRENAME_INT_TH_CD", type="integer", nullable=true, options={"comment"="รหัสคำนำหน้าชื่อ ภาษาไทย ที่ใช้ภายใน"})
     */
    private $prenameIntThCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_FNAME_TH", type="string", length=50, nullable=true, options={"comment"="ชื่อภาษาไทย"})
     */
    private $personFnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_MNAME_TH", type="string", length=50, nullable=true, options={"comment"="ชื่อกลางภาษาไทย"})
     */
    private $personMnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_LNAME_TH", type="string", length=50, nullable=true, options={"comment"="นามสกุลภาษาไทย"})
     */
    private $personLnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_FNAME_EN", type="string", length=50, nullable=true, options={"comment"="ชื่อภาษาอังกฤษ"})
     */
    private $personFnameEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_LNAME_EN", type="string", length=50, nullable=true, options={"comment"="ชื่อกลางภาษาอังกฤษ"})
     */
    private $personLnameEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_MNAME_EN", type="string", length=50, nullable=true, options={"comment"="นามสกุลภาษาอังกฤษ"})
     */
    private $personMnameEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="POSITION_NAME_TH", type="string", length=80, nullable=true, options={"comment"="ชื่อตำแหน่งภาษาไทย"})
     */
    private $positionNameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="POSITION_NAME_EN", type="string", length=80, nullable=true, options={"comment"="ชื่อตำแหน่งภาษาอังกฤษ"})
     */
    private $positionNameEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="POSITION_LINE_CD", type="string", length=3, nullable=true, options={"comment"="รหัสสายงาน 800 :สายวิชาการ"})
     */
    private $positionLineCd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ID_CARD_NO", type="bigint", nullable=true, options={"comment"="เลขประจำตัวประชาชน"})
     */
    private $idCardNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PASSPORT_NO", type="string", length=20, nullable=true, options={"comment"="เลขที่หนังสือเดินทาง"})
     */
    private $passportNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="GENDER_FLAG", type="string", length=1, nullable=true, options={"comment"="เพศ M:ชาย,F:หญิง"})
     */
    private $genderFlag;

    /**
     * @var string|null
     *
     * @ORM\Column(name="BUASRI_ID", type="string", length=15, nullable=true, options={"comment"="บัวศรีไอดี"})
     */
    private $buasriId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="GAFE_EMAIL", type="string", length=50, nullable=true, options={"comment"="อีเมล GAFE"})
     */
    private $gafeEmail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="OTHER_EMAIL", type="string", length=50, nullable=true, options={"comment"="email ที่ติดต่อได้"})
     */
    private $otherEmail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ADDRESS_DESC", type="string", length=100, nullable=true, options={"comment"="รายละเอียดที่อยู่"})
     */
    private $addressDesc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TELEPHONE_NO", type="string", length=50, nullable=true, options={"comment"="โทรศัพท์ที่ทำงาน"})
     */
    private $telephoneNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MOBILE_NO", type="string", length=50, nullable=true, options={"comment"="มือถือส่วนตัว"})
     */
    private $mobileNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FAX_NO", type="string", length=50, nullable=true, options={"comment"="โทรสาร"})
     */
    private $faxNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="EXTERNAL_PROVIDER_ID", type="string", length=4000, nullable=true, options={"comment"="EXTERNAL PROVIDER ID"})
     */
    private $externalProviderId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PROVIDER_ID", type="integer", nullable=true, options={"comment"="PROVIDER API"})
     */
    private $providerId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PICTURE_PROFILE", type="string", length=500, nullable=true, options={"comment"="รูป"})
     */
    private $pictureProfile;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TRANSFER_FLAG", type="string", length=1, nullable=true, options={"comment"="สถานะการย้ายข้อมูล"})
     */
    private $transferFlag;

    /**
     * @var string|null
     *
     * @ORM\Column(name="EXPERIENCE_SETUP", type="decimal", precision=5, scale=2, nullable=true, options={"comment"="จำนวนปีของประสบการณ์ตั้งต้น"})
     */
    private $experienceSetup;

    /**
     * @var string|null
     *
     * @ORM\Column(name="EXPERIENCE_CAL", type="decimal", precision=5, scale=2, nullable=true, options={"comment"="จำนวนปีของประสบการณ์ที่คำนวณจากระบบ"})
     */
    private $experienceCal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=200, nullable=true, options={"comment"="หมายเหตุ"})
     */
    private $remark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true, options={"comment"="สถานะการใช้งาน"})
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true, options={"comment"="วันที่เพิ่มข้อมูล"})
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true, options={"comment"="ผู้เพิ่มข้อมูล"})
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true, options={"comment"="วันที่แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true, options={"comment"="ผู้แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true, options={"comment"="ชื่อโปรแกรมที่ทำการแก้ไข"})
     */
    private $programCd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="THEME_ID", type="integer", nullable=true)
     */
    private $themeId;

    private $keyGen;

    /**
     * Set itsPersonId.
     *
     * @param int|null $itsPersonId
     *
     * @return ItsPersonMaster
     */
    public function setItsPersonId($itsPersonId = null)
    {
        $this->itsPersonId = $itsPersonId;

        return $this;
    }

    /**
     * Get itsPersonId.
     *
     * @return int
     */
    public function getItsPersonId()
    {
        return $this->itsPersonId;
    }

    /**
     * Set personId.
     *
     * @param int|null $personId
     *
     * @return ItsPersonMaster
     */
    public function setPersonId($personId = null)
    {
        $this->personId = $personId;

        return $this;
    }

    /**
     * Get personId.
     *
     * @return int|null
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * Set extPersonFlag.
     *
     * @param string|null $extPersonFlag
     *
     * @return ItsPersonMaster
     */
    public function setExtPersonFlag($extPersonFlag = null)
    {
        $this->extPersonFlag = $extPersonFlag;

        return $this;
    }

    /**
     * Get extPersonFlag.
     *
     * @return string|null
     */
    public function getExtPersonFlag()
    {
        return $this->extPersonFlag;
    }

    /**
     * Set deptCd.
     *
     * @param int|null $deptCd
     *
     * @return ItsPersonMaster
     */
    public function setDeptCd($deptCd = null)
    {
        $this->deptCd = $deptCd;

        return $this;
    }

    /**
     * Get deptCd.
     *
     * @return int|null
     */
    public function getDeptCd()
    {
        return $this->deptCd;
    }

    /**
     * Set prenameExtEnCd.
     *
     * @param int|null $prenameExtEnCd
     *
     * @return ItsPersonMaster
     */
    public function setPrenameExtEnCd($prenameExtEnCd = null)
    {
        $this->prenameExtEnCd = $prenameExtEnCd;

        return $this;
    }

    /**
     * Get prenameExtEnCd.
     *
     * @return int|null
     */
    public function getPrenameExtEnCd()
    {
        return $this->prenameExtEnCd;
    }

    /**
     * Set prenameIntEnCd.
     *
     * @param int|null $prenameIntEnCd
     *
     * @return ItsPersonMaster
     */
    public function setPrenameIntEnCd($prenameIntEnCd = null)
    {
        $this->prenameIntEnCd = $prenameIntEnCd;

        return $this;
    }

    /**
     * Get prenameIntEnCd.
     *
     * @return int|null
     */
    public function getPrenameIntEnCd()
    {
        return $this->prenameIntEnCd;
    }

    /**
     * Set prenameExtThCd.
     *
     * @param int|null $prenameExtThCd
     *
     * @return ItsPersonMaster
     */
    public function setPrenameExtThCd($prenameExtThCd = null)
    {
        $this->prenameExtThCd = $prenameExtThCd;

        return $this;
    }

    /**
     * Get prenameExtThCd.
     *
     * @return int|null
     */
    public function getPrenameExtThCd()
    {
        return $this->prenameExtThCd;
    }

    /**
     * Set prenameIntThCd.
     *
     * @param int|null $prenameIntThCd
     *
     * @return ItsPersonMaster
     */
    public function setPrenameIntThCd($prenameIntThCd = null)
    {
        $this->prenameIntThCd = $prenameIntThCd;

        return $this;
    }

    /**
     * Get prenameIntThCd.
     *
     * @return int|null
     */
    public function getPrenameIntThCd()
    {
        return $this->prenameIntThCd;
    }

    /**
     * Set personFnameTh.
     *
     * @param string|null $personFnameTh
     *
     * @return ItsPersonMaster
     */
    public function setPersonFnameTh($personFnameTh = null)
    {
        $this->personFnameTh = $personFnameTh;

        return $this;
    }

    /**
     * Get personFnameTh.
     *
     * @return string|null
     */
    public function getPersonFnameTh()
    {
        return $this->personFnameTh;
    }

    /**
     * Set personMnameTh.
     *
     * @param string|null $personMnameTh
     *
     * @return ItsPersonMaster
     */
    public function setPersonMnameTh($personMnameTh = null)
    {
        $this->personMnameTh = $personMnameTh;

        return $this;
    }

    /**
     * Get personMnameTh.
     *
     * @return string|null
     */
    public function getPersonMnameTh()
    {
        return $this->personMnameTh;
    }

    /**
     * Set personLnameTh.
     *
     * @param string|null $personLnameTh
     *
     * @return ItsPersonMaster
     */
    public function setPersonLnameTh($personLnameTh = null)
    {
        $this->personLnameTh = $personLnameTh;

        return $this;
    }

    /**
     * Get personLnameTh.
     *
     * @return string|null
     */
    public function getPersonLnameTh()
    {
        return $this->personLnameTh;
    }

    /**
     * Set personFnameEn.
     *
     * @param string|null $personFnameEn
     *
     * @return ItsPersonMaster
     */
    public function setPersonFnameEn($personFnameEn = null)
    {
        $this->personFnameEn = $personFnameEn;

        return $this;
    }

    /**
     * Get personFnameEn.
     *
     * @return string|null
     */
    public function getPersonFnameEn()
    {
        return $this->personFnameEn;
    }

    /**
     * Set personLnameEn.
     *
     * @param string|null $personLnameEn
     *
     * @return ItsPersonMaster
     */
    public function setPersonLnameEn($personLnameEn = null)
    {
        $this->personLnameEn = $personLnameEn;

        return $this;
    }

    /**
     * Get personLnameEn.
     *
     * @return string|null
     */
    public function getPersonLnameEn()
    {
        return $this->personLnameEn;
    }

    /**
     * Set personMnameEn.
     *
     * @param string|null $personMnameEn
     *
     * @return ItsPersonMaster
     */
    public function setPersonMnameEn($personMnameEn = null)
    {
        $this->personMnameEn = $personMnameEn;

        return $this;
    }

    /**
     * Get personMnameEn.
     *
     * @return string|null
     */
    public function getPersonMnameEn()
    {
        return $this->personMnameEn;
    }

    /**
     * Set positionNameTh.
     *
     * @param string|null $positionNameTh
     *
     * @return ItsPersonMaster
     */
    public function setPositionNameTh($positionNameTh = null)
    {
        $this->positionNameTh = $positionNameTh;

        return $this;
    }

    /**
     * Get positionNameTh.
     *
     * @return string|null
     */
    public function getPositionNameTh()
    {
        return $this->positionNameTh;
    }

    /**
     * Set positionNameEn.
     *
     * @param string|null $positionNameEn
     *
     * @return ItsPersonMaster
     */
    public function setPositionNameEn($positionNameEn = null)
    {
        $this->positionNameEn = $positionNameEn;

        return $this;
    }

    /**
     * Get positionNameEn.
     *
     * @return string|null
     */
    public function getPositionNameEn()
    {
        return $this->positionNameEn;
    }

    /**
     * Set positionLineCd.
     *
     * @param string|null $positionLineCd
     *
     * @return ItsPersonMaster
     */
    public function setPositionLineCd($positionLineCd = null)
    {
        $this->positionLineCd = $positionLineCd;

        return $this;
    }

    /**
     * Get positionLineCd.
     *
     * @return string|null
     */
    public function getPositionLineCd()
    {
        return $this->positionLineCd;
    }

    /**
     * Set idCardNo.
     *
     * @param int|null $idCardNo
     *
     * @return ItsPersonMaster
     */
    public function setIdCardNo($idCardNo = null)
    {
        $this->idCardNo = $idCardNo;

        return $this;
    }

    /**
     * Get idCardNo.
     *
     * @return int|null
     */
    public function getIdCardNo()
    {
        return $this->idCardNo;
    }

    /**
     * Set passportNo.
     *
     * @param string|null $passportNo
     *
     * @return ItsPersonMaster
     */
    public function setPassportNo($passportNo = null)
    {
        $this->passportNo = $passportNo;

        return $this;
    }

    /**
     * Get passportNo.
     *
     * @return string|null
     */
    public function getPassportNo()
    {
        return $this->passportNo;
    }

    /**
     * Set genderFlag.
     *
     * @param string|null $genderFlag
     *
     * @return ItsPersonMaster
     */
    public function setGenderFlag($genderFlag = null)
    {
        $this->genderFlag = $genderFlag;

        return $this;
    }

    /**
     * Get genderFlag.
     *
     * @return string|null
     */
    public function getGenderFlag()
    {
        return $this->genderFlag;
    }

    /**
     * Set buasriId.
     *
     * @param string|null $buasriId
     *
     * @return ItsPersonMaster
     */
    public function setBuasriId($buasriId = null)
    {
        $this->buasriId = $buasriId;

        return $this;
    }

    /**
     * Get buasriId.
     *
     * @return string|null
     */
    public function getBuasriId()
    {
        return $this->buasriId;
    }

    /**
     * Set gafeEmail.
     *
     * @param string|null $gafeEmail
     *
     * @return ItsPersonMaster
     */
    public function setGafeEmail($gafeEmail = null)
    {
        $this->gafeEmail = $gafeEmail;

        return $this;
    }

    /**
     * Get gafeEmail.
     *
     * @return string|null
     */
    public function getGafeEmail()
    {
        return $this->gafeEmail;
    }

    /**
     * Set otherEmail.
     *
     * @param string|null $otherEmail
     *
     * @return ItsPersonMaster
     */
    public function setOtherEmail($otherEmail = null)
    {
        $this->otherEmail = $otherEmail;

        return $this;
    }

    /**
     * Get otherEmail.
     *
     * @return string|null
     */
    public function getOtherEmail()
    {
        return $this->otherEmail;
    }

    /**
     * Set addressDesc.
     *
     * @param string|null $addressDesc
     *
     * @return ItsPersonMaster
     */
    public function setAddressDesc($addressDesc = null)
    {
        $this->addressDesc = $addressDesc;

        return $this;
    }

    /**
     * Get addressDesc.
     *
     * @return string|null
     */
    public function getAddressDesc()
    {
        return $this->addressDesc;
    }

    /**
     * Set telephoneNo.
     *
     * @param string|null $telephoneNo
     *
     * @return ItsPersonMaster
     */
    public function setTelephoneNo($telephoneNo = null)
    {
        $this->telephoneNo = $telephoneNo;

        return $this;
    }

    /**
     * Get telephoneNo.
     *
     * @return string|null
     */
    public function getTelephoneNo()
    {
        return $this->telephoneNo;
    }

    /**
     * Set mobileNo.
     *
     * @param string|null $mobileNo
     *
     * @return ItsPersonMaster
     */
    public function setMobileNo($mobileNo = null)
    {
        $this->mobileNo = $mobileNo;

        return $this;
    }

    /**
     * Get mobileNo.
     *
     * @return string|null
     */
    public function getMobileNo()
    {
        return $this->mobileNo;
    }

    /**
     * Set faxNo.
     *
     * @param string|null $faxNo
     *
     * @return ItsPersonMaster
     */
    public function setFaxNo($faxNo = null)
    {
        $this->faxNo = $faxNo;

        return $this;
    }

    /**
     * Get faxNo.
     *
     * @return string|null
     */
    public function getFaxNo()
    {
        return $this->faxNo;
    }

    /**
     * Set externalProviderId.
     *
     * @param string|null $externalProviderId
     *
     * @return ItsPersonMaster
     */
    public function setExternalProviderId($externalProviderId = null)
    {
        $this->externalProviderId = $externalProviderId;

        return $this;
    }

    /**
     * Get externalProviderId.
     *
     * @return string|null
     */
    public function getExternalProviderId()
    {
        return $this->externalProviderId;
    }

    /**
     * Set providerId.
     *
     * @param int|null $providerId
     *
     * @return ItsPersonMaster
     */
    public function setProviderId($providerId = null)
    {
        $this->providerId = $providerId;

        return $this;
    }

    /**
     * Get providerId.
     *
     * @return int|null
     */
    public function getProviderId()
    {
        return $this->providerId;
    }

    /**
     * Set pictureProfile.
     *
     * @param string|null $pictureProfile
     *
     * @return ItsPersonMaster
     */
    public function setPictureProfile($pictureProfile = null)
    {
        $this->pictureProfile = $pictureProfile;

        return $this;
    }

    /**
     * Get pictureProfile.
     *
     * @return string|null
     */
    public function getPictureProfile()
    {
        return $this->pictureProfile;
    }

    /**
     * Set transferFlag.
     *
     * @param string|null $transferFlag
     *
     * @return ItsPersonMaster
     */
    public function setTransferFlag($transferFlag = null)
    {
        $this->transferFlag = $transferFlag;

        return $this;
    }

    /**
     * Get transferFlag.
     *
     * @return string|null
     */
    public function getTransferFlag()
    {
        return $this->transferFlag;
    }

    /**
     * Set experienceSetup.
     *
     * @param string|null $experienceSetup
     *
     * @return ItsPersonMaster
     */
    public function setExperienceSetup($experienceSetup = null)
    {
        $this->experienceSetup = $experienceSetup;

        return $this;
    }

    /**
     * Get experienceSetup.
     *
     * @return string|null
     */
    public function getExperienceSetup()
    {
        return $this->experienceSetup;
    }

    /**
     * Set experienceCal.
     *
     * @param string|null $experienceCal
     *
     * @return ItsPersonMaster
     */
    public function setExperienceCal($experienceCal = null)
    {
        $this->experienceCal = $experienceCal;

        return $this;
    }

    /**
     * Get experienceCal.
     *
     * @return string|null
     */
    public function getExperienceCal()
    {
        return $this->experienceCal;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsPersonMaster
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsPersonMaster
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsPersonMaster
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsPersonMaster
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsPersonMaster
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsPersonMaster
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsPersonMaster
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }

    /**
     * Set themeId.
     *
     * @param int|null $themeId
     *
     * @return ItsPersonMaster
     */
    public function setThemeId($themeId = null)
    {
        $this->themeId = $themeId;

        return $this;
    }

    /**
     * Get themeId.
     *
     * @return int|null
     */
    public function getThemeId()
    {
        return $this->themeId;
    }

    /**
     * Get keyCd.
     *
     * @return string|null
     */
    public function getKeyCd()
    {
        return self::_keyCd;
    }

    /**
     * Get keyGen.
     *
     * @return string|null
     */
    public function getKeyGen()
    {
        return $this->keyGen;
    }

    public function setKeyGen($keyGen = null)
    {        
        $this->keyGen = $keyGen;

        return $this;
    }
}
