<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks as HasLifecycleCallbacks;

/**
 * ItsPersonType
 *
 * @ORM\Table(name="ITS_PERSON_TYPE")
 * @ORM\Entity
 * @HasLifecycleCallbacks
 */
class ItsPersonType extends BaseEntity
{
    const _keyCd = 'PERSON_TYPE';

    /**
     * @var int
     *
     * @ORM\Column(name="ITS_PERSON_TYPE_ID", type="integer", nullable=false, options={"comment"="รหัสบุคคลตามประเภทบุคคล"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Classes\KeyGenerator")
     */
    private $itsPersonTypeId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ITS_PERSON_ID", type="integer", nullable=true, options={"comment"="รหัสบุคคลภายในระบบ ITS"})
     */
    private $itsPersonId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PERSON_TYPE_ID", type="integer", nullable=true, options={"comment"="ประเภทบุคคล"})
     */
    private $personTypeId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=200, nullable=true, options={"comment"="หมายเหตุ"})
     */
    private $remark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true, options={"comment"="สถานะการใช้งาน"})
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true, options={"comment"="วันที่เพิ่มข้อมูล"})
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true, options={"comment"="ผู้เพิ่มข้อมูล"})
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true, options={"comment"="วันที่แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true, options={"comment"="ผู้แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true, options={"comment"="ชื่อโปรแกรมที่ทำการแก้ไข"})
     */
    private $programCd;


    private $keyGen;

    /**
     * Get itsPersonTypeId.
     *
     * @return int
     */
    public function getItsPersonTypeId()
    {
        return $this->itsPersonTypeId;
    }

    /**
     * Set itsPersonId.
     *
     * @param int|null $itsPersonId
     *
     * @return ItsPersonType
     */
    public function setItsPersonId($itsPersonId = null)
    {
        $this->itsPersonId = $itsPersonId;

        return $this;
    }

    /**
     * Get itsPersonId.
     *
     * @return int|null
     */
    public function getItsPersonId()
    {
        return $this->itsPersonId;
    }

    /**
     * Set personTypeId.
     *
     * @param int|null $personTypeId
     *
     * @return ItsPersonType
     */
    public function setPersonTypeId($personTypeId = null)
    {
        $this->personTypeId = $personTypeId;

        return $this;
    }

    /**
     * Get personTypeId.
     *
     * @return int|null
     */
    public function getPersonTypeId()
    {
        return $this->personTypeId;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsPersonType
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsPersonType
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsPersonType
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsPersonType
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsPersonType
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsPersonType
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsPersonType
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }

    /**
     * Get keyCd.
     *
     * @return string|null
     */
    public function getKeyCd()
    {
        return self::_keyCd;
    }

    /**
     * Get keyGen.
     *
     * @return string|null
     */
    public function getKeyGen()
    {
        return $this->keyGen;
    }

    public function setKeyGen($keyGen = null)
    {
        $this->keyGen = $keyGen;

        return $this;
    }
}
