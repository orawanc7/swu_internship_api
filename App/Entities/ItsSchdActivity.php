<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks as HasLifecycleCallbacks;

/**
 * ItsSchdActivity
 *
 * @ORM\Table(name="ITS_SCHD_ACTIVITY")
 * @ORM\Entity
 *  @HasLifecycleCallbacks 
 */
class ItsSchdActivity extends BaseEntity
{
    const _keyCd = 'SCHD_ACTIVITY';

    /**
     * @var int
     *
     * @ORM\Column(name="ACTIVITY_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Classes\KeyGenerator")     
     */
    private $activityId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ROUND_ID", type="smallint", nullable=true)
     */
    private $roundId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVITY_SET_NO", type="string", length=10, nullable=true)
     */
    private $activitySetNo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ACTIVITY_SEQ_ID", type="integer", nullable=true)
     */
    private $activitySeqId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVITY_NAME_TH", type="string", length=500, nullable=true)
     */
    private $activityNameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVITY_NAME_EN", type="string", length=500, nullable=true)
     */
    private $activityNameEn;

    /**
     * @var int|null
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=true)
     */
    private $docId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="REF_ACTIVITY_ID", type="integer", nullable=true)
     */
    private $refActivityId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="START_DATE", type="date", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="END_DATE", type="date", nullable=true)
     */
    private $endDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVITY_FLAG", type="string", length=1, nullable=true)
     */
    private $activityFlag;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=200, nullable=true)
     */
    private $remark;



    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;


    
    /**
     * @var int|null
     *
     * @ORM\Column(name="PERSON_TYPE_ID", type="integer", nullable=true)
     */
    private $personTypeId;

    

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="datetime", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="datetime", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;



    /**
     * Get activityId.
     *
     * @return int
     */
    public function getActivityId()
    {
        return $this->activityId;
    }

    /**
     * Set roundId.
     *
     * @param int|null $roundId
     *
     * @return ItsSchdActivity
     */
    public function setRoundId($roundId = null)
    {
        $this->roundId = $roundId;

        return $this;
    }

    /**
     * Get roundId.
     *
     * @return int|null
     */
    public function getRoundId()
    {
        return $this->roundId;
    }

    /**
     * Set activitySetNo.
     *
     * @param string|null $activitySetNo
     *
     * @return ItsSchdActivity
     */
    public function setActivitySeteNo($activitySetNo = null)
    {
        $this->activitySetNo = $activitySetNo;

        return $this;
    }

    /**
     * Get activitySetNo.
     *
     * @return string|null
     */
    public function getActivitySetNo()
    {
        return $this->activitySetNo;
    }

    /**
     * Set activitySeqId.
     *
     * @param int|null $activitySeqId
     *
     * @return ItsSchdActivity
     */
    public function setActivitySeqId($activitySeqId = null)
    {
        $this->activitySeqId = $activitySeqId;

        return $this;
    }

    /**
     * Get activitySeqId.
     *
     * @return int|null
     */
    public function getActivitySeqId()
    {
        return $this->activitySeqId;
    }

    /**
     * Set activityNameTh.
     *
     * @param string|null $activityNameTh
     *
     * @return ItsSchdActivity
     */
    public function setActivityNameTh($activityNameTh = null)
    {
        $this->activityNameTh = $activityNameTh;

        return $this;
    }

    /**
     * Get activityNameTh.
     *
     * @return string|null
     */
    public function getActivityNameTh()
    {
        return $this->activityNameTh;
    }

    /**
     * Set activityNameEn.
     *
     * @param string|null $activityNameEn
     *
     * @return ItsSchdActivity
     */
    public function setActivityNameEn($activityNameEn = null)
    {
        $this->activityNameEn = $activityNameEn;

        return $this;
    }

    /**
     * Get activityNameEn.
     *
     * @return string|null
     */
    public function getActivityNameEn()
    {
        return $this->activityNameEn;
    }

    /**
     * Set docId.
     *
     * @param int|null $docId
     *
     * @return ItsSchdActivity
     */
    public function setDocId($docId = null)
    {
        $this->docId = $docId;

        return $this;
    }

    /**
     * Get docId.
     *
     * @return int|null
     */
    public function getDocId()
    {
        return $this->docId;
    }

    /**
     * Set refActivityId.
     *
     * @param int|null $refActivityId
     *
     * @return ItsSchdActivity
     */
    public function setRefActivityId($refActivityId = null)
    {
        $this->refActivityId = $refActivityId;

        return $this;
    }

    /**
     * Get refActivityId.
     *
     * @return int|null
     */
    public function getRefActivityId()
    {
        return $this->refActivityId;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime|null $startDate
     *
     * @return ItsSchdActivity
     */
    public function setStartDate($startDate = null)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime|null
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime|null $endDate
     *
     * @return ItsSchdActivity
     */
    public function setEndDate($endDate = null)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime|null
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set activityFlag.
     *
     * @param string|null $activityFlag
     *
     * @return ItsSchdActivity
     */
    public function setActivityFlag($activityFlag = null)
    {
        $this->activityFlag = $activityFlag;

        return $this;
    }

    /**
     * Get activityFlag.
     *
     * @return string|null
     */
    public function getActivityFlag()
    {
        return $this->activityFlag;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsSchdActivity
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsSchdActivity
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Get personTypeId.
     *
     * @return int
     */
    public function getPersonTypeId()
    {
        return $this->personTypeId;
    }

    /**
     * Set personTypeId.
     *
     * @param int|null $personTypeId
     *
     * @return ItsSchdActivity
     */
    public function setPersonTypeId($personTypeId = null)
    {
        $this->personTypeId = $personTypeId;

        return $this;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsSchdActivity
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsSchdActivity
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsSchdActivity
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsSchdActivity
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsSchdActivity
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }

    /**
     * Get keyCd.
     *
     * @return string|null
     */
    public function getKeyCd()
    {
        return self::_keyCd;
    }

    /**
     * Get keyGen.
     *
     * @return string|null
     */
    public function getKeyGen()
    {
        return substr($this->roundId, 0, 3) ;
    }
}
