<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks as HasLifecycleCallbacks;

/**
 * ItsSchdAttach
 *
 * @ORM\Table(name="ITS_SCHD_ATTACH", indexes={@ORM\Index(name="its_schd_attach_index1", columns={"ACTIVITY_ID"}), @ORM\Index(name="its_schd_attach_index3", columns={"REF_DOC_ID"}), @ORM\Index(name="its_schd_attach_index2", columns={"DOC_ID"})})
 * @ORM\Entity
 * @HasLifecycleCallbacks 
 */
class ItsSchdAttach extends BaseEntity
{
    const _keyCd = 'SCHD_ATTACH';

    /**
     * @var int
     *
     * @ORM\Column(name="ATTACH_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Classes\KeyGenerator")
     */
    private $attachId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ACTIVITY_ID", type="integer", nullable=true)
     */
    private $activityId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=true)
     */
    private $docId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="REF_DOC_ID", type="integer", nullable=true)
     */
    private $refDocId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ATTACH_TYPE", type="string", length=1, nullable=true)
     */
    private $attachType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LINK_URL", type="string", length=200, nullable=true)
     */
    private $linkUrl;

    /**
     * @var int|null
     *
     * @ORM\Column(name="FILE_ID", type="integer", nullable=true)
     */
    private $fileId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FILE_NAME", type="string", length=300, nullable=true)
     */
    private $fileName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FILE_EXTENSION", type="string", length=20, nullable=true)
     */
    private $fileExtension;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FILE_PATH", type="string", length=100, nullable=true)
     */
    private $filePath;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=200, nullable=true)
     */
    private $remark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;



    /**
     * Get attachId.
     *
     * @return int
     */
    public function getAttachId()
    {
        return $this->attachId;
    }

    /**
     * Set activityId.
     *
     * @param int|null $activityId
     *
     * @return ItsSchdAttach
     */
    public function setActivityId($activityId = null)
    {
        $this->activityId = $activityId;

        return $this;
    }

    /**
     * Get activityId.
     *
     * @return int|null
     */
    public function getActivityId()
    {
        return $this->activityId;
    }

    /**
     * Set docId.
     *
     * @param int|null $docId
     *
     * @return ItsSchdAttach
     */
    public function setDocId($docId = null)
    {
        $this->docId = $docId;

        return $this;
    }

    /**
     * Get docId.
     *
     * @return int|null
     */
    public function getDocId()
    {
        return $this->docId;
    }

    /**
     * Set refDocId.
     *
     * @param int|null $refDocId
     *
     * @return ItsSchdAttach
     */
    public function setRefDocId($refDocId = null)
    {
        $this->refDocId = $refDocId;

        return $this;
    }

    /**
     * Get refDocId.
     *
     * @return int|null
     */
    public function getRefDocId()
    {
        return $this->refDocId;
    }

    /**
     * Set attachType.
     *
     * @param string|null $attachType
     *
     * @return ItsSchdAttach
     */
    public function setAttachType($attachType = null)
    {
        $this->attachType = $attachType;

        return $this;
    }

    /**
     * Get attachType.
     *
     * @return string|null
     */
    public function getAttachType()
    {
        return $this->attachType;
    }

    /**
     * Set linkUrl.
     *
     * @param string|null $linkUrl
     *
     * @return ItsSchdAttach
     */
    public function setLinkUrl($linkUrl = null)
    {
        $this->linkUrl = $linkUrl;

        return $this;
    }

    /**
     * Get linkUrl.
     *
     * @return string|null
     */
    public function getLinkUrl()
    {
        return $this->linkUrl;
    }

    /**
     * Set fileId.
     *
     * @param int|null $fileId
     *
     * @return ItsSchdAttach
     */
    public function setFileId($fileId = null)
    {
        $this->fileId = $fileId;

        return $this;
    }

    /**
     * Get fileId.
     *
     * @return int|null
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * Set fileName.
     *
     * @param string|null $fileName
     *
     * @return ItsSchdAttach
     */
    public function setFileName($fileName = null)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName.
     *
     * @return string|null
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set fileExtension.
     *
     * @param string|null $fileExtension
     *
     * @return ItsSchdAttach
     */
    public function setFileExtension($fileExtension = null)
    {
        $this->fileExtension = $fileExtension;

        return $this;
    }

    /**
     * Get fileExtension.
     *
     * @return string|null
     */
    public function getFileExtension()
    {
        return $this->fileExtension;
    }

    /**
     * Set filePath.
     *
     * @param string|null $filePath
     *
     * @return ItsSchdAttach
     */
    public function setFilePath($filePath = null)
    {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * Get filePath.
     *
     * @return string|null
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsSchdAttach
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsSchdAttach
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsSchdAttach
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsSchdAttach
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsSchdAttach
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsSchdAttach
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsSchdAttach
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }

    /**
     * Get keyCd.
     *
     * @return string|null
     */
    public function getKeyCd()
    {
        return self::_keyCd;
    }

    /**
     * Get keyGen.
     *
     * @return string|null
     */
    public function getKeyGen()
    {
        return substr($this->activityId, 0, 5) ;
    }
}
