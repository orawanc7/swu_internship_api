<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks as HasLifecycleCallbacks;

/**
 * ItsSchdDocStatus
 *
 * @ORM\Table(name="ITS_SCHD_DOC_STATUS", indexes={@ORM\Index(name="its_schd_doc_status_index1", columns={"ACTIVITY_ID"}), @ORM\Index(name="its_schd_doc_status_index2", columns={"DOC_ID"})})
 * @ORM\Entity
 * @HasLifecycleCallbacks 
 */
class ItsSchdDocStatus extends BaseEntity
{
    const _keyCd = 'SCHD_DOC_STATUS';

    /**
     * @var int
     *
     * @ORM\Column(name="DOC_STATUS_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Classes\KeyGenerator")
     */
    private $docStatusId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ACTIVITY_ID", type="integer", nullable=true)
     */
    private $activityId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=true)
     */
    private $docId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="REF_DOC_ID", type="integer", nullable=true)
     */
    private $refDocId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ITS_USER_ID", type="integer", nullable=true)
     */
    private $itsUserId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PERSON_TYPE_ID", type="integer", nullable=true)
     */
    private $personTypeId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="STATUS_FLAG", type="string", length=1, nullable=true)
     */
    private $statusFlag;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=200, nullable=true)
     */
    private $remark;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="datetime", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="datetime", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;

    /**
     * Get docStatusId.
     *
     * @return int
     */
    public function getDocStatusId()
    {
        return $this->docStatusId;
    }

    /**
     * Set activityId.
     *
     * @param int|null $activityId
     *
     * @return ItsSchdDocStatus
     */
    public function setActivityId($activityId = null)
    {
        $this->activityId = $activityId;

        return $this;
    }

    /**
     * Get activityId.
     *
     * @return int|null
     */
    public function getActivityId()
    {
        return $this->activityId;
    }

    /**
     * Set docId.
     *
     * @param int|null $docId
     *
     * @return ItsSchdDocStatus
     */
    public function setDocId($docId = null)
    {
        $this->docId = $docId;

        return $this;
    }

    /**
     * Get docId.
     *
     * @return int|null
     */
    public function getDocId()
    {
        return $this->docId;
    }

    /**
     * Set refDocId.
     *
     * @param int|null $refDocId
     *
     * @return ItsSchdDocStatus
     */
    public function setRefDocId($refDocId = null)
    {
        $this->refDocId = $refDocId;

        return $this;
    }

    /**
     * Get refDocId.
     *
     * @return int|null
     */
    public function getRefDocId()
    {
        return $this->refDocId;
    }

    /**
     * Set itsUserId.
     *
     * @param int|null $itsUserId
     *
     * @return ItsSchdDocStatus
     */
    public function setItsUserId($itsUserId = null)
    {
        $this->itsUserId = $itsUserId;

        return $this;
    }

    /**
     * Get itsUserId.
     *
     * @return int|null
     */
    public function getItsUserId()
    {
        return $this->itsUserId;
    }

    /**
     * Set personTypeId.
     *
     * @param int|null $personTypeId
     *
     * @return ItsSchdDocStatus
     */
    public function setPersonTypeId($personTypeId = null)
    {
        $this->personTypeId = $personTypeId;

        return $this;
    }

    /**
     * Get personTypeId.
     *
     * @return int|null
     */
    public function getPersonTypeId()
    {
        return $this->personTypeId;
    }

    /**
     * Set statusFlag.
     *
     * @param string|null $statusFlag
     *
     * @return ItsSchdDocStatus
     */
    public function setStatusFlag($statusFlag = null)
    {
        $this->statusFlag = $statusFlag;

        return $this;
    }

    /**
     * Get statusFlag.
     *
     * @return string|null
     */
    public function getStatusFlag()
    {
        return $this->statusFlag;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsSchdDocStatus
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsSchdDocStatus
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsSchdDocStatus
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsSchdDocStatus
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsSchdDocStatus
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsSchdDocStatus
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }

    /**
     * Get keyCd.
     *
     * @return string|null
     */
    public function getKeyCd()
    {
        return self::_keyCd;
    }

    /**
     * Get keyGen.
     *
     * @return string|null
     */
    public function getKeyGen()
    {
        return substr($this->activityId, 0, 3) ;
    }
    
}
