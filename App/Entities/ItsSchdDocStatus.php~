<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks as HasLifecycleCallbacks;

/**
 * ItsSchdDocStatus
 *
 * @ORM\Table(name="ITS_SCHD_DOC_STATUS", indexes={@ORM\Index(name="its_schd_doc_status_index1", columns={"ACTIVITY_ID"}), @ORM\Index(name="its_schd_doc_status_index2", columns={"DOC_ID"})})
 * @ORM\Entity
 * @HasLifecycleCallbacks 
 */
class ItsSchdDocStatus extends BaseEntity
{
    const _keyCd = 'SCHD_DOC_STATUS';

    /**
     * @var int
     *
     * @ORM\Column(name="DOC_STATUS_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Classes\KeyGenerator")
     */
    private $docStatusId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ACTIVITY_ID", type="integer", nullable=true)
     */
    private $activityId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=true)
     */
    private $docId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="REF_DOC_ID", type="integer", nullable=true)
     */
    private $refDocId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ITS_USER_ID", type="integer", nullable=true)
     */
    private $itsUserId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PERSON_TYPE_ID", type="integer", nullable=true)
     */
    private $personTypeId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="STATUS_FLAG", type="integer", nullable=true)
     */
    private $statusFlag;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=200, nullable=true)
     */
    private $remark;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="datetime", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="datetime", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;

    /**
     * Get keyCd.
     *
     * @return string|null
     */
    public function getKeyCd()
    {
        return self::_keyCd;
    }

    /**
     * Get keyGen.
     *
     * @return string|null
     */
    public function getKeyGen()
    {
        return substr($this->activityId, 0, 3) ;
    }

}
