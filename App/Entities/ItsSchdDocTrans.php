<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks as HasLifecycleCallbacks;

/**
 * ItsSchdDocTrans
 *
 * @ORM\Table(name="ITS_SCHD_DOC_TRANS", indexes={@ORM\Index(name="its_schd_doc_trans_index1", columns={"ACTIVITY_ID"}), @ORM\Index(name="its_schd_doc_trans_index3", columns={"REF_DOC_ID"}), @ORM\Index(name="its_schd_doc_trans_index2", columns={"DOC_ID"})})
 * @ORM\Entity
 * @HasLifecycleCallbacks 
 */
class ItsSchdDocTrans extends BaseEntity
{
    const _keyCd = 'SCHD_DOC_TRANS';
    /**
     * @var int
     *
     * @ORM\Column(name="DOC_TRANS_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Classes\KeyGenerator")
     */
    private $docTransId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ACTIVITY_ID", type="integer", nullable=true)
     */
    private $activityId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="DOC_ID", type="integer", nullable=true)
     */
    private $docId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="REF_DOC_ID", type="integer", nullable=true)
     */
    private $refDocId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="SENDER_ID", type="integer", nullable=true)
     */
    private $senderId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="SENDER_PERSON_TYPE_ID", type="integer", nullable=true)
     */
    private $senderPersonTypeId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SENDER_REMARK", type="string", length=500, nullable=true)
     */
    private $senderRemark;

    /**
     * @var int|null
     *
     * @ORM\Column(name="RECEIVER_ID", type="integer", nullable=true)
     */
    private $receiverId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="RECEIVER_PERSON_TYPE_ID", type="integer", nullable=true)
     */
    private $receiverPersonTypeId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="RECEIVER_REMARK", type="string", length=500, nullable=true)
     */
    private $receiverRemark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="STATUS_FLAG", type="string", length=1, nullable=true)
     */
    private $statusFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;



    /**
     * Get docTransId.
     *
     * @return int
     */
    public function getDocTransId()
    {
        return $this->docTransId;
    }

    /**
     * Set activityId.
     *
     * @param int|null $activityId
     *
     * @return ItsSchdDocTrans
     */
    public function setActivityId($activityId = null)
    {
        $this->activityId = $activityId;

        return $this;
    }

    /**
     * Get activityId.
     *
     * @return int|null
     */
    public function getActivityId()
    {
        return $this->activityId;
    }

    /**
     * Set docId.
     *
     * @param int|null $docId
     *
     * @return ItsSchdDocTrans
     */
    public function setDocId($docId = null)
    {
        $this->docId = $docId;

        return $this;
    }

    /**
     * Get docId.
     *
     * @return int|null
     */
    public function getDocId()
    {
        return $this->docId;
    }

    /**
     * Set refDocId.
     *
     * @param int|null $refDocId
     *
     * @return ItsSchdDocTrans
     */
    public function setRefDocId($refDocId = null)
    {
        $this->refDocId = $refDocId;

        return $this;
    }

    /**
     * Get refDocId.
     *
     * @return int|null
     */
    public function getRefDocId()
    {
        return $this->refDocId;
    }

    /**
     * Set senderId.
     *
     * @param int|null $senderId
     *
     * @return ItsSchdDocTrans
     */
    public function setSenderId($senderId = null)
    {
        $this->senderId = $senderId;

        return $this;
    }

    /**
     * Get senderId.
     *
     * @return int|null
     */
    public function getSenderId()
    {
        return $this->senderId;
    }

    /**
     * Set senderPersonTypeId.
     *
     * @param int|null $senderPersonTypeId
     *
     * @return ItsSchdDocTrans
     */
    public function setSenderPersonTypeId($senderPersonTypeId = null)
    {
        $this->senderPersonTypeId = $senderPersonTypeId;

        return $this;
    }

    /**
     * Get senderPersonTypeId.
     *
     * @return int|null
     */
    public function getSenderPersonTypeId()
    {
        return $this->senderPersonTypeId;
    }

    /**
     * Set senderRemark.
     *
     * @param string|null $senderRemark
     *
     * @return ItsSchdDocTrans
     */
    public function setSenderRemark($senderRemark = null)
    {
        $this->senderRemark = $senderRemark;

        return $this;
    }

    /**
     * Get senderRemark.
     *
     * @return string|null
     */
    public function getSenderRemark()
    {
        return $this->senderRemark;
    }

    /**
     * Set receiverId.
     *
     * @param int|null $receiverId
     *
     * @return ItsSchdDocTrans
     */
    public function setReceiverId($receiverId = null)
    {
        $this->receiverId = $receiverId;

        return $this;
    }

    /**
     * Get receiverId.
     *
     * @return int|null
     */
    public function getReceiverId()
    {
        return $this->receiverId;
    }

    /**
     * Set receiverPersonTypeId.
     *
     * @param int|null $receiverPersonTypeId
     *
     * @return ItsSchdDocTrans
     */
    public function setReceiverPersonTypeId($receiverPersonTypeId = null)
    {
        $this->receiverPersonTypeId = $receiverPersonTypeId;

        return $this;
    }

    /**
     * Get receiverPersonTypeId.
     *
     * @return int|null
     */
    public function getReceiverPersonTypeId()
    {
        return $this->receiverPersonTypeId;
    }

    /**
     * Set receiverRemark.
     *
     * @param string|null $receiverRemark
     *
     * @return ItsSchdDocTrans
     */
    public function setReceiverRemark($receiverRemark = null)
    {
        $this->receiverRemark = $receiverRemark;

        return $this;
    }

    /**
     * Get receiverRemark.
     *
     * @return string|null
     */
    public function getReceiverRemark()
    {
        return $this->receiverRemark;
    }

    /**
     * Set statusFlag.
     *
     * @param string|null $statusFlag
     *
     * @return ItsSchdDocTrans
     */
    public function setStatusFlag($statusFlag = null)
    {
        $this->statusFlag = $statusFlag;

        return $this;
    }

    /**
     * Get statusFlag.
     *
     * @return string|null
     */
    public function getStatusFlag()
    {
        return $this->statusFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsSchdDocTrans
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsSchdDocTrans
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsSchdDocTrans
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsSchdDocTrans
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsSchdDocTrans
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }

    /**
     * Get keyCd.
     *
     * @return string|null
     */
    public function getKeyCd()
    {
        return self::_keyCd;
    }

    /**
     * Get keyGen.
     *
     * @return string|null
     */
    public function getKeyGen()
    {
        return substr($this->activityId, 0, 3) ;
    }
}
