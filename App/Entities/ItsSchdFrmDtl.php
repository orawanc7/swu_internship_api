<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks as HasLifecycleCallbacks;

/**
 * ItsSchdFrmDtl
 *
 * @ORM\Table(name="ITS_SCHD_FRM_DTL", indexes={@ORM\Index(name="its_schd_frm_dtl_index1", columns={"FRM_HDR_ID"})})
 * @ORM\Entity
 * @HasLifecycleCallbacks 
 */
class ItsSchdFrmDtl extends BaseEntity
{
    const _keyCd = 'SCHD_FRM_DTL';
    
    /**
     * @var int
     *
     * @ORM\Column(name="FRM_DTL_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Classes\KeyGenerator")
     */
    private $frmDtlId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="FRM_HDR_ID", type="integer", nullable=true)
     */
    private $frmHdrId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="QUESTION", type="string", length=50, nullable=true)
     */
    private $question;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ANSWER", type="string", length=4000, nullable=true)
     */
    private $answer;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="datetime", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="datetime", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;



    /**
     * Get frmDtlId.
     *
     * @return int
     */
    public function getFrmDtlId()
    {
        return $this->frmDtlId;
    }

    /**
     * Set frmHdrId.
     *
     * @param int|null $frmHdrId
     *
     * @return ItsSchdFrmDtl
     */
    public function setFrmHdrId($frmHdrId = null)
    {
        $this->frmHdrId = $frmHdrId;

        return $this;
    }

    /**
     * Get frmHdrId.
     *
     * @return int|null
     */
    public function getFrmHdrId()
    {
        return $this->frmHdrId;
    }

    /**
     * Set question.
     *
     * @param string|null $question
     *
     * @return ItsSchdFrmDtl
     */
    public function setQuestion($question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question.
     *
     * @return string|null
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set answer.
     *
     * @param string|null $answer
     *
     * @return ItsSchdFrmDtl
     */
    public function setAnswer($answer = null)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer.
     *
     * @return string|null
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsSchdFrmDtl
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsSchdFrmDtl
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsSchdFrmDtl
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsSchdFrmDtl
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsSchdFrmDtl
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }

    /**
     * Get keyCd.
     *
     * @return string|null
     */
    public function getKeyCd()
    {
        return self::_keyCd;
    }

    /**
     * Get keyGen.
     *
     * @return string|null
     */
    public function getKeyGen()
    {
        return substr($this->frmHdrId, 0, 3) ;
    }
}
