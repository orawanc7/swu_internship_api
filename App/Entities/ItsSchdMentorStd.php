<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks as HasLifecycleCallbacks;

/**
 * ItsSchdMentorStd
 *
 * @ORM\Table(name="ITS_SCHD_MENTOR_STD", indexes={@ORM\Index(name="its_schd_mentor_std_index1", columns={"ROUND_ID"}), @ORM\Index(name="its_schd_mentor_std_index3", columns={"ITS_PERSON_TYPE_ID"}), @ORM\Index(name="its_schd_mentor_std_index2", columns={"ITS_STUDENT_ID"})})
 * @ORM\Entity
 * @HasLifecycleCallbacks
 */
class ItsSchdMentorStd extends BaseEntity
{
    const _keyCd = 'MENTOR';

    /**
     * @var int
     *
     * @ORM\Column(name="MENTOR_STD_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Classes\KeyGenerator")
     */
    private $mentorStdId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ROUND_ID", type="smallint", nullable=true)
     */
    private $roundId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ITS_PERSON_TYPE_ID", type="integer", nullable=true)
     */
    private $itsPersonTypeId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ITS_STUDENT_ID", type="integer", nullable=true)
     */
    private $itsStudentId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=200, nullable=true)
     */
    private $remark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;



    /**
     * Get mentorStdId.
     *
     * @return int
     */
    public function getMentorStdId()
    {
        return $this->mentorStdId;
    }

    /**
     * Set roundId.
     *
     * @param int|null $roundId
     *
     * @return ItsSchdMentorStd
     */
    public function setRoundId($roundId = null)
    {
        $this->roundId = $roundId;

        return $this;
    }

    /**
     * Get roundId.
     *
     * @return int|null
     */
    public function getRoundId()
    {
        return $this->roundId;
    }

    /**
     * Set itsPersonTypeId.
     *
     * @param int|null $itsPersonTypeId
     *
     * @return ItsSchdMentorStd
     */
    public function setItsPersonTypeId($itsPersonTypeId = null)
    {
        $this->itsPersonTypeId = $itsPersonTypeId;

        return $this;
    }

    /**
     * Get itsPersonTypeId.
     *
     * @return int|null
     */
    public function getItsPersonTypeId()
    {
        return $this->itsPersonTypeId;
    }

    /**
     * Set itsStudentId.
     *
     * @param int|null $itsStudentId
     *
     * @return ItsSchdMentorStd
     */
    public function setItsStudentId($itsStudentId = null)
    {
        $this->itsStudentId = $itsStudentId;

        return $this;
    }

    /**
     * Get itsStudentId.
     *
     * @return int|null
     */
    public function getItsStudentId()
    {
        return $this->itsStudentId;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsSchdMentorStd
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsSchdMentorStd
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsSchdMentorStd
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsSchdMentorStd
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsSchdMentorStd
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsSchdMentorStd
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsSchdMentorStd
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }

    /**
     * Get keyCd.
     *
     * @return string|null
     */
    public function getKeyCd()
    {
        return self::_keyCd;
    }

    /**
     * Get keyGen.
     *
     * @return string|null
     */
    public function getKeyGen()
    {
        return substr($this->roundId, 0, 3) ;
    }

}
