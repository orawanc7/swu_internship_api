<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsSchdPersonStd
 *
 * @ORM\Table(name="ITS_SCHD_PERSON_STD")
 * @ORM\Entity
 */
class ItsSchdPersonStd
{
    /**
     * @var int
     *
     * @ORM\Column(name="PERSON_STD_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ITS_SCHD_PERSON_STD_PERSON_STD", allocationSize=1, initialValue=1)
     */
    private $personStdId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ROUND_ID", type="smallint", nullable=true)
     */
    private $roundId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="SEQ_ID", type="integer", nullable=true)
     */
    private $seqId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ITS_PERSON_TYPE_ID", type="integer", nullable=true)
     */
    private $itsPersonTypeId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ITS_STUDENT_ID", type="integer", nullable=true)
     */
    private $itsStudentId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=200, nullable=true)
     */
    private $remark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;



    /**
     * Get personStdId.
     *
     * @return int
     */
    public function getPersonStdId()
    {
        return $this->personStdId;
    }

    /**
     * Set roundId.
     *
     * @param int|null $roundId
     *
     * @return ItsSchdPersonStd
     */
    public function setRoundId($roundId = null)
    {
        $this->roundId = $roundId;

        return $this;
    }

    /**
     * Get roundId.
     *
     * @return int|null
     */
    public function getRoundId()
    {
        return $this->roundId;
    }

    /**
     * Set seqId.
     *
     * @param int|null $seqId
     *
     * @return ItsSchdPersonStd
     */
    public function setSeqId($seqId = null)
    {
        $this->seqId = $seqId;

        return $this;
    }

    /**
     * Get seqId.
     *
     * @return int|null
     */
    public function getSeqId()
    {
        return $this->seqId;
    }

    /**
     * Set itsPersonTypeId.
     *
     * @param int|null $itsPersonTypeId
     *
     * @return ItsSchdPersonStd
     */
    public function setItsPersonTypeId($itsPersonTypeId = null)
    {
        $this->itsPersonTypeId = $itsPersonTypeId;

        return $this;
    }

    /**
     * Get itsPersonTypeId.
     *
     * @return int|null
     */
    public function getItsPersonTypeId()
    {
        return $this->itsPersonTypeId;
    }

    /**
     * Set itsStudentId.
     *
     * @param int|null $itsStudentId
     *
     * @return ItsSchdPersonStd
     */
    public function setItsStudentId($itsStudentId = null)
    {
        $this->itsStudentId = $itsStudentId;

        return $this;
    }

    /**
     * Get itsStudentId.
     *
     * @return int|null
     */
    public function getItsStudentId()
    {
        return $this->itsStudentId;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsSchdPersonStd
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsSchdPersonStd
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsSchdPersonStd
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsSchdPersonStd
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsSchdPersonStd
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsSchdPersonStd
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsSchdPersonStd
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }
}
