<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks as HasLifecycleCallbacks;

/**
 * ItsSchdRound
 *
 * @ORM\Table(name="ITS_SCHD_ROUND")
 * @ORM\Entity
 *  @HasLifecycleCallbacks 
 */
class ItsSchdRound extends BaseEntity
{
    const _keyCd = 'SCHD_ROUND';

    /**
     * @var int
     *
     * @ORM\Column(name="ROUND_ID", type="bigint", nullable=false, options={"comment"="รหัสรอบการฝึกปฏิบัติการสอนฯ (611XX) 2 หลักแรก คือ ปีการศึกษา หลักที่ 3 ภาค Running 2 หลัก"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Classes\KeyGenerator")
     */
    private $roundId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="YEAR", type="integer", nullable=true, options={"comment"="ปีการศึกษา"})
     */
    private $year;

    /**
     * @var smallint|null
     *
     * @ORM\Column(name="SEM_CD", type="smallint", nullable=true, options={"comment"="ภาคการศึกษา"})
     */
    private $semCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ROUND_NAME_TH", type="string", length=80, nullable=true, options={"comment"="ชื่อรอบการฝึกปฏิบัติการสอนฯภาษาไทย"})
     */
    private $roundNameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ROUND_NAME_EN", type="string", length=80, nullable=true, options={"comment"="ชื่อรอบการฝึกปฏิบัติการสอนฯภาษาอังกฤษ"})
     */
    private $roundNameEn;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="START_DATE", type="date", nullable=true, options={"comment"="วันที่เริ่มต้น"})
     */
    private $startDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="END_DATE", type="date", nullable=true, options={"comment"="วันที่สิ้นสุด"})
     */
    private $endDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true, options={"comment"="สถานะการใช้งาน"})
     */
    private $activeFlag;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=200, nullable=true, options={"comment"="หมายเหตุ"})
     */
    private $remark;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true, options={"comment"="วันที่เพิ่มข้อมูล"})
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true, options={"comment"="ผู้เพิ่มข้อมูล"})
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="datetime", nullable=true, options={"comment"="วันที่แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true, options={"comment"="ผู้แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true, options={"comment"="ชื่อโปรแกรมที่ทำการแก้ไข"})
     */
    private $programCd;

    /**
     * Set roundId.
     *
     * @param bigint|null $year
     *
     * @return ItsSchdRound
     */
    public function setRoundId($roundId = null)
    {
        $this->roundId = $roundId;

        return $this;
    }


    /**
     * Get roundId.
     *
     * @return int
     */
    public function getRoundId()
    {
        return $this->roundId;
    }

    /**
     * Set year.
     *
     * @param int|null $year
     *
     * @return ItsSchdRound
     */
    public function setYear($year = null)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year.
     *
     * @return int|null
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set semCd.
     *
     * @param smallint|null $semCd
     *
     * @return ItsSchdRound
     */
    public function setSemCd($semCd = null)
    {
        $this->semCd = $semCd;

        return $this;
    }

    /**
     * Get semCd.
     *
     * @return smallint|null
     */
    public function getSemCd()
    {
        return $this->semCd;
    }

    /**
     * Set roundNameTh.
     *
     * @param string|null $roundNameTh
     *
     * @return ItsSchdRound
     */
    public function setRoundNameTh($roundNameTh = null)
    {
        $this->roundNameTh = $roundNameTh;

        return $this;
    }

    /**
     * Get roundNameTh.
     *
     * @return string|null
     */
    public function getRoundNameTh()
    {
        return $this->roundNameTh;
    }

    /**
     * Set roundNameEn.
     *
     * @param string|null $roundNameEn
     *
     * @return ItsSchdRound
     */
    public function setRoundNameEn($roundNameEn = null)
    {
        $this->roundNameEn = $roundNameEn;

        return $this;
    }

    /**
     * Get roundNameEn.
     *
     * @return string|null
     */
    public function getRoundNameEn()
    {
        return $this->roundNameEn;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime|null $startDate
     *
     * @return ItsSchdRound
     */
    public function setStartDate($startDate = null)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime|null
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime|null $endDate
     *
     * @return ItsSchdRound
     */
    public function setEndDate($endDate = null)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime|null
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsSchdRound
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsSchdRound
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsSchdRound
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsSchdRound
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsSchdRound
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;


        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsSchdRound
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsSchdRound
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }

    /**
     * Get keyCd.
     *
     * @return string|null
     */
    public function getKeyCd()
    {
        return self::_keyCd;
    }

    /**
     * Get keyGen.
     *
     * @return string|null
     */
    public function getKeyGen()
    {
        return substr($this->year, 2, 2) . $this->semCd;
    }
}
