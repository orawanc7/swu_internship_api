<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsSchdSchoolHdr
 *
 * @ORM\Table(name="ITS_SCHD_SCHOOL_HDR", indexes={@ORM\Index(name="its_schd_school_hdr_index1", columns={"ROUND_ID"})})
 * @ORM\Entity
 */
class ItsSchdSchoolHdr
{
    /**
     * @var int
     *
     * @ORM\Column(name="SCHOOL_HDR_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ITS_SCHD_SCHOOL_HDR_SCHOOL_HDR", allocationSize=1, initialValue=1)
     */
    private $schoolHdrId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ROUND_ID", type="smallint", nullable=true)
     */
    private $roundId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="SCHOOL_ID", type="integer", nullable=true)
     */
    private $schoolId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=400, nullable=true)
     */
    private $remark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;



    /**
     * Get schoolHdrId.
     *
     * @return int
     */
    public function getSchoolHdrId()
    {
        return $this->schoolHdrId;
    }

    /**
     * Set roundId.
     *
     * @param int|null $roundId
     *
     * @return ItsSchdSchoolHdr
     */
    public function setRoundId($roundId = null)
    {
        $this->roundId = $roundId;

        return $this;
    }

    /**
     * Get roundId.
     *
     * @return int|null
     */
    public function getRoundId()
    {
        return $this->roundId;
    }

    /**
     * Set schoolId.
     *
     * @param int|null $schoolId
     *
     * @return ItsSchdSchoolHdr
     */
    public function setSchoolId($schoolId = null)
    {
        $this->schoolId = $schoolId;

        return $this;
    }

    /**
     * Get schoolId.
     *
     * @return int|null
     */
    public function getSchoolId()
    {
        return $this->schoolId;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsSchdSchoolHdr
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsSchdSchoolHdr
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsSchdSchoolHdr
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsSchdSchoolHdr
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsSchdSchoolHdr
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsSchdSchoolHdr
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsSchdSchoolHdr
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }
}
