<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks as HasLifecycleCallbacks;

/**
 * ItsSchdSeminar
 *
 * @ORM\Table(name="ITS_SCHD_SEMINAR", indexes={@ORM\Index(name="its_schd_seminar_index1", columns={"ACTIVITY_ID"})})
 * @ORM\Entity
* @HasLifecycleCallbacks
 */
class ItsSchdSeminar extends BaseEntity
{
    const _keyCd = 'SCHD_SEMINAR';

    /**
     * @var int
     *
     * @ORM\Column(name="SEMINAR_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Classes\KeyGenerator")
     */
    private $seminarId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ACTIVITY_ID", type="integer", nullable=true)
     */
    private $activityId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PERSON_TYPE_ID", type="integer", nullable=true)
     */
    private $personTypeId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ATTENDEE_ID", type="integer", nullable=true)
     */
    private $attendeeId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="START_DATE", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="END_DATE", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=200, nullable=true)
     */
    private $remark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;



    /**
     * Get seminarId.
     *
     * @return int
     */
    public function getSeminarId()
    {
        return $this->seminarId;
    }

    /**
     * Set activityId.
     *
     * @param int|null $activityId
     *
     * @return ItsSchdSeminar
     */
    public function setActivityId($activityId = null)
    {
        $this->activityId = $activityId;

        return $this;
    }

    /**
     * Get activityId.
     *
     * @return int|null
     */
    public function getActivityId()
    {
        return $this->activityId;
    }

    /**
     * Set personTypeId.
     *
     * @param int|null $personTypeId
     *
     * @return ItsSchdSeminar
     */
    public function setPersonTypeId($personTypeId = null)
    {
        $this->personTypeId = $personTypeId;

        return $this;
    }

    /**
     * Get personTypeId.
     *
     * @return int|null
     */
    public function getPersonTypeId()
    {
        return $this->personTypeId;
    }

    /**
     * Set attendeeId.
     *
     * @param int|null $attendeeId
     *
     * @return ItsSchdSeminar
     */
    public function setAttendeeId($attendeeId = null)
    {
        $this->attendeeId = $attendeeId;

        return $this;
    }

    /**
     * Get attendeeId.
     *
     * @return int|null
     */
    public function getAttendeeId()
    {
        return $this->attendeeId;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime|null $startDate
     *
     * @return ItsSchdSeminar
     */
    public function setStartDate($startDate = null)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime|null
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime|null $endDate
     *
     * @return ItsSchdSeminar
     */
    public function setEndDate($endDate = null)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime|null
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsSchdSeminar
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsSchdSeminar
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsSchdSeminar
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsSchdSeminar
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsSchdSeminar
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsSchdSeminar
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsSchdSeminar
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }

    /**
     * Get keyCd.
     *
     * @return string|null
     */
    public function getKeyCd()
    {
        return self::_keyCd;
    }

    /**
     * Get keyGen.
     *
     * @return string|null
     */
    public function getKeyGen()
    {
        return substr($this->activityId, 0, 3) ;
    }
}
