<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks as HasLifecycleCallbacks;

/**
 * ItsSchdStdCourse
 *
 * @ORM\Table(name="ITS_SCHD_STD_COURSE", indexes={@ORM\Index(name="its_schd_std_course_index1", columns={"ACTIVITY_ID"}), @ORM\Index(name="its_schd_std_course_index2", columns={"ITS_STUDENT_ID"})})
 * @ORM\Entity
 */
class ItsSchdStdCourse extends BaseEntity
{
    const _keyCd = 'SCHD_STD_COURSE';

    /**
     * @var int
     *
     * @ORM\Column(name="STD_COURSE_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Classes\KeyGenerator")
     */
    private $stdCourseId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ACTIVITY_ID", type="integer", nullable=true)
     */
    private $activityId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ITS_STUDENT_ID", type="integer", nullable=true)
     */
    private $itsStudentId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ITS_COURSE_CD", type="string", length=15, nullable=true)
     */
    private $itsCourseCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ITS_COURSE_NAME", type="string", length=100, nullable=true)
     */
    private $itsCourseName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LEVEL_CLASS", type="string", length=100, nullable=true)
     */
    private $levelClass;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREDIT_AMT", type="string", length=10, nullable=true)
     */
    private $creditAmt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MINUTES_QTY", type="smallint", nullable=true)
     */
    private $minutesQty;

    /**
     * @var string|null
     *
     * @ORM\Column(name="HOUR_AMT", type="string", length=30, nullable=true)
     */
    private $hourAmt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SELECTED_FLAG", type="string", length=1, nullable=true)
     */
    private $selectedFlag;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=200, nullable=true)
     */
    private $remark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="datetime", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="datetime", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;



    /**
     * Get stdCourseId.
     *
     * @return int
     */
    public function getStdCourseId()
    {
        return $this->stdCourseId;
    }

    /**
     * Set activityId.
     *
     * @param int|null $activityId
     *
     * @return ItsSchdStdCourse
     */
    public function setActivityId($activityId = null)
    {
        $this->activityId = $activityId;

        return $this;
    }

    /**
     * Get activityId.
     *
     * @return int|null
     */
    public function getActivityId()
    {
        return $this->activityId;
    }

    /**
     * Set itsStudentId.
     *
     * @param int|null $itsStudentId
     *
     * @return ItsSchdStdCourse
     */
    public function setItsStudentId($itsStudentId = null)
    {
        $this->itsStudentId = $itsStudentId;

        return $this;
    }

    /**
     * Get itsStudentId.
     *
     * @return int|null
     */
    public function getItsStudentId()
    {
        return $this->itsStudentId;
    }

    /**
     * Set itsCourseCd.
     *
     * @param string|null $itsCourseCd
     *
     * @return ItsSchdStdCourse
     */
    public function setItsCourseCd($itsCourseCd = null)
    {
        $this->itsCourseCd = $itsCourseCd;

        return $this;
    }

    /**
     * Get itsCourseCd.
     *
     * @return string|null
     */
    public function getItsCourseCd()
    {
        return $this->itsCourseCd;
    }

    /**
     * Set itsCourseName.
     *
     * @param string|null $itsCourseName
     *
     * @return ItsSchdStdCourse
     */
    public function setItsCourseName($itsCourseName = null)
    {
        $this->itsCourseName = $itsCourseName;

        return $this;
    }

    /**
     * Get itsCourseName.
     *
     * @return string|null
     */
    public function getItsCourseName()
    {
        return $this->itsCourseName;
    }

    /**
     * Set levelClass.
     *
     * @param string|null $levelClass
     *
     * @return ItsSchdStdCourse
     */
    public function setLevelClass($levelClass = null)
    {
        $this->levelClass = $levelClass;

        return $this;
    }

    /**
     * Get levelClass.
     *
     * @return string|null
     */
    public function getLevelClass()
    {
        return $this->levelClass;
    }

    /**
     * Set creditAmt.
     *
     * @param string|null $creditAmt
     *
     * @return ItsSchdStdCourse
     */
    public function setCreditAmt($creditAmt = null)
    {
        $this->creditAmt = $creditAmt;

        return $this;
    }

    /**
     * Get creditAmt.
     *
     * @return string|null
     */
    public function getCreditAmt()
    {
        return $this->creditAmt;
    }

    /**
     * Set minutesQty.
     *
     * @param int|null $minutesQty
     *
     * @return ItsSchdStdCourse
     */
    public function setMinutesQty($minutesQty = null)
    {
        $this->minutesQty = $minutesQty;

        return $this;
    }

    /**
     * Get minutesQty.
     *
     * @return int|null
     */
    public function getMinutesQty()
    {
        return $this->minutesQty;
    }

    /**
     * Set hourAmt.
     *
     * @param string|null $hourAmt
     *
     * @return ItsSchdStdCourse
     */
    public function setHourAmt($hourAmt = null)
    {
        $this->hourAmt = $hourAmt;

        return $this;
    }

    /**
     * Get hourAmt.
     *
     * @return string|null
     */
    public function getHourAmt()
    {
        return $this->hourAmt;
    }

    /**
     * Set selectedFlag.
     *
     * @param string|null $selectedFlag
     *
     * @return ItsSchdStdCourse
     */
    public function setSelectedFlag($selectedFlag = null)
    {
        $this->selectedFlag = $selectedFlag;

        return $this;
    }

    /**
     * Get selectedFlag.
     *
     * @return string|null
     */
    public function getSelectedFlag()
    {
        return $this->selectedFlag;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsSchdStdCourse
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsSchdStdCourse
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsSchdStdCourse
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsSchdStdCourse
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsSchdStdCourse
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsSchdStdCourse
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsSchdStdCourse
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }

    /**
     * Get keyCd.
     *
     * @return string|null
     */
    public function getKeyCd()
    {
        return self::_keyCd;
    }

    /**
     * Get keyGen.
     *
     * @return string|null
     */
    public function getKeyGen()
    {
        return substr($this->activityId, 0, 3) ;
    }
}
