<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsSchdStdInfo
 *
 * @ORM\Table(name="ITS_SCHD_STD_INFO", indexes={@ORM\Index(name="its_schd_std_info_index1", columns={"STUDENT_ID"})})
 * @ORM\Entity
 */
class ItsSchdStdInfo
{
    /**
     * @var int
     *
     * @ORM\Column(name="ITS_STUDENT_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $itsStudentId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ROUND_ID", type="smallint", nullable=true)
     */
    private $roundId;

    /**
     * @var int
     *
     * @ORM\Column(name="STUDENT_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $studentId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="COURSE_CD", type="string", length=10, nullable=true)
     */
    private $courseCd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="COURSE_NO", type="integer", nullable=true)
     */
    private $courseNo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ITS_MAJOR_ID", type="integer", nullable=true)
     */
    private $itsMajorId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PERSON_TYPE_ID", type="integer", nullable=true)
     */
    private $personTypeId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LANGUAGE_DOC", type="string", length=1, nullable=true)
     */
    private $languageDoc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CAL_FULL_SCORE_AMT", type="decimal", precision=5, scale=2, nullable=true)
     */
    private $calFullScoreAmt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="CAL_GRADE", type="integer", nullable=true)
     */
    private $calGrade;

    /**
     * @var int|null
     *
     * @ORM\Column(name="USER_GRADE", type="integer", nullable=true)
     */
    private $userGrade;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="SEMINAR_AMT", type="boolean", nullable=true)
     */
    private $seminarAmt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=100, nullable=true)
     */
    private $remark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVITY_SET_NO", type="string", length=10, nullable=true)
     */
    private $activitySetNo;


    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="SCHOOL_HDR_ID", type="integer", nullable=true)
     */
    private $schoolHdrId;



    /**
     * Set itsStudentId.
     *
     * @param int $itsStudentId
     *
     * @return ItsSchdStdInfo
     */
    public function setItsStudentId($itsStudentId)
    {
        $this->itsStudentId = $itsStudentId;

        return $this;
    }

    /**
     * Get itsStudentId.
     *
     * @return int
     */
    public function getItsStudentId()
    {
        return $this->itsStudentId;
    }

    /**
     * Set roundId.
     *
     * @param int|null $roundId
     *
     * @return ItsSchdStdInfo
     */
    public function setRoundId($roundId = null)
    {
        $this->roundId = $roundId;

        return $this;
    }

    /**
     * Get roundId.
     *
     * @return int|null
     */
    public function getRoundId()
    {
        return $this->roundId;
    }

    /**
     * Set studentId.
     *
     * @param int $studentId
     *
     * @return ItsSchdStdInfo
     */
    public function setStudentId($studentId)
    {
        $this->studentId = $studentId;

        return $this;
    }

    /**
     * Get studentId.
     *
     * @return int
     */
    public function getStudentId()
    {
        return $this->studentId;
    }

    /**
     * Set courseCd.
     *
     * @param string|null $courseCd
     *
     * @return ItsSchdStdInfo
     */
    public function setCourseCd($courseCd = null)
    {
        $this->courseCd = $courseCd;

        return $this;
    }

    /**
     * Get courseCd.
     *
     * @return string|null
     */
    public function getCourseCd()
    {
        return $this->courseCd;
    }

    /**
     * Set courseNo.
     *
     * @param int|null $courseNo
     *
     * @return ItsSchdStdInfo
     */
    public function setCourseNo($courseNo = null)
    {
        $this->courseNo = $courseNo;

        return $this;
    }

    /**
     * Get courseNo.
     *
     * @return int|null
     */
    public function getCourseNo()
    {
        return $this->courseNo;
    }

    /**
     * Set itsMajorId.
     *
     * @param int|null $itsMajorId
     *
     * @return ItsSchdStdInfo
     */
    public function setItsMajorId($itsMajorId = null)
    {
        $this->itsMajorId = $itsMajorId;

        return $this;
    }

    /**
     * Get itsMajorId.
     *
     * @return int|null
     */
    public function getItsMajorId()
    {
        return $this->itsMajorId;
    }

    /**
     * Set personTypeId.
     *
     * @param int|null $personTypeId
     *
     * @return ItsSchdStdInfo
     */
    public function setPersonTypeId($personTypeId = null)
    {
        $this->personTypeId = $personTypeId;

        return $this;
    }

    /**
     * Get personTypeId.
     *
     * @return int|null
     */
    public function getPersonTypeId()
    {
        return $this->personTypeId;
    }

    /**
     * Set languageDoc.
     *
     * @param string|null $languageDoc
     *
     * @return ItsSchdStdInfo
     */
    public function setLanguageDoc($languageDoc = null)
    {
        $this->languageDoc = $languageDoc;

        return $this;
    }

    /**
     * Get languageDoc.
     *
     * @return string|null
     */
    public function getLanguageDoc()
    {
        return $this->languageDoc;
    }

    /**
     * Set calFullScoreAmt.
     *
     * @param string|null $calFullScoreAmt
     *
     * @return ItsSchdStdInfo
     */
    public function setCalFullScoreAmt($calFullScoreAmt = null)
    {
        $this->calFullScoreAmt = $calFullScoreAmt;

        return $this;
    }

    /**
     * Get calFullScoreAmt.
     *
     * @return string|null
     */
    public function getCalFullScoreAmt()
    {
        return $this->calFullScoreAmt;
    }

    /**
     * Set calGrade.
     *
     * @param int|null $calGrade
     *
     * @return ItsSchdStdInfo
     */
    public function setCalGrade($calGrade = null)
    {
        $this->calGrade = $calGrade;

        return $this;
    }

    /**
     * Get calGrade.
     *
     * @return int|null
     */
    public function getCalGrade()
    {
        return $this->calGrade;
    }

    /**
     * Set userGrade.
     *
     * @param int|null $userGrade
     *
     * @return ItsSchdStdInfo
     */
    public function setUserGrade($userGrade = null)
    {
        $this->userGrade = $userGrade;

        return $this;
    }

    /**
     * Get userGrade.
     *
     * @return int|null
     */
    public function getUserGrade()
    {
        return $this->userGrade;
    }

    /**
     * Set seminarAmt.
     *
     * @param bool|null $seminarAmt
     *
     * @return ItsSchdStdInfo
     */
    public function setSeminarAmt($seminarAmt = null)
    {
        $this->seminarAmt = $seminarAmt;

        return $this;
    }

    /**
     * Get seminarAmt.
     *
     * @return bool|null
     */
    public function getSeminarAmt()
    {
        return $this->seminarAmt;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsSchdStdInfo
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsSchdStdInfo
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set activitySetNo.
     *
     * @param string|null $activitySetNo
     *
     * @return ItsSchdStdInfo
     */
    public function setActivitySetNo($activitySetNo = null)
    {
        $this->activitySetNo = $activitySetNo;

        return $this;
    }

    /**
     * Get activitySetNo.
     *
     * @return string|null
     */
    public function getActivitySetNo()
    {
        return $this->activitySetNo;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsSchdStdInfo
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsSchdStdInfo
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsSchdStdInfo
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsSchdStdInfo
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsSchdStdInfo
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }

    /**
     * Set schoolHdrId.
     *
     * @param int|null $schoolHdrId
     *
     * @return ItsSchdStdInfo
     */
    public function setSchoolHdrId($schoolHdrId = null)
    {
        $this->schoolHdrId = $schoolHdrId;

        return $this;
    }

    /**
     * Get schoolHdrId.
     *
     * @return int|null
     */
    public function getSchoolHdrId()
    {
        return $this->schoolHdrId;
    }
}
