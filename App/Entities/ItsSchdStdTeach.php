<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsSchdStdTeach
 *
 * @ORM\Table(name="ITS_SCHD_STD_TEACH", indexes={@ORM\Index(name="its_schd_std_teach_index2", columns={"STD_COURSE_ID"}), @ORM\Index(name="its_schd_std_teach_index1", columns={"ACTIVITY_ID"})})
 * @ORM\Entity
 */
class ItsSchdStdTeach
{
    /**
     * @var int
     *
     * @ORM\Column(name="STD_TEACH_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ITS_SCHD_STD_TEACH_STD_TEACH_I", allocationSize=1, initialValue=1)
     */
    private $stdTeachId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ACTIVITY_ID", type="integer", nullable=true)
     */
    private $activityId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="STD_COURSE_ID", type="integer", nullable=true)
     */
    private $stdCourseId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ITS_STUDENT_ID", type="integer", nullable=true)
     */
    private $itsStudentId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="TEACH_SEQ_NO", type="integer", nullable=true)
     */
    private $teachSeqNo;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="TEACH_DAY", type="boolean", nullable=true)
     */
    private $teachDay;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TEACH_TITLE", type="string", length=500, nullable=true)
     */
    private $teachTitle;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="START_TIME", type="date", nullable=true)
     */
    private $startTime;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="END_TIME", type="date", nullable=true)
     */
    private $endTime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TEACH_HOUR", type="decimal", precision=5, scale=2, nullable=true)
     */
    private $teachHour;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TEACH_ADDR_DESC", type="string", length=200, nullable=true)
     */
    private $teachAddrDesc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=200, nullable=true)
     */
    private $remark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;



    /**
     * Get stdTeachId.
     *
     * @return int
     */
    public function getStdTeachId()
    {
        return $this->stdTeachId;
    }

    /**
     * Set activityId.
     *
     * @param int|null $activityId
     *
     * @return ItsSchdStdTeach
     */
    public function setActivityId($activityId = null)
    {
        $this->activityId = $activityId;

        return $this;
    }

    /**
     * Get activityId.
     *
     * @return int|null
     */
    public function getActivityId()
    {
        return $this->activityId;
    }

    /**
     * Set stdCourseId.
     *
     * @param int|null $stdCourseId
     *
     * @return ItsSchdStdTeach
     */
    public function setStdCourseId($stdCourseId = null)
    {
        $this->stdCourseId = $stdCourseId;

        return $this;
    }

    /**
     * Get stdCourseId.
     *
     * @return int|null
     */
    public function getStdCourseId()
    {
        return $this->stdCourseId;
    }

    /**
     * Set itsStudentId.
     *
     * @param int|null $itsStudentId
     *
     * @return ItsSchdStdTeach
     */
    public function setItsStudentId($itsStudentId = null)
    {
        $this->itsStudentId = $itsStudentId;

        return $this;
    }

    /**
     * Get itsStudentId.
     *
     * @return int|null
     */
    public function getItsStudentId()
    {
        return $this->itsStudentId;
    }

    /**
     * Set teachSeqNo.
     *
     * @param int|null $teachSeqNo
     *
     * @return ItsSchdStdTeach
     */
    public function setTeachSeqNo($teachSeqNo = null)
    {
        $this->teachSeqNo = $teachSeqNo;

        return $this;
    }

    /**
     * Get teachSeqNo.
     *
     * @return int|null
     */
    public function getTeachSeqNo()
    {
        return $this->teachSeqNo;
    }

    /**
     * Set teachDay.
     *
     * @param bool|null $teachDay
     *
     * @return ItsSchdStdTeach
     */
    public function setTeachDay($teachDay = null)
    {
        $this->teachDay = $teachDay;

        return $this;
    }

    /**
     * Get teachDay.
     *
     * @return bool|null
     */
    public function getTeachDay()
    {
        return $this->teachDay;
    }

    /**
     * Set teachTitle.
     *
     * @param string|null $teachTitle
     *
     * @return ItsSchdStdTeach
     */
    public function setTeachTitle($teachTitle = null)
    {
        $this->teachTitle = $teachTitle;

        return $this;
    }

    /**
     * Get teachTitle.
     *
     * @return string|null
     */
    public function getTeachTitle()
    {
        return $this->teachTitle;
    }

    /**
     * Set startTime.
     *
     * @param \DateTime|null $startTime
     *
     * @return ItsSchdStdTeach
     */
    public function setStartTime($startTime = null)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime.
     *
     * @return \DateTime|null
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set endTime.
     *
     * @param \DateTime|null $endTime
     *
     * @return ItsSchdStdTeach
     */
    public function setEndTime($endTime = null)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime.
     *
     * @return \DateTime|null
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set teachHour.
     *
     * @param string|null $teachHour
     *
     * @return ItsSchdStdTeach
     */
    public function setTeachHour($teachHour = null)
    {
        $this->teachHour = $teachHour;

        return $this;
    }

    /**
     * Get teachHour.
     *
     * @return string|null
     */
    public function getTeachHour()
    {
        return $this->teachHour;
    }

    /**
     * Set teachAddrDesc.
     *
     * @param string|null $teachAddrDesc
     *
     * @return ItsSchdStdTeach
     */
    public function setTeachAddrDesc($teachAddrDesc = null)
    {
        $this->teachAddrDesc = $teachAddrDesc;

        return $this;
    }

    /**
     * Get teachAddrDesc.
     *
     * @return string|null
     */
    public function getTeachAddrDesc()
    {
        return $this->teachAddrDesc;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsSchdStdTeach
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsSchdStdTeach
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsSchdStdTeach
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsSchdStdTeach
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsSchdStdTeach
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsSchdStdTeach
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsSchdStdTeach
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }
}
