<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsStdMaster
 *
 * @ORM\Table(name="ITS_STD_MASTER")
 * @ORM\Entity
 */
class ItsStdMaster
{
    /**
     * @var int
     *
     * @ORM\Column(name="STUDENT_ID", type="integer", nullable=false, options={"comment"="เลขประจำตัวนิสิต"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ITS_STD_MASTER_STUDENT_ID_seq", allocationSize=1, initialValue=1)
     */
    private $studentId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PRENAME_TH_CD", type="integer", nullable=true, options={"comment"="คำนำหน้าชื่อภาษาไทย"})
     */
    private $prenameThCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FNAME_TH", type="string", length=40, nullable=true, options={"comment"="ชื่อนิสิตภาษาไทย"})
     */
    private $fnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MNAME_TH", type="string", length=40, nullable=true, options={"comment"="ชื่อนิสิตกลางภาษาไทย"})
     */
    private $mnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LNAME_TH", type="string", length=40, nullable=true, options={"comment"="นามสกุลนิสิต ภาษาไทย"})
     */
    private $lnameTh;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PRENAME_EN_CD", type="integer", nullable=true, options={"comment"="รหัสคำนำหน้าชื่อภาษาอังกฤษ"})
     */
    private $prenameEnCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FNAME_EN", type="string", length=40, nullable=true, options={"comment"="ชื่อนิสิตภาษาอังกฤษ"})
     */
    private $fnameEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MNAME_EN", type="string", length=40, nullable=true, options={"comment"="ชื่อกลางนิสิตภาษาอังกฤษ"})
     */
    private $mnameEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LNAME_EN", type="string", length=40, nullable=true, options={"comment"="นามสกุลนิสิต ภาษาอังกฤษ"})
     */
    private $lnameEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CURI_MAJOR_CD", type="string", length=10, nullable=true, options={"comment"="รหัสหลักสูตรวิชาเอก"})
     */
    private $curiMajorCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CURI_MINOR_CD", type="string", length=10, nullable=true, options={"comment"="รหัสหลักสูตรวิชาโท"})
     */
    private $curiMinorCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DEGREE_CD", type="string", length=4, nullable=true, options={"comment"="รหัสวุฒิการศึกษา"})
     */
    private $degreeCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LEVEL_CD", type="string", length=2, nullable=true, options={"comment"="รหัสระดับการศึกษา"})
     */
    private $levelCd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="DEPT_CD", type="integer", nullable=true, options={"comment"="รหัสคณะ"})
     */
    private $deptCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MAJOR_CD", type="string", length=10, nullable=true, options={"comment"="รหัสวิชาเอก"})
     */
    private $majorCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MINOR_CD", type="string", length=10, nullable=true, options={"comment"="รหัสวิชาโท"})
     */
    private $minorCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="STUDENT_GROUP_CD", type="string", length=10, nullable=true, options={"comment"="รหัสกลุ่มผู้เรียน"})
     */
    private $studentGroupCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ADDRESS_DESC", type="string", length=250, nullable=true, options={"comment"="รายละเอียดที่อยู่"})
     */
    private $addressDesc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="BUASRI_ID", type="string", length=15, nullable=true, options={"comment"="บัวศรีไอดี"})
     */
    private $buasriId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="GAFE_EMAIL", type="string", length=50, nullable=true, options={"comment"="อีเมล GAFE ของนิสิต"})
     */
    private $gafeEmail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="OTHER_EMAIL", type="string", length=70, nullable=true, options={"comment"="email ที่ติดต่อได้"})
     */
    private $otherEmail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TELEPHONE_NO", type="string", length=50, nullable=true, options={"comment"="เบอร์โทรศัพท์"})
     */
    private $telephoneNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MOBILE_NO", type="string", length=50, nullable=true, options={"comment"="เบอร์โทรศัพท์ มือถือ"})
     */
    private $mobileNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PICTURE_STD", type="string", length=150, nullable=true, options={"comment"="รูปภาพนิสิต"})
     */
    private $pictureStd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=100, nullable=true, options={"comment"="หมายเหตุ"})
     */
    private $remark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true, options={"comment"="สถานะการใช้งาน"})
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true, options={"comment"="วันที่เพิ่มข้อมูล"})
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true, options={"comment"="ผู้เพิ่มข้อมูล"})
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true, options={"comment"="วันที่แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true, options={"comment"="ผู้แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true, options={"comment"="ชื่อโปรแกรมที่ทำการแก้ไข"})
     */
    private $programCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ADM_YEAR", type="string", length=4, nullable=true)
     */
    private $admYear;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ADM_SEM_CD", type="string", length=1, nullable=true)
     */
    private $admSemCd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="THEME_ID", type="integer", nullable=true)
     */
    private $themeId;


    /**
     * Set studentId.
     *
     * @param int|null $studentId
     *
     * @return ItsStdMaster
     */
    public function setStudentId($studentId = null)
    {
        $this->studentId = $studentId;

        return $this;
    }

    /**
     * Get studentId.
     *
     * @return int
     */
    public function getStudentId()
    {
        return $this->studentId;
    }

    /**
     * Set prenameThCd.
     *
     * @param int|null $prenameThCd
     *
     * @return ItsStdMaster
     */
    public function setPrenameThCd($prenameThCd = null)
    {
        $this->prenameThCd = $prenameThCd;

        return $this;
    }

    /**
     * Get prenameThCd.
     *
     * @return int|null
     */
    public function getPrenameThCd()
    {
        return $this->prenameThCd;
    }

    /**
     * Set fnameTh.
     *
     * @param string|null $fnameTh
     *
     * @return ItsStdMaster
     */
    public function setFnameTh($fnameTh = null)
    {
        $this->fnameTh = $fnameTh;

        return $this;
    }

    /**
     * Get fnameTh.
     *
     * @return string|null
     */
    public function getFnameTh()
    {
        return $this->fnameTh;
    }

    /**
     * Set mnameTh.
     *
     * @param string|null $mnameTh
     *
     * @return ItsStdMaster
     */
    public function setMnameTh($mnameTh = null)
    {
        $this->mnameTh = $mnameTh;

        return $this;
    }

    /**
     * Get mnameTh.
     *
     * @return string|null
     */
    public function getMnameTh()
    {
        return $this->mnameTh;
    }

    /**
     * Set lnameTh.
     *
     * @param string|null $lnameTh
     *
     * @return ItsStdMaster
     */
    public function setLnameTh($lnameTh = null)
    {
        $this->lnameTh = $lnameTh;

        return $this;
    }

    /**
     * Get lnameTh.
     *
     * @return string|null
     */
    public function getLnameTh()
    {
        return $this->lnameTh;
    }

    /**
     * Set prenameEnCd.
     *
     * @param int|null $prenameEnCd
     *
     * @return ItsStdMaster
     */
    public function setPrenameEnCd($prenameEnCd = null)
    {
        $this->prenameEnCd = $prenameEnCd;

        return $this;
    }

    /**
     * Get prenameEnCd.
     *
     * @return int|null
     */
    public function getPrenameEnCd()
    {
        return $this->prenameEnCd;
    }

    /**
     * Set fnameEn.
     *
     * @param string|null $fnameEn
     *
     * @return ItsStdMaster
     */
    public function setFnameEn($fnameEn = null)
    {
        $this->fnameEn = $fnameEn;

        return $this;
    }

    /**
     * Get fnameEn.
     *
     * @return string|null
     */
    public function getFnameEn()
    {
        return $this->fnameEn;
    }

    /**
     * Set mnameEn.
     *
     * @param string|null $mnameEn
     *
     * @return ItsStdMaster
     */
    public function setMnameEn($mnameEn = null)
    {
        $this->mnameEn = $mnameEn;

        return $this;
    }

    /**
     * Get mnameEn.
     *
     * @return string|null
     */
    public function getMnameEn()
    {
        return $this->mnameEn;
    }

    /**
     * Set lnameEn.
     *
     * @param string|null $lnameEn
     *
     * @return ItsStdMaster
     */
    public function setLnameEn($lnameEn = null)
    {
        $this->lnameEn = $lnameEn;

        return $this;
    }

    /**
     * Get lnameEn.
     *
     * @return string|null
     */
    public function getLnameEn()
    {
        return $this->lnameEn;
    }

    /**
     * Set curiMajorCd.
     *
     * @param string|null $curiMajorCd
     *
     * @return ItsStdMaster
     */
    public function setCuriMajorCd($curiMajorCd = null)
    {
        $this->curiMajorCd = $curiMajorCd;

        return $this;
    }

    /**
     * Get curiMajorCd.
     *
     * @return string|null
     */
    public function getCuriMajorCd()
    {
        return $this->curiMajorCd;
    }

    /**
     * Set curiMinorCd.
     *
     * @param string|null $curiMinorCd
     *
     * @return ItsStdMaster
     */
    public function setCuriMinorCd($curiMinorCd = null)
    {
        $this->curiMinorCd = $curiMinorCd;

        return $this;
    }

    /**
     * Get curiMinorCd.
     *
     * @return string|null
     */
    public function getCuriMinorCd()
    {
        return $this->curiMinorCd;
    }

    /**
     * Set degreeCd.
     *
     * @param string|null $degreeCd
     *
     * @return ItsStdMaster
     */
    public function setDegreeCd($degreeCd = null)
    {
        $this->degreeCd = $degreeCd;

        return $this;
    }

    /**
     * Get degreeCd.
     *
     * @return string|null
     */
    public function getDegreeCd()
    {
        return $this->degreeCd;
    }

    /**
     * Set levelCd.
     *
     * @param string|null $levelCd
     *
     * @return ItsStdMaster
     */
    public function setLevelCd($levelCd = null)
    {
        $this->levelCd = $levelCd;

        return $this;
    }

    /**
     * Get levelCd.
     *
     * @return string|null
     */
    public function getLevelCd()
    {
        return $this->levelCd;
    }

    /**
     * Set deptCd.
     *
     * @param int|null $deptCd
     *
     * @return ItsStdMaster
     */
    public function setDeptCd($deptCd = null)
    {
        $this->deptCd = $deptCd;

        return $this;
    }

    /**
     * Get deptCd.
     *
     * @return int|null
     */
    public function getDeptCd()
    {
        return $this->deptCd;
    }

    /**
     * Set majorCd.
     *
     * @param string|null $majorCd
     *
     * @return ItsStdMaster
     */
    public function setMajorCd($majorCd = null)
    {
        $this->majorCd = $majorCd;

        return $this;
    }

    /**
     * Get majorCd.
     *
     * @return string|null
     */
    public function getMajorCd()
    {
        return $this->majorCd;
    }

    /**
     * Set minorCd.
     *
     * @param string|null $minorCd
     *
     * @return ItsStdMaster
     */
    public function setMinorCd($minorCd = null)
    {
        $this->minorCd = $minorCd;

        return $this;
    }

    /**
     * Get minorCd.
     *
     * @return string|null
     */
    public function getMinorCd()
    {
        return $this->minorCd;
    }

    /**
     * Set studentGroupCd.
     *
     * @param string|null $studentGroupCd
     *
     * @return ItsStdMaster
     */
    public function setStudentGroupCd($studentGroupCd = null)
    {
        $this->studentGroupCd = $studentGroupCd;

        return $this;
    }

    /**
     * Get studentGroupCd.
     *
     * @return string|null
     */
    public function getStudentGroupCd()
    {
        return $this->studentGroupCd;
    }

    /**
     * Set addressDesc.
     *
     * @param string|null $addressDesc
     *
     * @return ItsStdMaster
     */
    public function setAddressDesc($addressDesc = null)
    {
        $this->addressDesc = $addressDesc;

        return $this;
    }

    /**
     * Get addressDesc.
     *
     * @return string|null
     */
    public function getAddressDesc()
    {
        return $this->addressDesc;
    }

    /**
     * Set buasriId.
     *
     * @param string|null $buasriId
     *
     * @return ItsStdMaster
     */
    public function setBuasriId($buasriId = null)
    {
        $this->buasriId = $buasriId;

        return $this;
    }

    /**
     * Get buasriId.
     *
     * @return string|null
     */
    public function getBuasriId()
    {
        return $this->buasriId;
    }

    /**
     * Set gafeEmail.
     *
     * @param string|null $gafeEmail
     *
     * @return ItsStdMaster
     */
    public function setGafeEmail($gafeEmail = null)
    {
        $this->gafeEmail = $gafeEmail;

        return $this;
    }

    /**
     * Get gafeEmail.
     *
     * @return string|null
     */
    public function getGafeEmail()
    {
        return $this->gafeEmail;
    }

    /**
     * Set otherEmail.
     *
     * @param string|null $otherEmail
     *
     * @return ItsStdMaster
     */
    public function setOtherEmail($otherEmail = null)
    {
        $this->otherEmail = $otherEmail;

        return $this;
    }

    /**
     * Get otherEmail.
     *
     * @return string|null
     */
    public function getOtherEmail()
    {
        return $this->otherEmail;
    }

    /**
     * Set telephoneNo.
     *
     * @param string|null $telephoneNo
     *
     * @return ItsStdMaster
     */
    public function setTelephoneNo($telephoneNo = null)
    {
        $this->telephoneNo = $telephoneNo;

        return $this;
    }

    /**
     * Get telephoneNo.
     *
     * @return string|null
     */
    public function getTelephoneNo()
    {
        return $this->telephoneNo;
    }

    /**
     * Set mobileNo.
     *
     * @param string|null $mobileNo
     *
     * @return ItsStdMaster
     */
    public function setMobileNo($mobileNo = null)
    {
        $this->mobileNo = $mobileNo;

        return $this;
    }

    /**
     * Get mobileNo.
     *
     * @return string|null
     */
    public function getMobileNo()
    {
        return $this->mobileNo;
    }

    /**
     * Set pictureStd.
     *
     * @param string|null $pictureStd
     *
     * @return ItsStdMaster
     */
    public function setPictureStd($pictureStd = null)
    {
        $this->pictureStd = $pictureStd;

        return $this;
    }

    /**
     * Get pictureStd.
     *
     * @return string|null
     */
    public function getPictureStd()
    {
        return $this->pictureStd;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsStdMaster
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsStdMaster
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsStdMaster
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsStdMaster
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsStdMaster
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsStdMaster
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsStdMaster
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }

    /**
     * Set admYear.
     *
     * @param string|null $admYear
     *
     * @return ItsStdMaster
     */
    public function setAdmYear($admYear = null)
    {
        $this->admYear = $admYear;

        return $this;
    }

    /**
     * Get admYear.
     *
     * @return string|null
     */
    public function getAdmYear()
    {
        return $this->admYear;
    }

    /**
     * Set admSemCd.
     *
     * @param string|null $admSemCd
     *
     * @return ItsStdMaster
     */
    public function setAdmSemCd($admSemCd = null)
    {
        $this->admSemCd = $admSemCd;

        return $this;
    }

    /**
     * Get admSemCd.
     *
     * @return string|null
     */
    public function getAdmSemCd()
    {
        return $this->admSemCd;
    }

    /**
     * Set themeId.
     *
     * @param int|null $themeId
     *
     * @return ItsStdMaster
     */
    public function setThemeId($themeId = null)
    {
        $this->themeId = $themeId;

        return $this;
    }

    /**
     * Get themeId.
     *
     * @return int|null
     */
    public function getThemeId()
    {
        return $this->themeId;
    }
}
