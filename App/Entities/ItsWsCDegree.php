<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsWsCDegree
 *
 * @ORM\Table(name="ITS_WS_C_DEGREE", indexes={@ORM\Index(name="IDX_950999ECD8862A63", columns={"LEVEL_GROUP_CD"})})
 * @ORM\Entity
 */
class ItsWsCDegree
{
    /**
     * @var string
     *
     * @ORM\Column(name="DEGREE_CD", type="string", length=4, nullable=false, options={"comment"="รหัสวุฒิการศึกษา"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ITS_WS_C_DEGREE_DEGREE_CD_seq", allocationSize=1, initialValue=1)
     */
    private $degreeCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DEGREE_SNAME_TH", type="string", length=50, nullable=true, options={"comment"="ชื่อวุฒิการศึกษาภาษาไทย แบบสั้น"})
     */
    private $degreeSnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DEGREE_LNAME_TH", type="string", length=100, nullable=true, options={"comment"="ชื่อวุฒิการศึกษาภาษาไทย แบบยาว"})
     */
    private $degreeLnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DEGREE_SNAME_ENG", type="string", length=50, nullable=true, options={"comment"="ชื่อวุฒิการศึกษาภาษาอังกฤษ แบบสั้น"})
     */
    private $degreeSnameEng;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DEGREE_LNAME_ENG", type="string", length=100, nullable=true, options={"comment"="ชื่อวุฒิการศึกษาภาษาอังกฤษ แบบยาว"})
     */
    private $degreeLnameEng;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true, options={"comment"="วันที่เพิ่มข้อมูล"})
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true, options={"comment"="ผู้เพิ่มข้อมูล"})
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true, options={"comment"="วันที่แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true, options={"comment"="ผู้แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateBy;

    /**
     * @var \App\Entities\ItsWsCLevelGroup
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\ItsWsCLevelGroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="LEVEL_GROUP_CD", referencedColumnName="LEVEL_GROUP_CD")
     * })
     */
    private $levelGroupCd;



    /**
     * Get degreeCd.
     *
     * @return string
     */
    public function getDegreeCd()
    {
        return $this->degreeCd;
    }

    /**
     * Set degreeSnameTh.
     *
     * @param string|null $degreeSnameTh
     *
     * @return ItsWsCDegree
     */
    public function setDegreeSnameTh($degreeSnameTh = null)
    {
        $this->degreeSnameTh = $degreeSnameTh;

        return $this;
    }

    /**
     * Get degreeSnameTh.
     *
     * @return string|null
     */
    public function getDegreeSnameTh()
    {
        return $this->degreeSnameTh;
    }

    /**
     * Set degreeLnameTh.
     *
     * @param string|null $degreeLnameTh
     *
     * @return ItsWsCDegree
     */
    public function setDegreeLnameTh($degreeLnameTh = null)
    {
        $this->degreeLnameTh = $degreeLnameTh;

        return $this;
    }

    /**
     * Get degreeLnameTh.
     *
     * @return string|null
     */
    public function getDegreeLnameTh()
    {
        return $this->degreeLnameTh;
    }

    /**
     * Set degreeSnameEng.
     *
     * @param string|null $degreeSnameEng
     *
     * @return ItsWsCDegree
     */
    public function setDegreeSnameEng($degreeSnameEng = null)
    {
        $this->degreeSnameEng = $degreeSnameEng;

        return $this;
    }

    /**
     * Get degreeSnameEng.
     *
     * @return string|null
     */
    public function getDegreeSnameEng()
    {
        return $this->degreeSnameEng;
    }

    /**
     * Set degreeLnameEng.
     *
     * @param string|null $degreeLnameEng
     *
     * @return ItsWsCDegree
     */
    public function setDegreeLnameEng($degreeLnameEng = null)
    {
        $this->degreeLnameEng = $degreeLnameEng;

        return $this;
    }

    /**
     * Get degreeLnameEng.
     *
     * @return string|null
     */
    public function getDegreeLnameEng()
    {
        return $this->degreeLnameEng;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsWsCDegree
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsWsCDegree
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsWsCDegree
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsWsCDegree
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set levelGroupCd.
     *
     * @param \App\Entities\ItsWsCLevelGroup|null $levelGroupCd
     *
     * @return ItsWsCDegree
     */
    public function setLevelGroupCd(\App\Entities\ItsWsCLevelGroup $levelGroupCd = null)
    {
        $this->levelGroupCd = $levelGroupCd;

        return $this;
    }

    /**
     * Get levelGroupCd.
     *
     * @return \App\Entities\ItsWsCLevelGroup|null
     */
    public function getLevelGroupCd()
    {
        return $this->levelGroupCd;
    }
}
