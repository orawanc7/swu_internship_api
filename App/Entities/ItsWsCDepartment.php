<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsWsCDepartment
 *
 * @ORM\Table(name="ITS_WS_C_DEPARTMENT", indexes={@ORM\Index(name="IDX_604074BC45DB10AA", columns={"IN_DEPT_CD"})})
 * @ORM\Entity
 */
class ItsWsCDepartment
{
    /**
     * @var int
     *
     * @ORM\Column(name="DEPT_CD", type="integer", nullable=false, options={"comment"="รหัสหน่วยงาน"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ITS_WS_C_DEPARTMENT_DEPT_CD_se", allocationSize=1, initialValue=1)
     */
    private $deptCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DEPT_LNAME_TH", type="string", length=150, nullable=true, options={"comment"="ชื่อหน่วยงานภาษาไทยแบบยาว"})
     */
    private $deptLnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DEPT_SNAME_TH", type="string", length=150, nullable=true, options={"comment"="ชื่อหน่วยงานภาษาไทยแบบสั้น"})
     */
    private $deptSnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DEPT_ABBV_TH", type="string", length=15, nullable=true, options={"comment"="ชื่อหน่วยงานภาษาไทยแบบย่อ"})
     */
    private $deptAbbvTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DEPT_LNAME_ENG", type="string", length=100, nullable=true, options={"comment"="ชื่อหน่วยงานภาษาอังกฤษแบบยาว"})
     */
    private $deptLnameEng;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DEPT_SNAME_ENG", type="string", length=50, nullable=true, options={"comment"="ชื่อหน่วยงานภาษาอังกฤษแบบสั้น"})
     */
    private $deptSnameEng;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DEPT_ABBV_ENG", type="string", length=15, nullable=true, options={"comment"="ชื่อหน่วยงานภาษาอังกฤษแบบย่อ"})
     */
    private $deptAbbvEng;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true, options={"comment"="สถานะการใช้งาน"})
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true, options={"comment"="วันที่เพิ่มข้อมูล"})
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true, options={"comment"="ผู้เพิ่มข้อมูล"})
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true, options={"comment"="วันที่แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true, options={"comment"="ผู้แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REGISTER_FLAG", type="string", length=1, nullable=true, options={"comment"="สถานะการใช้งานในระบบงานลงทะเบียนเรียน"})
     */
    private $registerFlag;

    /**
     * @var string|null
     *
     * @ORM\Column(name="GOV_DEPT_FLAG", type="string", length=1, nullable=true, options={"comment"="1.หน่วยงานตั้งใหม่
2หน่วยงานตามราชกิจจานุเบกษา
3.หน่วยงานสภามหาวิทยาลัย
 4.หน่วยงานตามประกาศโครงสร้างหน่วยงานใหม่ตอนออกนอกระบบ"})
     */
    private $govDeptFlag;

    /**
     * @var int|null
     *
     * @ORM\Column(name="FIN_IN_DEPT_CD", type="integer", nullable=true, options={"comment"="หน่วยงานที่ใช้สำหรับงานการเงิน"})
     */
    private $finInDeptCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DEPT_SPECIAL_FLAG", type="string", length=1, nullable=true)
     */
    private $deptSpecialFlag;

    /**
     * @var \App\Entities\ItsWsCDepartment
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\ItsWsCDepartment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IN_DEPT_CD", referencedColumnName="DEPT_CD")
     * })
     */
    private $inDeptCd;



    /**
     * Get deptCd.
     *
     * @return int
     */
    public function getDeptCd()
    {
        return $this->deptCd;
    }

    /**
     * Set deptLnameTh.
     *
     * @param string|null $deptLnameTh
     *
     * @return ItsWsCDepartment
     */
    public function setDeptLnameTh($deptLnameTh = null)
    {
        $this->deptLnameTh = $deptLnameTh;

        return $this;
    }

    /**
     * Get deptLnameTh.
     *
     * @return string|null
     */
    public function getDeptLnameTh()
    {
        return $this->deptLnameTh;
    }

    /**
     * Set deptSnameTh.
     *
     * @param string|null $deptSnameTh
     *
     * @return ItsWsCDepartment
     */
    public function setDeptSnameTh($deptSnameTh = null)
    {
        $this->deptSnameTh = $deptSnameTh;

        return $this;
    }

    /**
     * Get deptSnameTh.
     *
     * @return string|null
     */
    public function getDeptSnameTh()
    {
        return $this->deptSnameTh;
    }

    /**
     * Set deptAbbvTh.
     *
     * @param string|null $deptAbbvTh
     *
     * @return ItsWsCDepartment
     */
    public function setDeptAbbvTh($deptAbbvTh = null)
    {
        $this->deptAbbvTh = $deptAbbvTh;

        return $this;
    }

    /**
     * Get deptAbbvTh.
     *
     * @return string|null
     */
    public function getDeptAbbvTh()
    {
        return $this->deptAbbvTh;
    }

    /**
     * Set deptLnameEng.
     *
     * @param string|null $deptLnameEng
     *
     * @return ItsWsCDepartment
     */
    public function setDeptLnameEng($deptLnameEng = null)
    {
        $this->deptLnameEng = $deptLnameEng;

        return $this;
    }

    /**
     * Get deptLnameEng.
     *
     * @return string|null
     */
    public function getDeptLnameEng()
    {
        return $this->deptLnameEng;
    }

    /**
     * Set deptSnameEng.
     *
     * @param string|null $deptSnameEng
     *
     * @return ItsWsCDepartment
     */
    public function setDeptSnameEng($deptSnameEng = null)
    {
        $this->deptSnameEng = $deptSnameEng;

        return $this;
    }

    /**
     * Get deptSnameEng.
     *
     * @return string|null
     */
    public function getDeptSnameEng()
    {
        return $this->deptSnameEng;
    }

    /**
     * Set deptAbbvEng.
     *
     * @param string|null $deptAbbvEng
     *
     * @return ItsWsCDepartment
     */
    public function setDeptAbbvEng($deptAbbvEng = null)
    {
        $this->deptAbbvEng = $deptAbbvEng;

        return $this;
    }

    /**
     * Get deptAbbvEng.
     *
     * @return string|null
     */
    public function getDeptAbbvEng()
    {
        return $this->deptAbbvEng;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsWsCDepartment
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsWsCDepartment
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsWsCDepartment
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsWsCDepartment
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsWsCDepartment
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set registerFlag.
     *
     * @param string|null $registerFlag
     *
     * @return ItsWsCDepartment
     */
    public function setRegisterFlag($registerFlag = null)
    {
        $this->registerFlag = $registerFlag;

        return $this;
    }

    /**
     * Get registerFlag.
     *
     * @return string|null
     */
    public function getRegisterFlag()
    {
        return $this->registerFlag;
    }

    /**
     * Set govDeptFlag.
     *
     * @param string|null $govDeptFlag
     *
     * @return ItsWsCDepartment
     */
    public function setGovDeptFlag($govDeptFlag = null)
    {
        $this->govDeptFlag = $govDeptFlag;

        return $this;
    }

    /**
     * Get govDeptFlag.
     *
     * @return string|null
     */
    public function getGovDeptFlag()
    {
        return $this->govDeptFlag;
    }

    /**
     * Set finInDeptCd.
     *
     * @param int|null $finInDeptCd
     *
     * @return ItsWsCDepartment
     */
    public function setFinInDeptCd($finInDeptCd = null)
    {
        $this->finInDeptCd = $finInDeptCd;

        return $this;
    }

    /**
     * Get finInDeptCd.
     *
     * @return int|null
     */
    public function getFinInDeptCd()
    {
        return $this->finInDeptCd;
    }

    /**
     * Set deptSpecialFlag.
     *
     * @param string|null $deptSpecialFlag
     *
     * @return ItsWsCDepartment
     */
    public function setDeptSpecialFlag($deptSpecialFlag = null)
    {
        $this->deptSpecialFlag = $deptSpecialFlag;

        return $this;
    }

    /**
     * Get deptSpecialFlag.
     *
     * @return string|null
     */
    public function getDeptSpecialFlag()
    {
        return $this->deptSpecialFlag;
    }

    /**
     * Set inDeptCd.
     *
     * @param \App\Entities\ItsWsCDepartment|null $inDeptCd
     *
     * @return ItsWsCDepartment
     */
    public function setInDeptCd(\App\Entities\ItsWsCDepartment $inDeptCd = null)
    {
        $this->inDeptCd = $inDeptCd;

        return $this;
    }

    /**
     * Get inDeptCd.
     *
     * @return \App\Entities\ItsWsCDepartment|null
     */
    public function getInDeptCd()
    {
        return $this->inDeptCd;
    }
}
