<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsWsCLevel
 *
 * @ORM\Table(name="ITS_WS_C_LEVEL", indexes={@ORM\Index(name="IDX_E872A01FD8862A63", columns={"LEVEL_GROUP_CD"})})
 * @ORM\Entity
 */
class ItsWsCLevel
{
    /**
     * @var string
     *
     * @ORM\Column(name="LEVEL_CD", type="string", length=2, nullable=false, options={"comment"="รหัสระดับการศึกษา"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ITS_WS_C_LEVEL_LEVEL_CD_seq", allocationSize=1, initialValue=1)
     */
    private $levelCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LEVEL_SNAME_TH", type="string", length=10, nullable=true, options={"comment"="รหัสระดับการศึกษาภาษาไทย แบบสั้น"})
     */
    private $levelSnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LEVEL_LNAME_TH", type="string", length=50, nullable=true, options={"comment"="รหัสระดับการศึกษา ภาษาไทย แบบยาว"})
     */
    private $levelLnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LEVEL_SNAME_ENG", type="string", length=10, nullable=true, options={"comment"="รหัสระดับการศึกษาภาษาอังกฤษ แบบสั้น"})
     */
    private $levelSnameEng;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LEVEL_LNAME_ENG", type="string", length=50, nullable=true, options={"comment"="รหัสระดับการศึกษา ภาษาอังกฤษ แบบยาว"})
     */
    private $levelLnameEng;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PART_CD", type="integer", nullable=true, options={"comment"="รหัสสภาพนิสิต"})
     */
    private $partCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="GRADUATE_FLAG", type="string", length=1, nullable=true, options={"comment"="สำหรับคำนวณ FTBS U-undergraduate ,G-Graduate"})
     */
    private $graduateFlag;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MAX_CREDIT_REGISTER", type="integer", nullable=true)
     */
    private $maxCreditRegister;

    /**
     * @var int|null
     *
     * @ORM\Column(name="MAX_CREDIT_SUMMER_REGISTER", type="integer", nullable=true)
     */
    private $maxCreditSummerRegister;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true, options={"comment"="วันที่เพิ่มข้อมูล"})
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true, options={"comment"="ผู้เพิ่มข้อมูล"})
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true, options={"comment"="วันที่แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true, options={"comment"="ผู้แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVITY_PASS_FLAG", type="string", length=1, nullable=true, options={"comment"="กำหนดการผ่านหลักสูตรกิจกรรม  Y - ต้องผ่านหลักสูตรกิจกรรม,N - ไม่ต้องผ่านหลักสูตรกิจกรรม"})
     */
    private $activityPassFlag;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SECTION_NAME_ENG", type="string", length=2, nullable=true, options={"comment"="ตัวแรกของตอนที่ในตารางสอนภาษาอังกฤษ B, M, S, 1, D"})
     */
    private $sectionNameEng;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SECTION_NAME_TH", type="string", length=2, nullable=true, options={"comment"="ตัวแรกของตอนที่ในตารางสอนภาษาไทย ต, ท, พ,ส , ด "})
     */
    private $sectionNameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SUMMER_FLAG", type="string", length=1, nullable=true)
     */
    private $summerFlag;

    /**
     * @var \App\Entities\ItsWsCLevelGroup
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\ItsWsCLevelGroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="LEVEL_GROUP_CD", referencedColumnName="LEVEL_GROUP_CD")
     * })
     */
    private $levelGroupCd;



    /**
     * Get levelCd.
     *
     * @return string
     */
    public function getLevelCd()
    {
        return $this->levelCd;
    }

    /**
     * Set levelSnameTh.
     *
     * @param string|null $levelSnameTh
     *
     * @return ItsWsCLevel
     */
    public function setLevelSnameTh($levelSnameTh = null)
    {
        $this->levelSnameTh = $levelSnameTh;

        return $this;
    }

    /**
     * Get levelSnameTh.
     *
     * @return string|null
     */
    public function getLevelSnameTh()
    {
        return $this->levelSnameTh;
    }

    /**
     * Set levelLnameTh.
     *
     * @param string|null $levelLnameTh
     *
     * @return ItsWsCLevel
     */
    public function setLevelLnameTh($levelLnameTh = null)
    {
        $this->levelLnameTh = $levelLnameTh;

        return $this;
    }

    /**
     * Get levelLnameTh.
     *
     * @return string|null
     */
    public function getLevelLnameTh()
    {
        return $this->levelLnameTh;
    }

    /**
     * Set levelSnameEng.
     *
     * @param string|null $levelSnameEng
     *
     * @return ItsWsCLevel
     */
    public function setLevelSnameEng($levelSnameEng = null)
    {
        $this->levelSnameEng = $levelSnameEng;

        return $this;
    }

    /**
     * Get levelSnameEng.
     *
     * @return string|null
     */
    public function getLevelSnameEng()
    {
        return $this->levelSnameEng;
    }

    /**
     * Set levelLnameEng.
     *
     * @param string|null $levelLnameEng
     *
     * @return ItsWsCLevel
     */
    public function setLevelLnameEng($levelLnameEng = null)
    {
        $this->levelLnameEng = $levelLnameEng;

        return $this;
    }

    /**
     * Get levelLnameEng.
     *
     * @return string|null
     */
    public function getLevelLnameEng()
    {
        return $this->levelLnameEng;
    }

    /**
     * Set partCd.
     *
     * @param int|null $partCd
     *
     * @return ItsWsCLevel
     */
    public function setPartCd($partCd = null)
    {
        $this->partCd = $partCd;

        return $this;
    }

    /**
     * Get partCd.
     *
     * @return int|null
     */
    public function getPartCd()
    {
        return $this->partCd;
    }

    /**
     * Set graduateFlag.
     *
     * @param string|null $graduateFlag
     *
     * @return ItsWsCLevel
     */
    public function setGraduateFlag($graduateFlag = null)
    {
        $this->graduateFlag = $graduateFlag;

        return $this;
    }

    /**
     * Get graduateFlag.
     *
     * @return string|null
     */
    public function getGraduateFlag()
    {
        return $this->graduateFlag;
    }

    /**
     * Set maxCreditRegister.
     *
     * @param int|null $maxCreditRegister
     *
     * @return ItsWsCLevel
     */
    public function setMaxCreditRegister($maxCreditRegister = null)
    {
        $this->maxCreditRegister = $maxCreditRegister;

        return $this;
    }

    /**
     * Get maxCreditRegister.
     *
     * @return int|null
     */
    public function getMaxCreditRegister()
    {
        return $this->maxCreditRegister;
    }

    /**
     * Set maxCreditSummerRegister.
     *
     * @param int|null $maxCreditSummerRegister
     *
     * @return ItsWsCLevel
     */
    public function setMaxCreditSummerRegister($maxCreditSummerRegister = null)
    {
        $this->maxCreditSummerRegister = $maxCreditSummerRegister;

        return $this;
    }

    /**
     * Get maxCreditSummerRegister.
     *
     * @return int|null
     */
    public function getMaxCreditSummerRegister()
    {
        return $this->maxCreditSummerRegister;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsWsCLevel
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsWsCLevel
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsWsCLevel
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsWsCLevel
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set activityPassFlag.
     *
     * @param string|null $activityPassFlag
     *
     * @return ItsWsCLevel
     */
    public function setActivityPassFlag($activityPassFlag = null)
    {
        $this->activityPassFlag = $activityPassFlag;

        return $this;
    }

    /**
     * Get activityPassFlag.
     *
     * @return string|null
     */
    public function getActivityPassFlag()
    {
        return $this->activityPassFlag;
    }

    /**
     * Set sectionNameEng.
     *
     * @param string|null $sectionNameEng
     *
     * @return ItsWsCLevel
     */
    public function setSectionNameEng($sectionNameEng = null)
    {
        $this->sectionNameEng = $sectionNameEng;

        return $this;
    }

    /**
     * Get sectionNameEng.
     *
     * @return string|null
     */
    public function getSectionNameEng()
    {
        return $this->sectionNameEng;
    }

    /**
     * Set sectionNameTh.
     *
     * @param string|null $sectionNameTh
     *
     * @return ItsWsCLevel
     */
    public function setSectionNameTh($sectionNameTh = null)
    {
        $this->sectionNameTh = $sectionNameTh;

        return $this;
    }

    /**
     * Get sectionNameTh.
     *
     * @return string|null
     */
    public function getSectionNameTh()
    {
        return $this->sectionNameTh;
    }

    /**
     * Set summerFlag.
     *
     * @param string|null $summerFlag
     *
     * @return ItsWsCLevel
     */
    public function setSummerFlag($summerFlag = null)
    {
        $this->summerFlag = $summerFlag;

        return $this;
    }

    /**
     * Get summerFlag.
     *
     * @return string|null
     */
    public function getSummerFlag()
    {
        return $this->summerFlag;
    }

    /**
     * Set levelGroupCd.
     *
     * @param \App\Entities\ItsWsCLevelGroup|null $levelGroupCd
     *
     * @return ItsWsCLevel
     */
    public function setLevelGroupCd(\App\Entities\ItsWsCLevelGroup $levelGroupCd = null)
    {
        $this->levelGroupCd = $levelGroupCd;

        return $this;
    }

    /**
     * Get levelGroupCd.
     *
     * @return \App\Entities\ItsWsCLevelGroup|null
     */
    public function getLevelGroupCd()
    {
        return $this->levelGroupCd;
    }
}
