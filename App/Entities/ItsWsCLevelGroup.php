<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsWsCLevelGroup
 *
 * @ORM\Table(name="ITS_WS_C_LEVEL_GROUP")
 * @ORM\Entity
 */
class ItsWsCLevelGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="LEVEL_GROUP_CD", type="integer", nullable=false, options={"comment"="รหัสกลุุ่มระดับการศึกษา"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ITS_WS_C_LEVEL_GROUP_LEVEL_GRO", allocationSize=1, initialValue=1)
     */
    private $levelGroupCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LEVEL_GROUP_NAME_TH", type="string", length=30, nullable=true, options={"comment"="ชื่อกลุ่มระดับการศึกษาภาษาไทยแบบเต็ม"})
     */
    private $levelGroupNameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LEVEL_GROUP_SNAME_TH", type="string", length=20, nullable=true, options={"comment"="ชื่อกลุ่มระดับการศึกษาภาษาไทยแบบย่อ"})
     */
    private $levelGroupSnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LEVEL_GROUP_NAME_ENG", type="string", length=30, nullable=true, options={"comment"="ชื่อกลุ่มระดับการศึกษาภาษาอังกฤษแบบเต็ม"})
     */
    private $levelGroupNameEng;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LEVEL_GROUP_SNAME_ENG", type="string", length=20, nullable=true, options={"comment"="ชื่อกลุ่มระดับการศึกษาภาษาอังกฤษแบบย่อ"})
     */
    private $levelGroupSnameEng;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true, options={"comment"="วันที่เพิ่มข้อมูล"})
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true, options={"comment"="ผู้เพิ่มข้อมูล"})
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true, options={"comment"="วันที่แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true, options={"comment"="ผู้แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateBy;

    /**
     * @var int|null
     *
     * @ORM\Column(name="DIPLOMA_SEQ", type="integer", nullable=true)
     */
    private $diplomaSeq;



    /**
     * Get levelGroupCd.
     *
     * @return int
     */
    public function getLevelGroupCd()
    {
        return $this->levelGroupCd;
    }

    /**
     * Set levelGroupNameTh.
     *
     * @param string|null $levelGroupNameTh
     *
     * @return ItsWsCLevelGroup
     */
    public function setLevelGroupNameTh($levelGroupNameTh = null)
    {
        $this->levelGroupNameTh = $levelGroupNameTh;

        return $this;
    }

    /**
     * Get levelGroupNameTh.
     *
     * @return string|null
     */
    public function getLevelGroupNameTh()
    {
        return $this->levelGroupNameTh;
    }

    /**
     * Set levelGroupSnameTh.
     *
     * @param string|null $levelGroupSnameTh
     *
     * @return ItsWsCLevelGroup
     */
    public function setLevelGroupSnameTh($levelGroupSnameTh = null)
    {
        $this->levelGroupSnameTh = $levelGroupSnameTh;

        return $this;
    }

    /**
     * Get levelGroupSnameTh.
     *
     * @return string|null
     */
    public function getLevelGroupSnameTh()
    {
        return $this->levelGroupSnameTh;
    }

    /**
     * Set levelGroupNameEng.
     *
     * @param string|null $levelGroupNameEng
     *
     * @return ItsWsCLevelGroup
     */
    public function setLevelGroupNameEng($levelGroupNameEng = null)
    {
        $this->levelGroupNameEng = $levelGroupNameEng;

        return $this;
    }

    /**
     * Get levelGroupNameEng.
     *
     * @return string|null
     */
    public function getLevelGroupNameEng()
    {
        return $this->levelGroupNameEng;
    }

    /**
     * Set levelGroupSnameEng.
     *
     * @param string|null $levelGroupSnameEng
     *
     * @return ItsWsCLevelGroup
     */
    public function setLevelGroupSnameEng($levelGroupSnameEng = null)
    {
        $this->levelGroupSnameEng = $levelGroupSnameEng;

        return $this;
    }

    /**
     * Get levelGroupSnameEng.
     *
     * @return string|null
     */
    public function getLevelGroupSnameEng()
    {
        return $this->levelGroupSnameEng;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsWsCLevelGroup
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsWsCLevelGroup
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsWsCLevelGroup
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsWsCLevelGroup
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set diplomaSeq.
     *
     * @param int|null $diplomaSeq
     *
     * @return ItsWsCLevelGroup
     */
    public function setDiplomaSeq($diplomaSeq = null)
    {
        $this->diplomaSeq = $diplomaSeq;

        return $this;
    }

    /**
     * Get diplomaSeq.
     *
     * @return int|null
     */
    public function getDiplomaSeq()
    {
        return $this->diplomaSeq;
    }
}
