<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsWsCMajor
 *
 * @ORM\Table(name="ITS_WS_C_MAJOR", indexes={@ORM\Index(name="IDX_4FAC9105C3C4B237", columns={"DEPT_CD"})})
 * @ORM\Entity
 */
class ItsWsCMajor
{
    /**
     * @var string
     *
     * @ORM\Column(name="MAJOR_CD", type="string", length=10, nullable=false, options={"comment"="รหัสสาขาวิชา"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ITS_WS_C_MAJOR_MAJOR_CD_seq", allocationSize=1, initialValue=1)
     */
    private $majorCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MAJOR_SNAME_TH", type="string", length=150, nullable=true, options={"comment"="ชื่อสาขาวิชาแบบย่อภาษาไทย"})
     */
    private $majorSnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MAJOR_LNAME_TH", type="string", length=100, nullable=true, options={"comment"="ชื่อสาขาวิชาแบบยาวภาษาไทย"})
     */
    private $majorLnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MAJOR_SNAME_ENG", type="string", length=100, nullable=true, options={"comment"="ชื่อสาขาวิชาแบบย่อภาษาอังกฤษ"})
     */
    private $majorSnameEng;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MAJOR_LNAME_ENG", type="string", length=100, nullable=true, options={"comment"="ชื่อสาขาวิชาแบบยาวภาษาอังกฤษ"})
     */
    private $majorLnameEng;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true, options={"comment"="สถานะการใช้งาน"})
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true, options={"comment"="วันที่เพิ่มข้อมูล"})
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true, options={"comment"="ผู้เพิ่มข้อมูล"})
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true, options={"comment"="วันที่แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true, options={"comment"="ผู้แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=200, nullable=true, options={"comment"="หมายเหตุ/รายละเอียด"})
     */
    private $remark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true, options={"comment"="ชื่อโปรแกรมที่ทำการแก้ไข"})
     */
    private $programCd;

    /**
     * @var \App\Entities\ItsWsCDepartment
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\ItsWsCDepartment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="DEPT_CD", referencedColumnName="DEPT_CD")
     * })
     */
    private $deptCd;



    /**
     * Get majorCd.
     *
     * @return string
     */
    public function getMajorCd()
    {
        return $this->majorCd;
    }

    /**
     * Set majorSnameTh.
     *
     * @param string|null $majorSnameTh
     *
     * @return ItsWsCMajor
     */
    public function setMajorSnameTh($majorSnameTh = null)
    {
        $this->majorSnameTh = $majorSnameTh;

        return $this;
    }

    /**
     * Get majorSnameTh.
     *
     * @return string|null
     */
    public function getMajorSnameTh()
    {
        return $this->majorSnameTh;
    }

    /**
     * Set majorLnameTh.
     *
     * @param string|null $majorLnameTh
     *
     * @return ItsWsCMajor
     */
    public function setMajorLnameTh($majorLnameTh = null)
    {
        $this->majorLnameTh = $majorLnameTh;

        return $this;
    }

    /**
     * Get majorLnameTh.
     *
     * @return string|null
     */
    public function getMajorLnameTh()
    {
        return $this->majorLnameTh;
    }

    /**
     * Set majorSnameEng.
     *
     * @param string|null $majorSnameEng
     *
     * @return ItsWsCMajor
     */
    public function setMajorSnameEng($majorSnameEng = null)
    {
        $this->majorSnameEng = $majorSnameEng;

        return $this;
    }

    /**
     * Get majorSnameEng.
     *
     * @return string|null
     */
    public function getMajorSnameEng()
    {
        return $this->majorSnameEng;
    }

    /**
     * Set majorLnameEng.
     *
     * @param string|null $majorLnameEng
     *
     * @return ItsWsCMajor
     */
    public function setMajorLnameEng($majorLnameEng = null)
    {
        $this->majorLnameEng = $majorLnameEng;

        return $this;
    }

    /**
     * Get majorLnameEng.
     *
     * @return string|null
     */
    public function getMajorLnameEng()
    {
        return $this->majorLnameEng;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return ItsWsCMajor
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsWsCMajor
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsWsCMajor
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsWsCMajor
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsWsCMajor
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return ItsWsCMajor
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return ItsWsCMajor
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }

    /**
     * Set deptCd.
     *
     * @param \App\Entities\ItsWsCDepartment|null $deptCd
     *
     * @return ItsWsCMajor
     */
    public function setDeptCd(\App\Entities\ItsWsCDepartment $deptCd = null)
    {
        $this->deptCd = $deptCd;

        return $this;
    }

    /**
     * Get deptCd.
     *
     * @return \App\Entities\ItsWsCDepartment|null
     */
    public function getDeptCd()
    {
        return $this->deptCd;
    }
}
