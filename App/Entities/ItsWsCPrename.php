<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsWsCPrename
 *
 * @ORM\Table(name="ITS_WS_C_PRENAME")
 * @ORM\Entity
 */
class ItsWsCPrename
{
    /**
     * @var int
     *
     * @ORM\Column(name="PRENAME_CD", type="integer", nullable=false, options={"comment"="รหัสคำนำหน้าชื่อ"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ITS_WS_C_PRENAME_PRENAME_CD_se", allocationSize=1, initialValue=1)
     */
    private $prenameCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PRENAME_SNAME_TH", type="string", length=30, nullable=true, options={"comment"="คำนำหน้าชื่อแบบย่อภาษาไทย"})
     */
    private $prenameSnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PRENAME_LNAME_TH", type="string", length=50, nullable=true, options={"comment"="คำนำหน้าชื่อแบบยาวภาษาไทย"})
     */
    private $prenameLnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PRENAME_SNAME_ENG", type="string", length=30, nullable=true, options={"comment"="คำนำหน้าชื่อแบบย่อภาษาอังกฤษ"})
     */
    private $prenameSnameEng;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PRENAME_LNAME_ENG", type="string", length=50, nullable=true, options={"comment"="คำนำหน้าชื่อแบบยาวภาษาอังกฤษ"})
     */
    private $prenameLnameEng;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SEX_TYPE", type="string", length=1, nullable=true, options={"comment"="เพศ"})
     */
    private $sexType;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="date", nullable=true, options={"comment"="วันที่เพิ่มข้อมูล"})
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true, options={"comment"="ผู้เพิ่มข้อมูล"})
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="date", nullable=true, options={"comment"="วันที่แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true, options={"comment"="ผู้แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateBy;



    /**
     * Get prenameCd.
     *
     * @return int
     */
    public function getPrenameCd()
    {
        return $this->prenameCd;
    }

    /**
     * Set prenameSnameTh.
     *
     * @param string|null $prenameSnameTh
     *
     * @return ItsWsCPrename
     */
    public function setPrenameSnameTh($prenameSnameTh = null)
    {
        $this->prenameSnameTh = $prenameSnameTh;

        return $this;
    }

    /**
     * Get prenameSnameTh.
     *
     * @return string|null
     */
    public function getPrenameSnameTh()
    {
        return $this->prenameSnameTh;
    }

    /**
     * Set prenameLnameTh.
     *
     * @param string|null $prenameLnameTh
     *
     * @return ItsWsCPrename
     */
    public function setPrenameLnameTh($prenameLnameTh = null)
    {
        $this->prenameLnameTh = $prenameLnameTh;

        return $this;
    }

    /**
     * Get prenameLnameTh.
     *
     * @return string|null
     */
    public function getPrenameLnameTh()
    {
        return $this->prenameLnameTh;
    }

    /**
     * Set prenameSnameEng.
     *
     * @param string|null $prenameSnameEng
     *
     * @return ItsWsCPrename
     */
    public function setPrenameSnameEng($prenameSnameEng = null)
    {
        $this->prenameSnameEng = $prenameSnameEng;

        return $this;
    }

    /**
     * Get prenameSnameEng.
     *
     * @return string|null
     */
    public function getPrenameSnameEng()
    {
        return $this->prenameSnameEng;
    }

    /**
     * Set prenameLnameEng.
     *
     * @param string|null $prenameLnameEng
     *
     * @return ItsWsCPrename
     */
    public function setPrenameLnameEng($prenameLnameEng = null)
    {
        $this->prenameLnameEng = $prenameLnameEng;

        return $this;
    }

    /**
     * Get prenameLnameEng.
     *
     * @return string|null
     */
    public function getPrenameLnameEng()
    {
        return $this->prenameLnameEng;
    }

    /**
     * Set sexType.
     *
     * @param string|null $sexType
     *
     * @return ItsWsCPrename
     */
    public function setSexType($sexType = null)
    {
        $this->sexType = $sexType;

        return $this;
    }

    /**
     * Get sexType.
     *
     * @return string|null
     */
    public function getSexType()
    {
        return $this->sexType;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return ItsWsCPrename
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsWsCPrename
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsWsCPrename
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsWsCPrename
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }
}
