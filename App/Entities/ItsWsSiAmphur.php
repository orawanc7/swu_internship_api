<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsWsSiAmphur
 *
 * @ORM\Table(name="ITS_WS_SI_AMPHUR")
 * @ORM\Entity
 */
class ItsWsSiAmphur
{
    /**
     * @var int
     *
     * @ORM\Column(name="AMP_PRO_ID", type="integer", nullable=false, options={"comment"="ลำดับข้อมูลอำเภอ (autorun)"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ITS_WS_SI_AMPHUR_AMP_PRO_ID_se", allocationSize=1, initialValue=1)
     */
    private $ampProId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROVINCE_ID", type="string", length=2, nullable=true, options={"comment"="รหัสจังหวัด"})
     */
    private $provinceId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="AMPHUR_ID", type="string", length=4, nullable=true, options={"comment"="รหัสอำเภอ"})
     */
    private $amphurId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="AMPHUR_NAME", type="string", length=250, nullable=true, options={"comment"="ชื่ออำเภอไทย"})
     */
    private $amphurName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="AMPHUR_NAME_EN", type="string", length=250, nullable=true, options={"comment"="ชื่ออำเภออังกฤษ"})
     */
    private $amphurNameEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="AMPHUR_NAME_SH", type="string", length=50, nullable=true, options={"comment"="ชื่ออำเภอไทย (แบบสั้น)"})
     */
    private $amphurNameSh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="AMPHUR_NAME_EN_SH", type="string", length=100, nullable=true, options={"comment"="ชื่ออำเภออังกฤษ (แบบสั้น)"})
     */
    private $amphurNameEnSh;

    /**
     * @var int|null
     *
     * @ORM\Column(name="AMPHUR_STATUS", type="integer", nullable=true, options={"comment"="สถานะการใช้งาน 0 = ไม่ใช้งาน 1 = ใช้งาน"})
     */
    private $amphurStatus;

    /**
     * @var int|null
     *
     * @ORM\Column(name="COUNTRY_ID", type="integer", nullable=true, options={"comment"="รหัสประเทศ"})
     */
    private $countryId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ZONE_ID", type="integer", nullable=true, options={"comment"="รหัสภูมิภาค"})
     */
    private $zoneId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_DTM", type="string", length=1, nullable=true, options={"comment"="วันเวลาที่บันทึกข้อมูล"})
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=1, nullable=true, options={"comment"="ผู้บันทึกข้อมูล"})
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="datetime", nullable=true, options={"comment"="วันเวลาที่แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=3, nullable=true, options={"comment"="ผู้แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateBy;



    /**
     * Get ampProId.
     *
     * @return int
     */
    public function getAmpProId()
    {
        return $this->ampProId;
    }

    /**
     * Set provinceId.
     *
     * @param string|null $provinceId
     *
     * @return ItsWsSiAmphur
     */
    public function setProvinceId($provinceId = null)
    {
        $this->provinceId = $provinceId;
    
        return $this;
    }

    /**
     * Get provinceId.
     *
     * @return string|null
     */
    public function getProvinceId()
    {
        return $this->provinceId;
    }

    /**
     * Set amphurId.
     *
     * @param string|null $amphurId
     *
     * @return ItsWsSiAmphur
     */
    public function setAmphurId($amphurId = null)
    {
        $this->amphurId = $amphurId;
    
        return $this;
    }

    /**
     * Get amphurId.
     *
     * @return string|null
     */
    public function getAmphurId()
    {
        return $this->amphurId;
    }

    /**
     * Set amphurName.
     *
     * @param string|null $amphurName
     *
     * @return ItsWsSiAmphur
     */
    public function setAmphurName($amphurName = null)
    {
        $this->amphurName = $amphurName;
    
        return $this;
    }

    /**
     * Get amphurName.
     *
     * @return string|null
     */
    public function getAmphurName()
    {
        return $this->amphurName;
    }

    /**
     * Set amphurNameEn.
     *
     * @param string|null $amphurNameEn
     *
     * @return ItsWsSiAmphur
     */
    public function setAmphurNameEn($amphurNameEn = null)
    {
        $this->amphurNameEn = $amphurNameEn;
    
        return $this;
    }

    /**
     * Get amphurNameEn.
     *
     * @return string|null
     */
    public function getAmphurNameEn()
    {
        return $this->amphurNameEn;
    }

    /**
     * Set amphurNameSh.
     *
     * @param string|null $amphurNameSh
     *
     * @return ItsWsSiAmphur
     */
    public function setAmphurNameSh($amphurNameSh = null)
    {
        $this->amphurNameSh = $amphurNameSh;
    
        return $this;
    }

    /**
     * Get amphurNameSh.
     *
     * @return string|null
     */
    public function getAmphurNameSh()
    {
        return $this->amphurNameSh;
    }

    /**
     * Set amphurNameEnSh.
     *
     * @param string|null $amphurNameEnSh
     *
     * @return ItsWsSiAmphur
     */
    public function setAmphurNameEnSh($amphurNameEnSh = null)
    {
        $this->amphurNameEnSh = $amphurNameEnSh;
    
        return $this;
    }

    /**
     * Get amphurNameEnSh.
     *
     * @return string|null
     */
    public function getAmphurNameEnSh()
    {
        return $this->amphurNameEnSh;
    }

    /**
     * Set amphurStatus.
     *
     * @param int|null $amphurStatus
     *
     * @return ItsWsSiAmphur
     */
    public function setAmphurStatus($amphurStatus = null)
    {
        $this->amphurStatus = $amphurStatus;
    
        return $this;
    }

    /**
     * Get amphurStatus.
     *
     * @return int|null
     */
    public function getAmphurStatus()
    {
        return $this->amphurStatus;
    }

    /**
     * Set countryId.
     *
     * @param int|null $countryId
     *
     * @return ItsWsSiAmphur
     */
    public function setCountryId($countryId = null)
    {
        $this->countryId = $countryId;
    
        return $this;
    }

    /**
     * Get countryId.
     *
     * @return int|null
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * Set zoneId.
     *
     * @param int|null $zoneId
     *
     * @return ItsWsSiAmphur
     */
    public function setZoneId($zoneId = null)
    {
        $this->zoneId = $zoneId;
    
        return $this;
    }

    /**
     * Get zoneId.
     *
     * @return int|null
     */
    public function getZoneId()
    {
        return $this->zoneId;
    }

    /**
     * Set creationDtm.
     *
     * @param string|null $creationDtm
     *
     * @return ItsWsSiAmphur
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;
    
        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return string|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsWsSiAmphur
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;
    
        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsWsSiAmphur
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;
    
        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsWsSiAmphur
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;
    
        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }
}
