<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsWsSiProvince
 *
 * @ORM\Table(name="ITS_WS_SI_PROVINCE")
 * @ORM\Entity
 */
class ItsWsSiProvince
{
    /**
     * @var string
     *
     * @ORM\Column(name="PROVINCE_ID", type="string", length=50, nullable=false, options={"comment"="รหัสจังหวัด"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ITS_WS_SI_PROVINCE_PROVINCE_ID", allocationSize=1, initialValue=1)
     */
    private $provinceId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROVINCE_NAME", type="string", length=250, nullable=true, options={"comment"="ชื่อจังหวัดไทย"})
     */
    private $provinceName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROVINCE_NAME_EN", type="string", length=250, nullable=true, options={"comment"="ชื่อจังหวัดอังกฤษ"})
     */
    private $provinceNameEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROVINCE_NAME_SH", type="string", length=50, nullable=true, options={"comment"="ชื่อจังหวัดไทย (แบบสั้น)"})
     */
    private $provinceNameSh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROVINCE_NAME_EN_SH", type="string", length=50, nullable=true, options={"comment"="ชื่อจังหวัดอังกฤษ (แบบสั้น)"})
     */
    private $provinceNameEnSh;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PROVINCE_STATUS", type="integer", nullable=true, options={"comment"="สถานะการใช้งาน 0 = ไม่ใช้งาน 1 = ใช้งาน"})
     */
    private $provinceStatus;

    /**
     * @var int|null
     *
     * @ORM\Column(name="COUNTRY_ID", type="integer", nullable=true, options={"comment"="รหัสประเทศ"})
     */
    private $countryId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ZONE_ID", type="integer", nullable=true, options={"comment"="รหัสภูมิภาค"})
     */
    private $zoneId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_DTM", type="string", length=1, nullable=true, options={"comment"="วันเวลาที่บันทึกข้อมูล"})
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=1, nullable=true, options={"comment"="ผู้บันทึกข้อมูล"})
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="datetime", nullable=true, options={"comment"="วันเวลาที่แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=3, nullable=true, options={"comment"="ผู้แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateBy;



    /**
     * Get provinceId.
     *
     * @return string
     */
    public function getProvinceId()
    {
        return $this->provinceId;
    }

    /**
     * Set provinceName.
     *
     * @param string|null $provinceName
     *
     * @return ItsWsSiProvince
     */
    public function setProvinceName($provinceName = null)
    {
        $this->provinceName = $provinceName;
    
        return $this;
    }

    /**
     * Get provinceName.
     *
     * @return string|null
     */
    public function getProvinceName()
    {
        return $this->provinceName;
    }

    /**
     * Set provinceNameEn.
     *
     * @param string|null $provinceNameEn
     *
     * @return ItsWsSiProvince
     */
    public function setProvinceNameEn($provinceNameEn = null)
    {
        $this->provinceNameEn = $provinceNameEn;
    
        return $this;
    }

    /**
     * Get provinceNameEn.
     *
     * @return string|null
     */
    public function getProvinceNameEn()
    {
        return $this->provinceNameEn;
    }

    /**
     * Set provinceNameSh.
     *
     * @param string|null $provinceNameSh
     *
     * @return ItsWsSiProvince
     */
    public function setProvinceNameSh($provinceNameSh = null)
    {
        $this->provinceNameSh = $provinceNameSh;
    
        return $this;
    }

    /**
     * Get provinceNameSh.
     *
     * @return string|null
     */
    public function getProvinceNameSh()
    {
        return $this->provinceNameSh;
    }

    /**
     * Set provinceNameEnSh.
     *
     * @param string|null $provinceNameEnSh
     *
     * @return ItsWsSiProvince
     */
    public function setProvinceNameEnSh($provinceNameEnSh = null)
    {
        $this->provinceNameEnSh = $provinceNameEnSh;
    
        return $this;
    }

    /**
     * Get provinceNameEnSh.
     *
     * @return string|null
     */
    public function getProvinceNameEnSh()
    {
        return $this->provinceNameEnSh;
    }

    /**
     * Set provinceStatus.
     *
     * @param int|null $provinceStatus
     *
     * @return ItsWsSiProvince
     */
    public function setProvinceStatus($provinceStatus = null)
    {
        $this->provinceStatus = $provinceStatus;
    
        return $this;
    }

    /**
     * Get provinceStatus.
     *
     * @return int|null
     */
    public function getProvinceStatus()
    {
        return $this->provinceStatus;
    }

    /**
     * Set countryId.
     *
     * @param int|null $countryId
     *
     * @return ItsWsSiProvince
     */
    public function setCountryId($countryId = null)
    {
        $this->countryId = $countryId;
    
        return $this;
    }

    /**
     * Get countryId.
     *
     * @return int|null
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * Set zoneId.
     *
     * @param int|null $zoneId
     *
     * @return ItsWsSiProvince
     */
    public function setZoneId($zoneId = null)
    {
        $this->zoneId = $zoneId;
    
        return $this;
    }

    /**
     * Get zoneId.
     *
     * @return int|null
     */
    public function getZoneId()
    {
        return $this->zoneId;
    }

    /**
     * Set creationDtm.
     *
     * @param string|null $creationDtm
     *
     * @return ItsWsSiProvince
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;
    
        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return string|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsWsSiProvince
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;
    
        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsWsSiProvince
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;
    
        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsWsSiProvince
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;
    
        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }
}
