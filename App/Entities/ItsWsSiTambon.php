<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsWsSiTambon
 *
 * @ORM\Table(name="ITS_WS_SI_TAMBON")
 * @ORM\Entity
 */
class ItsWsSiTambon
{
    /**
     * @var int
     *
     * @ORM\Column(name="TAM_AMP_ID", type="integer", nullable=false, options={"comment"="ลำดับข้อมูลตำบล (autorun)"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ITS_WS_SI_TAMBON_TAM_AMP_ID_se", allocationSize=1, initialValue=1)
     */
    private $tamAmpId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROVINCE_ID", type="string", length=2, nullable=true, options={"comment"="รหัสจังหวัด"})
     */
    private $provinceId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="AMPHUR_ID", type="string", length=4, nullable=true, options={"comment"="รหัสอำเภอ"})
     */
    private $amphurId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TAMBON_ID", type="string", length=6, nullable=true, options={"comment"="รหัสตำบล"})
     */
    private $tambonId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TAMBON_NAME", type="string", length=250, nullable=true, options={"comment"="ชื่อตำบลไทย"})
     */
    private $tambonName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TAMBON_NAME_EN", type="string", length=250, nullable=true, options={"comment"="ชื่อตำบลอังกฤษ"})
     */
    private $tambonNameEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TAMBON_NAME_SH", type="string", length=50, nullable=true, options={"comment"="ชื่อตำบลไทย (แบบสั้น)"})
     */
    private $tambonNameSh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TAMBON_NAME_EN_SH", type="string", length=50, nullable=true, options={"comment"="ชื่อตำบลอังกฤษ (แบบสั้น)"})
     */
    private $tambonNameEnSh;

    /**
     * @var int|null
     *
     * @ORM\Column(name="TAMBON_STATUS", type="integer", nullable=true, options={"comment"="สถานะการใช้งาน 0 = ไม่ใช้งาน 1 = ใช้งาน"})
     */
    private $tambonStatus;

    /**
     * @var int|null
     *
     * @ORM\Column(name="COUNTRY_ID", type="integer", nullable=true, options={"comment"="รหัสประเทศ"})
     */
    private $countryId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ZONE_ID", type="integer", nullable=true, options={"comment"="รหัสภูมิภาค"})
     */
    private $zoneId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_DTM", type="string", length=1, nullable=true, options={"comment"="วันเวลาที่บันทึกข้อมูล"})
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=1, nullable=true, options={"comment"="ผู้บันทึกข้อมูล"})
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="datetime", nullable=true, options={"comment"="วันเวลาที่แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=3, nullable=true, options={"comment"="ผู้แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateBy;



    /**
     * Get tamAmpId.
     *
     * @return int
     */
    public function getTamAmpId()
    {
        return $this->tamAmpId;
    }

    /**
     * Set provinceId.
     *
     * @param string|null $provinceId
     *
     * @return ItsWsSiTambon
     */
    public function setProvinceId($provinceId = null)
    {
        $this->provinceId = $provinceId;
    
        return $this;
    }

    /**
     * Get provinceId.
     *
     * @return string|null
     */
    public function getProvinceId()
    {
        return $this->provinceId;
    }

    /**
     * Set amphurId.
     *
     * @param string|null $amphurId
     *
     * @return ItsWsSiTambon
     */
    public function setAmphurId($amphurId = null)
    {
        $this->amphurId = $amphurId;
    
        return $this;
    }

    /**
     * Get amphurId.
     *
     * @return string|null
     */
    public function getAmphurId()
    {
        return $this->amphurId;
    }

    /**
     * Set tambonId.
     *
     * @param string|null $tambonId
     *
     * @return ItsWsSiTambon
     */
    public function setTambonId($tambonId = null)
    {
        $this->tambonId = $tambonId;
    
        return $this;
    }

    /**
     * Get tambonId.
     *
     * @return string|null
     */
    public function getTambonId()
    {
        return $this->tambonId;
    }

    /**
     * Set tambonName.
     *
     * @param string|null $tambonName
     *
     * @return ItsWsSiTambon
     */
    public function setTambonName($tambonName = null)
    {
        $this->tambonName = $tambonName;
    
        return $this;
    }

    /**
     * Get tambonName.
     *
     * @return string|null
     */
    public function getTambonName()
    {
        return $this->tambonName;
    }

    /**
     * Set tambonNameEn.
     *
     * @param string|null $tambonNameEn
     *
     * @return ItsWsSiTambon
     */
    public function setTambonNameEn($tambonNameEn = null)
    {
        $this->tambonNameEn = $tambonNameEn;
    
        return $this;
    }

    /**
     * Get tambonNameEn.
     *
     * @return string|null
     */
    public function getTambonNameEn()
    {
        return $this->tambonNameEn;
    }

    /**
     * Set tambonNameSh.
     *
     * @param string|null $tambonNameSh
     *
     * @return ItsWsSiTambon
     */
    public function setTambonNameSh($tambonNameSh = null)
    {
        $this->tambonNameSh = $tambonNameSh;
    
        return $this;
    }

    /**
     * Get tambonNameSh.
     *
     * @return string|null
     */
    public function getTambonNameSh()
    {
        return $this->tambonNameSh;
    }

    /**
     * Set tambonNameEnSh.
     *
     * @param string|null $tambonNameEnSh
     *
     * @return ItsWsSiTambon
     */
    public function setTambonNameEnSh($tambonNameEnSh = null)
    {
        $this->tambonNameEnSh = $tambonNameEnSh;
    
        return $this;
    }

    /**
     * Get tambonNameEnSh.
     *
     * @return string|null
     */
    public function getTambonNameEnSh()
    {
        return $this->tambonNameEnSh;
    }

    /**
     * Set tambonStatus.
     *
     * @param int|null $tambonStatus
     *
     * @return ItsWsSiTambon
     */
    public function setTambonStatus($tambonStatus = null)
    {
        $this->tambonStatus = $tambonStatus;
    
        return $this;
    }

    /**
     * Get tambonStatus.
     *
     * @return int|null
     */
    public function getTambonStatus()
    {
        return $this->tambonStatus;
    }

    /**
     * Set countryId.
     *
     * @param int|null $countryId
     *
     * @return ItsWsSiTambon
     */
    public function setCountryId($countryId = null)
    {
        $this->countryId = $countryId;
    
        return $this;
    }

    /**
     * Get countryId.
     *
     * @return int|null
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * Set zoneId.
     *
     * @param int|null $zoneId
     *
     * @return ItsWsSiTambon
     */
    public function setZoneId($zoneId = null)
    {
        $this->zoneId = $zoneId;
    
        return $this;
    }

    /**
     * Get zoneId.
     *
     * @return int|null
     */
    public function getZoneId()
    {
        return $this->zoneId;
    }

    /**
     * Set creationDtm.
     *
     * @param string|null $creationDtm
     *
     * @return ItsWsSiTambon
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;
    
        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return string|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsWsSiTambon
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;
    
        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsWsSiTambon
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;
    
        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsWsSiTambon
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;
    
        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }
}
