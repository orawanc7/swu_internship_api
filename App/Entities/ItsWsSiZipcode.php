<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItsWsSiZipcode
 *
 * @ORM\Table(name="ITS_WS_SI_ZIPCODE")
 * @ORM\Entity
 */
class ItsWsSiZipcode
{
    /**
     * @var int
     *
     * @ORM\Column(name="ZIPCODE_ID", type="integer", nullable=false, options={"comment"="ลำดับไปรษณีย์ (autorun)"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ITS_WS_SI_ZIPCODE_ZIPCODE_ID_s", allocationSize=1, initialValue=1)
     */
    private $zipcodeId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROVINCE_ID", type="string", length=2, nullable=true, options={"comment"="รหัสจังหวัด"})
     */
    private $provinceId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="AMPHUR_ID", type="string", length=2, nullable=true, options={"comment"="รหัสอำเภอ"})
     */
    private $amphurId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TAMBON_ID", type="string", length=2, nullable=true, options={"comment"="รหัสตำบล"})
     */
    private $tambonId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REFERENCE_CODE", type="string", length=6, nullable=true, options={"comment"="รหัสอ้างอิง (จังหวัด+อำเภอ+ตำบล)"})
     */
    private $referenceCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ZIPCODE", type="string", length=5, nullable=true, options={"comment"="เลขที่ไปรษณีย์"})
     */
    private $zipcode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROVINCE_NAME", type="string", length=250, nullable=true, options={"comment"="ชื่อจังหวัด"})
     */
    private $provinceName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="AMPHUR_NAME", type="string", length=250, nullable=true, options={"comment"="ชื่ออำเภอ"})
     */
    private $amphurName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TAMBON_NAME", type="string", length=250, nullable=true, options={"comment"="ชื่อตำบล"})
     */
    private $tambonName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_DTM", type="string", length=1, nullable=true, options={"comment"="วันเวลาที่บันทึกข้อมูล"})
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=1, nullable=true, options={"comment"="ผู้บันทึกข้อมูล"})
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="datetime", nullable=true, options={"comment"="วันเวลาที่แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=3, nullable=true, options={"comment"="ผู้แก้ไขข้อมูลล่าสุด"})
     */
    private $lastUpdateBy;



    /**
     * Get zipcodeId.
     *
     * @return int
     */
    public function getZipcodeId()
    {
        return $this->zipcodeId;
    }

    /**
     * Set provinceId.
     *
     * @param string|null $provinceId
     *
     * @return ItsWsSiZipcode
     */
    public function setProvinceId($provinceId = null)
    {
        $this->provinceId = $provinceId;
    
        return $this;
    }

    /**
     * Get provinceId.
     *
     * @return string|null
     */
    public function getProvinceId()
    {
        return $this->provinceId;
    }

    /**
     * Set amphurId.
     *
     * @param string|null $amphurId
     *
     * @return ItsWsSiZipcode
     */
    public function setAmphurId($amphurId = null)
    {
        $this->amphurId = $amphurId;
    
        return $this;
    }

    /**
     * Get amphurId.
     *
     * @return string|null
     */
    public function getAmphurId()
    {
        return $this->amphurId;
    }

    /**
     * Set tambonId.
     *
     * @param string|null $tambonId
     *
     * @return ItsWsSiZipcode
     */
    public function setTambonId($tambonId = null)
    {
        $this->tambonId = $tambonId;
    
        return $this;
    }

    /**
     * Get tambonId.
     *
     * @return string|null
     */
    public function getTambonId()
    {
        return $this->tambonId;
    }

    /**
     * Set referenceCode.
     *
     * @param string|null $referenceCode
     *
     * @return ItsWsSiZipcode
     */
    public function setReferenceCode($referenceCode = null)
    {
        $this->referenceCode = $referenceCode;
    
        return $this;
    }

    /**
     * Get referenceCode.
     *
     * @return string|null
     */
    public function getReferenceCode()
    {
        return $this->referenceCode;
    }

    /**
     * Set zipcode.
     *
     * @param string|null $zipcode
     *
     * @return ItsWsSiZipcode
     */
    public function setZipcode($zipcode = null)
    {
        $this->zipcode = $zipcode;
    
        return $this;
    }

    /**
     * Get zipcode.
     *
     * @return string|null
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set provinceName.
     *
     * @param string|null $provinceName
     *
     * @return ItsWsSiZipcode
     */
    public function setProvinceName($provinceName = null)
    {
        $this->provinceName = $provinceName;
    
        return $this;
    }

    /**
     * Get provinceName.
     *
     * @return string|null
     */
    public function getProvinceName()
    {
        return $this->provinceName;
    }

    /**
     * Set amphurName.
     *
     * @param string|null $amphurName
     *
     * @return ItsWsSiZipcode
     */
    public function setAmphurName($amphurName = null)
    {
        $this->amphurName = $amphurName;
    
        return $this;
    }

    /**
     * Get amphurName.
     *
     * @return string|null
     */
    public function getAmphurName()
    {
        return $this->amphurName;
    }

    /**
     * Set tambonName.
     *
     * @param string|null $tambonName
     *
     * @return ItsWsSiZipcode
     */
    public function setTambonName($tambonName = null)
    {
        $this->tambonName = $tambonName;
    
        return $this;
    }

    /**
     * Get tambonName.
     *
     * @return string|null
     */
    public function getTambonName()
    {
        return $this->tambonName;
    }

    /**
     * Set creationDtm.
     *
     * @param string|null $creationDtm
     *
     * @return ItsWsSiZipcode
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;
    
        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return string|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return ItsWsSiZipcode
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;
    
        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return ItsWsSiZipcode
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;
    
        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return ItsWsSiZipcode
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;
    
        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }
}
