<?php

namespace App\Repositories;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;

class BaseRepository
{

    protected $entity;

    public function __construct()
    {
        $isDevMode = false;
        $dbParam = array(
            'url' => getenv("DB_URI")
        );

        $entitiesPath = array(BASE_PATH . "/App/Entities");
        $proxiesPath = BASE_PATH . "/bootstrap/proxies";//


        $config = Setup::createAnnotationMetadataConfiguration($entitiesPath, $isDevMode, null, null, false);

        $config->addCustomNumericFunction('ROUND', 'App\Classes\DQL\Round');
        $config->addCustomNumericFunction('FLOOR', 'App\Classes\DQL\Floor');
        $config->addCustomNumericFunction('TO_NUMBER', 'App\Classes\DQL\CastNumber');
        $config->setProxyDir($proxiesPath);//
        $config->setProxyNamespace('GeneratedProxies');//

        //
        if ($isDevMode) {
            $config->setAutoGenerateProxyClasses(true);
        } else {
            $config->setAutoGenerateProxyClasses(false);
        }
        //

        $this->entity = EntityManager::create($dbParam, $config);

        $dbh = $this->entity->getConnection();
        $sth = $dbh->prepare("ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD HH24:MI:SS'");
        $sth->execute();
    }

    public function paginate($dql, $start = 1, $limit = 10)
    {
        $query = $dql
            ->setFirstResult($start) // Offset
            ->setMaxResults($limit); // Limit

        $paginator = new Paginator($query);

        return $paginator;
    }

    public function getEntityManager()
    {
        return $this->entity;
    }
}
