<?php

namespace App\Repositories;

use App\Entities\ItsDocDownload;
use App\Entities\ItsDocGroupDownload;

class DocDownloadRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }
    
    public function list() {
        
        $query =  $this->em->createQueryBuilder()
        ->select (
            '
            d.docDownloadId,d.docDownloadNameTh,d.docDownloadNameEn,d.activeFlag,d.seqId,
            g.docGroupDownloadId,g.docGroupDownloadNameTh
            '            
        )
        ->from(ItsDocDownload::class,'d')                  
        ->leftJoin(ItsDocGroupDownload::class, 'g', 'WITH', 'd.docGroupDownloadId = g.docGroupDownloadId')
        ->orderBy('g.docGroupDownloadId', 'ASC')        
        ->addOrderBy('d.docDownloadId', 'ASC')        
        ->getQuery();        

        return $query->getResult();         
    }

    public function getActive() {
        
        $query =  $this->em->createQueryBuilder()
        ->select (
            '
            d.docDownloadId,d.docDownloadNameTh,d.docDownloadNameEn,d.activeFlag,d.fileExtension,
            g.docGroupDownloadId,g.docGroupDownloadNameTh
            '            
        )
        ->from(ItsDocDownload::class,'d')                  
        ->leftJoin(ItsDocGroupDownload::class, 'g', 'WITH', 'd.docGroupDownloadId = g.docGroupDownloadId')
        ->where ('d.activeFlag = :activeFlag')
        ->setParameter('activeFlag','Y')            
        ->orderBy('g.seqId', 'ASC')        
        ->addOrderBy('d.seqId', 'ASC')        
        ->getQuery();        

        return $query->getResult();         
    }

    public function get($id) {    
        
        $query =  $this->em->createQueryBuilder()
        ->select (
            '
            d.docDownloadId,d.docDownloadNameTh,d.docDownloadNameEn,d.activeFlag,d.remark,d.fileExtension,d.seqId,
            g.docGroupDownloadId,g.docGroupDownloadNameTh
            '            
        )
        ->from(ItsDocDownload::class,'d')                  
        ->leftJoin(ItsDocGroupDownload::class, 'g', 'WITH', 'd.docGroupDownloadId = g.docGroupDownloadId')
        ->where ('d.docDownloadId = :docDownloadId')
        ->setParameter('docDownloadId',(int) $id)            
        ->getQuery();

        return  $query->getOneOrNullResult();
    }

    public function save($key,$data) {   
        
        try {
            $rep = $this->em->getRepository(ItsDocDownload::class);
            $docDownload = $rep->findOneBy(['docDownloadId'=>$key]);
            
            
            //Update
            if ($docDownload!=null) {                 
                $docDownload->setDocDownloadId($data['docDownloadId']);
                $docDownload->setDocGroupDownloadId($data['docGroupDownloadId']);
                $docDownload->setDocDownloadNameTh($data['docDownloadNameTh']);
                $docDownload->setDocDownloadNameEn($data['docDownloadNameEn']);
                $docDownload->setActiveFlag($data['activeFlag']);                

                $this->em->merge($docDownload);
                $this->em->flush();        
            } else {
            //Insert                                 
                $docDownload = new ItsDocDownload();                        
                $docDownload->setDocDownloadId($data['docDownloadId']);
                $docDownload->setDocGroupDownloadId($data['docGroupDownloadId']);
                $docDownload->setDocDownloadNameTh($data['docDownloadNameTh']);
                $docDownload->setDocDownloadNameEn($data['docDownloadNameEn']);
                $docDownload->setActiveFlag($data['activeFlag']);                

                $this->em->persist($docDownload);
                $this->em->flush();      
            }
        } catch (\Throwable $th) {
            throw $th;
        }        
        
    }

    public function saveFileExtension ($key,$fileExtension) {   
        
        try {
            $rep = $this->em->getRepository(ItsDocDownload::class);
            $docDownload = $rep->findOneBy(['docDownloadId'=>$key]);
                        
            //Update
            if ($docDownload!=null) {                 
                $docDownload->setFileExtension($fileExtension);
                
                $this->em->merge($docDownload);
                $this->em->flush();        
            }
        } catch (\Throwable $th) {
            throw $th;
        }        
        
    }

    public function delete($key) {   
        try {
            $rep = $this->em->getRepository(ItsDocDownload::class);
            $docDownloadType = $rep->findOneBy(['docDownloadId'=>$key]);
            //Update
            if ($docDownloadType!=null) {                 
                $this->em->remove($docDownloadType);
                $this->em->flush();        
            } 
        } catch (\Throwable $th) {
            throw $th;
        }                
    }

}