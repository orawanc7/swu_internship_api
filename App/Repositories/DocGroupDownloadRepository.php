<?php

namespace App\Repositories;

use App\Entities\ItsDocGroupDownload;

class DocGroupDownloadRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }
    
    public function list() {
        $query =  $this->em->createQueryBuilder()
        ->select (
            'a.docGroupDownloadId,a.docGroupDownloadNameTh,a.docGroupDownloadNameEn,a.activeFlag,a.seqId    
            '       
        )
        ->from(ItsDocGroupDownload::class,'a')          
        ->orderBy('a.seqId', 'ASC')        
        ->getQuery();        

        return $query->getResult();         
    }

    public function get($id) {    
        
        $query =  $this->em->createQueryBuilder()
        ->select (
            'a.docGroupDownloadId,a.docGroupDownloadNameTh,a.docGroupDownloadNameEn,a.activeFlag,a.seqId'            
        )
        ->from(ItsDocGroupDownload::class,'a')          
        ->where ('a.docGroupDownloadId = :docGroupDownloadId')
        ->setParameter('docGroupDownloadId',(int) $id)            
        ->getQuery();

        return  $query->getOneOrNullResult();
    }

    public function save($key,$data) {   
        
        try {
            $rep = $this->em->getRepository(ItsDocGroupDownload::class);
            $docGroupDownload = $rep->findOneBy(['docGroupDownloadId'=>$key]);
            //Update
            if ($docGroupDownload!=null) {                 
                $this->em->merge($data);
                $this->em->flush();        
            } else {
            //Insert                 
                $this->em->persist($data);
                $this->em->flush();      
            }
        } catch (\Throwable $th) {
            throw $th;
        }        
        
    }

    public function delete($key) {   
        try {
            $rep = $this->em->getRepository(ItsDocGroupDownload::class);
            $docGroupDownload = $rep->findOneBy(['docGroupDownloadId'=>$key]);
            //Update
            if ($docGroupDownload!=null) {                 
                $this->em->remove($docGroupDownload);
                $this->em->flush();        
            } 
        } catch (\Throwable $th) {
            throw $th;
        }                
    }

}