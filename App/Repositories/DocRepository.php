<?php

namespace App\Repositories;

use App\Entities\ItsCDocMaster;
use App\Entities\ItsCDocType;
use App\Entities\ItsFrmHdr;

class DocRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }
    
    public function list() {
        // return $this->getEntityManager()->getRepository(ItsCDocMaster::class)
        // ->findBy([], ['docTypeId' => 'ASC']);

        $query =  $this->em->createQueryBuilder()
        ->select (
            '
            a.docId,a.docNameTh,a.docNameEn,a.docCdTh,a.docCdEn,a.docFinalFlag,a.activeFlag,a.remark,
            d.docTypeId,d.docTypeTh
            '            
        )
        ->from(ItsCDocMaster::class,'a')          
        ->innerJoin('a.docType', 'd')     
        ->orderBy('a.docId', 'ASC')        
        ->getQuery();        

        return $query->getResult();         
    }

    public function get($id) {    
        
        $query =  $this->em->createQueryBuilder()
        ->select (
            '
            d.docId,d.docNameTh,d.docNameEn,d.docCdTh,d.docCdEn,d.docFinalFlag,d.activeFlag,d.remark,
            dt.docTypeId,dt.docTypeTh,
            f.frmTemplate
            '           
        )
        ->from(ItsCDocMaster::class,'d')      
        ->innerJoin('d.docType', 'dt')        
        ->leftJoin(ItsFrmHdr::class,'f','WITH','d.docId = f.docId')   
        ->where ('d.docId = :docId')
        ->setParameter('docId',(int) $id)            
        ->getQuery();

        return  $query->getOneOrNullResult();
    }

    public function save($key,$data) {   
        
        try {
            $rep = $this->em->getRepository(ItsCDocMaster::class);
            $doc = $rep->findOneBy(['docId'=>$key]);

            $repDocType = $this->em->getRepository(ItsCDocType::class);
            $docType = $repDocType->findOneBy(['docTypeId'=>$data['docTypeId']]);

            //Update
            if ($doc!=null) {                 
                $doc->setDocId($data['docId']);
                $doc->setDocType($docType);
                $doc->setDocNameTh($data['docNameTh']);
                $doc->setDocNameEn($data['docNameEn']);
                $doc->setDocCdTh($data['docCdTh']);
                $doc->setDocCdEn($data['docCdEn']);
                $doc->setDocFinalFlag($data['docFinalFlag']);
                $doc->setActiveFlag($data['activeFlag']);
                $doc->setRemark($data['remark']);

                $this->em->merge($doc);
                $this->em->flush();        
            } else {
            //Insert                                 
                $doc = new ItsCDocMaster();                        
                $doc->setDocId($data['docId']);
                $doc->setDocType($docType);
                $doc->setDocNameTh($data['docNameTh']);
                $doc->setDocNameEn($data['docNameEn']);
                $doc->setDocCdTh($data['docCdTh']);
                $doc->setDocCdEn($data['docCdEn']);
                $doc->setDocFinalFlag($data['docFinalFlag']);
                $doc->setActiveFlag($data['activeFlag']);
                $doc->setRemark($data['remark']);                

                $this->em->persist($doc);
                $this->em->flush();      
            }
        } catch (\Throwable $th) {
            throw $th;
        }        
        
    }

    public function delete($key) {   
        try {
            $rep = $this->em->getRepository(ItsCDocMaster::class);
            $docType = $rep->findOneBy(['docId'=>$key]);
            //Update
            if ($docType!=null) {                 
                $this->em->remove($docType);
                $this->em->flush();        
            } 
        } catch (\Throwable $th) {
            throw $th;
        }                
    }

}