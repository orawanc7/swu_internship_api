<?php

namespace App\Repositories;

use App\Entities\ItsCDocType;

class DocTypeRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }
    
    public function list() {
        // return $this->getEntityManager()->getRepository(ItsCDocType::class)
        // ->findBy([], ['docTypeId' => 'ASC']);

        $query =  $this->em->createQueryBuilder()
        ->select (
            'a.docTypeId,a.docTypeTh,a.docTypeEn,a.activeFlag,a.docScreenFlag,a.remark'            
        )
        ->from(ItsCDocType::class,'a')          
        ->orderBy('a.docTypeId', 'ASC')        
        ->getQuery();        

        return $query->getResult();         
    }

    public function get($id) {    
        
        $query =  $this->em->createQueryBuilder()
        ->select (
            'a.docTypeId,a.docTypeTh,a.docTypeEn,a.activeFlag,a.docScreenFlag,a.remark'
        )
        ->from(ItsCDocType::class,'a')          
        ->where ('a.docTypeId = :docTypeId')
        ->setParameter('docTypeId',(int) $id)            
        ->getQuery();

        return  $query->getOneOrNullResult();
    }

    public function save($key,$data) {   
        
        try {
            $rep = $this->em->getRepository(ItsCDocType::class);
            $docType = $rep->findOneBy(['docTypeId'=>$key]);
            //Update
            if ($docType!=null) {                 
                $this->em->merge($data);
                $this->em->flush();        
            } else {
            //Insert                 
                $this->em->persist($data);
                $this->em->flush();      
            }
        } catch (\Throwable $th) {
            throw $th;
        }        
        
    }

    public function delete($key) {   
        try {
            $rep = $this->em->getRepository(ItsCDocType::class);
            $docType = $rep->findOneBy(['docTypeId'=>$key]);
            //Update
            if ($docType!=null) {                 
                $this->em->remove($docType);
                $this->em->flush();        
            } 
        } catch (\Throwable $th) {
            throw $th;
        }                
    }

}