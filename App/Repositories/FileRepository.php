<?php

namespace App\Repositories;

use App\Entities\ItsKeyConfig;
use App\Entities\ItsKeyLastnumber;

class FileRepository extends BaseRepository
{
    private $em;    

    public function __construct($em = null)
    {
        if ($em == null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();
        } else {
            $this->em = $em;
        }
    }

    public function getLastFileId($keyGen)
    {
        $keyCd = 'ATTACH';
        
        
        // Lock Table
        $query =  $this->em->createQueryBuilder()
            ->update(ItsKeyLastnumber::class, 'l')
            ->set('l.lastNo', 'l.lastNo')
            ->where('l.keyCd = :keyCd')
            ->andWhere('l.keyGen = :keyGen')
            ->setParameter('keyCd', $keyCd)
            ->setParameter('keyGen', $keyGen)
            ->getQuery();
        $query->execute();        

        $repConfig = $this->em->getRepository(ItsKeyConfig::class);
        $keyConfig = $repConfig->findOneBy(
            [
                'keyCd' => $keyCd
            ]
        );   

        $repLast = $this->em->getRepository(ItsKeyLastnumber::class);
        $key = $repLast->findOneBy(
            [
                'keyCd' => $keyCd,
                'keyGen' => $keyGen
            ]
        );

        $lastNo = 1;


        if ($key != null) {
            $lastNo = $key->getLastNo();

            $lastNo += 1;

            $key->setLastNo($lastNo);

            $this->em->merge($key);
            $this->em->flush();
        } else {
            $key = new ItsKeyLastnumber();
            $key->setKeyCd($keyConfig);
            $key->setKeyGen($keyGen);
            $key->setLastNo($lastNo);

            $this->em->persist($key);
            $this->em->flush();
        }

        if ($keyConfig->getKeyGenFlag() == "Y") {
            $runningNumber = $keyConfig->getRunningNumber();

            $lastNoFormat = str_pad($lastNo, $runningNumber, "0", STR_PAD_LEFT);

            return $keyGen . $lastNoFormat;
        } else {
            return $lastNo;
        }
    }
}
