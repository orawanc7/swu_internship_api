<?php

namespace App\Repositories;

use App\Entities\ItsFrmHdr;
use App\Entities\ItsCDocMaster;

class FrmRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }
    
    public function getAll() {
        
        $query =  $this->em->createQueryBuilder()
        ->select (
            '
            a.frmHdrId,a.frmNameTh,a.frmNameEn,a.activeFlag,a.remark,
            d.docId,d.docNameTh
            '            
        )
        ->from(ItsFrmHdr::class,'a')          
        ->innerJoin(ItsCDocMaster::class, 'd', 'WITH', 'a.docId = d.docId')
        ->orderBy('a.frmHdrId', 'ASC')        
        ->getQuery();        

        return $query->getResult();         
    }

    public function get($id) {    
        
        $query =  $this->em->createQueryBuilder()
        ->select (
            '
            a.frmHdrId,a.frmNameTh,a.frmNameEn,a.activeFlag,a.remark,
            d.docId,d.docNameTh
            '
        )
        ->from(ItsFrmHdr::class,'a')          
        ->innerJoin(ItsCDocMaster::class, 'd', 'WITH', 'a.docId = d.docId')
        ->where ('a.frmHdrId = :frmHdrId')
        ->setParameter('frmHdrId',(int) $id)            
        ->getQuery();

        return  $query->getOneOrNullResult();
    }

    public function getTemplate($id) {    
        
        $query =  $this->em->createQueryBuilder()
        ->select (
            '
            a.frmHdrId,a.frmTemplate
            '
        )
        ->from(ItsFrmHdr::class,'a')       
        ->where ('a.frmHdrId = :frmHdrId')   
        ->setParameter('frmHdrId',(int) $id)            
        ->getQuery();

        return  $query->getOneOrNullResult();
    }

    public function save($key,$data) {   
        
        try {
            $rep = $this->em->getRepository(ItsFrmHdr::class);
            $frm = $rep->findOneBy(['frmHdrId'=>$key]);
            

            //Update
            if ($frm!=null) {                 
                $frm->setDocId($data['docId']);
                $frm->setFrmNameTh($data['frmNameTh']);
                $frm->setFrmNameEn($data['frmNameEn']);            
                $frm->setActiveFlag($data['activeFlag']);
                $frm->setRemark($data['remark']);

                $this->em->merge($frm);
                $this->em->flush();        
            } else {
            //Insert                                 
                $frm = new ItsFrmHdr();                        
                $frm->setDocId($data['docId']);
                $frm->setFrmNameTh($data['frmNameTh']);
                $frm->setFrmNameEn($data['frmNameEn']);                
                $frm->setActiveFlag($data['activeFlag']);
                $frm->setRemark($data['remark']);        

                $this->em->persist($frm);
                $this->em->flush();      

                return $frm->getFrmHdrId();
            }
        } catch (\Throwable $th) {
            throw $th;
        }        
        
    }


    public function saveTemplate ($key,$template) {           
        try {
            $rep = $this->em->getRepository(ItsFrmHdr::class);
            $frm = $rep->findOneBy(['frmHdrId'=>$key]);            

            //Update
            if ($frm!=null) {                 
                $frm->setFrmTemplate($template);
                
                $this->em->merge($frm);
                $this->em->flush();        
            }
        } catch (\Throwable $th) {
            throw $th;
        }        
        
    }

    public function delete($key) {   
        try {
            $rep = $this->em->getRepository(ItsFrmHdr::class);
            $frm = $rep->findOneBy(['frmHdrId'=>$key]);
            //Update
            if ($frm!=null) {                 
                $this->em->remove($frm);
                $this->em->flush();        
            } 
        } catch (\Throwable $th) {
            throw $th;
        }                
    }

}