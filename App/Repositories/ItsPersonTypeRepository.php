<?php

namespace App\Repositories;

use App\Entities\ItsPersonMaster;
use App\Entities\ItsPersonType;

class ItsPersonTypeRepository extends BaseRepository
{
    private $em;

    public function __construct($em = null)
    {
        if ($em == null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();
        } else {
            $this->em = $em;
        }
    }

    public function getMentorByItsPersonId($itsPersonId)
    {
        $personTypeMentor = getenv('PERSON_TYPE_MENTOR');        

        $query =  $this->em->createQueryBuilder()
            ->select(
                '
                pt.itsPersonTypeId,
                p.itsPersonId
                '
            )
            ->from(ItsPersonType::class, 'pt')
            ->leftJoin(ItsPersonMaster::class, 'p', 'WITH', 'pt.itsPersonId = p.itsPersonId')
            ->where('pt.itsPersonId = :itsPersonId')
            ->andWhere('pt.personTypeId = :personTypeId')
            ->setParameter('itsPersonId', $itsPersonId)
            ->setParameter('personTypeId', $personTypeMentor)
            ->getQuery();

        return  $query->getOneOrNullResult();        
    }
}
