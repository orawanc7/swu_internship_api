<?php

namespace App\Repositories;

use App\Entities\ItsCPersonGroup;

class PersonGroupRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }
    
    public function list() {
        $query =  $this->em->createQueryBuilder()
        ->select (
            'a.personGroupId,a.personGroupNameTh,a.personGroupNameEn,a.activeFlag,a.remark'            
        )
        ->from(ItsCPersonGroup::class,'a')          
        ->orderBy('a.personGroupId', 'ASC')        
        ->getQuery();        

        return $query->getResult();         
    }

    public function get($id) {    
        
        $query =  $this->em->createQueryBuilder()
        ->select (
            'a.personGroupId,a.personGroupNameTh,a.personGroupNameEn,a.activeFlag,a.remark'            
        )
        ->from(ItsCPersonGroup::class,'a')          
        ->where ('a.personGroupId = :personGroupId')
        ->setParameter('personGroupId',(int) $id)            
        ->getQuery();

        return  $query->getOneOrNullResult();
    }

    public function save($key,$data) {   
        
        try {
            $rep = $this->em->getRepository(ItsCPersonGroup::class);
            $personGroup = $rep->findOneBy(['personGroupId'=>$key]);
            //Update
            if ($personGroup!=null) {                 
                $this->em->merge($data);
                $this->em->flush();        
            } else {
            //Insert                 
                $this->em->persist($data);
                $this->em->flush();      
            }
        } catch (\Throwable $th) {
            throw $th;
        }        
        
    }

    public function delete($key) {   
        try {
            $rep = $this->em->getRepository(ItsCPersonGroup::class);
            $personGroup = $rep->findOneBy(['personGroupId'=>$key]);
            //Update
            if ($personGroup!=null) {                 
                $this->em->remove($personGroup);
                $this->em->flush();        
            } 
        } catch (\Throwable $th) {
            throw $th;
        }                
    }

}