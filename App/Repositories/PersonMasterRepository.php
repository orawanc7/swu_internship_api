<?php

namespace App\Repositories;

use App\Entities\ItsPersonMaster;
use App\Entities\ItsPersonType;
use App\Entities\ItsWsCDepartment;
use App\Entities\ItsWsCPrename;

class PersonMasterRepository extends BaseRepository
{
    private $em;

    public function __construct($em = null)
    {
        if ($em == null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();
        } else {
            $this->em = $em;
        }
    }

    public function get($itsPersonId)
    {
        $query =  $this->em->createQueryBuilder()
            ->select('
                pm.personId,pm.itsPersonId,pm.personFnameTh,pm.personLnameTh,pm.telephoneNo,pm.mobileNo,pm.otherEmail,
                p.prenameCd, p.prenameSnameTh
            ')
            ->from(ItsPersonMaster::class, 'pm')
            ->leftJoin(ItsWsCPrename::class, 'p', 'WITH', 'pm.prenameIntThCd = p.prenameCd')
            ->where('pm.itsPersonId = :itsPersonId')
            ->setParameter('itsPersonId', $itsPersonId)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    public function getExternalByName($name)
    {
        $query =  $this->em->createQueryBuilder()
            ->select('
                pm.personId,pm.itsPersonId,pm.personFnameTh,pm.personLnameTh,pm.telephoneNo,pm.mobileNo,
                p.prenameSnameTh
            ')
            ->from(ItsPersonMaster::class, 'pm')
            ->leftJoin(ItsWsCPrename::class, 'p', 'WITH', 'pm.prenameIntThCd = p.prenameCd')
            ->where('CONCAT(pm.personFnameTh, \' \', pm.personLnameTh) LIKE :name')
            ->andWhere('pm.extPersonFlag = :extPersonFlag')
            ->setParameter('name', '%' . $name . '%')
            ->setParameter('extPersonFlag', 'Y')
            ->getQuery();

        return $query->getResult();
    }

    public function getExternalByDeptAndName($deptCd, $name)
    {
        $query =  $this->em->createQueryBuilder()
            ->select('
                pm.personId,pm.itsPersonId,pm.personFnameTh,pm.personLnameTh,pm.telephoneNo,pm.mobileNo,
                p.prenameSnameTh
            ')
            ->from(ItsPersonMaster::class, 'pm')
            ->leftJoin(ItsWsCPrename::class, 'p', 'WITH', 'pm.prenameIntThCd = p.prenameCd')
            ->where('CONCAT(pm.personFnameTh, \' \', pm.personLnameTh) LIKE :name')
            ->andWhere('pm.deptCd = :deptCd')
            ->andWhere('pm.extPersonFlag = :extPersonFlag')
            ->setParameter('name', '%' . $name . '%')
            ->setParameter('extPersonFlag', 'Y')
            ->setParameter('deptCd', $deptCd)
            ->getQuery();

        return $query->getResult();
    }

    public function getTeacherByPersonId($personId)
    {
        $subquery = $this->em->createQueryBuilder()
            ->select('pt')
            ->from(ItsPersonType::class, 'pt')
            ->where('pt.itsPersonId = pm.itsPersonId')
            ->andWhere('pt.personTypeId IN (:personTypeId)');

        $personType = [
            getEnv('PERSON_TYPE_TEACHER'),
            getEnv('PERSON_TYPE_MENTOR')
        ];
        

        $query =  $this->em->createQueryBuilder()
            ->select('
                pm.personId,pm.itsPersonId,pm.personFnameTh,pm.personLnameTh,pm.telephoneNo,pm.mobileNo,pm.buasriId,pm.gafeEmail,pm.otherEmail,pm.addressDesc,pm.genderFlag,pm.themeId,
                p.prenameSnameTh,
                d.deptLnameTh,
                in_d.deptLnameTh indeptLnameTh
            ')
            ->from(ItsPersonMaster::class, 'pm')
            ->leftJoin(ItsWSCPrename::class, 'p', 'WITH', 'pm.prenameIntThCd = p.prenameCd')
            ->leftJoin(ItsWsCDepartment::class, 'd', 'WITH', 'pm.deptCd = d.deptCd')
            ->leftJoin(ItsWsCDepartment::class, 'in_d', 'WITH', '(FLOOR(pm.deptCd/100))*100 = in_d.deptCd')
            ->where('pm.personId IN (:personId) ')
            ->andWhere(
                $subquery->expr()->exists(
                    $subquery->getDql()
                )
            )
            ->setParameter('personId', $personId)
            ->setParameter('personTypeId', $personType)
            ->getQuery();

            

        return $query->getOneOrNullResult();
    }

    public function getTeacherByBuasriId($buasriId)
    {        
        $subquery = $this->em->createQueryBuilder()
            ->select('pt')
            ->from(ItsPersonType::class, 'pt')
            ->where('pt.itsPersonId = pm.itsPersonId')
            ->andWhere('pt.personTypeId IN (:personTypeId)');

        $personType = [
            getEnv('PERSON_TYPE_TEACHER'),
            getEnv('PERSON_TYPE_MENTOR')
        ];
        

        $query =  $this->em->createQueryBuilder()
            ->select('
                pm.personId,pm.itsPersonId,pm.personFnameTh,pm.personLnameTh,pm.telephoneNo,pm.mobileNo,pm.buasriId,pm.gafeEmail,pm.otherEmail,pm.addressDesc,pm.genderFlag,pm.themeId,
                p.prenameSnameTh,
                d.deptLnameTh,
                in_d.deptLnameTh indeptLnameTh
            ')
            ->from(ItsPersonMaster::class, 'pm')
            ->leftJoin(ItsWSCPrename::class, 'p', 'WITH', 'pm.prenameIntThCd = p.prenameCd')
            ->leftJoin(ItsWsCDepartment::class, 'd', 'WITH', 'pm.deptCd = d.deptCd')
            ->leftJoin(ItsWsCDepartment::class, 'in_d', 'WITH', '(FLOOR(pm.deptCd/100))*100 = in_d.deptCd')
            ->where('pm.buasriId IN (:buasriId) ')
            ->andWhere(
                $subquery->expr()->exists(
                    $subquery->getDql()
                )
            )
            ->setParameter('buasriId', $buasriId)
            ->setParameter('personTypeId', $personType)
            ->getQuery();

        return $query->getOneOrNullResult();
    }    
    
    public function getPersonTypeByItsPersonId($itsPersonId)
    {        
        $query =  $this->em->createQueryBuilder()
            ->select('
                pt.personTypeId
            ')
            ->from(ItsPersonType::class, 'pt')            
            ->where('pt.itsPersonId = :itsPersonId ')
            ->setParameter('itsPersonId', $itsPersonId)            
            ->getQuery();

        return $query->getResult();
    }  

    public function save($data)
    {                
        try {
            $repPerson = $this->em->getRepository(ItsPersonMaster::class);
            $person = $repPerson->findOneBy(
                [
                    'itsPersonId' => $data->getItsPersonId()
                ]
            );
            //Update
            if ($person != null) {
                $person->setOtherEmail($data->getOtherEmail());
                $person->setMobileNo($data->getMobileNo());
                $person->setTelephoneNo($data->getTelephoneNo());

                $this->em->merge($person);
                $this->em->flush();

            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

}
