<?php

namespace App\Repositories;

use App\Entities\ItsCPersonType;
use App\Entities\ItsCPersonGroup;

class PersonTypeRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }
    
    public function list() {
        
        $query =  $this->em->createQueryBuilder()
        ->select (
            '
            a.personTypeId,a.personTypeNameTh,a.personTypeNameEn,a.activeFlag,a.remark,
            p.personGroupId,p.personGroupNameTh
            '            
        )
        ->from(ItsCPersonType::class,'a')                  
        ->leftJoin(ItsCPersonGroup::class, 'p', 'WITH', 'a.personGroupId = p.personGroupId')
        ->orderBy('a.personTypeId', 'ASC')        
        ->getQuery();        

        return $query->getResult();         
    }

    public function get($id) {    
        
        $query =  $this->em->createQueryBuilder()
        ->select (
            '
            a.personTypeId,a.personTypeNameTh,a.personTypeNameEn,a.activeFlag,a.remark,
            p.personGroupId,p.personGroupNameTh
            '            
        )
        ->from(ItsCPersonType::class,'a')                  
        ->leftJoin(ItsCPersonGroup::class, 'p', 'WITH', 'a.personGroupId = p.personGroupId')
        ->where ('a.personTypeId = :personTypeId')
        ->setParameter('personTypeId',(int) $id)            
        ->getQuery();

        return  $query->getOneOrNullResult();
    }

    public function save($key,$data) {   
        
        try {
            $rep = $this->em->getRepository(ItsCPersonType::class);
            $personType = $rep->findOneBy(['personTypeId'=>$key]);
            
            //Update
            if ($personType!=null) {                 
                $personType->setPersonTypeId($data['personTypeId']);
                $personType->setPersonGroupId($data['personGroupId']);
                $personType->setPersonTypeNameTh($data['personTypeNameTh']);
                $personType->setPersonTypeNameEn($data['personTypeNameEn']);
                $personType->setActiveFlag($data['activeFlag']);
                $personType->setRemark($data['remark']);

                $this->em->merge($personType);
                $this->em->flush();        
            } else {
            //Insert                                 
                $personType = new ItsCPersonType();                        
                $personType->setPersonTypeId($data['personTypeId']);
                $personType->setPersonGroupId($data['personGroupId']);
                $personType->setPersonTypeNameTh($data['personTypeNameTh']);
                $personType->setPersonTypeNameEn($data['personTypeNameEn']);
                $personType->setActiveFlag($data['activeFlag']);
                $personType->setRemark($data['remark']);    

                $this->em->persist($personType);
                $this->em->flush();      
            }
        } catch (\Throwable $th) {
            throw $th;
        }        
        
    }

    public function delete($key) {   
        try {
            $rep = $this->em->getRepository(ItsCPersonType::class);
            $personTypeType = $rep->findOneBy(['personTypeId'=>$key]);
            //Update
            if ($personTypeType!=null) {                 
                $this->em->remove($personTypeType);
                $this->em->flush();        
            } 
        } catch (\Throwable $th) {
            throw $th;
        }                
    }

}