<?php

namespace App\Repositories;

use App\Entities\ItsCProvince;

class ProvinceRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }
    
    public function list() {
        $query =  $this->em->createQueryBuilder()
        ->select (
            '
            a.provinceId,a.provinceNameTh,a.provinceNameEn,a.activeFlag
            '            
        )
        ->from(ItsCProvince::class,'a')                   
        ->orderBy('a.provinceId', 'ASC')        
        ->getQuery();        

        return $query->getResult();         
    }

    public function get($id) {    
        
        $query =  $this->em->createQueryBuilder()
        ->select (
            '
            a.provinceId,a.provinceNameTh,a.provinceNameEn,a.activeFlag
            '           
        )
        ->from(ItsCProvince::class,'a')              
        ->where ('a.provinceId = :provinceId')
        ->setParameter('provinceId',(int) $id)            
        ->getQuery();

        return  $query->getOneOrNullResult();
    }

    public function save($key,$data) {   
        
        try {
            $rep = $this->em->getRepository(ItsCProvince::class);
            $school = $rep->findOneBy(['provinceId'=>$key]);

            
            //Update
            if ($school!=null) {                 
                $school->setSchoolId($data['provinceId']);
                $school->setSchoolNameTh($data['provinceNameTh']);
                $school->setSchoolNameEn($data['provinceNameEn']);
                $school->setActiveFlag($data['activeFlag']);

                $this->em->merge($school);
                $this->em->flush();        
            } else {
            //Insert                                 
                $doc = new ItsCProvince();                        
                $school->setSchoolId($data['provinceId']);
                $school->setSchoolNameTh($data['provinceNameTh']);
                $school->setSchoolNameEn($data['provinceNameEn']);
                $school->setActiveFlag($data['activeFlag']);  

                $this->em->persist($school);
                $this->em->flush();      
            }
        } catch (\Throwable $th) {
            throw $th;
        }        
        
    }

    public function delete($key) {   
        try {
            $rep = $this->em->getRepository(ItsCProvince::class);
            $school = $rep->findOneBy(['provinceId'=>$key]);
            //Update
            if ($school!=null) {                 
                $this->em->remove($school);
                $this->em->flush();        
            } 
        } catch (\Throwable $th) {
            throw $th;
        }                
    }

}