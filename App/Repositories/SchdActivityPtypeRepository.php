<?php

namespace App\Repositories;

use App\Entities\ItsSchdActivityPtype;

class SchdActivityPtypeRepository extends BaseRepository
{

    private $em;

    public function __construct($em = null)
    {
        if ($em == null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();
        } else {
            $this->em = $em;
        }
    }
    
    public function getByActivity($activityId)
    {
        $query =  $this->em->createQueryBuilder()
            ->select('
                a.activityId,a.personTypeId
            ')
            ->from(ItsSchdActivityPtype::class, 'a')
            ->where('a.activityId = :activityId')
            ->setParameter('activityId', (int)$activityId)
            ->orderBy('a.personTypeId', 'ASC')            
            ->getQuery();

        return  $query->getResult();
    }    
}
