<?php

namespace App\Repositories;

use App\Entities\ItsSchdActivity;
use App\Entities\ItsSchdRound;
use App\Entities\ItsSchdActivityPtype;
use App\Entities\ItsCDocMaster;
use App\Entities\ItsCDocType;
use App\Entities\ItsFrmHdr;


class SchdActivityRepository extends BaseRepository
{

    private $em;

    public function __construct($em = null)
    {
        if ($em == null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();
        } else {
            $this->em = $em;
        }
    }
    
    public function getByRoundActivitySetNo($roundId,$activitySetNo)
    {
        $query =  $this->em->createQueryBuilder()
            ->select('
                a.activityId,a.activitySeqId,a.activityNameTh,a.activityNameEn,
                a.docId,a.refActivityId,
                a.startDate,a.endDate,a.activityFlag,a.activeFlag,a.remark,
                COALESCE(a.refActivityId,0) as orderRefActivityId
            ')
            ->from(ItsSchdActivity::class, 'a')
            ->where('a.roundId = :roundId')
            ->andWhere('a.activitySetNo = :activitySetNo')
            ->setParameter('roundId', (int)$roundId)
            ->setParameter('activitySetNo', $activitySetNo)
            ->orderBy('orderRefActivityId', 'ASC')
            ->addOrderBy('a.activitySeqId', 'ASC')
            ->getQuery();

        return  $query->getResult();
    }

    public function get($id)
    {
        $query =  $this->em->createQueryBuilder()
            ->select('
            r.roundId,r.semCd,r.year,
            a.activityId,a.activitySeqId,a.activityNameTh,a.activityNameEn,
            a.docId,a.refActivityId,a.roundId,
            a.startDate,a.endDate,a.activityFlag,a.activeFlag,a.remark,a.personTypeId,
            d.docNameTh,
            dt.docTypeId,dt.docScreenFlag,
            f.frmTemplate
            ')
            ->from(ItsSchdActivity::class, 'a')
            ->innerJoin(ItsSchdRound::class,'r','WITH','a.roundId = r.roundId')               
            ->leftJoin(ItsCDocMaster::class,'d','WITH','a.docId = d.docId')   
            ->leftJoin(ItsCDocType::class,'dt','WITH','d.docType = dt.docTypeId')   
            ->leftJoin(ItsFrmHdr::class,'f','WITH','d.docId = f.docId')   
            ->where('a.activityId = :activityId')
            ->setParameter('activityId', (int)$id)
            ->getQuery();

        return  $query->getOneOrNullResult();
    }

    
    public function save($key, $data)
    {                
        $activityId = 0;
        try {            
            $this->em->getConnection()->beginTransaction();

            $rep = $this->em->getRepository(ItsSchdActivity::class);
            $activity = $rep->findOneBy(['activityId' => $key]);
            
            //Update
            if ($activity != null) {
                $activity->setRoundId($data['roundId']);
                $activity->setActivitySetNo($data['activitySetNo']);
                $activity->setActivitySeqId($data['activitySeqId']);
                $activity->setActivityNameTh($data['activityNameTh']);
                $activity->setActivityNameEn($data['activityNameTh']);
                $activity->setDocId($data['docId']);
                $activity->setRefActivityId($data['refActivityId']);
                $activity->setStartDate($data['startDate']);          
                $activity->setEndDate($data['endDate']);          
                $activity->setActivityFlag($data['activityFlag']);
                $activity->setActiveFlag($data['activeFlag']);
                $activity->setRemark($data['remark']);   
                
                $this->em->merge($activity);
                $this->em->flush();
            } else {
                //Insert    
                $activity = new ItsSchdActivity();                  
                $activity->setRoundId($data['roundId']);
                $activity->setActivitySetNo($data['activitySetNo']);      
                $activity->setActivitySeqId($data['activitySeqId']);                
                $activity->setActivityNameTh($data['activityNameTh']);
                $activity->setActivityNameEn($data['activityNameTh']);
                $activity->setDocId($data['docId']);
                $activity->setRefActivityId($data['refActivityId']);
                $activity->setStartDate($data['startDate']);          
                $activity->setEndDate($data['endDate']);         
                $activity->setActivityFlag($data['activityFlag']); 
                $activity->setActiveFlag($data['activeFlag']);
                $activity->setRemark($data['remark']);   

                $this->em->persist($activity);
                $this->em->flush();                                
            }

            $activityId  = $activity->getActivityId();                        

            $repPType = $this->em->getRepository(ItsSchdActivityPtype::class);
                        
            $activityPType = $repPType->findBy([
                'activityId' => $activityId
            ]);
            
            if ($activityPType != null) {              
                
                foreach ($activityPType as $a) {                         
                    $this->em->remove($a);
                    $this->em->flush();
                }                
            }
            
            if (strlen($data['personTypeId'])>0) {
                $personType = \explode(",",$data['personTypeId']);
                foreach ($personType as $p) {
                    $activityPType = new ItsSchdActivityPtype();                        
                    $activityPType->setActivityId($activityId);
                    $activityPType->setPersonTypeId($p);
                    
                    $this->em->persist($activityPType);
                    $this->em->flush();        
                }
            }

            $this->em->clear();
            $this->em->getConnection()->commit();

            return $activityId;
        } catch (\Throwable $th) {      
            $this->em->getConnection()->rollBack();      
            throw $th;
        }
    }

    public function saveParent($key, $data)
    {                
        $activityId = 0;
        try {            
            $this->em->getConnection()->beginTransaction();

            $rep = $this->em->getRepository(ItsSchdActivity::class);
            $activity = $rep->findOneBy(['activityId' => $key]);

            $refActivityId = 0;
            if (strlen($data['refActivityId'])>0) {
                $refActivityId = $data['refActivityId'];
            }
           
            //Update
            if ($activity != null) {
                $currentSeqId = $activity->getActivitySeqId();                

                $start = 0;$end = 0;$counter=0;
                if ($data['activitySeqId'] < $currentSeqId) {
                    $start =   $data['activitySeqId'];
                    $end =   $currentSeqId - 1;
                    $counter=1;
                } else {
                    $start =   $currentSeqId+1;
                    $end =   $data['activitySeqId'];
                    $counter=-1;
                }

                $sql = "
                    UPDATE its_schd_activity
                    SET activity_seq_id = activity_seq_id + :counter
                    WHERE activity_seq_id >= :start
                    AND activity_seq_id <= :end
                    AND COALESCE(ref_activity_id,0) = :refActivityId
                ";
                $rowsAffected = $this->em->getConnection()->executeUpdate($sql, [
                    'refActivityId' => (int) $refActivityId,
                    'start' => (int) $start,
                    'end' => (int) $end,
                    'counter' => (int) $counter
                ]);
                               

                $activity->setRefActivityId($data['refActivityId']);
                $activity->setActivitySeqId($data['activitySeqId']);
                                
                $this->em->merge($activity);
                $this->em->flush();
            }
            
            $this->em->clear();
            $this->em->getConnection()->commit();
            return true;
        } catch (\Throwable $th) {       
            $this->em->getConnection()->rollBack();     
            throw $th;
        }
    }

    public function delete($key)
    {
        try {
            $rep = $this->em->getRepository(ItsSchdActivity::class);
            $activity = $rep->findOneBy(['activityId' => $key]);
            
            if ($activity != null) {
                $this->em->remove($activity);
                $this->em->flush();                
            }

        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
