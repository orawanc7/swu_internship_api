<?php

namespace App\Repositories;

use App\Entities\ItsSchdAttach;

class SchdAttachRepository extends BaseRepository
{

    private $em;

    public function __construct($em = null)
    {
        if ($em == null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();
        } else {
            $this->em = $em;
        }
    }
    

    public function get($attachId)
    {                
        $qb = $this->em->createQueryBuilder();
        $query  = $qb->select(
            '
                a.attachId,a.fileId,a.fileName,a.fileExtension,a.filePath,a.attachType,a.linkUrl
            ')
            ->from(ItsSchdAttach::class, 'a')
            ->where('a.attachId = :attachId')                        
            ->setParameter('attachId', (int)$attachId)                        
            ->getQuery();        

        return $query->getOneOrNullResult();
    }  
    
    public function getByRefDocId($refDocId)
    {                
        $qb = $this->em->createQueryBuilder();
        $query  = $qb->select(
            '
                a.attachId,a.fileId,a.fileName,a.fileExtension,a.attachType,a.linkUrl
            ')
            ->from(ItsSchdAttach::class, 'a')
            ->where('a.refDocId = :refDocId')                        
            ->setParameter('refDocId', (int)$refDocId)                        
            ->getQuery();        

        return $query->getResult();
    }  
        
    public function delete($key)
    {
        try {
            $rep = $this->em->getRepository(ItsSchdAttach::class);
            $attach = $rep->findOneBy(['attachId' => $key]);

            if ($attach != null) {
                $filePath = $attach->getFilePath();

                $this->em->remove($attach);
                $this->em->flush();

                return $filePath;
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
