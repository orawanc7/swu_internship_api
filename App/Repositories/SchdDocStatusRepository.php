<?php

namespace App\Repositories;

use App\Entities\ItsSchdDocStatus;
use App\Entities\ItsSchdDocTrans;
use App\Entities\ItsSchdActivityPtype;
use App\Entities\ItsSchdActivity;
use App\Entities\ItsCDocMaster;
use App\Entities\ItsCDocType;
use App\Entities\ItsSchdPersonStd;

class SchdDocStatusRepository extends BaseRepository
{

    private $em;

    public function __construct($em = null)
    {
        if ($em == null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();
        } else {
            $this->em = $em;
        }
    }
    
    public function getByRoundActivitySetNoOwner($roundId,$activitySetNo,$personTypeId,$itsUserId)
    {        
        $qb = $this->em->createQueryBuilder();
        $query =  $this->em->createQueryBuilder()
            ->select('
                a.activityId,a.startDate,a.endDate,
                d.docId,d.docNameTh,d.docNameEn,d.docFinalFlag,
                0 waitCount,
                0 returnCount,
                0 passCount
            ')
            ->from(ItsSchdActivity::class, 'a')
            ->innerJoin(ItsCDocMaster::class,'d','WITH','a.docId = d.docId')     
            ->innerJoin(ItsCDocType::class,'dt','WITH','dt.docTypeId = d.docType')     
            // ->leftJoin(
            //     ItsSchdDocStatus::class,
            //     's',
            //     'WITH',
            //     $qb->expr()->andX(
            //         $qb->expr()->eq('a.activityId', 's.activityId'),
            //         $qb->expr()->eq('s.personTypeId', ':personTypeId'),
            //         $qb->expr()->eq('s.itsUserId', ':itsUserId')
            //     )
            // )    
            ->where('a.roundId = :roundId')        
            ->andWhere('a.activitySetNo = :activitySetNo')    
            ->andWhere('a.personTypeId = :personTypeId')
            ->setParameter('roundId', (int)$roundId)    
            ->setParameter('activitySetNo', $activitySetNo)         
            ->setParameter('personTypeId', (int)$personTypeId)
            ->orderBy('a.startDate', 'ASC')            
            ->addOrderBy('dt.docTypeId', 'ASC')
            ->addOrderBy('d.docId', 'ASC')
            ->getQuery();        

        $result = $query->getResult();

        // SUM(TO_NUMBER (COALESCE(s.statusFlag,\'W\',\'1\',\'0\'))) as waitCount,
        // SUM(TO_NUMBER (COALESCE(s.statusFlag,\'R\',\'1\',\'0\'))) as returnCount,
        // SUM(TO_NUMBER (COALESCE(s.statusFlag,\'Y\',\'1\',\'0\'))) as passCount
        foreach ($result as $r) {            
            $resultSum =  $this->em->createQueryBuilder()
            ->select('
                s.statusFlag                
            ')
            ->from(ItsSchdDocStatus::class, 's')            
            ->where('s.activityId = :activityId')            
            ->andWhere('s.personTypeId = :personTypeId')
            ->andWhere('s.itsUserId = :itsUserId')
            ->setParameter('activityId', (int) $r['activityId'])            
            ->setParameter('personTypeId', (int)$personTypeId)
            ->setParameter('itsUserId', (int)$itsUserId)
            ->getQuery()
            ->getResult();           
            

            if ($resultSum) {
                $waitCount = 0;$returnCount = 0;$passCount = 0;
                $seq = 0;
                foreach ($resultSum as $sum) {
                    if ($sum['statusFlag']=="W")    {
                        $waitCount++;
                    } else if ($sum['statusFlag']=="R")    {
                        $returnCount++;
                    } else if ($sum['statusFlag']=="Y")    {
                        $passCount++;
                    }
                }                
                $result[$seq]['waitCount'] = $waitCount;
                $result[$seq]['returnCount'] = $returnCount;
                $result[$seq]['passCount'] = $passCount;          

                $seq++;      
            }   
            
            return $result;
        }
    }     
    
    
    public function getByRoundNotOwnerCanViewer($roundId,$activitySetNo,$personTypeId,$itsUserId)
    {        
        $subquery = $this->em->createQueryBuilder()
        ->select('apt')
        ->from(ItsSchdActivityPtype::class, 'apt')
        ->where('apt.activityId = a.activityId')
        ->andWhere('apt.personTypeId IN (:personTypeId)');

        $qb = $this->em->createQueryBuilder();
        $query  = $qb->select(
            '
                a.activityId,a.startDate,a.endDate,
                d.docId,d.docNameTh,d.docNameEn
            ')
            ->from(ItsSchdActivity::class, 'a')
            ->innerJoin(ItsCDocMaster::class,'d','WITH','a.docId = d.docId')     
            ->innerJoin(ItsCDocType::class,'dt','WITH','dt.docTypeId = d.docType')                   
            ->where('a.roundId = :roundId')        
            ->andWhere('a.activitySetNo = :activitySetNo')                    
            ->andWhere('a.personTypeId NOT IN (:personTypeId)')
            ->andWhere(
                $subquery->expr()->exists(
                    $subquery->getDql()
                )
            )
            ->setParameter('roundId', (int)$roundId)     
            ->setParameter('activitySetNo', $activitySetNo) 
            ->setParameter('personTypeId', (int)$personTypeId)
            ->orderBy('a.startDate', 'ASC')            
            ->addOrderBy('dt.docTypeId', 'ASC')
            ->addOrderBy('d.docId', 'ASC')
            ->getQuery();        

        return $query->getResult();
    }  

    public function getByRoundChecker($roundId,$personTypeId,$itsUserId)
    {                
        $ownerPersonType = [
            getEnv('PERSON_TYPE_STUDENT')
        ];

        $subquery = $this->em->createQueryBuilder()
        ->select('apt')
        ->from(ItsSchdActivityPtype::class, 'apt')
        ->where('apt.activityId = a.activityId')
        ->andWhere('apt.personTypeId IN (:personTypeId)');

        $qb = $this->em->createQueryBuilder();
        $query  = $qb->select(
            '
                a.activityId,a.startDate,a.endDate,
                d.docId,d.docNameTh,d.docNameEn,
                0 waitCount,
                0 returnCount,
                0 passCount                
            ')
            ->from(ItsSchdActivity::class, 'a')
            ->innerJoin(ItsCDocMaster::class,'d','WITH','a.docId = d.docId')   
            ->innerJoin(ItsCDocType::class,'dt','WITH','dt.docTypeId = d.docType')       
            ->where('a.roundId = :roundId')        
            ->andWhere('a.personTypeId IN (:ownerPersonType)')
            ->andWhere(
                $subquery->expr()->exists(
                    $subquery->getDql()
                )
            )
            ->setParameter('roundId', (int)$roundId)            
            ->setParameter('ownerPersonType', $ownerPersonType)
            ->setParameter('personTypeId', $personTypeId)
            ->orderBy('a.startDate', 'ASC')            
            ->addOrderBy('dt.docTypeId', 'ASC')
            ->addOrderBy('d.docId', 'ASC')
            ->getQuery();
        
        $result =  $query->getResult();

        foreach ($result as $r) {            
            $resultSum =  $this->em->createQueryBuilder()
            ->select('
                t.statusFlag                
            ')
            ->from(ItsSchdDocTrans::class, 't')            
            ->where('t.activityId = :activityId')            
            ->andWhere('t.receiverPersonTypeId = :personTypeId')
            ->andWhere('t.receiverId = :itsUserId')
            ->setParameter('activityId', (int) $r['activityId'])            
            ->setParameter('personTypeId', (int)$personTypeId)
            ->setParameter('itsUserId', (int)$itsUserId)
            ->getQuery()
            ->getResult();           
            

            if ($resultSum) {
                $waitCount = 0;$returnCount = 0;$passCount = 0;
                $seq = 0;
                foreach ($resultSum as $sum) {
                    if ($sum['statusFlag']=="W")    {
                        $waitCount++;
                    } else if ($sum['statusFlag']=="R")    {
                        $returnCount++;
                    } else if ($sum['statusFlag']=="Y")    {
                        $passCount++;
                    }
                }                
                $result[$seq]['waitCount'] = $waitCount;
                $result[$seq]['returnCount'] = $returnCount;
                $result[$seq]['passCount'] = $passCount;          

                $seq++;      
            }   
            
            return $result;
        }
    }    
    
    public function getActivityByReceiver($activityId,$personTypeId,$itsUserId)
    {                               
        $qb = $this->em->createQueryBuilder();
        $query  = $qb->select(
            '
                a.activityId,a.startDate,a.endDate,
                d.docId,d.docNameTh,d.docNameEn
            ')
            ->from(ItsSchdActivity::class, 'a')
            ->innerJoin(ItsSchdDocTrans::class,'dt','WITH','d.docId = d.docId')   
            ->innerJoin(ItsCDocType::class,'dt','WITH','dt.docTypeId = d.docType')       
            ->leftJoin(
                ItsSchdSeminar::class,
                's',
                'WITH',
                $qb->expr()->andX(
                    $qb->expr()->eq('a.activityId', 's.activityId'),
                    $qb->expr()->eq('s.personTypeId', ':personTypeId'),
                    $qb->expr()->eq('s.attendeeId', ':attendeeId')
                )
            )   
            ->where('a.roundId = :roundId')        
            ->andWhere('a.personTypeId IN (:ownerPersonType)')
            ->andWhere(
                $subquery->expr()->exists(
                    $subquery->getDql()
                )
            )
            ->setParameter('roundId', (int)$roundId)            
            ->setParameter('ownerPersonType', $ownerPersonType)
            ->setParameter('personTypeId', $personTypeId)
            ->orderBy('a.startDate', 'ASC')            
            ->addOrderBy('dt.docTypeId', 'ASC')
            ->addOrderBy('d.docId', 'ASC')
            ->getQuery();
        
        $result =  $query->getResult();

        
    }   

    public function send($data)
    {                 
        try {
            $this->em->getConnection()->beginTransaction();

            $rep = $this->em->getRepository(ItsSchdDocStatus::class);
            $docStatus = $rep->findOneBy(['refDocId' => $data['refDocId']]);                              
            
            if ($docStatus != null) {                                                
                $docStatus->setRemark($data['remark']);
                $docStatus->setStatusFlag("W");   
                $this->em->merge($docStatus);
                $this->em->flush();
            } else {
                $docStatus = new ItsSchdDocStatus();                                    
                $docStatus->setActivityId($data['activityId']);
                $docStatus->setDocId($data['docId']);
                $docStatus->setRefDocId($data['refDocId']);
                $docStatus->setItsUserId($data['itsUserId']);
                $docStatus->setPersonTypeId($data['personTypeId']);            
                $docStatus->setRemark($data['remark']);
                $docStatus->setStatusFlag("W"); 
                $this->em->persist($docStatus);
                $this->em->flush();          
            }

            //หา Teacher
            $repTeacher = $this->em->getRepository(ItsSchdPersonStd::class);
            $teacher = $repTeacher->findBy(['itsStudentId' => $data['itsUserId']]);    
            
            
            if ($teacher!=null) {
                foreach ($teacher as $t) {
                    
                    $docTrans = new ItsSchdDocTrans();                                    
                    $docTrans->setActivityId($data['activityId']);
                    $docTrans->setDocId($data['docId']);
                    $docTrans->setRefDocId($data['refDocId']);
                    $docTrans->setSenderId($data['itsUserId']);
                    $docTrans->setSenderPersonTypeId($data['personTypeId']);            
                    $docTrans->setReceiverId($t->getItsPersonTypeId());
                    $docTrans->setReceiverPersonTypeId(getenv('PERSON_TYPE_TEACHER'));            
                    $docTrans->setSenderRemark($data['remark']);
                    $docTrans->setStatusFlag("W"); 

                    $this->em->persist($docTrans);
                    $this->em->flush();          
                }
            }

            $this->em->clear();
            $this->em->getConnection()->commit();

            return $docStatus->getDocStatusId();        
        } catch (\Throwable $th) {      
            $this->em->getConnection()->rollBack();      
            throw $th;        
        }                    
        
    }
}
