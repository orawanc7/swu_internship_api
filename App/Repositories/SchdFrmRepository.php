<?php

namespace App\Repositories;

use App\Entities\ItsSchdFrmHdr;
use App\Entities\ItsSchdFrmDtl;
use App\Entities\ItsSchdAttach;

class SchdFrmRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }

    public function getByActivityOwner($activityId,$itsUserId,$personTypeId) 
    {                
        $qb = $this->em->createQueryBuilder();
        $query  = $qb->select(
            '
                a.frmHdrId
            ')
            ->from(ItsSchdFrmHdr::class, 'a')
            ->where('a.activityId = :activityId')   
            ->andWhere('a.itsUserId = :itsUserId')                        
            ->andWhere('a.personTypeId = :personTypeId')                        
            ->setParameter('activityId', (int)$activityId) 
            ->setParameter('itsUserId', (int)$itsUserId)                        
            ->setParameter('personTypeId', (int)$personTypeId) 
            ->orderBy('a.frmHdrId', 'ASC')            
            ->getQuery();        

        return $query->getOneOrNullResult();
    }  

    public function getDtlByFrmHdrId($frmHdrId) 
    {                
        $qb = $this->em->createQueryBuilder();
        $query  = $qb->select(
            '
                a.question,a.answer
            ')
            ->from(ItsSchdFrmDtl::class, 'a')
            ->where('a.frmHdrId = :frmHdrId')   
            ->setParameter('frmHdrId', (int)$frmHdrId) 
            ->getQuery();        

        return $query->getResult();
    }  
        
    public function save($key,$frm,$frmDtl,$attach) {   
        
        try {
            $this->em->getConnection()->beginTransaction();

            $frm_hdr_id = $key;

            $rep = $this->em->getRepository(ItsSchdFrmHdr::class);

            if ($frm_hdr_id) {
                $schdFrmHdr = $rep->findOneBy(['frmHdrId' => $frm_hdr_id]);  
            } else {
                $schdFrmHdr = $rep->findOneBy(
                    [
                        'activityId' => $frm['activityId'],
                        'itsUserId' => $frm['itsUserId'],
                        'personTypeId' => $frm['personTypeId'],
                    ]
                );  
            }
            
            $repDtl = $this->em->getRepository(ItsSchdFrmDtl::class);
            
            if ($schdFrmHdr == null) {                                                
                $schdFrmHdr = new ItsSchdFrmHdr();                                    
                $schdFrmHdr->setActivityId($frm['activityId']);
                $schdFrmHdr->setItsUserId($frm['itsUserId']);
                $schdFrmHdr->setPersonTypeId($frm['personTypeId']);
                $schdFrmHdr->setActiveFlag("Y");   
                $this->em->persist($schdFrmHdr);
                $this->em->flush();          

                $frm_hdr_id = $schdFrmHdr->getFrmHdrId();
            }          
            
            foreach ($frmDtl as $dtl) {
                $schdFrmDtl = $repDtl->findOneBy(['frmHdrId' => $frm_hdr_id,'question' => $dtl['question'] ]);  

                if ($schdFrmDtl==null) {
                    $schdFrmDtl = new ItsSchdFrmDtl();                                    
                    $schdFrmDtl->setFrmHdrId($frm_hdr_id);
                    $schdFrmDtl->setQuestion($dtl['question']);
                    $schdFrmDtl->setAnswer($dtl['answer']);                    
                    $this->em->persist($schdFrmDtl);
                    $this->em->flush();          
                } else {
                    $schdFrmDtl->setAnswer($dtl['answer']);                    
                    $this->em->merge($schdFrmDtl);
                    $this->em->flush();    
                }                
            }
            
            
            if ($attach) {
                $schdAttach = new ItsSchdAttach();                                    
                $schdAttach->setActivityId($attach['activityId']);
                $schdAttach->setDocId($attach['docId']);
                $schdAttach->setRefDocId($frm_hdr_id);
                $schdAttach->setFileId($attach['fileId']);
                $schdAttach->setFileName($attach['fileName']);
                $schdAttach->setFileExtension($attach['fileExt']);            
                $schdAttach->setFilePath($attach['filePath']);
                $schdAttach->setAttachType($attach['attachType']);            
                $this->em->persist($schdAttach);
                $this->em->flush();
            }
            
            $this->em->clear();
            $this->em->getConnection()->commit();

            return $frm_hdr_id;    
        } catch (\Throwable $th) {      
            $this->em->getConnection()->rollBack();      
            throw $th;        
        }      
        
    }    

}