<?php

namespace App\Repositories;

use App\Entities\ItsSchdMentorStd;
use App\Entities\ItsPersonType;
use App\Entities\ItsCPersonType;
use App\Entities\ItsSchdStdInfo;
use App\Entities\ItsStdMaster;
use App\Entities\ItsPersonMaster;
use App\Entities\ItsSchdSchoolHdr;
use App\Entities\ItsCSchool;
use App\Entities\ItsWsCMajor;
use App\Entities\ItsWsCDepartment;
use App\Entities\ItsWsCPrename;
use App\Entities\ItsSchdRound;

class SchdMentorStdRepository extends BaseRepository
{
    private $em;


    public function __construct($em = null)
    {
        if ($em == null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();
        } else {
            $this->em = $em;
        }
    }

    public function get($mentorStdId)
    {        
        $query =  $this->em->createQueryBuilder()
            ->select(
                '
                    sms.mentorStdId,
                    pm.itsPersonId,pm.personId,pm.personFnameTh,pm.personLnameTh,pm.mobileNo,pm.otherEmail,
                    pt.itsPersonTypeId,
                    p.prenameCd,p.prenameSnameTh,
                    cpt.personTypeId,cpt.personTypeNameTh 
                '
            )
            ->from(ItsSchdMentorStd::class, 'sms')            
            ->innerJoin(ItsPersonType::class,'pt','WITH','sms.itsPersonTypeId = pt.itsPersonTypeId')            
            ->innerJoin(ItsCPersonType::class,'cpt','WITH','pt.personTypeId = cpt.personTypeId')            
            ->innerJoin(ItsPersonMaster::class,'pm','WITH','pt.itsPersonId = pm.itsPersonId')            
            ->innerJoin(ItsWsCPrename::class, 'p', 'WITH', 'pm.prenameIntThCd = p.prenameCd')                        
            ->where('sms.mentorStdId =  :mentorStdId ')                                    
            ->setParameter('mentorStdId', $mentorStdId)                                    
            ->getQuery();

        return $query->getOneOrNullResult();
    }  
    
    public function getStudentByPersonRound($itsPersonId,$roundId)
    {
        
        $query =  $this->em->createQueryBuilder()
            ->select(
                '
                r.semCd,r.year,
                si.itsStudentId, si.courseCd,
                sm.studentId,sm.fnameTh,sm.lnameTh,
                p.prenameSnameTh,                
                m.majorLnameTh,m.majorSnameTh,
                d.deptLnameTh,d.deptSnameTh,
                s.schoolId,s.schoolNameTh,s.schoolNameEn
                '
            )
            ->from(ItsSchdMentorStd::class, 'sms')            
            ->innerJoin(ItsPersonType::class,'pt','WITH','sms.itsPersonTypeId = pt.itsPersonTypeId')            
            ->innerJoin(ItsSchdStdInfo::class,'si','WITH','sms.itsStudentId = si.itsStudentId')
            ->innerJoin(ItsSchdRound::class,'r','WITH','sms.roundId = r.roundId')
            ->innerJoin(ItsStdMaster::class,'sm','WITH','si.studentId = sm.studentId')
            ->innerJoin(ItsWsCPrename::class, 'p', 'WITH', 'sm.prenameThCd = p.prenameCd')                        
            ->innerJoin(ItsWsCMajor::class, 'm', 'WITH', 'sm.majorCd = m.majorCd')            
            ->innerJoin(ItsWsCDepartment::class, 'd', 'WITH', 'sm.deptCd = d.deptCd')            
            ->innerJoin(ItsSchdSchoolHdr::class,'ss','WITH','si.schoolHdrId = ss.schoolHdrId')
            ->innerJoin(ItsCSchool::class,'s','WITH','ss.schoolId = s.schoolId')            
            ->where('pt.itsPersonId =  :itsPersonId ')            
            ->andWhere ('sms.roundId = :roundId ')      
            ->andWhere ('sms.activeFlag = :activeFlag ')      
            ->setParameter('itsPersonId', (int) $itsPersonId)
            ->setParameter('roundId', $roundId)
            ->setParameter('activeFlag', 'Y')
            ->orderBy('sm.studentId', 'ASC')          
            ->getQuery();

        return $query->getResult();
    }    

    public function getPersonByItsStudent($itsStudentId)
    {        
        $query =  $this->em->createQueryBuilder()
            ->select(
                '
                    sms.mentorStdId,
                    pm.itsPersonId,pm.personId,               
                    pm.personFnameTh,pm.personLnameTh,pm.mobileNo,pm.otherEmail,
                    p.prenameCd,p.prenameSnameTh,
                    cpt.personTypeId,cpt.personTypeNameTh 
                '
            )
            ->from(ItsSchdMentorStd::class, 'sms')            
            ->innerJoin(ItsPersonType::class,'pt','WITH','sms.itsPersonTypeId = pt.itsPersonTypeId')            
            ->innerJoin(ItsCPersonType::class,'cpt','WITH','pt.personTypeId = cpt.personTypeId')            
            ->innerJoin(ItsPersonMaster::class,'pm','WITH','pt.itsPersonId = pm.itsPersonId')            
            ->innerJoin(ItsWsCPrename::class, 'p', 'WITH', 'pm.prenameIntThCd = p.prenameCd')                        
            ->where('sms.itsStudentId =  :itsStudentId ')                        
            ->andWhere ('sms.activeFlag = :activeFlag ')      
            ->setParameter('itsStudentId', $itsStudentId)            
            ->setParameter('activeFlag', 'Y')                   
            ->getQuery();

        return $query->getResult();
    }    
    

    public function getByItsStudentItsPersonType($itsStudentId,$itsPersonTypeId)
    {        
        $query =  $this->em->createQueryBuilder()
            ->select(
                '
                    sms.mentorStdId,
                    pm.itsPersonId,pm.personId,               
                    pm.personFnameTh,pm.personLnameTh,pm.mobileNo,pm.otherEmail,
                    p.prenameCd,p.prenameSnameTh,
                    cpt.personTypeId,cpt.personTypeNameTh 
                '
            )
            ->from(ItsSchdMentorStd::class, 'sms')            
            ->innerJoin(ItsPersonType::class,'pt','WITH','sms.itsPersonTypeId = pt.itsPersonTypeId')            
            ->innerJoin(ItsCPersonType::class,'cpt','WITH','pt.personTypeId = cpt.personTypeId')            
            ->innerJoin(ItsPersonMaster::class,'pm','WITH','pt.itsPersonId = pm.itsPersonId')            
            ->innerJoin(ItsWsCPrename::class, 'p', 'WITH', 'pm.prenameIntThCd = p.prenameCd')                        
            ->where('sms.itsStudentId =  :itsStudentId ')                        
            ->andWhere('sms.itsPersonTypeId =  :itsPersonTypeId ') 
            ->andWhere ('sms.activeFlag = :activeFlag ')      
            ->setParameter('itsStudentId', $itsStudentId)            
            ->setParameter('itsPersonTypeId', $itsPersonTypeId)
            ->setParameter('activeFlag', 'Y')                   
            ->getQuery();

        return $query->getOneOrNullResult();
    }    

    public function save($key, $data)
    {
        try {
            $personTypeMentor = getenv('PERSON_TYPE_MENTOR');
            $mentorStdId = $key;
                        
            $this->em->getConnection()->beginTransaction();
            
            $genKey = substr($data['roundId'], 0, 3);

            //Add ItsPersonMaster
            if (strlen($data['itsPersonId']) == 0) {                
                $itsPerson = new ItsPersonMaster(); 
                $itsPerson->setKeyGen($genKey);
                $itsPerson->setExtPersonFlag('Y');
                $itsPerson->setPrenameIntThCd($data['prenameIntThCd']);
                $itsPerson->setPersonFnameTh($data['personFnameTh']);
                $itsPerson->setPersonLnameTh($data['personLnameTh']);
                $itsPerson->setDeptCd($data['deptCd']);
                $itsPerson->setMobileNo($data['mobileNo']);
                $itsPerson->setOtherEmail($data['otherEmail']);
                $itsPerson->setActiveFlag('A');
                $this->em->persist($itsPerson);
                $this->em->flush();
            } else {
                $repItsPerson = $this->em->getRepository(ItsPersonMaster::class);
                $itsPerson = $repItsPerson->findOneBy(['itsPersonId' => $data['itsPersonId']]);
                if ($itsPerson) {
                    $itsPerson->setMobileNo($data['mobileNo']);
                    $itsPerson->setOtherEmail($data['otherEmail']);
                    $this->em->merge($itsPerson);
                    $this->em->flush();
                }
            }

            $itsPersonId = $itsPerson->getItsPersonId();

            // Add ItsPersonType
            if (strlen($data['itsPersonTypeId']) == 0) {
                $itsPersonType = new ItsPersonType();
                $itsPersonType->setKeyGen($genKey);
                $itsPersonType->setItsPersonId($itsPersonId);
                $itsPersonType->setPersonTypeId($personTypeMentor);
                $itsPersonType->setActiveFlag('Y');
                $this->em->persist($itsPersonType);
                $this->em->flush();
            } else {
                $repItsPersonType = $this->em->getRepository(ItsPersonType::class);
                $itsPersonType = $repItsPersonType->findOneBy(['itsPersonTypeId' => $data['itsPersonTypeId']]);
            }

            $itsPersonTypeId = $itsPersonType->getItsPersonTypeId();

            // Add ItsSchdMentorStd
            if (strlen($mentorStdId) == 0) {
                $mentor = new ItsSchdMentorStd();
                $mentor->setRoundId($data['roundId']);
                $mentor->setItsStudentId($data['itsStudentId']);
                $mentor->setItsPersonTypeId($itsPersonTypeId);
                $mentor->setActiveFlag('Y');
                $this->em->persist($mentor);
                $this->em->flush();

                $mentorStdId = $mentor->getMentorStdId();
            }            

            $this->em->getConnection()->commit();

            return $mentorStdId;
        } catch (\Throwable $th) {
            $this->em->getConnection()->rollBack();
            throw $th;
        }
    }

    public function delete($key)
    {
        try {
            $rep = $this->em->getRepository(ItsSchdMentorStd::class);
            $mentor = $rep->findOneBy(['mentorStdId' => $key]);
            //Update
            if ($mentor != null) {
                $this->em->remove($mentor);
                $this->em->flush();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
