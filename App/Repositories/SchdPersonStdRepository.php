<?php

namespace App\Repositories;

use App\Entities\ItsSchdPersonStd;
use App\Entities\ItsPersonType;
use App\Entities\ItsCPersonType;
use App\Entities\ItsPersonMaster;
use App\Entities\ItsSchdStdInfo;
use App\Entities\ItsStdMaster;
use App\Entities\ItsSchdSchoolHdr;
use App\Entities\ItsCSchool;
use App\Entities\ItsWsCMajor;
use App\Entities\ItsWsCDepartment;
use App\Entities\ItsWsCPrename;
use App\Entities\ItsSchdRound;

class SchdPersonStdRepository extends BaseRepository
{
    private $em;

    public function __construct($em = null)
    {
        if ($em == null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();
        } else {
            $this->em = $em;
        }
    }

    public function getStudentByPersonRound($itsPersonId,$roundId)
    {
        
        $query =  $this->em->createQueryBuilder()
            ->select(
                '
                r.semCd,r.year,
                si.itsStudentId, si.courseCd,
                sm.studentId,sm.fnameTh,sm.lnameTh,
                p.prenameSnameTh,                
                m.majorLnameTh,m.majorSnameTh,
                d.deptLnameTh,d.deptSnameTh,
                s.schoolId,s.schoolNameTh,s.schoolNameEn
                '
            )
            ->from(ItsSchdPersonStd::class, 'sps')            
            ->innerJoin(ItsPersonType::class,'pt','WITH','sps.itsPersonTypeId = pt.itsPersonTypeId')            
            ->innerJoin(ItsSchdStdInfo::class,'si','WITH','sps.itsStudentId = si.itsStudentId')
            ->innerJoin(ItsSchdRound::class,'r','WITH','sps.roundId = r.roundId')
            ->innerJoin(ItsStdMaster::class,'sm','WITH','si.studentId = sm.studentId')
            ->innerJoin(ItsWsCPrename::class, 'p', 'WITH', 'sm.prenameThCd = p.prenameCd')                        
            ->innerJoin(ItsWsCMajor::class, 'm', 'WITH', 'sm.majorCd = m.majorCd')            
            ->innerJoin(ItsWsCDepartment::class, 'd', 'WITH', 'sm.deptCd = d.deptCd')            
            ->innerJoin(ItsSchdSchoolHdr::class,'ss','WITH','si.schoolHdrId = ss.schoolHdrId')
            ->innerJoin(ItsCSchool::class,'s','WITH','ss.schoolId = s.schoolId')            
            ->where('pt.itsPersonId =  :itsPersonId ')            
            ->andWhere ('sps.roundId = :roundId ')      
            ->andWhere ('sps.activeFlag = :activeFlag ')      
            ->setParameter('itsPersonId', (int) $itsPersonId)
            ->setParameter('roundId', $roundId)
            ->setParameter('activeFlag', 'Y')
            ->orderBy('sm.studentId', 'ASC')          
            ->getQuery();

        return $query->getResult();
    }    

    public function getPersonByItsStudent($itsStudentId)
    {        
        $query =  $this->em->createQueryBuilder()
            ->select(
                '
                    pm.itsPersonId,pm.personId,               
                    pm.personFnameTh,pm.personLnameTh,
                    p.prenameSnameTh,
                    cpt.personTypeId,cpt.personTypeNameTh 
                '
            )
            ->from(ItsSchdPersonStd::class, 'sps')            
            ->innerJoin(ItsPersonType::class,'pt','WITH','sps.itsPersonTypeId = pt.itsPersonTypeId')            
            ->innerJoin(ItsCPersonType::class,'cpt','WITH','pt.personTypeId = cpt.personTypeId')            
            ->innerJoin(ItsPersonMaster::class,'pm','WITH','pt.itsPersonId = pm.itsPersonId')            
            ->innerJoin(ItsWsCPrename::class, 'p', 'WITH', 'pm.prenameIntThCd = p.prenameCd')                        
            ->where('sps.itsStudentId =  :itsStudentId ')                        
            ->andWhere ('sps.activeFlag = :activeFlag ')      
            ->setParameter('itsStudentId', $itsStudentId)            
            ->setParameter('activeFlag', 'Y')
            ->orderBy('sps.seqId', 'ASC')          
            ->getQuery();

        return $query->getResult();
    }    
}
