<?php

namespace App\Repositories;

use App\Entities\ItsSchdRoundActivitySet;

class SchdRoundCourseRepository extends BaseRepository
{
    private $em;

    public function __construct($em = null)
    {
        if ($em == null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();
        } else {
            $this->em = $em;
        }
    }

    public function getByRoundId($roundId)
    {
        $query =  $this->em->createQueryBuilder()
            ->select('a.roundId,a.activitySetNo')
            ->from(ItsSchdRoundActivitySet::class, 'a')
            ->where('a.roundId = :roundId')
            ->setParameter('roundId', (int)$roundId)
            ->orderBy('a.activitySetNo', 'ASC')
            ->getQuery();

        return $query->getResult();
    }
    
}
