<?php

namespace App\Repositories;

use App\Entities\ItsSchdRound;
use App\Entities\ItsSchdRoundActivitySet;

class SchdRoundRepository extends BaseRepository
{

    private $em;

    public function __construct($em = null)
    {
        if ($em == null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();
        } else {
            $this->em = $em;
        }
    }

    public function getAll()
    {
        $query =  $this->em->createQueryBuilder()
            ->select('a.roundId,a.roundNameTh,a.roundNameEn,a.year,a.semCd,a.startDate,a.endDate,a.activeFlag,a.remark')
            ->from(ItsSchdRound::class, 'a')
            ->orderBy('a.roundNameTh', 'ASC')
            ->getQuery();

        return $query->getResult();
    }

    public function get($id)
    {
        $query =  $this->em->createQueryBuilder()
            ->select('a.roundId,a.roundNameTh,a.roundNameEn,a.year,a.semCd,a.startDate,a.endDate,a.activeFlag,a.remark')
            ->from(ItsSchdRound::class, 'a')
            ->where('a.roundId = :roundId')
            ->setParameter('roundId', (int)$id)
            ->getQuery();

        return  $query->getOneOrNullResult();
    }

    public function save($key, $data)
    {                
        $roundId = 0;
        try {
            $this->em->getConnection()->beginTransaction();

            $rep = $this->em->getRepository(ItsSchdRound::class);
            $round = $rep->findOneBy(['roundId' => $key]);

            $repCourse = $this->em->getRepository(ItsSchdRoundActivitySet::class);
            $roundCourse = $repCourse->findBy(['roundId' => $key]);

            
            //Update
            if ($round != null) {
                $round->setYear($data['year']);
                $round->setSemCd($data['semCd']);
                $round->setRoundNameTh($data['roundNameTh']);
                $round->setRoundNameEn($data['roundNameEn']);                
                $round->setStartDate($data['startDate']);          
                $round->setEndDate($data['endDate']);          
                $round->setActiveFlag($data['activeFlag']);
                $round->setRemark($data['remark']);   
                
                $this->em->merge($round);
                $this->em->flush();
            } else {
                //Insert    
                $round = new ItsSchdRound();                        
                $round->setYear($data['year']);
                $round->setSemCd($data['semCd']);
                $round->setRoundNameTh($data['roundNameTh']);
                $round->setRoundNameEn($data['roundNameEn']);                
                $round->setStartDate($data['startDate']);          
                $round->setEndDate($data['endDate']);          
                $round->setActiveFlag($data['activeFlag']);
                $round->setRemark($data['remark']);   

                $this->em->persist($round);
                $this->em->flush();                                
            }

            $roundId  = $round->getRoundId();

            if ($roundCourse != null) {
                foreach ($roundCourse as $c) {
                    $this->em->remove($c);
                    $this->em->flush();
                }                
            }
            if (count($data['activitySetNo'])>0) {
                foreach ($data['activitySetNo'] as $c ) {
                    $roundActivitySet = new ItsSchdRoundActivitySet();                        
                    $roundActivitySet->setRoundId($roundId);
                    $roundActivitySet->setActivitySetNo($c);
                    
                    $this->em->persist($roundCourse);
                    $this->em->flush();                
                }
            }            

            $this->em->clear();
            $this->em->getConnection()->commit();

            return $roundId;
        } catch (\Throwable $th) {
            $this->em->getConnection()->rollBack();
            throw $th;
        }
    }

    public function delete($key)
    {
        try {
            $this->em->getConnection()->beginTransaction();

            $repActivitySet = $this->em->getRepository(ItsSchdRoundActivitySet::class);
            $roundActivitySet = $repActivitySet->findBy(['roundId' => $key]);
            foreach ($roundCourse as $c) {
                $this->em->remove($c);
                $this->em->flush();
            } 

            $rep = $this->em->getRepository(ItsSchdRound::class);
            $round = $rep->findOneBy(['roundId' => $key]);
            
            if ($round != null) {
                $this->em->remove($round);
                $this->em->flush();                
            }

            $this->em->clear();
            $this->em->getConnection()->commit();
        } catch (\Throwable $th) {
            $this->em->getConnection()->rollBack();
            throw $th;
        }
    }
}
