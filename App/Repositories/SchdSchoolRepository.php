<?php

namespace App\Repositories;

use App\Entities\ItsSchdSchoolHdr;
use App\Entities\ItsCSchool;
use App\Entities\ItsCProvince;

class SchdSchoolRepository extends BaseRepository
{

    private $em;

    public function __construct($em = null)
    {
        if ($em == null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();
        } else {
            $this->em = $em;
        }
    }

    public function getByRoundId($roundId)
    {
        $query =  $this->em->createQueryBuilder()
            ->select('
                a.schoolHdrId,a.remark,a.activeFlag,
                a.schoolId,s.schoolNameTh,s.schoolNameEn,
                p.provinceId,p.provinceNameTh,p.provinceNameEn
            '
            )
            ->from(ItsSchdSchoolHdr::class, 'a')
            ->innerJoin(ItsCSchool::class,'s','WITH','a.schoolId = s.schoolId')
            ->leftJoin(ItsCProvince::class,'p','WITH','s.provinceId = p.provinceId')
            ->where('a.roundId = :roundId')
            ->setParameter('roundId', (int)$roundId)
            ->orderBy('a.schoolId', 'ASC')
            ->getQuery();

        return $query->getResult();
    }

    public function get($id)
    {
        $query =  $this->em->createQueryBuilder()
            ->select('
                a.schoolHdrId,a.remark,a.activeFlag,
                a.schoolId,s.schoolNameTh,s.schoolNameEn,
                p.provinceId,p.provinceNameTh,p.provinceNameEn
            '
            )
            ->from(ItsSchdSchoolHdr::class, 'a')
            ->innerJoin(ItsCSchool::class,'s','WITH','a.schoolId = s.schoolId')
            ->leftJoin(ItsCProvince::class,'p','WITH','s.provinceId = p.provinceId')
            ->where('a.schoolHdrId = :schoolHdrId')
            ->setParameter('schoolHdrId', (int)$id)
            ->getQuery();

        return  $query->getOneOrNullResult();
    }

    public function save($key, $data)
    {                
        $schoolHdrId = 0;
        try {
            $this->em->getConnection()->beginTransaction();

            $rep = $this->em->getRepository(ItsSchdSchoolHdr::class);
            $school = $rep->findOneBy(['schoolHdrId' => $key]);
            
            //Update
            if ($school != null) {
                $school->setSchoolHdrId($data['schoolHdrId']);
                $school->setRoundId($data['roundId']);
                $school->setSchoolId($data['schoolId']);
                $school->setActiveFlag($data['activeFlag']);
                $school->setRemark($data['remark']);
                
                $this->em->merge($school);
                $this->em->flush();
            } else {
                //Insert    
                $school = new ItsSchdSchoolHdr();                        
                $school->setSchoolHdrId($data['schoolHdrId']);
                $school->setRoundId($data['roundId']);
                $school->setSchoolId($data['schoolId']);
                $school->setActiveFlag($data['activeFlag']);
                $school->setRemark($data['remark']);

                $this->em->persist($school);
                $this->em->flush();                                
            }

            $schoolHdrId  = $school->getSchoolHdrId();
            
            $this->em->clear();
            $this->em->getConnection()->commit();

            return $schoolHdrId;
        } catch (\Throwable $th) {
            $this->em->getConnection()->rollBack();
            throw $th;
        }
    }

    public function delete($key)
    {
        try {
            $this->em->getConnection()->beginTransaction();
           
            $rep = $this->em->getRepository(ItsSchdSchoolHdr::class);
            $school = $rep->findOneBy(['schoolHdrId' => $key]);
            
            if ($school != null) {
                $this->em->remove($school);
                $this->em->flush();                
            }

            $this->em->clear();
            $this->em->getConnection()->commit();
        } catch (\Throwable $th) {
            $this->em->getConnection()->rollBack();
            throw $th;
        }
    }
}
