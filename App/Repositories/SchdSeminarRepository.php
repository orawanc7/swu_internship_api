<?php

namespace App\Repositories;

use App\Entities\ItsSchdActivity;
use App\Entities\ItsSchdSeminar;
use App\Entities\ItsSchdActivityPtype;

class SchdSeminarRepository extends BaseRepository {
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }

    public function getSeminarByRoundAttendee($roundId,$activitySetNo,$personTypeId,$attendeeId) {
       

        $subquery = $this->em->createQueryBuilder()
        ->select('apt')
        ->from(ItsSchdActivityPtype::class, 'apt')
        ->where('apt.activityId = a.activityId')
        ->andWhere('apt.personTypeId IN (:personTypeId)');

        $qb = $this->em->createQueryBuilder();
        $query  = $qb->select(
            '
            a.activityId,a.activitySeqId,a.activityNameTh,a.activityNameEn,a.startDate,a.endDate,
            s.seminarId,s.personTypeId,s.attendeeId,s.startDate as seminarStartDate,s.endDate as seminarEndDate
            '
        )
            ->from(ItsSchdActivity::class, 'a')            
            ->leftJoin(
                ItsSchdSeminar::class,
                's',
                'WITH',
                $qb->expr()->andX(
                    $qb->expr()->eq('a.activityId', 's.activityId'),                    
                    $qb->expr()->eq('s.personTypeId', ':personTypeId'),
                    $qb->expr()->eq('s.attendeeId', ':attendeeId')
                )
            )    
            ->where('a.roundId = :roundId')
            ->andWhere('a.activitySetNo = :activitySetNo')
            ->andWhere('a.activityFlag = :activityFlag')
            ->andWhere(
                $subquery->expr()->exists(
                    $subquery->getDql()
                )
            )
            ->setParameter('roundId', (int)$roundId)
            ->setParameter('activitySetNo', $activitySetNo)
            ->setParameter('activityFlag', 'S')
            ->setParameter('personTypeId', (int) $personTypeId)
            ->setParameter('attendeeId', (int) $attendeeId)
            ->orderBy('a.startDate', 'ASC')            
            ->getQuery();
               
        // echo $query->getSql();
        return  $query->getResult();
    }

    public function saveCheck($checkType,$activityId,$personTypeId,$attendeeId)
    {                        
        try {
            $this->em->getConnection()->beginTransaction();

            $rep = $this->em->getRepository(ItsSchdSeminar::class);
            $seminar = $rep->findOneBy(
                [
                    'activityId' => $activityId,
                    'personTypeId' => $personTypeId,
                    'attendeeId' => $attendeeId
                ]
            );
            
            //Update
            if ($seminar != null) {
                if ($checkType=="IN") {
                    $seminar->setStartDate(new \DateTime('NOW'));
                }
                if ($checkType=="OUT") {
                    $seminar->setEndDate(new \DateTime('NOW'));
                }                
                
                $this->em->merge($seminar);
                $this->em->flush();
            } else {
                //Insert    
                $seminar = new ItsSchdSeminar();   
                if ($checkType=="IN") {
                    $seminar->setStartDate(new \DateTime('NOW'));
                }
                if ($checkType=="OUT") {
                    $seminar->setEndDate(new \DateTime('NOW'));
                }                                           
                $seminar->setActivityId($activityId);
                $seminar->setPersonTypeId($personTypeId);
                $seminar->setAttendeeId($attendeeId);
                $seminar->setActiveFlag("Y");
                $seminar->setRemark($data['remark']);

                $this->em->persist($seminar);
                $this->em->flush();                                
            }

            $seminarId  = $seminar->getSeminarId();
            
            $this->em->clear();
            $this->em->getConnection()->commit();

            return $seminarId;
        } catch (\Throwable $th) {
            $this->em->getConnection()->rollBack();
            throw $th;
        }
    }
}
