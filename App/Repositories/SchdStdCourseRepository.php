<?php

namespace App\Repositories;

use App\Entities\ItsSchdStdCourse;
use App\Entities\ItsSchdDocStatus;

class SchdStdCourseRepository extends BaseRepository
{

    private $em;

    public function __construct($em = null)
    {
        if ($em == null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();
        } else {
            $this->em = $em;
        }
    }

    public function get($stdCourseId)
    {
        $query =  $this->em->createQueryBuilder()
            ->select('
                c.stdCourseId,c.itsCourseCd,c.itsCourseName,c.levelClass,c.creditAmt,c.minutesQty,c.hourAmt,c.remark
            ')
            ->from(ItsSchdStdCourse::class, 'c')
            ->where('c.stdCourseId = :stdCourseId')
            ->setParameter('stdCourseId', $stdCourseId)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    public function getByItsStudent($itsStudentId)
    {
        $qb = $this->em->createQueryBuilder();
        $query  = $qb->select(
            '
                c.stdCourseId,c.itsCourseCd,c.itsCourseName,c.levelClass,c.creditAmt,c.minutesQty,c.hourAmt,c.remark,c.selectedFlag,
                s.docStatusId,s.statusFlag
            '
        )
        ->from(ItsSchdStdCourse::class, 'c')
        ->leftJoin(ItsSchdDocStatus::class,'s','WITH','c.stdCourseId = s.refDocId')   
        ->where('c.itsStudentId = :itsStudentId')
        ->setParameter('itsStudentId', $itsStudentId)
        ->orderBy('c.itsCourseCd', 'ASC')
        ->getQuery();

        return $query->getResult();
    }
    
    public function save($key, $data)
    {
        try {                        
            $rep = $this->em->getRepository(ItsSchdStdCourse::class);
            $course = $rep->findOneBy(['stdCourseId' => $key]);            
            
            //Update
            if ($course != null) {
                $course->setItsStudentId($data['itsStudentId']);
                $course->setActivityId($data['activityId']);
                $course->setItsCourseCd($data['itsCourseCd']);
                $course->setItsCourseName($data['itsCourseName']);                
                $course->setLevelClass($data['levelClass']);                
                $course->setCreditAmt($data['creditAmt']);                
                $course->setMinutesQty($data['minutesQty']);                
                $course->setHourAmt($data['hourAmt']);    
                $course->setRemark($data['remark']);   
                
                $this->em->merge($course);
                $this->em->flush();
            } else {
                //Insert    
                $course = new ItsSchdStdCourse();                        
                $course->setItsStudentId($data['itsStudentId']);
                $course->setActivityId($data['activityId']);
                $course->setItsCourseCd($data['itsCourseCd']);
                $course->setItsCourseName($data['itsCourseName']);                
                $course->setLevelClass($data['levelClass']);                
                $course->setCreditAmt($data['creditAmt']);                
                $course->setMinutesQty($data['minutesQty']);                
                $course->setHourAmt($data['hourAmt']);    
                $course->setRemark($data['remark']);   

                $this->em->persist($course);
                $this->em->flush();                                
            }
            
            return $course;
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    
    public function delete($key)
    {
        try {
            $rep = $this->em->getRepository(ItsSchdStdCourse::class);
            $stdCourse = $rep->findOneBy(['stdCourseId' => $key]);
            //Update
            if ($stdCourse != null) {
                $this->em->remove($stdCourse);
                $this->em->flush();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
