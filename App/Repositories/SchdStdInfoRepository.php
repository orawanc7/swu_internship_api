<?php

namespace App\Repositories;

use App\Entities\ItsSchdStdInfo;
use App\Entities\ItsSchdRound;
use App\Entities\ItsStdMaster;
use App\Entities\ItsSchdSchoolHdr;
use App\Entities\ItsWsCPrename;
use App\Entities\ItsWsCMajor;
use App\Entities\ItsWsCDepartment;
use App\Entities\ItsCSchool;
use App\Entities\ItsCProvince;

class SchdStdInfoRepository extends BaseRepository
{
    private $em;

    public function __construct($em = null)
    {
        if ($em == null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();
        } else {
            $this->em = $em;
        }
    }

    public function get($itsStudentId)
    {

        $query =  $this->em->createQueryBuilder()
            ->select(
                '
                    r.semCd,r.year,
                    si.itsStudentId, si.courseCd,si.roundId,si.personTypeId,si.activitySetNo,
                    sm.studentId,sm.fnameTh,sm.lnameTh,sm.buasriId,sm.gafeEmail,sm.otherEmail,sm.telephoneNo,sm.mobileNo,
                    p.prenameSnameTh,                
                    m.majorLnameTh,m.majorSnameTh,
                    d.deptLnameTh,d.deptSnameTh,
                    s.schoolId,s.schoolNameTh,s.schoolNameEn
                '
            )
            ->from(ItsSchdStdInfo::class, 'si')           
            ->innerJoin(ItsSchdRound::class,'r','WITH','si.roundId = r.roundId')
            ->innerJoin(ItsStdMaster::class,'sm','WITH','si.studentId = sm.studentId')
            ->innerJoin(ItsWsCPrename::class, 'p', 'WITH', 'sm.prenameThCd = p.prenameCd')                        
            ->innerJoin(ItsWsCMajor::class, 'm', 'WITH', 'sm.majorCd = m.majorCd')            
            ->innerJoin(ItsWsCDepartment::class, 'd', 'WITH', 'sm.deptCd = d.deptCd')                         
            ->innerJoin(ItsSchdSchoolHdr::class,'h','WITH','si.schoolHdrId = h.schoolHdrId')
            ->innerJoin(ItsCSchool::class,'s','WITH','h.schoolId = s.schoolId')            
            ->where('si.itsStudentId =  :itsStudentId ')                        
            ->setParameter('itsStudentId', $itsStudentId)            
            ->getQuery();

        return $query->getOneOrNullResult();
    }    

    public function getByStudentRound($studentId,$roundId)
    {

        $query =  $this->em->createQueryBuilder()
            ->select(
                '
                    si.itsStudentId,si.courseCd,si.personTypeId,si.activitySetNo,
                    s.schoolId,s.schoolNameTh,s.schoolNameEn
                '
            )
            ->from(ItsSchdStdInfo::class, 'si')            
            ->innerJoin(ItsSchdSchoolHdr::class,'h','WITH','si.schoolHdrId = h.schoolHdrId')
            ->innerJoin(ItsCSchool::class,'s','WITH','h.schoolId = s.schoolId')
            ->leftJoin(ItsCProvince::class,'p','WITH','s.provinceId = p.provinceId')
            ->where('si.studentId =  :studentId ')            
            ->andWhere ('si.roundId = :roundId ')      
            ->setParameter('studentId', $studentId)
            ->setParameter('roundId', $roundId)
            ->getQuery();

        return $query->getOneOrNullResult();
    }    
}
