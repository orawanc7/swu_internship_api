<?php

namespace App\Repositories;

use App\Entities\ItsCSchool;
use App\Entities\ItsWsSiProvince;
use App\Entities\ItsWsSiAmphur;
use App\Entities\ItsWsSiTambon;

class SchoolRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }
    
    public function list() {
        // return $this->getEntityManager()->getRepository(ItsCSchool::class)
        // ->findBy([], ['docTypeId' => 'ASC']);

        $query =  $this->em->createQueryBuilder()
        ->select (
            '
            a.schoolId,a.schoolNameTh,a.schoolNameEn,a.activeFlag,
            p.provinceId,p.provinceName,p.provinceNameEn
            '            
        )
        ->from(ItsCSchool::class,'a')           
        ->leftJoin(ItsWsSiProvince::class, 'p', 'WITH', 'a.provinceId = p.provinceId')             
        ->orderBy('a.schoolId', 'ASC')        
        ->getQuery();        

        return $query->getResult();         
    }

    public function get($id) {    
        
        $qb = $this->em->createQueryBuilder();
        $query =  $qb->select (
            '
            a.schoolId,a.schoolNameTh,a.schoolNameEn,a.schoolSubId,a.schoolTypeId,
            a.campusName,a.addressNo,a.soi,a.street,a.districtId,t.tambonId,t.tambonName,m.amphurId,m.amphurName,a.postcodeId,
            a.addressDesc,a.faxNo,a.telephoneNo,a.mobileNo,a.schoolGoogleMap,a.schoolWebsite,
            a.schoolEmail,a.schoolFbLink,a.pictureSchool,a.blacklistFlag,a.blacklistRemark,
            a.activeFlag,p.provinceId,p.provinceName
            '           
        )
        ->from(ItsCSchool::class,'a')        
        ->leftJoin(ItsWsSiProvince::class, 'p', 'WITH', 'a.provinceId = p.provinceId') 
        ->leftJoin(
            ItsWsSiAmphur::class,'m','WITH',
            $qb->expr()->andX(
                $qb->expr()->eq('a.amphurId', 'm.amphurId'),                    
                $qb->expr()->eq('a.provinceId', 'm.provinceId')
            )
        )    
        ->leftJoin(
            ItsWsSiTambon::class,'t','WITH',
            $qb->expr()->andX(
                $qb->expr()->eq('m.amphurId', 't.amphurId'),                    
                $qb->expr()->eq('m.provinceId', 't.provinceId'),
                $qb->expr()->eq('a.districtId', 't.tambonId')
            )
        )                  
        ->where ('a.schoolId = :schoolId')
        ->setParameter('schoolId',(int) $id)            
        ->getQuery();

        return  $query->getOneOrNullResult();
    }

    public function save($key,$data) {   
        
        try {
            $rep = $this->em->getRepository(ItsCSchool::class);
            $school = $rep->findOneBy(['schoolId'=>$key]);

            
            //Update
            if ($school!=null) {                 
                $school->setSchoolId($data['schoolId']);
                $school->setSchoolNameTh($data['schoolNameTh']);
                $school->setSchoolNameEn($data['schoolNameEn']);
                $school->setSchoolSubId($data['schoolSubId']);
                $school->setSchoolTypeId($data['schoolTypeId']);
                $school->setCampusName($data['campusName']);
                $school->setAddressNo($data['addressNo']);
                $school->setSoi($data['soi']);
                $school->setStreet($data['street']);
                $school->setDistrictId($data['districtId']);
                $school->setAmphurId($data['amphurId']);
                $school->setPostcodeId($data['postcodeId']);
                $school->setProvinceId($data['provinceId']);
                $school->setAddressDesc($data['addressDesc']);
                $school->setFaxNo($data['faxNo']);
                $school->setTelephoneNo($data['telephoneNo']);
                $school->setMobileNo($data['mobileNo']);
                $school->setSchoolGoogleMap($data['schoolGoogleMap']);
                $school->setSchoolWebsite($data['schoolWebsite']);
                $school->setSchoolEmail($data['schoolEmail']);
                $school->setSchoolFbLink($data['schoolFbLink']);
               // $school->setPictureSchool($data['pictureSchool']);
                $school->setBlacklistFlag($data['blacklistFlag']);
                $school->setBlacklistRemark($data['blacklistRemark']);
                $school->setActiveFlag($data['activeFlag']);
                $this->em->merge($school);
                $this->em->flush();        
            } else {
            //Insert                                 
                $school = new ItsCSchool();                        
                $school->setSchoolId($data['schoolId']);
                $school->setSchoolNameTh($data['schoolNameTh']);
                $school->setSchoolNameEn($data['schoolNameEn']);
                $school->setSchoolSubId($data['schoolSubId']);
                $school->setSchoolTypeId($data['schoolTypeId']);
                $school->setCampusName($data['campusName']);
                $school->setAddressNo($data['addressNo']);
                $school->setSoi($data['soi']);
                $school->setStreet($data['street']);
                $school->setDistrictId($data['districtId']);
                $school->setAmphurId($data['amphurId']);
                $school->setPostcodeId($data['postcodeId']);
                $school->setProvinceId($data['provinceId']);
                $school->setAddressDesc($data['addressDesc']);
                $school->setFaxNo($data['faxNo']);
                $school->setTelephoneNo($data['telephoneNo']);
                $school->setMobileNo($data['mobileNo']);
                $school->setSchoolGoogleMap($data['schoolGoogleMap']);
                $school->setSchoolWebsite($data['schoolWebsite']);
                $school->setSchoolEmail($data['schoolEmail']);
                $school->setSchoolFbLink($data['schoolFbLink']);
               // $school->setPictureSchool($data['pictureSchool']);
                $school->setBlacklistFlag($data['blacklistFlag']);
                $school->setBlacklistRemark($data['blacklistRemark']);
                $school->setActiveFlag($data['activeFlag']); 

                $this->em->persist($school);
                $this->em->flush();      
            }
        } catch (\Throwable $th) {
            throw $th;
        }        
        
    }
    public function saveFilePictureSchool ($key,$fileExtension) {   
        
        try {
            $rep = $this->em->getRepository(ItsCSchool::class);
            $pictureSchool = $rep->findOneBy(['schoolId'=>$key]);
                        
            //Update
            if ($pictureSchool!=null) {                 
                $pictureSchool->setPictureSchool($fileExtension);
                
                $this->em->merge($pictureSchool);
                $this->em->flush();        
            }
        } catch (\Throwable $th) {
            throw $th;
        }        
        
    }

    public function delete($key) {   
        try {
            $rep = $this->em->getRepository(ItsCSchool::class);
            $school = $rep->findOneBy(['schoolId'=>$key]);
            //Update
            if ($school!=null) {                 
                $this->em->remove($school);
                $this->em->flush();        
            } 
        } catch (\Throwable $th) {
            throw $th;
        }                
    }

}