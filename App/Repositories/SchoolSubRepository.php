<?php

namespace App\Repositories;

use App\Entities\ItsCSchoolSub;

class SchoolSubRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }
    
    public function list() {     
        $query =  $this->em->createQueryBuilder()
        ->select (
            '
            a.schoolSubId,a.schoolSubNameTh,a.schoolSubNameEn,a.activeFlag
            '            
        )
        ->from(ItsCSchoolSub::class,'a')             
        ->orderBy('a.schoolSubId', 'ASC')        
        ->getQuery();        

        return $query->getResult();         
    }

    public function get($id) {    
        
        $query =  $this->em->createQueryBuilder()
        ->select (
            '
            a.schoolSubId,a.schoolSubNameTh,a.schoolSubNameEn,a.activeFlag
            '           
        )
        ->from(ItsCSchoolSub::class,'a')  
        ->where ('a.schoolSubId = :schoolSubId')
        ->setParameter('schoolSubId',(int) $id)            
        ->getQuery();

        return  $query->getOneOrNullResult();
    }

    public function save($key,$data) {   
        
            
        
    }

    public function delete($key) {   
                    
    }

}