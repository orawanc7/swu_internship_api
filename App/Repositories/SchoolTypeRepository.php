<?php

namespace App\Repositories;

use App\Entities\ItsCSchoolType;

class SchoolTypeRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }
    
    public function list() {     
        $query =  $this->em->createQueryBuilder()
        ->select (
            '
            a.schoolTypeId,a.schoolTypeNameTh,a.schoolTypeNameEn,a.activeFlag
            '            
        )
        ->from(ItsCSchoolType::class,'a')             
        ->orderBy('a.schoolTypeId', 'ASC')        
        ->getQuery();        

        return $query->getResult();         
    }

    public function get($id) {    
        
        $query =  $this->em->createQueryBuilder()
        ->select (
            '
            a.schoolTypeId,a.schoolTypeNameTh,a.schoolTypeNameEn,a.activeFlag
            '           
        )
        ->from(ItsCSchoolType::class,'a')  
        ->where ('a.schoolTypeId = :schoolTypeId')
        ->setParameter('schoolTypeId',(int) $id)            
        ->getQuery();

        return  $query->getOneOrNullResult();
    }

    public function save($key,$data) {   
        
            
        
    }

    public function delete($key) {   
                    
    }

}