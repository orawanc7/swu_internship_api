<?php

namespace App\Repositories;

use App\Entities\ItsStdMaster;
use App\Entities\ItsWsCMajor;
use App\Entities\ItsWsCDepartment;
use App\Entities\ItsWsCPrename;

class StdMasterRepository extends BaseRepository {

    private $em;

    public function __construct($em = null)
    {
        if ($em == null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();
        } else {
            $this->em = $em;
        }
    }

    public function getByStudentId($studentId)
    {        
        $query =  $this->em->createQueryBuilder()
            ->select(
                '
                sm.studentId,sm.fnameTh,sm.lnameTh,sm.buasriId,sm.gafeEmail,sm.otherEmail,sm.telephoneNo,sm.mobileNo,sm.addressDesc,
                sm.admYear,sm.admSemCd,
                sm.themeId,
                p.prenameSnameTh,                
                m.majorLnameTh,m.majorLnameEng,
                d.deptLnameTh,d.deptLnameEng
            '
            )
            ->from(ItsStdMaster::class, 'sm')
            ->innerJoin(ItsWsCPrename::class, 'p', 'WITH', 'sm.prenameThCd = p.prenameCd')                        
            ->innerJoin(ItsWsCMajor::class, 'm', 'WITH', 'sm.majorCd = m.majorCd')            
            ->innerJoin(ItsWsCDepartment::class, 'd', 'WITH', 'sm.deptCd = d.deptCd')            
            ->where('sm.studentId = :studentId')
            ->setParameter('studentId', $studentId)
            ->getQuery();

        //echo $query->getSql();

        return $query->getOneOrNullResult();
    }

    public function getByBuasriId($buasriId)
    {        
        $query =  $this->em->createQueryBuilder()
            ->select(
                '
                sm.studentId,sm.fnameTh,sm.lnameTh,sm.buasriId,sm.gafeEmail,sm.otherEmail,sm.telephoneNo,sm.mobileNo,sm.addressDesc,
                sm.admYear,sm.admSemCd,
                sm.themeId,
                p.prenameSnameTh,                
                m.majorLnameTh,m.majorLnameEng,
                d.deptLnameTh,d.deptLnameEng
            '
            )
            ->from(ItsStdMaster::class, 'sm')
            ->innerJoin(ItsWsCPrename::class, 'p', 'WITH', 'sm.prenameThCd = p.prenameCd')                        
            ->innerJoin(ItsWsCMajor::class, 'm', 'WITH', 'sm.majorCd = m.majorCd')            
            ->innerJoin(ItsWsCDepartment::class, 'd', 'WITH', 'sm.deptCd = d.deptCd')            
            ->where('sm.buasriId = :buasriId')
            ->setParameter('buasriId', $buasriId)
            ->getQuery();

        //echo $query->getSql();

        return $query->getOneOrNullResult();
    }

    public function save($data)
    {
        try {
            $repStudent = $this->em->getRepository(ItsStdMaster::class);
            $student = $repStudent->findOneBy(
                [
                    'studentId' => $data->getStudentId()
                ]
            );
            //Update
            if ($student != null) {
                $student->setOtherEmail($data->getOtherEmail());
                $student->setMobileNo($data->getMobileNo());
                $student->setTelephoneNo($data->getTelephoneNo());

                $this->em->merge($student);
                $this->em->flush();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}