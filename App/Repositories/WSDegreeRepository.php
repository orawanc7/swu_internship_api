<?php

namespace App\Repositories;

use App\Entities\ItsWsCDegree;

class WSDegreeRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }

    public function list() {
        
        $query =  $this->em->createQueryBuilder()
        ->select (
            'a.degreeCd,a.degreeLnameTh,a.degreeLnameEng,a.degreeSnameTh,a.degreeSnameEng,
            l.levelGroupCd,l.levelGroupNameTh,l.levelGroupNameEng'
        )
        ->from(ItsWsCDegree::class,'a')          
        ->innerJoin('a.levelGroupCd','l')
        ->orderBy('a.degreeCd', 'ASC')        
        ->getQuery();        

        return $query->getResult();                
    }

}