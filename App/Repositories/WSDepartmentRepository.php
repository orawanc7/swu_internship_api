<?php

namespace App\Repositories;

use App\Entities\ItsWsCDepartment;

class WSDepartmentRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }

    public function list() {
        
        $query =  $this->em->createQueryBuilder()
        ->select ('a.deptCd,a.deptLnameTh,a.deptLnameEng,a.deptSnameTh,a.deptSnameEng,a.activeFlag')
        ->from(ItsWsCDepartment::class,'a')          
        ->orderBy('a.deptCd', 'ASC')        
        ->getQuery();        

        return $query->getResult();                
    }

}