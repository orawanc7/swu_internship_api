<?php

namespace App\Repositories;

use App\Entities\ItsWsCLevelGroup;

class WSLevelGroupRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }

    public function list() {
        
        $query =  $this->em->createQueryBuilder()
        ->select (
            'a.levelGroupCd,a.levelGroupNameTh,a.levelGroupNameEng'
        )
        ->from(ItsWsCLevelGroup::class,'a')                  
        ->orderBy('a.levelGroupCd', 'ASC')        
        ->getQuery();        

        return $query->getResult();                
    }

}