<?php

namespace App\Repositories;

use App\Entities\ItsWsCMajor;

class WSMajorRepository extends BaseRepository {
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }

    public function list() {
        
        $query =  $this->em->createQueryBuilder()
        ->select ('a.majorCd,a.majorSnameTh,a.majorLnameTh,a.majorSnameEng,a.majorLnameEng,a.activeFlag')
        ->from(ItsWsCMajor::class,'a')          
        ->orderBy('a.majorCd', 'ASC')        
        ->getQuery();        

        return $query->getResult();                
    }

}