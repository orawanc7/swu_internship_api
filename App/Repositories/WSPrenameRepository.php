<?php

namespace App\Repositories;

use App\Entities\ItsWsCPrename;

class WSPrenameRepository extends BaseRepository
{
    private $em;

    public function __construct($em = null)
    {
        if ($em == null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();
        } else {
            $this->em = $em;
        }
    }

    public function list()
    {
        $query =  $this->em->createQueryBuilder()
            ->select('a.prenameCd,a.prenameLnameTh,a.prenameSnameTh,a.prenameLnameEng,a.prenameSnameEng')
            ->from(ItsWsCPrename::class, 'a')
            ->orderBy('a.prenameCd', 'ASC')
            ->getQuery();

        return $query->getResult();
    }
}
