<?php

namespace App\Repositories;

use App\Entities\ItsWsSiAmphur;
use App\Entities\ItsWsSiProvince;

class WSSiAmphurRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }

    public function list() {
        
        $query =  $this->em->createQueryBuilder()
        ->select ('a.ampProId,a.amphurId,a.amphurName,a.amphurNameEn,a.provinceId,p.provinceName,a.amphurStatus')
        ->from(ItsWsSiAmphur::class,'a')   
        ->innerJoin(ItsWsSiProvince::class,'p','WITH','a.provinceId = p.provinceId')       
        ->addOrderBy('p.provinceName', 'ASC')
        ->addOrderBy('a.amphurName', 'ASC')
        ->getQuery();

        return $query->getResult();                
    } 
    public function listAmphurByProvince($id) {        
        $query =  $this->em->createQueryBuilder()
        ->select ('a.ampProId,a.amphurId,a.amphurName,a.amphurNameEn,a.provinceId,p.provinceName,a.amphurStatus')
        ->from(ItsWsSiAmphur::class,'a')   
        ->innerJoin(ItsWsSiProvince::class,'p','WITH','a.provinceId = p.provinceId')    
        ->where ('a.provinceId = :provinceId')
        ->setParameter('provinceId',$id)     
        ->orderBy('a.amphurName', 'ASC')
        ->getQuery();

        return $query->getResult();                
    }

}