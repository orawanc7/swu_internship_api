<?php

namespace App\Repositories;

use App\Entities\ItsWsSiProvince;

class WSSiProvinceRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }

    public function list() {
        
        $query =  $this->em->createQueryBuilder()
        ->select ('p.provinceId, p.provinceName, p.provinceNameEn, p.provinceNameSh, p.provinceNameEnSh, p.provinceStatus, p.countryId, p.zoneId')
        ->from(ItsWsSiProvince::class,'p')       
        ->addOrderBy('p.provinceId', 'ASC')
        ->getQuery();

        return $query->getResult();                
    }

}