<?php

namespace App\Repositories;

use App\Entities\ItsWsSiTambon;
use App\Entities\ItsWsSiAmphur;

class WSSiTambonRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }

    public function list() {

        $qb = $this->em->createQueryBuilder();
        $query =  $qb->select ('t.tamAmpId, t.provinceId, t.amphurId, a.amphurName, t.tambonId, t.tambonName, t.tambonNameEn, t.tambonNameSh, t.tambonNameEnSh, t.tambonStatus, t.countryId, t.zoneId')
        ->from(ItsWsSiTambon::class,'t')   
        ->leftJoin(
            ItsWsSiAmphur::class,'a','WITH',
            $qb->expr()->andX(
                $qb->expr()->eq('t.amphurId', 'a.amphurId'),                    
                $qb->expr()->eq('t.provinceId', 'a.provinceId')
            )
        )          
        ->addOrderBy('t.provinceId', 'ASC')
        ->addOrderBy('t.amphurId', 'ASC')
        ->addOrderBy('t.tambonId', 'ASC')
        ->getQuery();
        return $query->getResult();                
    }
    public function listTambonByAmphur($amphurId, $provinceId) {  
        $qb = $this->em->createQueryBuilder();
        $query =  $qb->select ('t.tamAmpId, t.provinceId, t.amphurId, t.tambonId, t.tambonName, t.tambonNameEn,
         t.tambonNameSh, t.tambonNameEnSh, t.tambonStatus, t.countryId, t.zoneId')
        ->from(ItsWsSiTambon::class,'t')  
        ->where ('t.amphurId = :amphurId')
        ->andWhere ('t.provinceId = :provinceId')
            ->setParameter('amphurId',$amphurId)           
            ->setParameter('provinceId',$provinceId)           
        ->orderBy('t.tambonName', 'ASC')
        ->getQuery();
        return $query->getResult();                         
    }

}