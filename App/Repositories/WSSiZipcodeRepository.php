<?php

namespace App\Repositories;

use App\Entities\ItsWsSiZipcode;
use App\Entities\ItsWsSiTambon;
use App\Entities\ItsWsSiAmphur;
use App\Entities\ItsWsSiProvince;

class WSSiZipcodeRepository extends BaseRepository {
    
    private $em;

    public function __construct($em=null) {        
        if ($em==null) {
            parent::__construct();
            $this->em =  $this->getEntityManager();            
        } else {
            $this->em = $em;
        }
    }

    public function list() {

        $qb = $this->em->createQueryBuilder();
        $query =  $qb->select ('z.zipcodeId, z.zipcode, z.referenceCode
        ,t.tamAmpId, t.tambonId, t.tambonName, t.tambonNameEn
        ,a.ampProId, a.amphurId, a.amphurName, a.amphurNameEn 
        ,p.provinceId, p.provinceName, p.provinceNameEn')
        ->from(ItsWsSiZipcode::class,'z')   
        ->leftJoin(
            ItsWsSiTambon::class,'t','WITH',
            $qb->expr()->andX(
                $qb->expr()->eq('z.tambonId', 't.tambonId'),                    
                $qb->expr()->eq('z.amphurId', 't.amphurId'),
                $qb->expr()->eq('z.provinceId', 't.provinceId')
            )
        )
        ->leftJoin(
            ItsWsSiAmphur::class,'a','WITH',
            $qb->expr()->andX(
                $qb->expr()->eq('z.amphurId', 'a.amphurId'),                    
                $qb->expr()->eq('z.provinceId', 'a.provinceId')
            )
        )
        ->leftJoin(ItsWsSiProvince::class,'p','WITH','z.provinceId = p.provinceId')             
        ->addOrderBy('t.provinceId', 'ASC')
        ->addOrderBy('t.amphurId', 'ASC')
        ->addOrderBy('t.tambonId', 'ASC')
        ->getQuery();

        return $query->getResult();                
    }

}