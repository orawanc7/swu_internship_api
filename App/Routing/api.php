<?php

/**
 *  Api 
 * */
$router->map('POST', '/api/Login', 'App\Apis\LoginController@index', 'login');

$router->map('POST', '/api/setCurrentRoundId', 'App\Apis\LoginController@setCurrentRoundId', 'setCurrentRoundId');

$router->map('GET', '/api/WSMajor', 'App\Apis\WSMajorController@list', 'wsmajor.list');
$router->map('GET', '/api/WSDepartment', 'App\Apis\WSDepartmentController@list', 'wsdepartment.list');
$router->map('GET', '/api/WSPrename', 'App\Apis\WSPrenameController@list', 'wsprename.list');
$router->map('GET', '/api/WSLevelGroup', 'App\Apis\WSLevelGroupController@list', 'wslevelgroup.list');
$router->map('GET', '/api/WSDegree', 'App\Apis\WSDegreeController@list', 'wsdegree.list');

/* DocType */
$router->map('GET', '/api/DocType', 'App\Apis\DocTypeController@list', 'doctype.list');
$router->map('GET', '/api/DocType/[a:docTypeId]', 'App\Apis\DocTypeController@get', 'doctype.get');
$router->map('POST', '/api/DocType/save', 'App\Apis\DocTypeController@save', 'doctype.save');
$router->map('DELETE', '/api/DocType/save/[a:docTypeId]', 'App\Apis\DocTypeController@delete', 'doctype.delete');

/* Doc */
$router->map('GET', '/api/Doc', 'App\Apis\DocController@list', 'doc.list');
$router->map('GET', '/api/Doc/[a:docId]', 'App\Apis\DocController@get', 'doc.get');
$router->map('POST', '/api/Doc/save', 'App\Apis\DocController@save', 'doc.save');
$router->map('DELETE', '/api/Doc/save/[a:docId]', 'App\Apis\DocController@delete', 'doc.delete');

/* PersonGroup */
$router->map('GET', '/api/PersonGroup', 'App\Apis\PersonGroupController@list', 'persongroup.list');
$router->map('GET', '/api/PersonGroup/[a:personGroupId]', 'App\Apis\PersonGroupController@get', 'persongroup.get');
$router->map('POST', '/api/PersonGroup/save', 'App\Apis\PersonGroupController@save', 'persongroup.save');
$router->map('DELETE', '/api/PersonGroup/save/[a:personGroupId]', 'App\Apis\PersonGroupController@delete', 'persongroup.delete');

/* PersonType */
$router->map('GET', '/api/PersonType', 'App\Apis\PersonTypeController@list', 'persontype.list');
$router->map('GET', '/api/PersonType/[a:personTypeId]', 'App\Apis\PersonTypeController@get', 'persontype.get');
$router->map('POST', '/api/PersonType/save', 'App\Apis\PersonTypeController@save', 'persontype.save');
$router->map('DELETE', '/api/PersonType/save/[a:personTypeId]', 'App\Apis\PersonTypeController@delete', 'persontype.delete');

/* Person */
$router->map('GET', '/api/Person/get/[a:itsPersonId]', 'App\Apis\PersonController@get', 'person.get');
$router->map('GET', '/api/Person/getExternalByDeptAndName', 'App\Apis\PersonController@getExternalByDeptAndName', 'Person.getExternalByDeptAndName');

/* ItsPersonType */
$router->map('GET', '/api/ItsPersonType/getMentorByItsPersonId/[a:itsPersonId]', 'App\Apis\ItsPersonTypeController@getMentorByItsPersonId', 'ItsPersonType.getMentorByItsPersonId');

/* Province */
/*$router->map('GET', '/api/Province', 'App\Apis\ProvinceController@list', 'province.list');
$router->map('GET', '/api/Province/[a:provinceId]', 'App\Apis\ProvinceController@get', 'province.get');
$router->map('POST', '/api/Province/save', 'App\Apis\ProvinceController@save', 'province.save');
$router->map('DELETE', '/api/Province/save/[a:provinceId]', 'App\Apis\ProvinceController@delete', 'province.delete');*/
/*WSSiProvince*/ 
$router->map('GET', '/api/WSSiProvince', 'App\Apis\WSSiProvinceController@list', 'wssiprovince.list');
$router->map('GET', '/api/WSSiProvince/[a:provinceId]', 'App\Apis\WSSiProvinceController@get', 'wssiprovince.get');
$router->map('POST', '/api/WSSiProvince/save', 'App\Apis\WSSiProvinceController@save', 'wssiprovince.save');
$router->map('DELETE', '/api/WSSiProvince/save/[a:provinceId]', 'App\Apis\WSSiProvinceController@delete', 'wssiprovince.delete');

/* WSSiAmphur */
$router->map('GET', '/api/WSSiAmphur', 'App\Apis\WSSiAmphurController@list', 'wssiamphur.list');
$router->map('GET', '/api/WSSiAmphur/listAmphurByProvince', 'App\Apis\WSSiAmphurController@listAmphurByProvince', 'wssiamphur.getbyprovinceid');
$router->map('GET', '/api/WSSiAmphur/getAmphurByProvince', 'App\Apis\WSSiAmphurController@getAmphurByProvince', 'wssiamphur.getbyamphurid');
$router->map('GET', '/api/WSSiAmphur/[a:ampProId]', 'App\Apis\WSSiAmphurController@get', 'wssiamphur.get');
$router->map('POST', '/api/WSSiAmphur/save', 'App\Apis\WSSiAmphurController@save', 'wssiamphur.save');
$router->map('DELETE', '/api/WSSiAmphur/save/[a:ampProId]', 'App\Apis\WSSiAmphurController@delete', 'wssiamphur.delete');

/* WSSiTambon */
$router->map('GET', '/api/WSSiTambon', 'App\Apis\WSSiTambonController@list', 'wssitambon.list');
$router->map('GET', '/api/WSSiTambon/listTambonByAmphur', 'App\Apis\WSSiTambonController@listTambonByAmphur', 'wssitambon.getbyprovinceid');
$router->map('GET', '/api/WSSiTambon/[a:tambonId]', 'App\Apis\WSSiTambonController@get', 'wssitambon.get');
$router->map('POST', '/api/WSSiTambon/save', 'App\Apis\WSSiTambonController@save', 'wssitambon.save');
$router->map('DELETE', '/api/WSSiTambon/save/[a:tambonId]', 'App\Apis\WSSiTambonController@delete', 'wssitambon.delete');




/* SchoolSub */
$router->map('GET', '/api/SchoolSub', 'App\Apis\SchoolSubController@list', 'schoolsub.list');
$router->map('GET', '/api/SchoolSub/[a:schoolSubId]', 'App\Apis\SchoolSubController@get', 'schoolsub.get');
$router->map('POST', '/api/SchoolSub/save', 'App\Apis\SchoolSubController@save', 'schoolsub.save');
$router->map('DELETE', '/api/SchoolSub/save/[a:schoolSubId]', 'App\Apis\SchoolSubController@delete', 'schoolsub.delete');
/* SchoolType */
$router->map('GET', '/api/SchoolType', 'App\Apis\SchoolTypeController@list', 'schooltype.list');
$router->map('GET', '/api/SchoolType/[a:schoolTypeId]', 'App\Apis\SchoolTypeController@get', 'schooltype.get');
$router->map('POST', '/api/SchoolType/save', 'App\Apis\SchoolTypeController@save', 'schooltype.save');
$router->map('DELETE', '/api/SchoolType/save/[a:schoolTypeId]', 'App\Apis\SchoolTypeController@delete', 'schooltype.delete');


/* School */
$router->map('GET', '/api/School', 'App\Apis\SchoolController@list', 'school.list');
$router->map('GET', '/api/School/[a:schoolId]', 'App\Apis\SchoolController@get', 'school.get');
$router->map('POST', '/api/School/save', 'App\Apis\SchoolController@save', 'school.save');
$router->map('DELETE', '/api/School/save/[a:schoolId]', 'App\Apis\SchoolController@delete', 'school.delete');
$router->map('GET', '/api/School/download/[a:schoolId]', 'App\Apis\SchoolController@download', 'school.download');

/* DocGroupDownload */
$router->map('GET', '/api/DocGroupDownload', 'App\Apis\DocGroupDownloadController@list', 'DocGroupDownload.list');
$router->map('GET', '/api/DocGroupDownload/[a:docGroupDownloadId]', 'App\Apis\DocGroupDownloadController@get', 'DocGroupDownload.get');
$router->map('POST', '/api/DocGroupDownload/save', 'App\Apis\DocGroupDownloadController@save', 'DocGroupDownload.save');
$router->map('DELETE', '/api/DocGroupDownload/save/[a:docGroupDownloadId]', 'App\Apis\DocGroupDownloadController@delete', 'DocGroupDownload.delete');

/* DocDownload */
$router->map('GET', '/api/DocDownload/getAll', 'App\Apis\DocDownloadController@getAll', 'DocDownload.getAll');
$router->map('GET', '/api/DocDownload/getActive', 'App\Apis\DocDownloadController@getActive', 'DocDownload.getActive');
$router->map('GET', '/api/DocDownload/get/[a:docDownloadId]', 'App\Apis\DocDownloadController@get', 'DocDownload.get');
$router->map('GET', '/api/DocDownload/download/[a:docDownloadId]', 'App\Apis\DocDownloadController@download', 'DocDownload.download');
$router->map('POST', '/api/DocDownload/save', 'App\Apis\DocDownloadController@save', 'DocDownload.save');
$router->map('DELETE', '/api/DocDownload/save/[a:docDownloadId]', 'App\Apis\DocDownloadController@delete', 'DocDownload.delete');


/* Frm */
$router->map('GET', '/api/Frm/getAll', 'App\Apis\FrmController@getAll', 'Frm.getAll');
$router->map('GET', '/api/Frm/get/[a:frmHdrId]', 'App\Apis\FrmController@get', 'Frm.get');
$router->map('GET', '/api/Frm/getTemplate/[a:frmHdrId]', 'App\Apis\FrmController@getTemplate', 'Frm.gettemplate');
$router->map('POST', '/api/Frm/save', 'App\Apis\FrmController@save', 'Frm.save');
$router->map('DELETE', '/api/Frm/save/[a:frmHdrId]', 'App\Apis\FrmController@delete', 'Frm.delete');
$router->map('POST', '/api/Frm/saveTemplate', 'App\Apis\FrmController@saveTemplate', 'Frm.savetemplate');

/* SchdRound */
$router->map('GET', '/api/SchdRound/getAll', 'App\Apis\SchdRoundController@getAll', 'SchdRound.getAll');
$router->map('GET', '/api/SchdRound/get/[a:roundId]', 'App\Apis\SchdRoundController@get', 'SchdRound.get');
$router->map('POST', '/api/SchdRound/save', 'App\Apis\SchdRoundController@save', 'SchdRound.save');
$router->map('DELETE', '/api/SchdRound/save/[a:roundId]', 'App\Apis\SchdRoundController@delete', 'SchdRound.delete');

/* SchdRoundCourse */
$router->map('GET', '/api/SchdRoundCourse/getByRoundId/[a:roundId]', 'App\Apis\SchdRoundCourseController@getByRoundId', 'SchdRoundCourse.getByRoundId');

/* SchdActivity */
$router->map('GET', '/api/SchdActivity/getByRoundActivitySetNo/[a:roundId]/[a:activitySetNo]', 'App\Apis\SchdActivityController@getByRoundActivitySetNo', 'SchdActivity.getByRoundActivitySetNo');
$router->map('GET', '/api/SchdActivity/get/[a:activityId]', 'App\Apis\SchdActivityController@get', 'SchdActivity.get');
$router->map('POST', '/api/SchdActivity/save', 'App\Apis\SchdActivityController@save', 'SchdActivity.save');
$router->map('POST', '/api/SchdActivity/saveParent', 'App\Apis\SchdActivityController@saveParent', 'SchdActivity.saveParent');
$router->map('DELETE', '/api/SchdActivity/save/[a:activityId]', 'App\Apis\SchdActivityController@delete', 'SchdActivity.delete');
$router->map('GET', '/api/SchdActivity/getActivityAndPType/[a:activityId]', 'App\Apis\SchdActivityController@getActivityAndPType', 'SchdActivity.getActivityAndPType');


/* SchdSchool */
$router->map('GET', '/api/SchdSchool/getByRoundId/[a:roundId]', 'App\Apis\SchdSchoolController@getByRoundId', 'SchdSchool.getByRoundId');
$router->map('GET', '/api/SchdSchool/get/[a:schoolHdrId]', 'App\Apis\SchdSchoolController@get', 'SchdSchool.get');
$router->map('POST', '/api/SchdSchool/save', 'App\Apis\SchdSchoolController@save', 'SchdSchool.save');
$router->map('DELETE', '/api/SchdSchool/save/[a:schoolHdrId]', 'App\Apis\SchdSchoolController@delete', 'SchdSchool.delete');

/* SchdStdInfo */
$router->map('GET', '/api/SchdStdInfo/get/[a:itsStudentId]', 'App\Apis\SchdStdInfoController@get', 'SchdStdInfo.get');
$router->map('GET', '/api/SchdStdInfo/getByStudentRound/[a:studentId]/[a:roundId]', 'App\Apis\SchdStdInfoController@getByStudentRound', 'SchdStdInfo.getByStudentRound');
$router->map('GET', '/api/SchdStdInfo/getRelateByItsStudent/[a:itsStudentId]', 'App\Apis\SchdStdInfoController@getRelateByItsStudent', 'SchdStdInfo.getRelateByItsStudent');


/* SchdSeminar */
$router->map('GET', '/api/SchdSeminar/getSeminarByRoundAttendee', 'App\Apis\SchdSeminarController@getSeminarByRoundAttendee', 'SchdSeminar.getSeminarByRoundAttendee');
$router->map('POST', '/api/SchdSeminar/saveCheck', 'App\Apis\SchdSeminarController@saveCheck', 'SchdSeminar.saveCheck');


/* SchdPersonStd */
$router->map('GET', '/api/SchdPersonStd/getStudentByPersonRound', 'App\Apis\SchdPersonStdController@getStudentByPersonRound', 'SchdPersonStd.getStudentByPersonRound');
$router->map('GET', '/api/SchdPersonStd/downloadDocStudent', 'App\Apis\SchdPersonStdController@downloadDocStudent', 'SchdPersonStd.downloadDocStudent');

/* SchdMentorStd */
$router->map('GET', '/api/SchdMentorStd/getStudentByPersonRound', 'App\Apis\SchdMentorStdController@getStudentByPersonRound', 'SchdMentorStd.getStudentByPersonRound');
$router->map('GET', '/api/SchdMentorStd/getPersonByItsStudent/[a:itsStudentId]', 'App\Apis\SchdMentorStdController@getPersonByItsStudent', 'SchdMentorStd.getPersonByItsStudent');
$router->map('GET', '/api/SchdMentorStd/getByItsStudentItsPersonType/[a:itsStudentId]/[a:itsPersonTypeId]', 'App\Apis\SchdMentorStdController@getByItsStudentItsPersonType', 'SchdMentorStd.getByItsStudentItsPersonType');
$router->map('GET', '/api/SchdMentorStd/get/[a:mentorStdId]', 'App\Apis\SchdMentorStdController@get', 'SchdMentorStd.get');
$router->map('POST', '/api/SchdMentorStd/save', 'App\Apis\SchdMentorStdController@save', 'SchdMentorStd.save');
$router->map('POST', '/api/SchdMentorStd/delete/[a:mentorStdId]', 'App\Apis\SchdMentorStdController@delete', 'SchdMentorStd.delete');


/* Profile */
$router->map('GET', '/api/Profile/getImageProfile/[a:itsUserId]', 'App\Apis\ProfileController@getImageProfile', 'Profile.getImageProfile');
$router->map('GET', '/api/Profile/getImageIcon/[a:itsUserId]', 'App\Apis\ProfileController@getImageIcon', 'Profile.getImageIcon');
$router->map('POST', '/api/Profile/save', 'App\Apis\ProfileController@save', 'Profile.save');
$router->map('POST', '/api/Profile/upload', 'App\Apis\ProfileController@upload', 'Profile.upload');

/* SchdDocStatus */
$router->map('GET', '/api/SchdDocStatus/getByRoundActivitySetNoOwner', 'App\Apis\SchdDocStatusController@getByRoundActivitySetNoOwner', 'SchdDocStatus.getByRoundActivitySetNoOwner');
$router->map('GET', '/api/SchdDocStatus/getByRoundChecker', 'App\Apis\SchdDocStatusController@getByRoundChecker', 'SchdDocStatus.getByRoundChecker');
$router->map('GET', '/api/SchdDocStatus/getByRoundNotOwnerCanViewer', 'App\Apis\SchdDocStatusController@getByRoundNotOwnerCanViewer', 'SchdDocStatus.getByRoundNotOwnerCanViewer');
$router->map('POST', '/api/SchdDocStatus/send', 'App\Apis\SchdDocStatusController@send', 'SchdDocStatus.send');


/* SchdStdCourse */
$router->map('GET', '/api/SchdStdCourse/get/[a:stdCourseId]', 'App\Apis\SchdStdCourseController@get', 'SchdStdCourse.get');
$router->map('GET', '/api/SchdStdCourse/getByItsStudent/[a:itsStudentId]', 'App\Apis\SchdStdCourseController@getByItsStudent', 'SchdStdCourse.getByItsStudent');
$router->map('POST', '/api/SchdStdCourse/save', 'App\Apis\SchdStdCourseController@save', 'SchdStdCourse.save');
$router->map('DELETE', '/api/SchdStdCourse/save/[a:stdCourseId]', 'App\Apis\SchdStdCourseController@delete', 'SchdStdCourse.delete');


/* SchdFrm */
$router->map('GET', '/api/SchdFrm/getByActivityOwner', 'App\Apis\SchdFrmController@getByActivityOwner', 'SchdFrmController.getByActivityOwner');
$router->map('POST', '/api/SchdFrm/save', 'App\Apis\SchdFrmController@save', 'SchdFrmController.save');

/* SchdAttach */
$router->map('GET', '/api/SchdAttach/getByRefDocId/[a:refDocId]', 'App\Apis\SchdAttachController@getByRefDocId', 'SchdAttachController.getByRefDocId');
$router->map('DELETE', '/api/SchdAttach/delete/[a:attachId]', 'App\Apis\SchdAttachController@delete', 'SchdAttachController.delete');
$router->map('GET', '/api/SchdAttach/download/[a:attachId]', 'App\Apis\SchdAttachController@download', 'SchdAttachController.download');