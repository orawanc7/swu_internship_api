<form action="{{route('api/SchdDocSend/saveCheck')}}" method="POST" id="frmActivityStatus">
<input type="hidden" name="doctype-activitycheck-get-docsend-url" id="doctype-activitycheck-get-docsend-url" value="{{route('api/SchdDocSend/getLastSendByUser') }}"> 
<input type="hidden" name="itsStudentId">
<input type="hidden" name="activityId">
<input type="hidden" name="stdCourseId">
<input type="hidden" name="itsUserId">
<input type="hidden" name="itsStatusSendId">
<input type="hidden" name="itsDocId">
<input type="hidden" name="confirmFlag">
<div class="card b-l-primary">    
    <div class="card-header">
        <div class="row">
            <div class="col-md-2">หมายเหตุนิสิต : </div>
            <div class="col-md-10" id="senderRemark"></div>
        </div>        
    </div>
    <div class="card-footer" id="doctype-activitycheck">
        <div class="row">
            <div class="col-md-12">
                <div class="input-data">
                    <textarea class="f-13 post-input msg-send" rows="3" cols="10"
                        required=""
                        placeholder="เขียนบางอย่าง....." id="receiverRemark" name="receiverRemark"></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span class="f-left pt-3">
                    <div id="doctype-activitycheck-status"></div>
                </span>
                <span class="f-right pt-3">
                    <button type="submit"
                            class="btn btn-primary waves-effect waves-light submit-checkstatus" value="Y" id="btnConfirmYes"><i class="icofont icofont-ui-check"></i> รับงาน
                    </button>
                    <button type="submit"
                            class="btn btn-danger waves-effect waves-light submit-checkstatus" value="N" id="btnConfirmNo"><i class="icofont icofont-close"></i> ส่งคืน
                    </button>
                    <button type="submit"
                            class="btn btn-secondary waves-effect waves-light submit-checkstatus" value="C" id="btnCancel"> ยกเลิก
                    </button>
                </span>
            </div>
        </div>                        
    </div>                            
</div>
</form>