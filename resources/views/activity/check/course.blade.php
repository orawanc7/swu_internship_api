<div class="card b-l-primary" id="div-doctype-course">    
    <input type="hidden" name="doctype-course-get-url" id="doctype-course-get-url" value="{{ route('api/SchdStdCourse/get') }}">
    <div class="card-header">
        <i class="icofont icofont-paper"></i> รายละเอียด
    </div>
    <div class="card-body">                               
        <div class="form-group row">                       
            <label for="itsCourseCd" class="col-md-2 col-form-label form-control-label font-weight-bold">รหัสวิชา</label>
            <div class="col-md-4">
                <input type="text" class="form-control" name="itsCourseCd" readonly>
            </div>
            <label for="itsCourseName" class="col-md-2 col-form-label form-control-label font-weight-bold">ชื่อรายวิชา</label>
            <div class="col-md-4">                
                <input type="text" class="form-control" name="itsCourseName" readonly>             
            </div>                        
        </div>            
        <div class="form-group row">
            <label for="levelClass" class="col-md-2 col-form-label form-control-label font-weight-bold">ระดับชั้น</label>
            <div class="col-md-4">                
                <input type="text" class="form-control" name="levelClass" readonly>
            </div>
            <label for="creditAmt" class="col-md-2 col-offset-sm-2 col-form-label form-control-label font-weight-bold">หน่วยกิต</label>
            <div class="col-md-4">                
                <input type="text" class="form-control" name="creditAmt" readonly>            
            </div>                               
        </div>
        <div class="form-group row">            
            <label for="minutesQty" class="col-md-2 col-form-label form-control-label font-weight-bold">นาทีต่อ 1 คาบ</label>
            <div class="col-md-4">                
                <input type="text" class="form-control" name="minutesQty" readonly>                             
            </div>     
            <label for="hourAmt" class="col-md-2 col-form-label form-control-label font-weight-bold">ชั่วโมงทั้งวิชา</label>
            <div class="col-md-4">                                
                <input type="text" class="form-control" name="hourAmt" readonly>        
            </div>                           
        </div>
        <div class="form-group row">            
            <label for="stdRemark" class="col-md-2 col-form-label form-control-label font-weight-bold">หมายเหตุ</label>
            <div class="col-md-10">                                     
                <input type="text" class="form-control" name="stdRemark" readonly>                
            </div>                           
        </div>     
            
        @include('activity.check.uploadlist')
    </div>            
</div>