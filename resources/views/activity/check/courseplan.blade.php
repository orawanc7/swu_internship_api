<div class="card b-l-primary" id="div-doctype-courseplan">
    <input type="hidden" name="doctype-courseplan-get-url" id="doctype-courseplan-get-url" value="{{ route('api/SchdStdCoursePlan/get') }}">
    <div class="card-body">
        <div class="form-group row">            
            <label for="stdCourseId" class="col-md-2 col-form-label form-control-label font-weight-bold">วิชาที่สอน</label>
            <div class="col-md-10">
                <input type="text" class="form-control" name="stdCourse" readonly> 
            </div>                
        </div>                            
        <div class="form-group row">
            <label for="weekId" class="col-md-2 col-form-label form-control-label font-weight-bold">สัปดาห์ที่</label>
            <div class="col-md-10">
                <input type="text" class="form-control" name="weekId" readonly> 
            </div>                  
        </div>                          
        <div class="form-group row">            
            <label for="stdRemark" class="col-md-2 col-form-label form-control-label font-weight-bold">หมายเหตุ</label>
            <div class="col-md-10">                
                <input type="text" name="remark" class="form-control" readonly>                
            </div>                           
        </div>        
        
        @include('activity.check.uploadlist')
    </div>        
</div>