<input type="hidden" id="doctype-form-list-url" value="{{ route ('api/Frm/listByDocId') }}">
<div class="card b-l-primary" id="div-doctype-form-save">
    <div class="card-body">
        <div id="div-form">
            <div class="form-group">
                <label for="" class="col-md-2 col-form-label form-control-label">ชื่ออาจารย์นิเทศ</label>
                <div class="col-md-10">                
                    -
                </div>       
            </div>            
            <div class="form-group">
                <label for="" class="col-md-2 col-form-label form-control-label"></label>
                <div class="col-md-10">                
                    <div class="checkbox-fade fade-in-primary">
                        <label>
                            <input type="checkbox" value="" disabled>
                            <span class="cr"><i class="cr-icon fa fa-check txt-primary"></i></span> ประจำโรงเรียน
                        </label>
                    </div>
                    <div class="checkbox-fade fade-in-primary">
                        <label>
                            <input type="checkbox" value="" disabled>
                            <span class="cr"><i class="cr-icon fa fa-check txt-primary"></i></span> การศึกษา
                        </label>
                    </div>
                    <div class="checkbox-fade fade-in-primary">
                        <label>
                            <input type="checkbox" value="" disabled>
                            <span class="cr"><i class="cr-icon fa fa-check txt-primary"></i></span> วิชาเฉพาะ
                        </label>
                    </div>
                </div>       
            </div>   
            <div class="form-group">
                <label for="" class="col-md-2 col-form-label form-control-label">การปฏิบัติการสอน</label>                
                <div class="col-md-10">                
                    <div class="form-group">
                        <div class="checkbox-fade fade-in-primary">
                            <label>
                                <input type="checkbox" value="" disabled>
                                <span class="cr"><i class="cr-icon fa fa-check txt-primary"></i></span> การปฏิบัติการสอน
                            </label>
                        </div>
                    </div>    
                    <div class="form-group">
                        <label for="" class="col-md-2 col-form-label form-control-label">ชั้น</label>
                        <div class="col-md-10">                
                            -
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-2 col-form-label form-control-label">กลุ่มสาระการเรียนรู้</label>
                        <div class="col-md-10">                
                            -
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-2 col-form-label form-control-label">เรื่อง</label>
                        <div class="col-md-10">                
                            -
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="checkbox-fade fade-in-primary">
                            <label>
                                <input type="checkbox" value="" disabled>
                                <span class="cr"><i class="cr-icon fa fa-check txt-primary"></i></span> งานอื่นๆที่เกี่ยวข้องกับหน้าที่ครู
                            </label>
                        </div>
                    </div>    
                    <div class="form-group">
                        <div class="col-md-10">                
                            -
                        </div>
                    </div>                    
                </div>       
            </div>   
            <div class="form-group">
                <label for="" class="col-md-2 col-form-label form-control-label">ครั้งที่</label>
                <div class="col-md-10">                
                    -
                </div>       
            </div>          
            <div class="form-group">
                <label for="" class="col-md-2 col-form-label form-control-label">วันที่</label>
                <div class="col-md-10">                
                    -
                </div>       
            </div>          
            <div class="form-group">
                <label for="" class="col-md-2 col-form-label form-control-label">รายละเอียดการนิเทศ</label>
                <div class="col-md-10">                
                    -
                </div>       
            </div>
        </div>                              
    </div>        
</div>
