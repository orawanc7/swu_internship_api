<input type="hidden" name="doctype-form-get-url" id="doctype-form-get-url" value="{{ route('api/Frm/get') }}">
<input type="hidden" name="doctype-form-user-id" id="doctype-form-user-id">
<div class="card b-l-primary" id="div-doctype-form">
    <div class="card-body">    
        <div class="form-group row">
            <div class="col-md-12">
                <div id="div-form">

                </div>         
            </div>
        </div>                             
        
        <div class="form-group row d-none" id="doctype-form-uploadlist">
            <div class="col-md-12">
                @include('activity.check.uploadlist')
            </div>
        </div>                    
        
    </div>        
</div>                                                                        