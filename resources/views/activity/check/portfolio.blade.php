<input type="hidden" name="doctype-portfolio-get-url" id="doctype-portfolio-get-url" value="{{ route('api/SchdStdPortfolio/get') }}">
<div class="card b-l-primary" id="div-doctype-portfolio">
    <div class="card-body">
        <div class="form-group row">            
            <label for="researchNameTh" class="col-md-2 col-form-label form-control-label">ชื่อผลงาน</label>
            <div class="col-md-10">
                <input type="text" name="portfolioName" class="form-control" placeholder="ชื่อผลงาน" readonly maxlength="400">
            </div>                
        </div>            
          
        <div class="form-group row">            
            <label for="remark" class="col-md-2 col-form-label form-control-label">หมายเหตุ</label>
            <div class="col-md-10">                
                <input type="text" name="remark" class="form-control" readonly maxlength="500">                
            </div>                           
        </div>                 
        
        @include('activity.check.uploadlist')
    </div>        
</div>