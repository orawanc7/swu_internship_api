<input type="hidden" name="doctype-research-get-url" id="doctype-research-get-url" value="{{ route('api/SchdStdResearch/get') }}">
<div class="card b-l-primary" id="div-doctype-research">
    <div class="card-body">
        <div class="form-group row">            
            <label for="researchNameTh" class="col-md-2 col-form-label form-control-label mandatory">ชื่อภาษาไทย</label>
            <div class="col-md-10">
                <input type="text" name="researchNameTh" class="form-control" placeholder="ชื่อภาษาไทย" readonly>
            </div>                
        </div>            
        <div class="form-group row d-none">            
            <label for="researchNameEn" class="col-md-2 col-form-label form-control-label mandatory">ชื่อภาษาอังฤษ</label>
            <div class="col-md-10">
                <input type="text" name="researchNameEn" class="form-control" placeholder="ชื่อภาษาอังฤษ" readonly>
            </div>                
        </div>
        <div class="form-group row">            
            <label for="abstractTh" class="col-md-2 col-form-label form-control-label mandatory">บทคัดย่อภาษาไทย</label>
            <div class="col-md-10">
                <textarea name="abstractTh" id="abstractTh" cols="30" rows="10" class="form-control" readonly></textarea>                    
            </div>                
        </div>

        <div class="form-group row d-none">            
            <label for="abstractEn" class="col-md-2 col-form-label form-control-label mandatory">บทคัดย่อภาษาอังกฤษ</label>
            <div class="col-md-10">
                <textarea name="abstractEn" id="abstractEn" cols="30" rows="10" class="form-control" readonly></textarea>
            </div>                
        </div>           
        <div class="form-group row">            
            <label for="remark" class="col-md-2 col-form-label form-control-label">หมายเหตุ</label>
            <div class="col-md-10">                
                <input type="text" name="remark" class="form-control" readonly>                
            </div>                           
        </div>                 
        
        @include('activity.check.uploadlist')
    </div>        
</div>