<input type="hidden" name="doctype-research-get-url" id="doctype-research-get-url" value="{{ route('api/SchdStdResearch/get') }}">
<div class="card b-l-primary" id="div-doctype-research">
    <div class="card-body">        
        <div class="form-group row">            
            <label for="remark" class="col-md-2 col-form-label form-control-label">หมายเหตุ</label>
            <div class="col-md-10">                
                <input type="text" name="remark" class="form-control" readonly>                
            </div>                           
        </div>                 
        
        @include('activity.check.uploadlist')
    </div>        
</div>