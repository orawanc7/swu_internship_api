<div class="modal fade" id="modalActivityStatus" tabindex="-1" role="dialog" aria-labelledby="modalActivityStatus" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <input type="hidden" id="doctype-activity-get-docstatus-url" value="{{ route ('api/SchdDocStatus/getByItsStudentIdDocId') }}">          
        <input type="hidden" id="doctype-activity-get-activitystep-url" value="{{ route ('api/SchdActivityStep/listByActivityStep') }}">          
        <input type="hidden" id="doctype-activity-get-receiver-url" value="{{ route ('api/SchdStdInfo/listRelateByItsStudentIdPersonType') }}">          

        <form action="{{route('api/SchdActivity/send')}}" method="POST" id="frmActivityStatus">
            <input type="hidden" name="doctype-activity-nextstepseq" id="doctype-activity-nextstepseq">

            <input type="hidden" name="itsRoundId">
            <input type="hidden" name="itsStudentId">
            <input type="hidden" name="activityId">
            <input type="hidden" name="docId">
            <input type="hidden" name="stdCourseId">
            <input type="hidden" name="senderTypeId">
            <input type="hidden" name="senderId">
            <input type="hidden" name="stepSeq">

            <div class="modal-content" id="doctype-activity-status">                
                <!--
                <div class="modal-header">
                    
                </div>
                -->
                <div class="modal-body">                    
                    <div class="form-group row">
                        <div class="col-md-2"><label for="">สถานะ</label></div>
                        <div class="col-md-10" id="doctype-activity-current-status"></div>
                    </div>                    
                    <div class="form-group row">
                        <div class="col-md-2"><label for="">ผู้รับ</label></div>
                        <div class="col-md-10" id="doctype-activity-receiver">                            
                                                        
                            <div class="form-check">
                            </div>                            
                        </div>
                    </div>                    
                    <div class="form-group row">
                        <div class="col-md-2"><label for="doctype-activity-remark">หมายเหตุ</label></div>
                        <div class="col-md-10">                            
                            <textarea id="doctype-activity-remark" name="remark" cols="30" rows="3" class="form-control" autofocus></textarea>                            
                        </div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> ยกเลิก</button>
                    <button type="submit" class="btn btn-success" id="btnActivitySend"><i class="icofont icofont-paper-plane text-white"></i> ส่งงาน</button>
                </div>
            </div>
        </form>
    </div>
</div>