<div class="card b-l-primary" id="div-doctype-teach">
    <input type="hidden" id="doctype-teach-list-day-url" value="{{ route ('api/Day') }}">
    <input type="hidden" name="doctype-teach-get-url" id="doctype-teach-get-url" value="{{ route('api/SchdStdTeach/listByStdCourse') }}">
    <div class="card-header">
            <div class="form-group row">                            
                <div class="col-md-2">
                    <span class="form-control-label">วิชาที่สอน</span>
                </div>
                <div class="col-md-10">
                    <span id="itsCourseInfo""></span>
                </div>                
            </div>            
            @include('activity.check.uploadlist')    
    </div>
    <div class="card-body">
        <table class="table table-sm table-striped table-hover" id="tbDocTypeTeach">                           
            <thead>
                <tr class="bg-primary">                 
                    <th style="width:15%;vertical-align:top" class="text-left text-sm-center">วันที่สอน</th>
                    <th style="width:10%;vertical-align:top" class="text-left text-sm-center">คาบที่สอน</th>                            
                    <th style="width:15%;vertical-align:top" class="text-left text-sm-center">ช่วงเวลาที่สอน</th>
                    <th style="width:15%;vertical-align:top" class="text-left text-sm-right">จำนวนนาทีที่สอนจริง</th>                            
                    <th style="width:35%;vertical-align:top">สถานที่สอน</th>
                </tr>
            </thead>            
            <tbody>
            </tbody>
        </table>        
    </div>
</div>                                                                        
