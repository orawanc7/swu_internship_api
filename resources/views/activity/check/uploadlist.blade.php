<div class="form-group row" id="div-doctype-upload">
    <div class="col-md-2">                
        <h6 class="m-b-15">เอกสารแนบ <i class="fa fa-paperclip"></i></h6>
    </div>
    <div class="col-md-10">
        <div id="div-doctype-upload-list" class="mt-2">
        </div>
    </div>            
</div>