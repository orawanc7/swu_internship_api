
<form action="{{ route ('api/SchdStdCourse/save') }}" id="frmDocTypeCourse">
<input type="hidden" name="itsRoundId">
<input type="hidden" name="itsStudentId">
<input type="hidden" name="stdCourseId">    
<div class="card b-l-primary" id="div-doctype-course-save">    
    <div class="card-body">                               
        <div class="form-group row">            
            <label for="itsCourseCd" class="col-md-2 col-form-label form-control-label mandatory">รหัสวิชา</label>
            <div class="col-md-4">
                <div class="input-data">
                    <input type="text" name="itsCourseCd" class="form-control" placeholder="รหัสวิชาที่" required maxlength="15" autofocus>
                </div>
            </div>
            <label for="itsCourseName" class="col-md-2 col-form-label form-control-label mandatory">ชื่อรายวิชา</label>
            <div class="col-md-4">
                <div class="input-data">
                    <input type="text" name="itsCourseName" class="form-control" placeholder="ชื่อรายวิชา" required maxlength="100">
                </div>
            </div>                        
        </div>            
        <div class="form-group row">
            <label for="levelClass" class="col-md-2 col-form-label form-control-label mandatory">ระดับชั้น</label>
            <div class="col-md-4">
                <div class="input-data">
                    <input type="text" name="levelClass" class="form-control" placeholder="ระดับชั้น" required maxlength="100">
                </div>
            </div>
            <label for="creditAmt" class="col-md-2 col-offset-sm-2 col-form-label form-control-label mandatory">หน่วยกิต</label>
            <div class="col-md-4">
                <div class="input-data">
                    <input type="text" name="creditAmt" class="form-control text-right" required>
                </div>
            </div>                               
        </div>
        <div class="form-group row">            
            <label for="minutesQty" class="col-md-2 col-form-label form-control-label mandatory">นาทีต่อ 1 คาบ</label>
            <div class="col-md-4">
                <div class="input-data">
                    <input type="text" name="minutesQty" class="form-control text-right" required data-error="">
                </div>
            </div>     
            <label for="hourAmt" class="col-md-2 col-form-label form-control-label mandatory">ชั่วโมงทั้งวิชา</label>
            <div class="col-md-4">                
                <div class="input-data">
                    <input type="text" name="hourAmt" class="form-control text-right" required maxlength="30">                
                </div>
            </div>                           
        </div>
        <div class="form-group row">            
            <label for="stdRemark" class="col-md-2 col-form-label form-control-label">หมายเหตุ</label>
            <div class="col-md-10">                
                <input type="text" name="stdRemark" class="form-control" maxlength="200">                
            </div>                           
        </div>
        
        @include('activity.make.uploadlist')
    </div>        
    <div class="card-footer text-right">
        <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> บันทึก</button>
        <button class="btn btn-secondary" type="reset" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
    </div>
</div>            
</form>                                                            


<div class="card b-l-primary" id="div-doctype-course-list"> 
    <div class="card-header">
        <i class="icofont icofont-files">รายวิชาทั้งหมด</i>
    </div>   
    <div class="card-body">                                            
        <input type="hidden" id="doctype-course-list-url" value="{{ route ('api/SchdStdCourse/listByDocItsStudentId') }}">                        
        <input type="hidden" id="doctype-course-get-url" value="{{ route ('api/SchdStdCourse/get') }}">                        
        <input type="hidden" id="doctype-course-delete-url" value="{{ route ('api/SchdStdCourse/save') }}">                        
        <table class="table table-sm table-striped table-hover" id="tbDocTypeCourse">                           
            <thead>
                    <tr class="bg-primary">                
                        <th style="width:10%">รหัสวิชา</th>
                        <th style="width:24%">ชื่อรายวิชา</th>
                        <th style="width:14%">ระดับชั้น</th>
                        <th style="width:12%" class="text-left text-sm-right">หน่วยกิต</th>
                        <th style="width:12%" class="text-left text-sm-right">นาทีต่อ 1 คาบ</th>
                        <th style="width:12%" class="text-left text-sm-right">ชั่วโมงทั้งวิชา</th>                
                        <th style="width:16%" class="text-left text-sm-right"></th>                                                              
                    </tr>
            </thead>            
            <tbody>
            </tbody>
        </table>        
    </div>    
</div>
