<form action="{{ route ('api/SchdStdCoursePlan/save') }}" id="frmDocTypeCoursePlan">
    <input type="hidden" name="itsRoundId">
    <input type="hidden" name="itsStudentId">
    <input type="hidden" name="stdPlanId">
    <div class="card b-l-primary" id="div-doctype-courseplan-save">
        <div class="card-body">            
            <div class="form-group row">            
                <label for="stdCourseId" class="col-md-2 col-form-label form-control-label mandatory">วิชาที่สอน</label>
                <div class="col-md-10">
                    <select name="stdCourseId" class="select2" autofocus required></select>
                </div>                
            </div>                            
            <div class="form-group row">
                <label for="weekId" class="col-md-2 col-form-label form-control-label mandatory">สัปดาห์ที่</label>
                <div class="col-md-10">
                    <select name="weekId" class="select2" required>
                        @for($i=1;$i<=20;$i++) 
                            <option value="{{$i}}">{{$i}}</option>
                        @endfor                        
                    </select>
                </div>                  
            </div>                          
            <div class="form-group row">            
                <label for="stdRemark" class="col-md-2 col-form-label form-control-label">หมายเหตุ</label>
                <div class="col-md-10">                
                    <input type="text" name="stdRemark" class="form-control" maxlength="200" placeholder="หมายเหตุ">                
                </div>                           
            </div>           
            @include('activity.make.uploadlist')             
        </div>     
        <div class="card-footer text-right">
            <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> บันทึก</button>
            <button class="btn btn-secondary" type="reset" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
        </div>   
    </div>                                                                        
</form>

<div class="card b-l-primary" id="div-doctype-courseplan-list">    
    <div class="card-body">        
        <div class="table-responsive-sm">
            <input type="hidden" id="doctype-courseplan-list-course-url" value="{{ route ('api/SchdStdCourse/listByItsStudentId') }}">                                                        
            <input type="hidden" id="doctype-courseplan-list-url" value="{{ route ('api/SchdStdCoursePlan/listByItsStudentId') }}">                        
            <input type="hidden" id="doctype-courseplan-get-url" value="{{ route ('api/SchdStdCoursePlan/get') }}">                        
            <input type="hidden" id="doctype-courseplan-delete-url" value="{{ route ('api/SchdStdCoursePlan/save') }}">                        
            <table class="table table-sm table-striped table-hover" id="tbDocTypeCoursePlan">                           
                <thead>
                        <tr>                
                            <th style="text-align:left;width:30%">วิชาที่สอน</th>
                            <th style="text-align:center;width:15%">สัปดาห์ที่</th>
                            <th style="text-align:left;width:39%">หมายเหตุ</th>
                            <th style="text-align:right;width:1ุ6%"></th>                
                        </tr>
                </thead>            
                <tbody>
                </tbody>
            </table>
        </div>
    </div>    
</div>