<input type="hidden" id="doctype-form-listanswer-url" value="{{ route ('api/Frm/listAnswerByDocUser') }}">                                            
<input type="hidden" id="doctype-form-listanswer-key-url" value="{{ route ('api/Frm/get') }}">  

<form action="{{ route ('api/Frm/saveWithStatus') }}" id="frmDocTypeForm">                            
    <input type="hidden" name="itsUserId">    
    <input type="hidden" name="itsRoundId">    
    <input type="hidden" name="itsStudentId">    
    <input type="hidden" name="activityId">
    <input type="hidden" name="docId">    
    <input type="hidden" name="frmHdrId">
    <input type="hidden" name="frmUserId">
    <input type="hidden" name="itsStatusSendId">
    <div class="card b-l-primary" id="div-doctype-form-save">
        <div class="card-body">    
            <div class="form-group row">
                <div class="col-md-12">
                    <div id="div-form">

                    </div>         
                </div>
            </div>                             
            
            <div class="form-group row">
                <div class="col-md-12">
                    @include('activity.make.uploadlist')
                </div>
            </div>
            

            <div class="card-footer text-right">
                <button class="btn btn-primary" type="submit" id="btnSave"><i class="fa fa-save"></i> บันทึก</button>
                <button class="btn btn-secondary" type="button" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>                
                <button class="btn btn-primary md-trigger" id="btnSend" type="button" disabled data-modal="confirmMessage"><i class="icofont icofont-paper-plane text-white"></i> <span id="divSend">ยืนยัน</span></button>
            </div>                  
        </div>        
    </div>                                                                        
</form>