<form action="{{ route ('api/SchdStdPortfolio/save') }}" id="frmDocTypePortfolio">
    <input type="hidden" id="doctype-portfolio-get-url" value="{{ route ('api/SchdStdPortfolio/getByItsStudentIdDocId') }}">                            
    <input type="hidden" name="itsRoundId">
    <input type="hidden" name="itsStudentId">
    <input type="hidden" name="activityId">
    <input type="hidden" name="docId">
    <input type="hidden" name="itsPortfolioId">
    <div class="card b-l-primary" id="div-doctype-portfolio-save">
        <div class="card-body">            
            <div class="form-group row">            
                <label for="portfolioName" class="col-md-2 col-form-label form-control-label mandatory">ชื่อผลงาน</label>
                <div class="col-md-10">
                    <input type="text" name="portfolioName" class="form-control" placeholder="ชื่อผลงาน" required maxlength="400" autofocus>
                </div>                
            </div>                        
           
            <div class="form-group row">            
                <label for="remark" class="col-md-2 col-form-label form-control-label">หมายเหตุ</label>
                <div class="col-md-10">                
                    <input type="text" name="remark" class="form-control" maxlength="200">                
                </div>                           
            </div>       
        
            <div class="form-group row">
                <div class="col-md-12">
                    @include('activity.make.uploadlist')
                </div>
            </div>
        </div>        
        <div class="card-footer text-right">
                <button class="btn btn-primary" type="submit" id="btnSave"><i class="fa fa-save"></i> บันทึก</button>
                <button class="btn btn-secondary" type="reset" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
                <button class="btn btn-primary" id="btnSend" disabled><i class="icofont icofont-paper-plane text-white"></i> <span id="divSend">ส่งงาน</span></button>                
            </div>                        
        </div>   
    </div>                                                                        
</form>