<form action="{{ route ('api/SchdStdResearch/save') }}" id="frmDocTypeResearch">
    <input type="hidden" id="doctype-research-get-url" value="{{ route ('api/SchdStdResearch/getByItsStudentIdDocId') }}">                        
    <input type="hidden" id="doctype-research-download-url" value="{{ route ('api/SchdStdResearch/download') }}">                        
    <input type="hidden" name="itsRoundId">
    <input type="hidden" name="itsStudentId">
    <input type="hidden" name="activityId">
    <input type="hidden" name="docId">
    <input type="hidden" name="researchId">
    <div class="card b-l-primary" id="div-doctype-research-save">
        <div class="card-body">                        
            <div class="form-group row">            
                <label for="remark" class="col-md-2 col-form-label form-control-label">หมายเหตุ</label>
                <div class="col-md-10">                
                    <input type="text" name="remark" class="form-control" maxlength="200">                
                </div>                           
            </div>       
        
            <div class="form-group row">
                <div class="col-md-12">
                    @include('activity.make.uploadlist')
                </div>
            </div>
        </div>        
        <div class="card-footer">
            <div class="row">
                <div class="col-6">                    
                    <button class="btn btn-primary d-none" type="button" id="btnPrint"><i class="icofont icofont-file-word text-blue"></i> พิมพ์หน้าปก</button>
                </div>

                <div class="col-6  text-right">
                    <button class="btn btn-primary" type="submit" id="btnSave"><i class="fa fa-save"></i> บันทึก</button>
                    <button class="btn btn-secondary" type="reset" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
                    <button class="btn btn-primary" id="btnSend" disabled><i class="icofont icofont-paper-plane text-white"></i> <span id="divSend">ส่งงาน</span></button>
                </div>
            </div>                        
        </div>   
    </div>                                                                        
</form>