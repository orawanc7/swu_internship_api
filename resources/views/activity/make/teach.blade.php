<form action="{{ route ('api/SchdStdTeach/save') }}" id="frmDocTypeTeach">
    <input type="hidden" name="itsRoundId">
    <input type="hidden" name="itsStudentId">
    <input type="hidden" name="stdTeachId">
    <div class="card b-l-primary" id="div-doctype-teach-save">
        <div class="card-header">
            <div class="form-group row">            
                <label for="stdCourseId" class="col-md-2 col-form-label form-control-label mandatory">วิชาที่สอน</label>
                <div class="col-md-10">
                    <div class="input-data">
                        <select name="stdCourseId" class="select2" autofocus required></select>
                    </div>
                </div>                
            </div>          
            @include('activity.make.uploadlist')     
        </div>
        <div class="card-body">                                
            
            <div class="form-group row">            
                <label for="teachDay" class="col-md-2 col-form-label form-control-label mandatory">วันที่สอน</label>
                <div class="col-md-4">
                    <div class="input-data">
                        <select name="teachDay" class="select2" required></select>
                    </div>
                </div>                
                <label for="teachSeqNo" class="col-md-2 col-form-label form-control-label mandatory">คาบที่</label>
                <div class="col-md-4">
                    <div class="input-data">
                        <div class="input-data">
                            <input type="text" name="teachSeqNo" class="form-control text-center" required maxlength="2">
                        </div>
                    </div>
                </div> 
            </div>            
            <div class="form-group row">            
                <label for="time" class="col-md-2 col-form-label form-control-label mandatory">ช่วงเวลาที่สอน <i class="icofont icofont-ui-calendar"></i></label>
                <div class="col-md-2">
                    <div class="input-data">
                        <div class="input-group">
                            <input type="text" name="startTime" class="form-control text-center time" required>&nbsp;-
                        </div>
                    </div>                    
                </div>                                
                <div class="col-md-2">
                    <div class="input-data">
                        <input type="text" name="endTime" class="form-control text-center time" required>
                    </div>
                </div>
            </div>
            <div class="form-group row">            
                <label for="teachHour" class="col-md-2 col-form-label form-control-label mandatory">จำนวนนาทีที่สอนจริง <i class="icofont icofont-hour-glass"></i></label>
                <div class="col-md-4">
                    <div class="input-data">
                        <input type="text" name="teachHour" class="form-control text-right" required>
                    </div>
                </div>                     
            </div>
            <div class="form-group row">            
                <label for="teachAddrDesc" class="col-md-2 col-form-label form-control-label">สถานที่สอน</label>
                <div class="col-md-10">
                    <input type="text" name="teachAddrDesc" class="form-control" maxlength="200" placeholder="สถานที่สอน">                
                </div>                           
            </div>                       
            <div class="form-group row">            
                <label for="stdRemark" class="col-md-2 col-form-label form-control-label">หมายเหตุ</label>
                <div class="col-md-10">                
                    <input type="text" name="stdRemark" class="form-control" maxlength="200" placeholder="หมายเหตุ">                
                </div>                           
            </div>                     
                        
        </div>
        <div class="card-footer text-right">
            <button class="btn btn-primary" type="submit" id="btnSave"><i class="fa fa-save"></i> บันทึก</button>
            <button class="btn btn-secondary" type="reset" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
        </div>        
    </div>                                                                        
</form>

<div class="card b-l-primary" id="div-doctype-teach-list">    
    <div class="card-body">                
        <input type="hidden" id="doctype-teach-list-course-url" value="{{ route ('api/SchdStdCourse/listByItsStudentId') }}">                        
        <input type="hidden" id="doctype-teach-list-day-url" value="{{ route ('api/Day') }}">                        
        <input type="hidden" id="doctype-teach-list-url" value="{{ route ('api/SchdStdTeach/listByItsStudentIdStdCourse') }}">                        
        <input type="hidden" id="doctype-teach-get-url" value="{{ route ('api/SchdStdTeach/get') }}">              
        <input type="hidden" id="doctype-teach-delete-url" value="{{ route ('api/SchdStdTeach/save') }}">                        
        <table class="table table-sm table-striped table-hover" id="tbDocTypeTeach">                           
            <thead>
                วิชาที่สอน : <span id="itsCourseInfo"></span>
                <span class="f-right">
                    <button class="btn btn-primary waves-effect waves-light text-white mb-2" id="btnSend" disabled><i class="icofont icofont-paper-plane text-white"></i> ส่งงาน</button>
                </span>
            </thead>
            <thead>
                    <tr class="bg-primary">                 
                        <th style="width:15%" class="text-left text-sm-center">วันที่สอน</th>
                        <th style="width:10%" class="text-left text-sm-center">คาบที่สอน</th>                            
                        <th style="width:15%" class="text-left text-sm-center">ช่วงเวลาที่สอน</th>
                        <th style="width:15%" class="text-left text-sm-right">จำนวนนาทีที่สอนจริง</th>                            
                        <th style="width:35%">สถานที่สอน</th>                        
                        <th style="width:10%" class="text-left text-sm-right"></th>                
                    </tr>
            </thead>            
            <tbody>
            </tbody>
        </table>        
    </div>    
</div>