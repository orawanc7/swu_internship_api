<div class="modal fade" id="upload" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title"><i class="fa fa-upload"></i> เอกสารแนบ</h4>
            </div>
            <div class="modal-body">            
                <div class="form-group row">
                    <div class="col-md-12">                        
                        <div class="form-radio">
                            <div class="radio radiofill radio-inline">
                                <label><input type="radio" name="uploadType" value="F" checked><i class="helper"></i> แนบไฟล์</label>
                            </div>
                        </div>
                        <form id="upload-form" method="post" action="{{ route('api/SchdUpload/upload') }}" class="dropzone dz-clickable">
                            <input type="hidden" name="docId" value="{{$activity['docId']}}">
                            <input type="hidden" name="itsRoundId" value="{{$activity['itsRoundId']}}">
                            <input type="hidden" name="senderId" value="{{ $itsStudentId }}">
                            <input type="hidden" name="stdCourseId">                                                
                            <input type="hidden" name="refDocId">                                                
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="dz-default dz-message">
                                        <span>ลากไฟล์มาตรงนี้ หรือ คลิกเลือกไฟล์ <br>
                                            เฉพาะไฟล์ 
                                            <i class="h3 icofont icofont-file-pdf text-danger" data-toggle="tooltip" data-placement="top" title="PDF"></i> 
                                            <i class="h3 icofont icofont-file-word text-blue" data-toggle="tooltip" data-placement="top" title="WORD"></i>
                                            <i class="h3 icofont icofont-file-excel text-success" data-toggle="tooltip" data-placement="top" title="EXCEL"></i>
                                            <i class="h3 icofont icofont-file-powerpoint text-danger" data-toggle="tooltip" data-placement="top" title="POWERPONT"></i>
                                            <i class="h3 icofont icofont-file-png text-primary" data-toggle="tooltip" data-placement="top" title="IMAGE PNG"></i>
                                            <i class="h3 icofont icofont-file-jpg text-primary" data-toggle="tooltip" data-placement="top" title="IMAGE JPG"></i>                        
                                        </span> <br>และขนาดไฟล์ไม่เกิน 20MB เท่านั้น
                                    </div>
                                </div>                        
                            </div> 
                        </form>                
                    </div>
                </div>   
                <div class="form-group row">
                    <div class="col-md-12">                                                
                        <div class="form-radio">
                            <div class="radio radiofill radio-inline">
                                <label><input type="radio" name="uploadType" value="L"><i class="helper"></i> แนบลิงค์</label>
                            </div>
                        </div>
                        <form id="upload-link-form" method="post" action="{{ route('api/SchdUpload/savelink') }}">
                            <input type="hidden" name="docId" value="{{$activity['docId']}}">
                            <input type="hidden" name="itsRoundId" value="{{$activity['itsRoundId']}}">
                            <input type="hidden" name="senderId" value="{{ $itsStudentId }}">
                            <input type="hidden" name="stdCourseId">
                            <input type="hidden" name="refDocId">
                            <div class="input-data">
                                <input type="text" class="form-control" disabled id="link_url" name="link_url" maxlength="200">
                            </div>                            
                        </form>                                               
                    </div>
                </div>           
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect " data-dismiss="modal"><i class="fa fa-times"></i> ปิด</button>
                <button type="button" id="btnUpload" class="btn btn-primary waves-effect waves-light d-none"><i class="fa fa-upload"></i> แนบไฟล์</button>                
                <button type="button" id="btnSaveLink" class="btn btn-primary waves-effect waves-light d-none"><i class="icofont icofont-external-link"></i> แนบลิงค์</button>                
            </div>
        </div>
    </div>
</div>