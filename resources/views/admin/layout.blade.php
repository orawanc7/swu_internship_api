<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Swu-Internship Admin @yield('title')</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <!--Date Picker Material Icon Css-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- iconfont -->
    <link rel="stylesheet" type="text/css" href="@relative('icon/icofont/css/icofont.css')">

    <!-- simple line icon -->
    <link rel="stylesheet" type="text/css" href="@relative('icon/simple-line-icons/css/simple-line-icons.css')">
    <link rel="stylesheet" type="text/css" href="@relative('css/jquery-ui.min.css')">    
    <link rel="stylesheet" type="text/css" href="@relative('css/bootstrap.min.css')">    
    <link rel="stylesheet" type="text/css" href="@relative('css/dataTables.1.10.18.bootstrap4.min.css')">    
    <link rel="stylesheet" type="text/css" href="@relative('css/responsive.bootstrap4.min.css')">      
    <link rel="stylesheet" type="text/css" href="@relative('css/buttons.bootstrap4.min.css')">            
    <link rel="stylesheet" type="text/css" href="@relative('css/select2.min.css')">
    <link rel="stylesheet" type="text/css" href="@relative('css/fullcalendar.min.css')">    
    <link rel="stylesheet" type="text/css" href="@relative('css/dropzone.min.css')">   
    <link rel="stylesheet" type="text/css" href="@relative('css/summernote-bs4.css')"> 
    <link rel="stylesheet" type="text/css" href="@relative('css/main.min.css')"> 
    <link rel="stylesheet" type="text/css" href="@relative('css/responsive.css')">
    <link rel="stylesheet" type="text/css" href="@relative('css/color/color-1.css')" id="color"/>
    <link rel="stylesheet" type="text/css" href="@relative('css/style.css')"/>
    @yield('style')
</head>
<body class="sidebar-mini fixed">
    <input type="hidden" name="current-path" id="current-path" value="@relative('')">
    <div class="wrapper">
        <div class="loader-bg">
            <div class="loader-bar"></div>
        </div>
        
        @include('header')       
        
        @include('admin.sidebar')


        <div class="content-wrapper">
            <!-- Container-fluid starts -->
            <!-- Main content starts -->
            <div class="container-fluid">
                @yield('content')
            </div>        
            <!-- Container-fluid ends -->
        </div>    

    </div> <!-- wrapper -->
    
    <script src="@relative('js/jquery.min.js')"></script>
    <script src="@relative('js/jquery-ui.min.js')"></script>
    <script src="@relative('js/jquery.dataTables.min.js')"></script>    
    <script src="@relative('js/bootstrap.bundle.min.js')"></script>        
    <script src="@relative('js/dataTables.1.10.18.bootstrap4.min.js')"></script>    
    <script src="@relative('js/dataTables.responsive.min.js')"></script>    
    <script src="@relative('js/responsive.bootstrap4.min.js')"></script>    
    <script src="@relative('js/dataTables.buttons.min.js')"></script>        
    <script src="@relative('js/buttons.bootstrap4.min.js')"></script>
    <script src="@relative('js/select2/select2.full.min.js')"></script>         
    <script src="@relative('js/moment.min.js')"></script>
    <script src="@relative('js/fullcalendar-th.min.js')"></script>
    <script src="@relative('js/locale_fullcalendar/th.js')"></script>
    <script src="@relative('js/dropzone.min.js')"></script>
    <script src="@relative('js/jquery.inputmask.bundle.min.js')"></script>
    <script src="@relative('js/summernote-bs4.min.js')"></script>
    <script src="@relative('js/jquery.blockUI.js')"></script>   
    <script src="@relative('js/jquery.slimscroll.min.js')"></script>    
    <script src="@relative('plugins/waves/js/waves.min.js')"></script>   
    <script src="@relative('js/main.js')"></script>           
      
    @yield('script')
</body>
</html>