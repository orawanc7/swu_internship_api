@extends('admin.layout')

@section('content')

<div class="row">
    <div class="col-sm-12 p-0">
        <div class="main-header">            
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item "><i class="icofont icofont-home"></i>
                    <a href="">หน้าหลัก</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">ข้อมูลพื้นฐานงานนิเทศก์</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin/province') }}">จังหวัด</a>
                </li>
                <li class="breadcrumb-item active">อำเภอ</li>                
            </ol>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <form id="frmSave" action="{{ route('api/Amphur/save')}}" method="post">
            <input type="hidden" name="provinceId" id="provinceId" value="{{ $province['provinceId']  }}">
            <div class="card">
                <div class="card-header">
                    จังหวัด : {{ $province['provinceNameTh']  }}
                </div>
                <div class="card-block">
                    <div class="form-group row">
                        <label for="amphurId" class="col-md-2 col-form-label mandatory">รหัสอำเภอ</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="amphurId" name="amphurId" required autofocus autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="amphurNameTh" class="col-md-2 col-form-label mandatory">ชื่ออำเภอไทย</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="amphurNameTh" name="amphurNameTh" required autocomplete="off">
                        </div>
                        <label for="amphurNameEn" class="col-md-2 col-form-label">ชื่ออำเภออังกฤษ</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="amphurNameEn" name="amphurNameEn" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 offset-md-2 checkbox-color checkbox-primary">
                            <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                            <label for="activeFlag" class="text-primary">
                                    สถานะ
                            </label>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;<button class="btn btn-sm btn-default" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
                </div>
            </div>     
        </form>   
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">        
            <div class="card-block">                
                <table id="table" class="table dt-responsive table-striped nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center;width:15%;">รหัสอำเภอ</th>
                            <th style="text-align:left;width:26%;">ชื่ออำเภอไทย</th>
                            <th style="text-align:left;width:26%;">ชื่ออำเภออังกฤษ</th>                                
                            <th style="text-align:center;width:10%">สถานะ</th>    
                            <th style="text-align:center;width:8%"></th>    
                            <td style="text-align:center;width:15%">การจัดการ</td>                    
                        </tr>
                    </thead>
                    <tbody>                             
                    </tbody>                        
                </table>                
            </div>
        </div>
    </div>
</div>    
@endsection


@section('script') 
<script src="@asset('js/default.datatables.js')"></script>
<script>   
    var table;
    $(document).ready(function () {    
                            
        table = $('#table').DataTable( {
            ajax: "{{route('api/Amphur/ListByProvince')}}/{{ $province['provinceId'] }}",
            type:'GET',
            data: {
                "provinceId" : "{{ $province['provinceId'] }}"
            },
            dataType:'json',
            columns:[
                {'data':'amphurId'},
                {'data':'amphurNameTh'},
                {'data':'amphurNameEn'},
                {'data':'activeFlag','width':'10%',render: function(data) {
                    if (data=="Y") {
                        return "<i class=\"fa fa-check\"></i>";
                    } else {
                        return "";
                    }
                }},   
                {'data':'amphurId' , render: function(data){
                    var url = "{{ route('admin/district')}}" + "/" + data;
                    return "<a class=\"btn btn-sm btn-info\" href=\"" + url + "\"><i class=\"fa fa-map-marker\"></i> ตำบล</a> ";
                }},
                {'data':'amphurId' , render: function(data){
                    return "<a class=\"btn btn-sm btn-primary\" href=\"javascript:Amphur.edit('" + data + "');\"><i class=\"fa fa-edit\"></i> แก้ไข</a> " +
                    "<a class=\"btn btn-sm btn-danger\" href=\"javascript:Amphur.delete('" + data + "');\"><i class=\"fa fa-trash\"></i> ลบ</a>";
                }}                         
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0,3,4],
                },
            ],         
            'processing': true         
        });       
             
        $('#frmSave').submit(function (e) { 
            e.preventDefault();
            
            Amphur.save($(this));

        });

        $('#btnCancel').click(function(){
            Amphur.clear();
        });
    });

    var Amphur = {
        clear: function () {            
            $('#frmSave')[0].reset();
        },
        edit: function(id) {
            $.ajax({
                url: "{{route('api/Amphur/')}}" + id,
                type: "get",                
                dataType: "json",
                success: function (response) {                    
                    if (response.data) {
                        Amphur.clear();

                        var data = response.data;
                        $('#amphurId').val(data.amphurId);
                        $('#amphurNameTh').val(data.amphurNameTh);
                        $('#amphurNameEn').val(data.amphurNameEn);                                           
                        if (data.activeFlag=="Y") {
                            $('#activeFlag').prop('checked',true);
                        }
                        $('#amphurNameTh').focus();
                    }
                }
            });
        },
        delete: function(id) {                        
            $.ajax({
                type: 'DELETE',
                url: $('#frmSave').attr('action') + '/' + id,
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        Amphur.clear();
                        table.ajax.reload();

                        $('#amphurId').focus();
                    } else {
                        alert(response.message);
                    }
                }
            });
        },
        save: function(frm) {

            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        Amphur.clear();
                        table.ajax.reload();

                        $('#amphurId').focus();
                    } else {
                        alert(response.message);
                    }
                }
            });

        }
    };
    
</script>
@endsection
