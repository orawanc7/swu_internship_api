@extends('admin.layout')

@section('content')

<div class="row">
    <div class="col-sm-12 p-0">
        <div class="main-header">            
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item "><i class="icofont icofont-home"></i>
                    <a href="">หน้าหลัก</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">ข้อมูลพื้นฐานงานนิเทศก์</a>
                </li>
                <li class="breadcrumb-item active">ทวีป</li>                
            </ol>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <form id="frmSave" action="{{ route('api/Continent/save')}}" method="post">
            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <label for="continentId" class="col-md-2 col-form-label mandatory">รหัสทวีป</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="continentId" name="continentId" required autofocus autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="continentNameTh" class="col-md-2 col-form-label mandatory">ชื่อทวีปไทย</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="continentNameTh" name="continentNameTh" required autocomplete="off">
                        </div>
                        <label for="continentNameEn" class="col-md-2 col-form-label">ชื่อทวีปอังกฤษ</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="continentNameEn" name="continentNameEn" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 offset-md-2 checkbox-color checkbox-primary">
                            <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                            <label for="activeFlag" class="text-primary">
                                    สถานะ
                            </label>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;<button class="btn btn-sm btn-default" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
                </div>
            </div>     
        </form>   
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">        
            <div class="card-block">                
                <table id="table" class="table dt-responsive table-striped nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center;width:15%;">รหัสทวีป</th>
                            <th style="text-align:left;width:30%;">ชื่อทวีปไทย</th>
                            <th style="text-align:left;width:30%;">ชื่อทวีปอังกฤษ</th>                                
                            <th style="text-align:center;width:10%">สถานะ</th>    
                            <td style="text-align:center;width:15%">การจัดการ</td>                    
                        </tr>
                    </thead>
                    <tbody>                             
                    </tbody>                        
                </table>                
            </div>
        </div>
    </div>
</div>    
@endsection


@section('script') 
<script src="@asset('js/default.datatables.js')"></script>
<script>   
    var table;
    $(document).ready(function () {    
                            
        table = $('#table').DataTable( {
            ajax: "{{route('api/Continent')}}",
            type:'GET',
            dataType:'json',
            columns:[
                {'data':'continentId'},
                {'data':'continentNameTh'},
                {'data':'continentNameEn'},
                {'data':'activeFlag','width':'10%',render: function(data) {
                    if (data=="Y") {
                        return "<i class=\"fa fa-check\"></i>";
                    } else {
                        return "";
                    }
                }},                
                {'data':'continentId' , render: function(data){
                    return "<a class=\"btn btn-sm btn-primary\" href=\"javascript:Continent.edit('" + data + "');\"><i class=\"fa fa-edit\"></i> แก้ไข</a> " +
                    "<a class=\"btn btn-sm btn-danger\" href=\"javascript:Continent.delete('" + data + "');\"><i class=\"fa fa-trash\"></i> ลบ</a>";
                }}
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0,3,4],
                },
            ],         
            'processing': true         
        });       
             
        $('#frmSave').submit(function (e) { 
            e.preventDefault();
            
            Continent.save($(this));

        });

        $('#btnCancel').click(function(){
            Continent.clear();
        });
    });

    var Continent = {
        clear: function () {            
            $('#frmSave')[0].reset();
        },
        edit: function(id) {
            $.ajax({
                url: "{{route('api/Continent/')}}" + id,
                type: "get",                
                dataType: "json",
                success: function (response) {                    
                    if (response.data) {
                        Continent.clear();

                        var data = response.data;
                        $('#continentId').val(data.continentId);
                        $('#continentNameTh').val(data.continentNameTh);
                        $('#continentNameEn').val(data.continentNameEn);                                           
                        if (data.activeFlag=="Y") {
                            $('#activeFlag').prop('checked',true);
                        }
                        $('#continentNameTh').focus();
                    }
                }
            });
        },
        delete: function(id) {                        
            $.ajax({
                type: 'DELETE',
                url: $('#frmSave').attr('action') + '/' + id,
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        Continent.clear();
                        table.ajax.reload();

                        $('#continentId').focus();
                    } else {
                        alert(response.message);
                    }
                }
            });
        },
        save: function(frm) {

            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        Continent.clear();
                        table.ajax.reload();

                        $('#continentId').focus();
                    } else {
                        alert(response.message);
                    }
                }
            });

        }
    };
    
</script>
@endsection
