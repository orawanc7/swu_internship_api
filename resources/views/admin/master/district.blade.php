@extends('admin.layout')

@section('content')

<div class="row">
    <div class="col-sm-12 p-0">
        <div class="main-header">            
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item "><i class="icofont icofont-home"></i>
                    <a href="">หน้าหลัก</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">ข้อมูลพื้นฐานงานนิเทศก์</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin/province') }}">จังหวัด</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin/amphur') }}/{{ $amphur['provinceId'] }}">อำเภอ</a>
                </li>
                <li class="breadcrumb-item active">ตำบล</li>                
            </ol>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <form id="frmSave" action="{{ route('api/District/save')}}" method="post">
            <input type="hidden" name="provinceId" id="provinceId" value="{{ $amphur['provinceId']  }}">
            <input type="hidden" name="amphurId" id="amphurId" value="{{ $amphur['amphurId']  }}">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            จังหวัด : {{ $amphur['provinceNameTh']  }}
                        </div>
                        <div class="col-md-6">
                            ตำบล : {{ $amphur['amphurNameTh']  }}
                        </div>
                    </div>                    
                </div>
                <div class="card-block">
                    <div class="form-group row">
                        <label for="districtId" class="col-md-2 col-form-label mandatory">รหัสตำบล</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="districtId" name="districtId" required autofocus autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="districtNameTh" class="col-md-2 col-form-label mandatory">ชื่อตำบลไทย</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="districtNameTh" name="districtNameTh" required autocomplete="off">
                        </div>
                        <label for="districtNameEn" class="col-md-2 col-form-label">ชื่อตำบลอังกฤษ</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="districtNameEn" name="districtNameEn" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 offset-md-2 checkbox-color checkbox-primary">
                            <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                            <label for="activeFlag" class="text-primary">
                                    สถานะ
                            </label>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;<button class="btn btn-sm btn-default" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
                </div>
            </div>     
        </form>   
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">        
            <div class="card-block">                
                <table id="table" class="table dt-responsive table-striped nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center;width:15%;">รหัสตำบล</th>
                            <th style="text-align:left;width:30%;">ชื่อตำบลไทย</th>
                            <th style="text-align:left;width:30%;">จังหวัด</th>
                            <th style="text-align:center;width:10%">สถานะ</th>    
                            <td style="text-align:center;width:15%">การจัดการ</td>                    
                        </tr>
                    </thead>
                    <tbody>                             
                    </tbody>                        
                </table>                
            </div>
        </div>
    </div>
</div>    
@endsection


@section('script') 
<script src="@asset('js/default.datatables.js')"></script>
<script>   
    var table;
    $(document).ready(function () {    
                            
        table = $('#table').DataTable( {
            ajax: "{{route('api/District/ListByAmphur')}}/{{ $amphur['amphurId'] }}",
            type:'GET',
            data: {
                "provinceId" : "{{ $amphur['provinceId'] }}"
            },
            dataType:'json',
            columns:[
                {'data':'districtId'},
                {'data':'districtNameTh'},
                {'data':'districtNameEn'},
                {'data':'activeFlag','width':'10%',render: function(data) {
                    if (data=="Y") {
                        return "<i class=\"fa fa-check\"></i>";
                    } else {
                        return "";
                    }
                }},                
                {'data':'districtId' , render: function(data){
                    return "<a class=\"btn btn-sm btn-primary\" href=\"javascript:District.edit('" + data + "');\"><i class=\"fa fa-edit\"></i> แก้ไข</a> " +
                    "<a class=\"btn btn-sm btn-danger\" href=\"javascript:District.delete('" + data + "');\"><i class=\"fa fa-trash\"></i> ลบ</a>";
                }}
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0,3,4],
                },
            ],         
            'processing': true         
        });       
             
        $('#frmSave').submit(function (e) { 
            e.preventDefault();
            
            District.save($(this));

        });

        $('#btnCancel').click(function(){
            District.clear();
        });
    });

    var District = {
        clear: function () {            
            $('#frmSave')[0].reset();
        },
        edit: function(id) {
            $.ajax({
                url: "{{route('api/District/')}}" + id,
                type: "get",                
                dataType: "json",
                success: function (response) {                    
                    if (response.data) {
                        District.clear();

                        var data = response.data;
                        $('#districtId').val(data.districtId);
                        $('#districtNameTh').val(data.districtNameTh);
                        $('#districtNameEn').val(data.districtNameEn);                                           
                        if (data.activeFlag=="Y") {
                            $('#activeFlag').prop('checked',true);
                        }
                        $('#districtNameTh').focus();
                    }
                }
            });
        },
        delete: function(id) {                        
            $.ajax({
                type: 'DELETE',
                url: $('#frmSave').attr('action') + '/' + id,
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        District.clear();
                        table.ajax.reload();

                        $('#districtId').focus();
                    } else {
                        alert(response.message);
                    }
                }
            });
        },
        save: function(frm) {

            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        District.clear();
                        table.ajax.reload();

                        $('#districtId').focus();
                    } else {
                        alert(response.message);
                    }
                }
            });

        }
    };
    
</script>
@endsection
