@extends('admin.layout')

@section('content')

<div class="row">
    <div class="col-sm-12 p-0">
        <div class="main-header">            
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item "><i class="icofont icofont-home"></i>
                    <a href="">หน้าหลัก</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">ข้อมูลพื้นฐานงานนิเทศก์</a>
                </li>
                <li class="breadcrumb-item active">กลุ่มสาระการเรียนรู้</li>                
            </ol>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <form id="frmSave" action="{{ route('api/LearningGroup/save')}}" method="post">
            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <label for="learningGroupId" class="col-md-2 col-form-label mandatory">รหัสกลุ่มการเรียนรู้</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="learningGroupId" name="learningGroupId" required autofocus autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="learningGroupNameTh" class="col-md-2 col-form-label mandatory">กลุ่มสาระการเรียนรู้ไทย</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="learningGroupNameTh" name="learningGroupNameTh" required autocomplete="off">
                        </div>
                        <label for="learningGroupNameEn" class="col-md-2 col-form-label">กลุ่มสาระการเรียนรู้อังกฤษ</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="learningGroupNameEn" name="learningGroupNameEn" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                            <label for="remark" class="col-md-2 col-form-label">หมายเหตุ</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" id="remark" name="remark" autocomplete="off">
                            </div>                    
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 offset-md-2 checkbox-color checkbox-primary">
                            <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                            <label for="activeFlag" class="text-primary">
                                    สถานะ
                            </label>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;<button class="btn btn-sm btn-default" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
                </div>
            </div>     
        </form>   
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">        
            <div class="card-block">                
                <table id="table" class="table dt-responsive table-striped nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center;width:15%;">รหัสกลุ่มการเรียนรู้</th>
                            <th style="text-align:left;width:30%;">กลุ่มสาระการเรียนรู้ไทย</th>
                            <th style="text-align:left;width:30%;">กลุ่มสาระการเรียนรู้อังกฤษ</th>                                
                            <th style="text-align:center;width:10%">สถานะ</th>    
                            <td style="text-align:center;width:15%">การจัดการ</td>                    
                        </tr>
                    </thead>
                    <tbody>                             
                    </tbody>                        
                </table>                
            </div>
        </div>
    </div>
</div>    
@endsection


@section('script') 
<script src="@asset('js/default.datatables.js')"></script>
<script>   
    var table;
    $(document).ready(function () {    
                            
        table = $('#table').DataTable( {
            ajax: "{{route('api/LearningGroup')}}",
            type:'GET',
            dataType:'json',
            columns:[
                {'data':'learningGroupId'},
                {'data':'learningGroupNameTh'},
                {'data':'learningGroupNameEn'},
                {'data':'activeFlag','width':'10%',render: function(data) {
                    if (data=="Y") {
                        return "<i class=\"fa fa-check\"></i>";
                    } else {
                        return "";
                    }
                }},                
                {'data':'learningGroupId' , render: function(data){
                    return "<a class=\"btn btn-sm btn-primary\" href=\"javascript:LearningGroup.edit('" + data + "');\"><i class=\"fa fa-edit\"></i> แก้ไข</a> " +
                    "<a class=\"btn btn-sm btn-danger\" href=\"javascript:LearningGroup.delete('" + data + "');\"><i class=\"fa fa-trash\"></i> ลบ</a>";
                }}
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0,3,4],
                },
            ],         
            'processing': true         
        });       
             
        $('#frmSave').submit(function (e) { 
            e.preventDefault();
            
            LearningGroup.save($(this));

        });

        $('#btnCancel').click(function(){
            LearningGroup.clear();
        });
    });

    var LearningGroup = {
        clear: function () {            
            $('#frmSave')[0].reset();
        },
        edit: function(id) {
            $.ajax({
                url: "{{route('api/LearningGroup/')}}" + id,
                type: "get",                
                dataType: "json",
                success: function (response) {                    
                    if (response.data) {
                        LearningGroup.clear();

                        var data = response.data;
                        $('#learningGroupId').val(data.learningGroupId);
                        $('#learningGroupNameTh').val(data.learningGroupNameTh);
                        $('#learningGroupNameEn').val(data.learningGroupNameEn);
                        $('#remark').val(data.remark);                        
                        if (data.activeFlag=="Y") {
                            $('#activeFlag').prop('checked',true);
                        }
                        $('#learningGroupNameTh').focus();
                    }
                }
            });
        },
        delete: function(id) {                        
            $.ajax({
                type: 'DELETE',
                url: $('#frmSave').attr('action') + '/' + id,
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        LearningGroup.clear();
                        table.ajax.reload();

                        $('#learningGroupId').focus();
                    } else {
                        alert(response.message);
                    }
                }
            });
        },
        save: function(frm) {

            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        LearningGroup.clear();
                        table.ajax.reload();

                        $('#learningGroupId').focus();
                    } else {
                        alert(response.message);
                    }
                }
            });

        }
    };
    
</script>
@endsection
