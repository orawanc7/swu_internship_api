@extends('admin.layout')

@section('content')

<div class="row">
    <div class="col-sm-12 p-0">
        <div class="main-header">            
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item "><i class="icofont icofont-home"></i>
                    <a href="">หน้าหลัก</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">ข้อมูลพื้นฐานงานนิเทศก์</a>
                </li>
                <li class="breadcrumb-item active">ประเภทบุคคลากร</li>                
            </ol>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <form id="frmSave" action="{{ route('api/PersonType/save')}}" method="post">
            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <label for="personTypeId" class="col-md-2 col-form-label mandatory">รหัสบุคคลากร</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="personTypeId" name="personTypeId" required autofocus autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="personTypeNameTh" class="col-md-2 col-form-label mandatory">ชื่อประเภทบุคคลากรไทย</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="personTypeNameTh" name="personTypeNameTh" required autocomplete="off">
                        </div>
                        <label for="personTypeNameEn" class="col-md-2 col-form-label">ชื่อประเภทบุคคลากรอังกฤษ</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="personTypeNameEn" name="personTypeNameEn" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                            <label for="remark" class="col-md-2 col-form-label">หมายเหตุ</label>
                            <div class="col-md-10">
                                <input class="form-control" type="text" id="remark" name="remark" autocomplete="off">
                            </div>                    
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 offset-md-2 checkbox-color checkbox-primary">
                            <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                            <label for="activeFlag" class="text-primary">
                                    สถานะ
                            </label>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;<button class="btn btn-sm btn-default" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
                </div>
            </div>     
        </form>   
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">        
            <div class="card-block">                
                <table id="table" class="table dt-responsive table-striped nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center;width:15%;">รหัสบุคคลากร</th>
                            <th style="text-align:left;width:30%;">ชื่อประเภทบุคคลากรไทย</th>
                            <th style="text-align:left;width:30%;">ชื่อประเภทบุคคลากรอังกฤษ</th>                                
                            <th style="text-align:center;width:10%">สถานะ</th>    
                            <td style="text-align:center;width:15%">การจัดการ</td>                    
                        </tr>
                    </thead>
                    <tbody>                             
                    </tbody>                        
                </table>                
            </div>
        </div>
    </div>
</div>    
@endsection


@section('script') 
<script src="@asset('js/default.datatables.js')"></script>
<script>   
    var table;
    $(document).ready(function () {    
                            
        table = $('#table').DataTable( {
            ajax: "{{route('api/PersonType')}}",
            type:'GET',
            dataType:'json',
            columns:[
                {'data':'personTypeId'},
                {'data':'personTypeNameTh'},
                {'data':'personTypeNameEn'},
                {'data':'activeFlag','width':'10%',render: function(data) {
                    if (data=="Y") {
                        return "<i class=\"fa fa-check\"></i>";
                    } else {
                        return "";
                    }
                }},                
                {'data':'personTypeId' , render: function(data){
                    return "<a class=\"btn btn-sm btn-primary\" href=\"javascript:PersonType.edit('" + data + "');\"><i class=\"fa fa-edit\"></i> แก้ไข</a> " +
                    "<a class=\"btn btn-sm btn-danger\" href=\"javascript:PersonType.delete('" + data + "');\"><i class=\"fa fa-trash\"></i> ลบ</a>";
                }}
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0,3,4],
                },
            ],         
            'processing': true         
        });       
             
        $('#frmSave').submit(function (e) { 
            e.preventDefault();
            
            PersonType.save($(this));

        });

        $('#btnCancel').click(function(){
            PersonType.clear();
        });
    });

    var PersonType = {
        clear: function () {            
            $('#frmSave')[0].reset();
        },
        edit: function(id) {
            $.ajax({
                url: "{{route('api/PersonType/')}}" + id,
                type: "get",                
                dataType: "json",
                success: function (response) {                    
                    if (response.data) {
                        PersonType.clear();

                        var data = response.data;
                        $('#personTypeId').val(data.personTypeId);
                        $('#personTypeNameTh').val(data.personTypeNameTh);
                        $('#personTypeNameEn').val(data.personTypeNameEn);
                        $('#remark').val(data.remark);                        
                        if (data.activeFlag=="Y") {
                            $('#activeFlag').prop('checked',true);
                        }
                        $('#personTypeNameTh').focus();
                    }
                }
            });
        },
        delete: function(id) {                        
            $.ajax({
                type: 'DELETE',
                url: $('#frmSave').attr('action') + '/' + id,
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        PersonType.clear();
                        table.ajax.reload();

                        $('#personTypeId').focus();
                    } else {
                        alert(response.message);
                    }
                }
            });
        },
        save: function(frm) {

            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        PersonType.clear();
                        table.ajax.reload();

                        $('#personTypeId').focus();
                    } else {
                        alert(response.message);
                    }
                }
            });

        }
    };
    
</script>
@endsection
