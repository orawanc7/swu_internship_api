@extends('admin.layout')

@section('content')

<div class="row">
    <div class="col-sm-12 p-0">
        <div class="main-header">            
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item "><i class="icofont icofont-home"></i>
                    <a href="">หน้าหลัก</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">ข้อมูลพื้นฐานงานนิเทศก์</a>
                </li>
                <li class="breadcrumb-item active">จังหวัด</li>                
            </ol>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">        
            <div class="card-block">                
                <table id="table" class="table dt-responsive table-striped nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:text-center;width:20%">รหัส</th>
                            <th style="text-align:text-left;width:30%">ชื่อจังหวัด</th>
                            <th style="text-align:text-left;width:30%">ภาค</th>                                                            
                            <th style="text-align:text-center;width:10%">สถานะ</th>      
                            <td style="text-align:center;width:10%"></td>                  
                        </tr>
                    </thead>
                    <tbody>                                     
                    </tbody>                        
                </table>                
            </div>
        </div>
    </div>
</div>    
@endsection


@section('script') 
<script src="@asset('js/default.datatables.js')"></script>
<script>       
    var table;
    $(document).ready(function () {                
        table = $('#table').DataTable( {            
            ajax: "{{route('api/Province')}}",
            type:'GET',
            dataType:'json',
            columns:[
                {'data':'provinceId','width':'20%'},
                {'data':'provinceNameTh','width':'35%'},
                {'data':'regionNameTh','width':'35%'},
                {'data':'activeFlag','width':'10%',render: function(data) {
                    if (data=="Y") {
                        return "<i class=\"fa fa-check\"></i>";
                    } else {
                        return "";
                    }
                }},
                {'data':'provinceId' , render: function(data){
                    var url = "{{ route('admin/amphur')}}" + "/" + data;
                    return "<a class=\"btn btn-sm btn-info\" href=\"" + url + "\"><i class=\"fa fa-map-marker\"></i> อำเภอ</a> ";
                }}
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0,3],
                },
            ]
        });

        //$( table.table().container() ).removeClass( 'form-inline' );
                    
    });
</script>
@endsection
