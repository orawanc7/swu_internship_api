@extends('admin.layout')

@section('content')

<div class="row">
    <div class="col-sm-12 p-0">
        <div class="main-header">            
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item "><i class="icofont icofont-home"></i>
                    <a href="">หน้าหลัก</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">ข้อมูลพื้นฐานงานนิเทศก์</a>
                </li>
                <li class="breadcrumb-item active">สถานศึกษา</li>                
            </ol>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <form id="frmSearch" action="{{ route('api/School/save')}}" method="post">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    <i class="fa fa-search"></i> เงื่อนไขการค้นหาข้อมูล
                </div>
                <div class="card-block">
                    <div class="form-group row">
                        <label for="schoolId" class="col-md-2 col-form-label">รหัสสถานศึกษา</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="schoolId" name="schoolId" autofocus autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="schoolNameTh" class="col-md-2 col-form-label">ชื่อสถานศึกษาไทย</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="schoolNameTh" name="schoolNameTh" autocomplete="off">
                        </div>
                        <label for="schoolNameEn" class="col-md-2 col-form-label">ชื่อสถานศึกษาอังกฤษ</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="schoolNameEn" name="schoolNameEn" autocomplete="off">
                        </div>
                    </div>                    
                    <div class="form-group row">
                        <label for="schoolNameTh" class="col-md-2 col-form-label">จังหวัด</label>
                        <div class="col-md-4">
                            <select name="provinceId" id="provinceId" class="form-control select2" ></select>
                        </div>
                        <label for="schoolNameEn" class="col-md-2 col-form-label">ประเภทสถานศึกษา</label>
                        <div class="col-md-4">
                            <select name="schoolTypeId" id="schoolTypeId" class="form-control select2"></select>
                        </div>
                    </div>                    
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> ค้นหาข้อมูล</button>&nbsp;<button class="btn btn-sm btn-default" id="btnCancel"><i class="fa fa-eraser"></i> ล้าง</button>
                </div>
            </div>     
        </form>   
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">        
            <div class="card-block">                
                <table id="table" class="table dt-responsive table-striped nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center;width:15%;">รหัสกลุ่มการเรียนรู้</th>
                            <th style="text-align:left;width:30%;">ชื่อสถานศึกษาไทย</th>
                            <th style="text-align:left;width:30%;">ชื่อสถานศึกษาอังกฤษ</th>                                
                            <th style="text-align:center;width:10%">สถานะ</th>    
                            <td style="text-align:center;width:15%">การจัดการ</td>                    
                        </tr>
                    </thead>
                    <tbody>                                              
                    </tbody>                        
                </table>                
            </div>
        </div>
    </div>
</div>    
@endsection


@section('script') 
<script src="@asset('js/default.datatables.js')"></script>
<script>   
    var table;
    $(document).ready(function () {    
                            
        table = $('#table').DataTable( {
            ajax: {
                url: "{{route('api/School')}}",
                type:'post',
                dataType:'json',
                dataSrc: function (json) {                    
                    return json;
                },
                data: function (d) {
                    if (1==2) {
                        return $.extend({}, d, {
                            "xxx": "joy"
                        });
                    }
                },
                error: function (xhr, error, thrown) {

                },
            },
            deferLoading: 0,
            columns:[
                {'data':'schoolId'},
                {'data':'schoolNameTh'},
                {'data':'schoolNameEn'},
                {'data':'activeFlag','width':'10%',render: function(data) {
                    if (data=="Y") {
                        return "<i class=\"fa fa-check\"></i>";
                    } else {
                        return "";
                    }
                }},                
                {'data':'schoolId' , render: function(data){
                    return "<a class=\"btn btn-sm btn-primary\" href=\"javascript:School.edit('" + data + "');\"><i class=\"fa fa-edit\"></i> แก้ไข</a> " +
                    "<a class=\"btn btn-sm btn-danger\" href=\"javascript:School.delete('" + data + "');\"><i class=\"fa fa-trash\"></i> ลบ</a>";
                }}
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0,3,4],
                },
            ],         
            "processing": true,
            "serverSide": true,
            "dom": "<'row'<'col-md-6'B><'col-md-6'>>" +
                "<'row'<'col-md-12'tr>>" +
                "<'row'<'col-md-6'i><'col-md-6'p>>",
            buttons: [
                {
                    text: "<i class=\"fa fa-plus\"></i> เพิ่ม",
                    className: "btn btn-info",
                    action: function (e, dt, node, config) {
                        location.href = "";
                    }
                }
            ]         
        });       
             
        $('#frmSearch').submit(function (e) { 
            e.preventDefault();
            
            School.search($(this));

        });

        $('#btnCancel').click(function(){
            School.clear();
        });
    });

    var School = {
        clear: function () {            
            $('#frmSearch')[0].reset();
            table.fnClearTable();
            alert('test');
        },        
        search: function(frm) {

            

        }
    };
    
</script>
@endsection
