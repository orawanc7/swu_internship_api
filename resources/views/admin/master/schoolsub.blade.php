@extends('admin.layout')

@section('content')

<div class="row">
    <div class="col-sm-12 p-0">
        <div class="main-header">            
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item "><i class="icofont icofont-home"></i>
                    <a href="">หน้าหลัก</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">ข้อมูลพื้นฐานงานนิเทศก์</a>
                </li>
                <li class="breadcrumb-item active">สังกัดสถานศึกษา</li>                
            </ol>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <form id="frmSave" action="{{ route('api/SchoolSub/save')}}" method="post">
            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <label for="schoolSubId" class="col-md-2 col-form-label mandatory">รหัสสังกัดสถานศึกษา</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="schoolSubId" name="schoolSubId" required autofocus autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="schoolSubNameTh" class="col-md-2 col-form-label mandatory">ชื่อสังกัดสถานศึกษาไทย</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="schoolSubNameTh" name="schoolSubNameTh" required autocomplete="off">
                        </div>
                        <label for="schoolSubNameEn" class="col-md-2 col-form-label">ชื่อสังกัดสถานศึกษาอังกฤษ</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="schoolSubNameEn" name="schoolSubNameEn" autocomplete="off">
                        </div>
                    </div>                    
                    <div class="form-group row">
                        <div class="col-md-12 offset-md-2 checkbox-color checkbox-primary">
                            <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                            <label for="activeFlag" class="text-primary">
                                    สถานะ
                            </label>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;<button class="btn btn-sm btn-default" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
                </div>
            </div>     
        </form>   
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">        
            <div class="card-block">                
                <table id="table" class="table dt-responsive table-striped nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center;width:15%;">รหัสกลุ่มการเรียนรู้</th>
                            <th style="text-align:left;width:30%;">ชื่อสังกัดสถานศึกษาไทย</th>
                            <th style="text-align:left;width:30%;">ชื่อสังกัดสถานศึกษาอังกฤษ</th>                                
                            <th style="text-align:center;width:10%">สถานะ</th>    
                            <td style="text-align:center;width:15%">การจัดการ</td>                    
                        </tr>
                    </thead>
                    <tbody>                             
                    </tbody>                        
                </table>                
            </div>
        </div>
    </div>
</div>    
@endsection


@section('script') 
<script src="@asset('js/default.datatables.js')"></script>
<script>   
    var table;
    $(document).ready(function () {    
                            
        table = $('#table').DataTable( {
            ajax: "{{route('api/SchoolSub')}}",
            type:'GET',
            dataType:'json',
            columns:[
                {'data':'schoolSubId'},
                {'data':'schoolSubNameTh'},
                {'data':'schoolSubNameEn'},
                {'data':'activeFlag','width':'10%',render: function(data) {
                    if (data=="Y") {
                        return "<i class=\"fa fa-check\"></i>";
                    } else {
                        return "";
                    }
                }},                
                {'data':'schoolSubId' , render: function(data){
                    return "<a class=\"btn btn-sm btn-primary\" href=\"javascript:SchoolSub.edit('" + data + "');\"><i class=\"fa fa-edit\"></i> แก้ไข</a> " +
                    "<a class=\"btn btn-sm btn-danger\" href=\"javascript:SchoolSub.delete('" + data + "');\"><i class=\"fa fa-trash\"></i> ลบ</a>";
                }}
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0,3,4],
                },
            ],         
            'processing': true         
        });       
             
        $('#frmSave').submit(function (e) { 
            e.preventDefault();
            
            SchoolSub.save($(this));

        });

        $('#btnCancel').click(function(){
            SchoolSub.clear();
        });
    });

    var SchoolSub = {
        clear: function () {            
            $('#frmSave')[0].reset();
        },
        edit: function(id) {            
            $.ajax({
                url: "{{route('api/SchoolSub/')}}" + id,
                type: "get",                
                dataType: "json",
                success: function (response) {                    
                    if (response.data) {
                        SchoolSub.clear();

                        var data = response.data;
                        $('#schoolSubId').val(data.schoolSubId);
                        $('#schoolSubNameTh').val(data.schoolSubNameTh);
                        $('#schoolSubNameEn').val(data.schoolSubNameEn);
                                              
                        if (data.activeFlag=="Y") {
                            $('#activeFlag').prop('checked',true);
                        }
                        $('#schoolSubNameTh').focus();
                    }
                }
            });
        },
        delete: function(id) {                        
            $.ajax({
                type: 'DELETE',
                url: $('#frmSave').attr('action') + '/' + id,
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        SchoolSub.clear();
                        table.ajax.reload();

                        $('#schoolSubId').focus();
                    } else {
                        alert(response.message);
                    }
                }
            });
        },
        save: function(frm) {

            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        SchoolSub.clear();
                        table.ajax.reload();

                        $('#schoolSubId').focus();
                    } else {
                        alert(response.message);
                    }
                }
            });

        }
    };
    
</script>
@endsection
