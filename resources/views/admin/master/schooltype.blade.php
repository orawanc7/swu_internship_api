@extends('admin.layout')

@section('content')

<div class="row">
    <div class="col-sm-12 p-0">
        <div class="main-header">            
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item "><i class="icofont icofont-home"></i>
                    <a href="">หน้าหลัก</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">ข้อมูลพื้นฐานงานนิเทศก์</a>
                </li>
                <li class="breadcrumb-item active">ประเภทสถานศึกษา</li>                
            </ol>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <form id="frmSave" action="{{ route('api/SchoolType/save')}}" method="post">
            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <label for="schoolTypeId" class="col-md-2 col-form-label mandatory">รหัสประเภทสถานศึกษา</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="schoolTypeId" name="schoolTypeId" required autofocus autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="schoolTypeNameTh" class="col-md-2 col-form-label mandatory">ชื่อประเภทสถานศึกษาไทย</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="schoolTypeNameTh" name="schoolTypeNameTh" required autocomplete="off">
                        </div>
                        <label for="schoolTypeNameEn" class="col-md-2 col-form-label">ชื่อประเภทสถานศึกษาอังกฤษ</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="schoolTypeNameEn" name="schoolTypeNameEn" autocomplete="off">
                        </div>
                    </div>                    
                    <div class="form-group row">
                        <div class="col-md-12 offset-md-2 checkbox-color checkbox-primary">
                            <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                            <label for="activeFlag" class="text-primary">
                                    สถานะ
                            </label>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;<button class="btn btn-sm btn-default" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
                </div>
            </div>     
        </form>   
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">        
            <div class="card-block">                
                <table id="table" class="table dt-responsive table-striped nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center;width:15%;">รหัสกลุ่มการเรียนรู้</th>
                            <th style="text-align:left;width:30%;">ชื่อประเภทสถานศึกษาไทย</th>
                            <th style="text-align:left;width:30%;">ชื่อประเภทสถานศึกษาอังกฤษ</th>                                
                            <th style="text-align:center;width:10%">สถานะ</th>    
                            <td style="text-align:center;width:15%">การจัดการ</td>                    
                        </tr>
                    </thead>
                    <tbody>                             
                    </tbody>                        
                </table>                
            </div>
        </div>
    </div>
</div>    
@endsection


@section('script') 
<script src="@asset('js/default.datatables.js')"></script>
<script>   
    var table;
    $(document).ready(function () {    
                            
        table = $('#table').DataTable( {
            ajax: "{{route('api/SchoolType')}}",
            type:'GET',
            dataType:'json',
            columns:[
                {'data':'schoolTypeId'},
                {'data':'schoolTypeNameTh'},
                {'data':'schoolTypeNameEn'},
                {'data':'activeFlag','width':'10%',render: function(data) {
                    if (data=="Y") {
                        return "<i class=\"fa fa-check\"></i>";
                    } else {
                        return "";
                    }
                }},                
                {'data':'schoolTypeId' , render: function(data){
                    return "<a class=\"btn btn-sm btn-primary\" href=\"javascript:SchoolType.edit('" + data + "');\"><i class=\"fa fa-edit\"></i> แก้ไข</a> " +
                    "<a class=\"btn btn-sm btn-danger\" href=\"javascript:SchoolType.delete('" + data + "');\"><i class=\"fa fa-trash\"></i> ลบ</a>";
                }}
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0,3,4],
                },
            ],         
            'processing': true         
        });       
             
        $('#frmSave').submit(function (e) { 
            e.preventDefault();
            
            SchoolType.save($(this));

        });

        $('#btnCancel').click(function(){
            SchoolType.clear();
        });
    });

    var SchoolType = {
        clear: function () {            
            $('#frmSave')[0].reset();
        },
        edit: function(id) {            
            $.ajax({
                url: "{{route('api/SchoolType/')}}" + id,
                type: "get",                
                dataType: "json",
                success: function (response) {                    
                    if (response.data) {
                        SchoolType.clear();

                        var data = response.data;
                        $('#schoolTypeId').val(data.schoolTypeId);
                        $('#schoolTypeNameTh').val(data.schoolTypeNameTh);
                        $('#schoolTypeNameEn').val(data.schoolTypeNameEn);
                                              
                        if (data.activeFlag=="Y") {
                            $('#activeFlag').prop('checked',true);
                        }
                        $('#schoolTypeNameTh').focus();
                    }
                }
            });
        },
        delete: function(id) {                        
            $.ajax({
                type: 'DELETE',
                url: $('#frmSave').attr('action') + '/' + id,
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        SchoolType.clear();
                        table.ajax.reload();

                        $('#schoolTypeId').focus();
                    } else {
                        alert(response.message);
                    }
                }
            });
        },
        save: function(frm) {

            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        SchoolType.clear();
                        table.ajax.reload();

                        $('#schoolTypeId').focus();
                    } else {
                        alert(response.message);
                    }
                }
            });

        }
    };
    
</script>
@endsection
