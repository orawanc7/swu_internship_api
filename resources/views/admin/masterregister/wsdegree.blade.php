@extends('admin.layout')

@section('content')

<div class="row">
    <div class="col-sm-12 p-0">
        <div class="main-header">            
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item "><i class="icofont icofont-home"></i>
                    <a href="">หน้าหลัก</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">ข้อมูลพื้นฐานงานทะเบียน</a>
                </li>
                <li class="breadcrumb-item active">วุฒิการศึกษา</li>                
            </ol>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">        
            <div class="card-block">                
                <table id="table" class="table dt-responsive table-striped nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:text-center;width:20%">รหัส</th>
                            <th style="text-align:text-left;width:30%">ชื่อวุฒิการศึกษาไทย</th>
                            <th style="text-align:text-left;width:25%">ชื่อวุฒิการศึกษาอังกฤษ</th>                                                            
                            <th style="text-align:text-center;width:25%">กลุ่มระดับการศึกษา</th>                        
                        </tr>
                    </thead>
                    <tbody>                                     
                    </tbody>                        
                </table>                
            </div>
        </div>
    </div>
</div>    
@endsection


@section('script') 
<script src="@asset('js/default.datatables.js')"></script>
<script>       
    var table;
    $(document).ready(function () {                
        table = $('#table').DataTable( {            
            ajax: "{{route('api/WSDegree')}}",
            type:'GET',
            dataType:'json',
            columns:[
                {'data':'degreeCd'},
                {'data':'degreeLnameTh'},
                {'data':'degreeLnameEng'},
                {'data':'levelGroupNameTh'},
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0,3],
                },
            ]
        });

        //$( table.table().container() ).removeClass( 'form-inline' );
                    
    });
</script>
@endsection
