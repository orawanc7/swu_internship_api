@extends('admin.layout')

@section('content')

<div class="row">
    <div class="col-sm-12 p-0">
        <div class="main-header">            
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item "><i class="icofont icofont-home"></i>
                    <a href="">หน้าหลัก</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">ข้อมูลพื้นฐานงานทะเบียน</a>
                </li>
                <li class="breadcrumb-item active">คำนำหน้าชื่อ</li>                
            </ol>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card">        
            <div class="card-block">                
                <table id="table" class="table dt-responsive table-striped nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center;width:20%;">รหัส</th>
                            <th style="text-align:left;width:40%;">ชื่อคำนำหน้าไทย</th>
                            <th style="text-align:left;width:40%;">ชื่อคำนำหน้าอังกฤษ</th>                                                                             
                        </tr>
                    </thead>
                    <tbody>                             
                    </tbody>                        
                </table>                
            </div>
        </div>
    </div>
</div>    
@endsection


@section('script') 
<script src="@asset('js/default.datatables.js')"></script>
<script>   
    var table;
    $(document).ready(function () {    
                            
        table = $('#table').DataTable( {
            ajax: "{{route('api/WSPrename')}}",
            type:'GET',
            dataType:'json',
            columns:[
                {'data':'prenameCd','width':'20%'},
                {'data':'prenameLnameTh','width':'40%'},
                {'data':'prenameLnameEng','width':'40%'}
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0],
                },
            ],         
            'processing': true         
        });                            
    });
</script>
@endsection
