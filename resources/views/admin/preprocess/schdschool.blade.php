@extends('admin.layout')

@section('content')

<div class="row">
    <div class="col-sm-12 p-0">
        <div class="main-header">            
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item "><i class="icofont icofont-home"></i>
                    <a href="">หน้าหลัก</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">ข้อมูลก่อนการฝึกสอน</a>
                </li>
                <li class="breadcrumb-item active">กำหนดสถานศึกษาตามรอบ</li>                
            </ol>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <form id="frmSearch" method="post">
            <div class="card">
                <div class="card-block">                    
                    <div class="form-group row">
                        <label for="continentNameTh" class="col-md-2 col-form-label mandatory">รอบการฝึกปฏิบัติการสอน</label>
                        <div class="col-md-4">              
                            <div class="input-group-append">
                                <select name="itsRoundId" id="itsRoundId" class='form-control' autofocus required></select>
                                <button class="btn btn-sm btn-primary"><i class="fa fa-search"></i> ค้นหา</button>
                            </div>                                          
                        </div>                        
                    </div>
                </div>
            </div>     
        </form>   
    </div>
</div>


<ul class="nav nav-tabs md-tabs" role="tablist">
    <li class="nav-item">
       <a class="nav-link active" data-toggle="tab" href="#active" role="tab"><i class="icofont icofont-check"></i>ปัจจุบัน</a>
        <div class="slide"></div>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#inactive" role="tab"><i class="icofont icofont-close"></i>ยกเลิก</a>
        <div class="slide"></div>
    </li>    
</ul>


<div class="tab-content">
    <div class="tab-pane active" id="active" role="tabpanel">
<!-- Active -->
<div class="row">
    <div class="col-sm-12">
        <div class="card">        
            <div class="card-block">                
                <table id="tbActive" class="table dt-responsive table-striped nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:left;width:40%;">สถานศึกษา</th>
                            <th style="text-align:left;width:45%;">จังหวัด</th>
                            <td style="text-align:center;width:15%">การจัดการ</td>                    
                        </tr>
                    </thead>
                    <tbody>                             
                    </tbody>                        
                </table>                
            </div>
        </div>
    </div>
</div>
<!-- Active -->        
    </div>
    <div class="tab-pane" id="inactive" role="tabpanel">
<!-- InActive -->
<div class="row">
    <div class="col-sm-12">
        <div class="card">        
            <div class="card-block">                
                <table id="tbInActive" class="table dt-responsive table-striped nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:left;width:40%;">สถานศึกษา</th>
                            <th style="text-align:left;width:45%;">จังหวัด</th>
                            <td style="text-align:center;width:15%">การจัดการ</td>                    
                        </tr>
                    </thead>
                    <tbody>                             
                    </tbody>                        
                </table>                
            </div>
        </div>
    </div>
</div>
<!-- Active -->         
    </div>    
</div>


@endsection


@section('script') 
<script src="@asset('js/default.datatables.js')"></script>
<script>   
    var tbActive;
    var tbInActive;
    $(document).ready(function () {                    
             
        $('#frmSearch').submit(function (e) { 
            e.preventDefault();            
            SchdSchool.search($(this));

        });

        $('#btnCancel').click(function(){
            SchdSchool.clear();
        });

        $('#itsRoundId').select2({
            
        });        

        SchdSchool.setupActive();
        SchdSchool.setupInActive();        
        SchdSchool.loadRound();
        tbActive.buttons().disable();
    });

    var SchdSchool = {
        setupActive: function() {
            tbActive = $('#tbActive').DataTable( {
                ajax: {
                    url: "{{route('api/SchdSchoolHdr/listActive')}}",
                    type: "get",
                    datatype: "json",
                    
                    data: function (d) {
                        return $.extend({}, d, {
                            "itsRoundId": $('#itsRoundId').val()
                        });
                    },
                    
                    dataSrc: function (json) {
                        tbActive.buttons().enable();

                        return json.data;
                    },
                    
                    error: function (xhr, error, thrown) {

                    }
                },            
                columns:[
                    {'data':'schoolNameTh'},                
                    {'data':'provinceNameTh'},
                    {'data':'schoolHdrId' , render: function(data){
                        return "<a class=\"btn btn-sm btn-danger\" href=\"javascript:SchdSchool.delete('" + data + "');\"><i class=\"fa fa-times\"></i> ยกเลิก</a>";
                    }}
                ],      
                columnDefs: [
                    {
                        "className": "text-center", "targets": [2],
                    },
                ],         
                'processing': true,
                serverSide:true,
                deferLoading: 0,
                "dom": "<'row'<'col-md-6'B><'col-md-6'f>>" +
                    "<'row'<'col-md-12'tr>>" +
                    "<'row'<'col-md-6'i><'col-md-6'p>>",
                buttons: [
                    {
                        text: "<i class=\"fa fa-plus\"></i> เพิ่มข้อมูล",
                        className: "btn btn-info",                    
                        action: function (e, dt, node, config) {
                            SchdSchool.addData();
                        }
                    }
                ]    
            });       
        },
        setupInActive: function() {
            tbInActive = $('#tbInActive').DataTable( {
                ajax: {
                    url: "{{route('api/SchdSchoolHdr/listInactive')}}",
                    type: "get",
                    datatype: "json",
                    
                    data: function (d) {
                        return $.extend({}, d, {
                            "itsRoundId": $('#itsRoundId').val()
                        });
                    },
                    
                    dataSrc: function (json) {
                        
                        return json.data;
                    },
                    
                    error: function (xhr, error, thrown) {

                    }
                },            
                columns:[
                    {'data':'schoolNameTh'},                
                    {'data':'provinceNameTh'},
                    {'data':'schoolHdrId' , render: function(data){
                        return "<a class=\"btn btn-sm btn-danger\" href=\"javascript:SchdSchool.delete('" + data + "');\"><i class=\"fa fa-times\"></i> ยกเลิก</a>";
                    }}
                ],      
                columnDefs: [
                    {
                        "className": "text-center", "targets": [2],
                    },
                ],         
                'processing': true,
                serverSide:true,
                deferLoading: 0,
                "dom": "<'row'<'col-md-6'><'col-md-6'f>>" +
                    "<'row'<'col-md-12'tr>>" +
                    "<'row'<'col-md-6'i><'col-md-6'p>>"
            });       
        },
        loadRound: function() {
            $.ajax({
                type: "get",
                url: "{{route('api/SchdSemYear')}}",                
                dataType: "json",
                success: function (response) {                    
                    if (response.data) {
                        var data = response.data;
                        
                        $.each(data, function (idx, item) {
                            $('#itsRoundId').append($('<option>', { 
                                value: item.itsRoundId,
                                text : item.roundNameTh
                            }));
                        });                        
                    }
                }
            });
        },
        clear: function () {            
            $('#frmSearch')[0].reset();
            $('#itsRoundId').trigger('change');
        },
        search: function(frm) {
            tbActive.ajax.reload();
            tbInActive.ajax.reload();
        },
        addData: function() {

        }
    };
    
</script>
@endsection
