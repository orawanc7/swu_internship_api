@extends('admin.layout')

@section('content')

<div class="row">
    <div class="col-sm-12 p-0">
        <div class="main-header">            
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item "><i class="icofont icofont-home"></i>
                    <a href="">หน้าหลัก</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">ข้อมูลก่อนการฝึกสอน</a>
                </li>
                <li class="breadcrumb-item active">รอบการฝึกปฏิบัติการสอน</li>                
            </ol>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <form id="frmSave" action="{{ route('api/SchdSemYear/save')}}" method="post">
            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <label for="itsRoundId" class="col-md-2 col-form-label mandatory">รหัสรอบ</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="itsRoundId" name="itsRoundId" required autocomplete="off" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="continentNameTh" class="col-md-2 col-form-label mandatory">ภาค/ปีการศึกษา</label>
                        <div class="col-md-4">
                            <div class="input-group-append">
                                <select name="semCd" id="semCd" class='form-control' autofocus required></select>/
                                <input class="form-control" type="text" id="year" name="year" required autocomplete="off" maxlength="4">
                            </div>                            
                        </div>
                        <label for="continentNameEn" class="col-md-2 col-form-label">ช่วงวันที่ฝึกปฏิบัติการสอน</label>
                        <div class="col-md-4 input-group">
                            <input class="form-control" type="text" id="startDate" name="startDate" autocomplete="off" maxlength="10">&nbsp;-&nbsp;
                            <input class="form-control" type="text" id="endDate" name="endDate" autocomplete="off" maxlength="10">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="roundNameTh" class="col-md-2 col-form-label mandatory">ชื่อรอบการฝึกปฏิบัติการสอน (ไทย)</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="roundNameTh" name="roundNameTh" required autocomplete="off" maxlength="80">
                        </div>
                        <label for="roundNameEn" class="col-md-2 col-form-label">ชื่อรอบการฝึกปฏิบัติการสอน (อังกฤษ)</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="roundNameEn" name="roundNameEn" autocomplete="off" maxlength="80">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="remark" class="col-md-2 col-form-label">หมายเหตุ</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="remark" name="remark" autocomplete="off" maxlength="200">
                        </div>                    
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 offset-md-2 checkbox-color checkbox-primary">
                            <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                            <label for="activeFlag" class="text-primary">
                                    สถานะ
                            </label>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;<button class="btn btn-sm btn-default" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
                </div>
            </div>     
        </form>   
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">        
            <div class="card-block">                
                <table id="table" class="table dt-responsive table-striped nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center;width:10%;">รหัสรอบ</th>
                            <th style="text-align:left;width:15%;">ภาค/ปีการศึกษา</th>
                            <th style="text-align:left;width:30%;">ชื่อรอบการฝึกปฏิบัติการสอน (ไทย)</th>   
                            <th style="text-align:left;width:20%;">ช่วงวันที่ฝึกปฏิบัติการสอน</th>                                                            
                            <th style="text-align:center;width:10%">สถานะ</th>    
                            <td style="text-align:center;width:15%">การจัดการ</td>                    
                        </tr>
                    </thead>
                    <tbody>                             
                    </tbody>                        
                </table>                
            </div>
        </div>
    </div>
</div>    
@endsection


@section('script') 
<script src="@asset('js/default.datatables.js')"></script>
<script>   
    var table;
    $(document).ready(function () {    
                            
        table = $('#table').DataTable( {
            ajax: "{{route('api/SchdSemYear')}}",
            type:'GET',
            dataType:'json',
            columns:[
                {'data':'itsRoundId'},                
                {'data':'year', render: function(data,type,row) {
                    return row.semCd + "/"  + row.year;
                }},
                {'data':'roundNameTh'},
                {'data':'startDate', render: function(data,type,row) {
                    var startDate = moment(row.startDate.date).format("DD/MM/YYYY");
                    var endDate = moment(row.endDate.date).format("DD/MM/YYYY");
                    return startDate + " - "  + endDate;
                }},
                {'data':'activeFlag',render: function(data) {
                    if (data=="Y") {
                        return "<i class=\"fa fa-check\"></i>";
                    } else {
                        return "";
                    }
                }},                
                {'data':'itsRoundId' , render: function(data){
                    return "<a class=\"btn btn-sm btn-primary\" href=\"javascript:SchdSemYear.edit('" + data + "');\"><i class=\"fa fa-edit\"></i> แก้ไข</a> " +
                    "<a class=\"btn btn-sm btn-danger\" href=\"javascript:SchdSemYear.delete('" + data + "');\"><i class=\"fa fa-trash\"></i> ลบ</a>";
                }}
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0,1,,3,4,5],
                },
            ],         
            'processing': true         
        });       
             
        $('#frmSave').submit(function (e) { 
            e.preventDefault();
            
            SchdSemYear.save($(this));

        });

        $('#btnCancel').click(function(){
            SchdSemYear.clear();
        });

        $('#semCd').select2({
            
        });

        SchdSemYear.loadSem();
    });

    var SchdSemYear = {
        loadSem: function() {
            $.ajax({
                type: "get",
                url: "{{route('api/Sem')}}",                
                dataType: "json",
                success: function (response) {                    
                    if (response.data) {
                        var data = response.data;
                        
                        $.each(data, function (idx, item) {
                            $('#semCd').append($('<option>', { 
                                value: item.semCd,
                                text : item.semCd 
                            }));
                        });                        
                    }
                }
            });
        },
        clear: function () {            
            $('#frmSave')[0].reset();
            $('#semCd').trigger('change');
        },
        edit: function(id) {
            $.ajax({
                url: "{{route('api/SchdSemYear/')}}" + id,
                type: "get",                
                dataType: "json",
                success: function (response) {                    
                    if (response.data) {
                        SchdSemYear.clear();                        

                        var data = response.data;
                        var startDate = moment(data.startDate.date).format("DD/MM/YYYY");
                        var endDate = moment(data.endDate.date).format("DD/MM/YYYY");

                        $('#itsRoundId').val(data.itsRoundId);
                        $('#roundNameTh').val(data.roundNameTh);
                        $('#roundNameEn').val(data.roundNameEn);
                        $('#year').val(data.year);
                        $('#semCd').val(data.semCd).trigger('change');
                        $('#startDate').val(startDate);
                        $('#endDate').val(endDate);
                        $('#remark').val(data.remark);
                        if (data.activeFlag=="Y") {
                            $('#activeFlag').prop('checked',true);
                        }
                        $('#semCd').focus();
                    }
                }
            });
        },
        delete: function(id) {                        
            $.ajax({
                type: 'DELETE',
                url: $('#frmSave').attr('action') + '/' + id,
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        SchdSemYear.clear();
                        table.ajax.reload();

                        $('#itsRoundId').focus();
                    } else {
                        alert(response.message);
                    }
                }
            });
        },
        save: function(frm) {

            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        SchdSemYear.clear();
                        table.ajax.reload();

                        $('#itsRoundId').focus();
                    } else {
                        alert(response.message);
                    }
                }
            });

        }
    };
    
</script>
@endsection
