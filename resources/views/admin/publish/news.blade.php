@extends('admin.layout')

@section('content')

<div class="row">
    <div class="col-sm-12 p-0">
        <div class="main-header">            
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item "><i class="icofont icofont-home"></i>
                    <a href="">หน้าหลัก</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">ข้อมูลเผยแพร่</a>
                </li>
                <li class="breadcrumb-item active">ข่าวประชาสัมพันธ์</li>                
            </ol>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <form id="frmSearch" action="{{ route('api/School/save')}}" method="post">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    <i class="fa fa-search"></i> เงื่อนไขการค้นหาข้อมูล
                </div>
                <div class="card-block">
                    <div class="form-group row">
                        <label for="schoolId" class="col-md-2 col-form-label">รหัสสถานศึกษา</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="schoolId" name="schoolId" autofocus autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="schoolNameTh" class="col-md-2 col-form-label">ชื่อสถานศึกษาไทย</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="schoolNameTh" name="schoolNameTh" autocomplete="off">
                        </div>
                        <label for="schoolNameEn" class="col-md-2 col-form-label">ชื่อสถานศึกษาอังกฤษ</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="schoolNameEn" name="schoolNameEn" autocomplete="off">
                        </div>
                    </div>                    
                    <div class="form-group row">
                        <label for="schoolNameTh" class="col-md-2 col-form-label">จังหวัด</label>
                        <div class="col-md-4">
                            <select name="provinceId" id="provinceId" class="form-control select2" ></select>
                        </div>
                        <label for="schoolNameEn" class="col-md-2 col-form-label">ประเภทสถานศึกษา</label>
                        <div class="col-md-4">
                            <select name="schoolTypeId" id="schoolTypeId" class="form-control select2"></select>
                        </div>
                    </div>                    
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> ค้นหาข้อมูล</button>&nbsp;<button class="btn btn-sm btn-default" id="btnCancel"><i class="fa fa-eraser"></i> ล้าง</button>
                </div>
            </div>     
        </form>   
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">        
            <div class="card-block">                
                <table id="table" class="table dt-responsive table-striped nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center;width:15%;">รหัสกลุ่มการเรียนรู้</th>
                            <th style="text-align:left;width:30%;">ชื่อสถานศึกษาไทย</th>
                            <th style="text-align:left;width:30%;">ชื่อสถานศึกษาอังกฤษ</th>                                
                            <th style="text-align:center;width:10%">สถานะ</th>    
                            <td style="text-align:center;width:15%">การจัดการ</td>                    
                        </tr>
                    </thead>
                    <tbody>                                              
                    </tbody>                        
                </table>                
            </div>
        </div>
    </div>
</div>

@endsection


@section('script') 
<script src="@asset('js/default.datatables.js')"></script>
<script>   
    $(document).ready(function () {    
                            
        
    });

    var News = {
        
    };
    
</script>
@endsection
