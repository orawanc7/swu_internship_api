

<!-- Side-Nav-->
<aside class="main-sidebar hidden-print " >
    <section class="sidebar" id="sidebar-scroll">               
    <!-- Sidebar Menu-->
    <ul class="sidebar-menu" id="sidebar-sticky-full">        
        <li class="{{ isset($_menu)?(($_menu=="masterregister")?"active":""):"" }} treeview">
            <a class="waves-effect waves-dark" href="index.html">
                <i class="icon-film"></i><span> ข้อมูลพื้นฐานงานทะเบียน</span><i class="icon-arrow-down"></i>
            </a>
            <ul class="treeview-menu">
                <li><a class="waves-effect waves-dark" href="{{ route('admin/wsdepartment') }}"><i class="icon-arrow-right"></i><span>หน่วยงาน</span></a></li>
                <li><a class="waves-effect waves-dark" href="{{ route('admin/wsmajor') }}"><i class="icon-arrow-right"></i><span>สาขาวิชาเอก</span></a></li>
                <li><a class="waves-effect waves-dark" href="{{ route('admin/wsprename') }}"><i class="icon-arrow-right"></i><span>คำนำหน้าชื่อ</span></a></li>
                <li><a class="waves-effect waves-dark" href="{{ route('admin/wsdegree') }}"><i class="icon-arrow-right"></i><span>วุฒิการศึกษา</span></a></li>                
            </ul>
        </li>

        <li class="{{ isset($_menu)?(($_menu=="master")?"active":""):"" }} treeview">
            <a class="waves-effect waves-dark" href="index.html">
                <i class="icon-film"></i><span> ข้อมูลพื้นฐานระบบนิเทศก์</span><i class="icon-arrow-down"></i>
            </a>
            <ul class="treeview-menu">
                <li><a class="waves-effect waves-dark" href="{{route('admin/continent')}}"><i class="icon-arrow-right"></i><span>ทวีป</span></a></li>         
                <li><a class="waves-effect waves-dark" href="{{route('admin/country')}}"><i class="icon-arrow-right"></i><span>ประเทศ</span></a></li>       
                <li><a class="waves-effect waves-dark" href="{{route('admin/region')}}"><i class="icon-arrow-right"></i><span>ภาค</span></a></li>
                <li><a class="waves-effect waves-dark" href="{{route('admin/province')}}"><i class="icon-arrow-right"></i><span>จังหวัด</span></a></li>
                <li><a class="waves-effect waves-dark" href="{{route('admin/learninggroup')}}"><i class="icon-arrow-right"></i><span>กลุ่มสาระการเรียนรู้</span></a></li>
                <li><a class="waves-effect waves-dark" href="{{route('admin/persontype')}}"><i class="icon-arrow-right"></i><span>ประเภทบุคคลากร</span></a></li>                
                <li><a class="waves-effect waves-dark" href="{{route('admin/majormaster')}}"><i class="icon-arrow-right"></i><span>สาขาวิชาเอกที่สอน</span></a></li>                
                <li><a class="waves-effect waves-dark" href="{{route('admin/schoolsub')}}"><i class="icon-arrow-right"></i><span>สังกัดสถานศึกษา</span></a></li>                
                <li><a class="waves-effect waves-dark" href="{{route('admin/schooltype')}}"><i class="icon-arrow-right"></i><span>ประเภทสถานศึกษา</span></a></li>                
                <li><a class="waves-effect waves-dark" href="{{route('admin/school')}}"><i class="icon-arrow-right"></i><span>สถานศึกษา</span></a></li>                
            </ul>
        </li>

        <li class="{{ isset($_menu)?(($_menu=="publish")?"active":""):"" }} treeview">
            <a class="waves-effect waves-dark" href="index.html">
                <i class="icon-film"></i><span> ข้อมูลเผยแพร่</span><i class="icon-arrow-down"></i>
            </a>
            <ul class="treeview-menu">
                <li><a class="waves-effect waves-dark" href="{{ route('admin/news') }}"><i class="icon-arrow-right"></i><span>ข่าวประชาสัมพันธ์</span></a></li>
                <li><a class="waves-effect waves-dark" href="{{ route('admin/docgroup') }}"><i class="icon-arrow-right"></i><span>เอกสาร</span></a></li>
            </ul>
        </li>
        
        <li class="{{ isset($_menu)?(($_menu=="preprocess")?"active":""):"" }} treeview treeview">
            <a class="waves-effect waves-dark" href="index.html">
                <i class="icon-book-open"></i><span> ข้อมูลก่อนการฝึกสอน</span><i class="icon-arrow-down"></i>
            </a>
            <ul class="treeview-menu">
                <li><a class="waves-effect waves-dark" href="{{route('admin/schdsemyear')}}"><i class="icon-arrow-right"></i><span>รอบการฝึกปฏิบัติการสอน</span></a></li>
                <li><a class="waves-effect waves-dark" href="{{route('admin/schdschool')}}"><i class="icon-arrow-right"></i><span>กำหนดสถานศึกษาตามรอบ</span></a></li>
                <li><a class="waves-effect waves-dark" href="dashboard3.html"><i class="icon-arrow-right"></i><span>ข้อมูลนิสิตแต่ละโรงเรียนตามสาขา</span></a></li>                
            </ul>
        </li>
        
    </ul>  <!-- End --> 
    </section> <!-- Section --> 
</aside>
