<header class="main-header-top hidden-print">
    <a href="{{route('')}}" class="logo"><img class="img-fluid able-logo" src="@relative('images/new-logo4.png')" alt="logo"></a> 
    
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->        
        <a href="#" data-toggle="offcanvas" class="sidebar-toggle hidden-md-up"></a>
        <!-- Navbar Right Menu-->
        <div class="navbar-custom-menu">
        <ul class="top-nav">
            <li class="dropdown d-none d-md-block">
                @if (getenv('HOST')) 
                    <span class="text-white"><i class="icofont icofont-server"></i> {{getenv('HOST')}}</span>
                @endif
                <a href="#!" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle drop icon-circle ">
                <span> <img class="rounded-circle " src="{{ icon($_SESSION['ITS_ID']) }}?{{ time() }}" style="width:40px;" alt="User Image" id="imgUser"></span>                
                    <span>{{ $_SESSION['NAME'] }}
                    <i class=" icofont icofont-simple-down"></i></span>                
                </a>
                <ul class="dropdown-menu settings-menu">            
                    <li><a href="{{route('profile')}}"><i class="icon-user"></i> ประวัติส่วนตัว</a></li>
                    <li class="p-0">
                        <div class="dropdown-divider m-0"></div>
                    </li>                        
                    <li><a href="{{route('api/logout')}}"><i class="icon-logout"></i> ออกจากระบบ</a></li>    
                </ul>
            </li>
            <li class="dropdown d-block d-md-none mr-2">
                <span><img class="rounded-circle " src="{{ icon($_SESSION['ITS_ID']) }}?{{ time() }}" style="width:40px;" alt="User Image" id="imgUser"></span>
            </li>
        </ul>        
        </div>
    </nav>      
</header>