<header class="main-header-top hidden-print">
<a href="index.html" class="logo"><img class="img-fluid able-logo" src="@relative('images/logo.png')" alt="logo"></a>

<nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" data-toggle="offcanvas" class="sidebar-toggle hidden-md-up"></a>
    <!-- Navbar Right Menu-->
    <div class="navbar-custom-menu">
    <ul class="top-nav">
        <!--Notification Menu-->
        <li class="dropdown notification-menu">
        <a href="#!" data-toggle="dropdown" aria-expanded="false" class="dropdown-toggle">
        <i class="icon-bell"></i>
        <span class="badge badge-danger header-badge">9</span>
        </a>
        <ul class="dropdown-menu">
            <li class="not-head">You have <b class="text-primary">4</b> new notifications.</li>
            <li class="bell-notification">
            <a href="javascript:;" class="media">
            <span class="mr-3 media-icon">
            <img class="rounded-circle" src="assets/images/avatar-1.png" alt="User Image">
            </span>
                <div class="media-body"><span class="block">Lisa sent you a mail</span><span class="text-muted block-time">2min ago</span></div></a>
            </li>
            <li class="bell-notification">
            <a href="javascript:;" class="media">
            <span class="mr-3 media-icon">
            <img class="rounded-circle" src="assets/images/avatar-2.png" alt="User Image">
            </span>
                <div class="media-body"><span class="block">Server Not Working</span><span class="text-muted block-time">20min ago</span></div></a>
            </li>
            <li class="bell-notification">
            <a href="javascript:;" class="media"><span class="mr-3 media-icon">
            <img class="rounded-circle" src="assets/images/avatar-3.png" alt="User Image">
            </span>
                <div class="media-body"><span class="block">Transaction xyz complete</span><span class="text-muted block-time">3 hours ago</span></div></a>
            </li>
            <li class="not-footer">
            <a href="#!">See all notifications.</a>
            </li>
        </ul>
        </li>
        <!-- chat dropdown -->
        <li class="pc-rheader-submenu ">
            <a href="#!" class="drop icon-circle displayChatbox">
                <i class="icon-bubbles"></i>
                <span class="badge badge-danger header-badge blink">5</span>
            </a>
        </li>
        <!-- window screen -->
        <li class="pc-rheader-submenu">
            <a href="#!" class="drop icon-circle" onclick="javascript:toggleFullScreen()">
                <i class="icon-size-fullscreen"></i>
            </a>
        </li>
        <!-- User Menu-->
        <li class="dropdown ">
        <a href="#!" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle drop icon-circle ">
        <span><img class="rounded-circle " src="{{ route('images/users/' . $_SESSION['ID'] . '.png') }}" style="width:40px;" alt="User Image"></span>
            <span>{{-- $_SESSION['NAME'] --}}
            <i class=" icofont icofont-simple-down"></i></span>
        </a>
        <ul class="dropdown-menu settings-menu">
            <li><a href="#!"><i class="icon-settings"></i> Settings</a></li>
            <li><a href="profile.html"><i class="icon-user"></i> Profile</a></li>
            <li class="p-0">
                <div class="dropdown-divider m-0"></div>
            </li>                        
            <li><a href="{{route('')}}"><i class="icon-logout"></i> Logout</a></li>

        </ul>
        </li>
    </ul>        
    </div>
</nav>      
</header>