<header class="main-header-top hidden-print">
<a href="index.html" class="logo"><img class="img-fluid able-logo" src="@relative('images/logo.png')" alt="logo"></a>

<nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" data-toggle="offcanvas" class="sidebar-toggle hidden-md-up"></a>
    <!-- Navbar Right Menu-->
    <div class="navbar-custom-menu">
    <ul class="top-nav">
        <!--Notification Menu-->
        <li class="dropdown notification-menu">
        <a href="#!" data-toggle="dropdown" aria-expanded="false" class="dropdown-toggle">
        <i class="icon-bell"></i>
        <span class="badge badge-danger header-badge"></span>
        </a>        
        </li>
        <!-- chat dropdown -->
        <!--<li class="pc-rheader-submenu ">
            <a href="#!" class="drop icon-circle displayChatbox">
                <i class="icon-bubbles"></i>
                <span class="badge badge-danger header-badge blink"></span>
            </a>
        </li>-->
        <!-- window screen -->
        <!--<li class="pc-rheader-submenu">
            <a href="#!" class="drop icon-circle" onclick="javascript:toggleFullScreen()">
                <i class="icon-size-fullscreen"></i>
            </a>
        </li>-->
        <!-- User Menu-->
        <li class="dropdown ">
        <a href="#!" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle drop icon-circle ">
        <span><img class="rounded-circle " src="{{ icon($_SESSION['ITS_ID']) }}?{{ time() }}" style="width:40px;" alt="User Image" id="imgUser"></span>
            <span>{{ $_SESSION['NAME'] }}
            <i class=" icofont icofont-simple-down"></i></span>
        </a>
        <ul class="dropdown-menu settings-menu">            
            <li><a href="{{route('profile')}}"><i class="icon-user"></i> ประวัติส่วนตัว</a></li>
            <li class="p-0">
                <div class="dropdown-divider m-0"></div>
            </li>                        
            <li><a href="{{route('api/logout')}}"><i class="icon-logout"></i> ออกจากระบบ</a></li>

        </ul>
        </li>
    </ul>        
    </div>
</nav>      
</header>