@extends('publiclayout')

@section('content')
<!-- Header Start -->
<div class="container" id="top-header">
    <div class="row">
        <div class="col-sm-6 col-xs-5">
            <a href="{{route('')}}">
                <img src="@relative('images/new-logo3.png')" class="img-fluid m-t-15">
            </a>
        </div>
        <div class="col-sm-6 col-xs-7 f-right">
            <ul class="socials-icon pull-right">                
                <li>                    
                    <div class="line-it-button" data-lang="en" data-type="friend" data-lineid="@edswu" data-count="true" data-home="true" style="display: none;"></div>
                    <script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script>                    
                </li>                
            </ul>
        </div>
    </div>
</div>
<!-- Header End -->

<!-- Nav-bar Start -->

<nav class="navbar navbar-expand-lg navbar-light navbar-inverse bg-inverse">
  
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="container">
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a<a class="nav-link" href="{{route('')}}">หน้าหลัก</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#services">การให้บริการ</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#aboutus">เกี่ยวกับเรา</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="http://edu.swu.ac.th" target="_blank">คณะศึกษาศาสตร์</a>
      </li> 
      <li class="nav-item">
        <a class="nav-link" href="https://www.swu.ac.th" target="_blank">มศว</a>
      </li>      
    </ul>
  </div>
  </div>

</nav>
<!--</nav>-->
<!--</div>-->
<!-- Nav-bar Ends -->

<!-- Header Start -->
<header id="header-bg" class="jarallax d-flex" data-jarallax='{"speed": 0.2}' style="background-image: url(assets/images/landingpage/banner-ln2.jpg);background-size: cover">
    <!-- Menu End -->
    <div class="container">
        <div id="home-header" class="home-content">
            <div class="col-md-7 col-sm-12">
                <h1>ระบบนิเทศเพื่อการศึกษา<br/><span>Swu Internship</span></h1>
                <div class=" text-align-xs">                                                        
                    @if (!isset($_SESSION['ITS_ID'])) 
                    <button class="btn btn-primary btn-md waves-effect waves-light m-r-30" data-toggle="modal" data-target="#login"><i class="icofont icofont-lock"></i> เข้าสู่ระบบ
                        &nbsp; <i class="icofont icofont-bubble-right"></i>
                    </button>                    
                    @else
                    <a class="btn btn-primary btn-md waves-effect waves-light m-r-30" href="{{ route($_SESSION['URL']) }}"><i class="icofont icofont-user"></i> เข้าใช้งานระบบ
                        &nbsp; <i class="icofont icofont-bubble-right"></i>
                    </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Header Ends -->

<!--Services start-->
<section id="services">
    <div class="section-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 text-center">
                    <div class="box-process">
                        <a href="#!" class="ourpro-icon-wrap ourpro-icon-effect-8">
                            <i class="icofont icofont-teacher ourpro-icon"></i>
                        </a>
                        <h6>อาจารย์</h6>
                        <p>เชื่อมโยงข้อมูลระหว่างอาจารย์และนิสิต</p>
                        <div><a href="{{ route('documents/คู่มือสำหรับอาจารย์_Desktop.pdf') }}">ดาวน์โหลดคู่มือ สำหรับ Desktop<i class="icofont icofont-download-alt"></i></a></div>
                        <div><a href="{{ route('documents/คู่มือสำหรับอาจารย์_Android.pdf') }}">ดาวน์โหลดคู่มือ สำหรับ Android <i class="icofont icofont-download-alt"></i></a></div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 text-center mb-25-xs">
                    <div class="box-process">
                        <a href="#!" class="ourpro-icon-wrap ourpro-icon-effect-8">
                            <i class="icofont icofont-user ourpro-icon"></i>
                        </a>
                        <h6>นิสิต</h6>
                        <p>เชื่อมโยงข้อมูลระหว่างนิสิตและอาจารย์</p>
                        <a href="{{ route('documents/คู่มือสำหรับนิสิต.pdf') }}">ดาวน์โหลดคู่มือ <i class="icofont icofont-download-alt"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 text-center mb-25-xs">
                    <div class="box-process">
                        <a href="#!" class="ourpro-icon-wrap ourpro-icon-effect-8">
                            <i class="icofont icofont-building ourpro-icon"></i>
                        </a>
                        <h6>สถานศึกษา</h6>
                        <p>ข้อมูลในระบบสำหรับสถานศึกษา</p>
                        <a href="#!">อ่านเพิ่มเติม <i class="icofont icofont-bubble-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 text-center">
                    <div class="box-process">
                        <a href="#!" class="ourpro-icon-wrap ourpro-icon-effect-8">
                            <i class="icofont icofont-business-man ourpro-icon"></i>
                        </a>
                        <h6>ผู้บริหาร</h6>
                        <p>ข้อมูลในระบบสำหรับผู้บริหาร</p>
                        <a href="#!">อ่านเพิ่มเติม <i class="icofont icofont-bubble-right"></i></a>
                    </div>
                </div>
            </div>                 
            <div class="row text-center" style="margin-top:50px;">
                <div class="col-12">                    
                    <a href=" https://bit.ly/2mkq79G" target="_blank"><h5><i class="icofont icofont-question-square"></i> แบบสอบถามความคิดเห็นการใช้งานระบบ SWU-ITS</h5></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Services Ends-->
<!--AboutUs Start Section-->

<section id="aboutus">
    <div id="about-sec" class="container-fluid about-sec">
        <div class="row d-flex about-sec">
            <div class="col-md-6 col-sm-12 p-0">
                <img src="@relative('assets/images/landingpage/aboutus2.jpg')" class="img-fluid" alt="">
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 tablet-sc">
                            <h2 class="title-sec title-center">เกี่ยวกับเรา</h2>
                            <p>
                                สำนักคอมพิวเตอร์ร่วมกับคณะศึกษาศาสตร์ พัฒนาระบบสารสนเทศเพื่อการบริหารจัดการในการปฏิบัติการสอนและฝึกประสบการณ์วิชาชีพครู สำหรับหลักสูตรการศึกษาบัณฑิต (กศ.บ. 5 ปี) มหาวิทยาลัยศรีนครินทรวิโรฒ เพื่อให้บริการข้อมูลสารสนเทศทางการศึกษาให้แก่นิสิตหลักสูตรการศึกษาบัณฑิต (กศ.บ. 5 ปี) มหาวิทยาลัยศรีนครินทรวิโรฒ
                            </p>                                                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--AboutUs End Section-->

<div class="copyright">
    <h5 class="text-center">Copyright © 2019. คณะศึกษาศาสตร์ มหาวิทยาลัยศรีนครินทรวิโรฒ</h5>
</div>


<!-- Move to up -->
<a class="waves-effect waves-light scrollup scrollup-icon"><i class="icofont icofont-arrow-up "></i></a>



<div class="modal fade modal-flex" id="login" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="sign-in-up">
                <div class="sign-in-up-left">
                    <!--<i class="icofont icofont-users f-64" aria-hidden="true"></i>-->
                    <img src="@relative('images/new-logo4.png')" alt="">
                </div>                
                <div class="sign-in-up-right">                
                    <form class="md-float-material pt-4" action="{{ route('api/login') }}" method="post" id="frmLogin">
                        <div class="alert alert-warning alert-dismissible" role="alert" style="display:none;"> 
                            <span id="message"></span>                                               
                        </div>
                        <input type="hidden" name="admin" id="admin" value="0">
                        <div class="md-input-wrapper">
                            <input type="text" class="md-form-control md-static text-lowercase" name="userId" id="userId" value="{{ $userId }}"/>
                            <label>Buasri ID</label>
                        </div>
                        <div class="md-input-wrapper">
                            <input type="password" class="md-form-control md-static" name="userPassword" id="userPassword"/>
                            <label>Password</label>
                        </div>

                        <div class="md-float-group b-none m-b-20">
                            <div class="rkmd-checkbox checkbox-rotate checkbox-ripple">
                                <label class="input-checkbox checkbox-primary">
                                    <input type="checkbox" id="remember" name="remember" value="Y" {{ $remember }}>
                                    <span class="checkbox"></span>
                                </label>
                                <label for="checkbox" class="captions">
                                    Remember Me
                                </label>
                            </div>
                        </div>
                        <div class="text-center m-b-20">
                            <button type="submit" class="btn btn-primary waves-effect"><i class="icofont icofont-unlock"></i> เข้าสู่ระบบ</button>
                            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal"><i class="fa fa-times"></i> ยกเลิก</button>
                        </div>  
                    </form>                                                          
                </div>
            </div>
        </div>
    </div>
    <!-- end of modal fade -->
</div>
@endsection

@section('script')
<script src="@relative('js/app/LandingPage.js')?{{ time()}}"></script>
<script>
    $(window).on('load',function(){
    $('#pre-loader').fadeOut('slow');
});
</script>
@endsection
