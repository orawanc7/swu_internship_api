<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Swu-Internship @yield('title')</title>
    <link rel="shortcut icon" href="@relative('images/favicon.ico')" type="image/x-icon">
    <link rel="icon" href="@relative('images/favicon.ico')" type="image/x-icon">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <!--Date Picker Material Icon Css-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- iconfont -->
    <link rel="stylesheet" type="text/css" href="@relative('icon/icofont/css/icofont.css')">

    <!-- simple line icon -->
    <link rel="stylesheet" type="text/css" href="@relative('icon/simple-line-icons/css/simple-line-icons.css')">
    <link rel="stylesheet" type="text/css" href="@relative('css/jquery-ui.min.css')">    
    <link rel="stylesheet" type="text/css" href="@relative('css/bootstrap.min.css')">    
    <link rel="stylesheet" type="text/css" href="@relative('css/select2.min.css')">
    <link rel="stylesheet" type="text/css" href="@relative('css/jquery.treetable.css')">    
    <link rel="stylesheet" type="text/css" href="@relative('css/bootstrap-datepicker3.min.css')">
    <link rel="stylesheet" type="text/css" href="@relative('css/jquery.treetable.theme.default.css')">
    <link rel="stylesheet" type="text/css" href="@relative('css/jquery.rtResponsiveTables.min.css')">    
    <link rel="stylesheet" type="text/css" href="@relative('css/basictable.css')"> 
    @yield('beforestyle')
    <link rel="stylesheet" type="text/css" href="@relative('css/common.css')?{{ time() }}">
    <link rel="stylesheet" type="text/css" href="@relative('css/component.css')">
    <link rel="stylesheet" type="text/css" href="@relative('css/main.min.css')?{{ time() }}"> 
    <link rel="stylesheet" type="text/css" href="@relative('css/responsive.css')">
    <link rel="stylesheet" type="text/css" href="@relative('css/style.css')">
    @if ($_SESSION['COLOR']==2)
        <link rel="stylesheet" type="text/css" href="@relative('css/color/color-2.css')" id="color"/>
    @elseif ($_SESSION['COLOR']==3)
        <link rel="stylesheet" type="text/css" href="@relative('css/color/color-3.css')" id="color"/>
    @elseif ($_SESSION['COLOR']==4)
        <link rel="stylesheet" type="text/css" href="@relative('css/color/color-4.css')" id="color"/>
    @elseif ($_SESSION['COLOR']==5)
        <link rel="stylesheet" type="text/css" href="@relative('css/color/color-5.css')" id="color"/>
    @elseif ($_SESSION['COLOR']==6)
        <link rel="stylesheet" type="text/css" href="@relative('css/color/color-6.css')" id="color"/>
    @elseif ($_SESSION['COLOR']==7)
        <link rel="stylesheet" type="text/css" href="@relative('css/color/color-7.css')" id="color"/>
    @elseif ($_SESSION['COLOR']==8)
        <link rel="stylesheet" type="text/css" href="@relative('css/color/color-8.css')" id="color"/>
    @elseif ($_SESSION['COLOR']==9)
        <link rel="stylesheet" type="text/css" href="@relative('css/color/color-9.css')" id="color"/>        
    @else    
        <link rel="stylesheet" type="text/css" href="@relative('css/color/color-1.css')" id="color"/>        
    @endif
    @yield('style')
</head>
<body class="horizontal-fixed fixed">    
    <input type="hidden" name="current-path" id="current-path" value="@relative('')">    
    <input type="hidden" name="its-id" id="its-id" value="{{ $_SESSION['ITS_ID'] }}">   
    <input type="text" name="current-its-round-id" id="current-its-round-id" value="{{ isset($_SESSION['CURRENT_ITS_ROUND_ID'])?$_SESSION['CURRENT_ITS_ROUND_ID']:"" }}">   
    
    <div class="wrapper">
        <div class="loader-bg">
            <div class="loader-bar"></div>
        </div>
        
        @include('header')        

        @include('sidebar') 

        <div class="content-wrapper">
            <!-- Container-fluid starts -->
            <!-- Main content starts -->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12 p-0">
                        <div class="main-header">
                        </div>
                    </div>
                </div>
                    
                @yield('content')                

                
                <div class="row">
                    <div class="col-md-12">
                        <footer class="f-fix text-center">                        
                            Copyright © 2019. คณะศึกษาศาสตร์ มหาวิทยาลัยศรีนครินทรวิโรฒ
                        </footer>
                    </div>                    
                </div>
                
            </div>        
            <!-- Container-fluid ends -->              
                        
        </div>            
    </div> <!-- wrapper -->
    


    @yield('modal')

    <!-- Move to up -->
    <a class="waves-effect waves-light scrollup scrollup-icon"><i class="icofont icofont-arrow-up "></i></a>

    <script>
            var color = true;
    </script>
    <script src="@relative('js/jquery.min.js')"></script>
    <script src="@relative('js/jquery-ui.min.js')"></script>
    <script src="@relative('js/jquery.validate.min.js')"></script>    
    <script src="@relative('js/additional-methods.min.js')"></script>    
    <script src="@relative('js/locale_jquery.validate/messages_th.min.js')"></script>            
    <script src="@relative('js/bootstrap.bundle.min.js')"></script>                 
    <script src="@relative('js/select2/select2.full.min.js')"></script>
    <script src="@relative('js/jquery.blockUI.js')"></script>
    <script src="@relative('js/moment.min.js')"></script>                 
    <script src="@relative('js/locale/th.js')"></script>                 
    <script src="@relative('js/jquery.slimscroll.min.js')"></script>       
    <script src="@relative('plugins/waves/js/waves.min.js')"></script>
    <script src="@relative('js/classie.js')"></script>
    <script src="@relative('js/modalEffects.js')"></script>        
    <script src="@relative('js/jquery.treetable.js')"></script>       
    <script src="@relative('js/bootstrap-datepicker.min.js ')"></script>           
    <script src="@relative('js/jquery.inputmask.bundle.min.js')"></script>                
    <script src="@relative('js/summernote-bs4.min.js')"></script> 
    <script src="@relative('js/typeahead.jquery.min.js')"></script>                
    <script src="@relative('js/jquery.rtResponsiveTables.min.js')"></script>       
    <script src="@relative('js/jquery.basictable.min.js')"></script>                
    
    @yield('beforescript')
    <script src="@relative('js/main.js')?{{ time() }}"></script>             
    <script src="@relative('js/form.js')?{{ time() }}"></script>
    @yield('script')
    <script>
        $(document).ready(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 600) {
                    $('.scrollup').fadeIn();
                } else {
                    $('.scrollup').fadeOut();
                }
            });

            $('.scrollup').click(function () {
                $("html, body").animate({
                    scrollTop: 0
                }, 600);
                return false;
            });
        });
    </script>
</body>
</html>