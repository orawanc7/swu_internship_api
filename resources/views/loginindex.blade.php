@extends('publiclayout')

@section('content')
<section class="login p-fixed d-flex text-center  common-img-bg">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-12">
                <div class="login-card card-block">
                    <form class="md-float-material" action="{{ route('api/login') }}" method="post">
                        <input type="hidden" name="admin" id="admin" value="{{ $admin }}">
                        <div class="text-center">
                            <img src="@relative('images/logo-blue.png')">
                        </div>
                        <div class="md-input-wrapper">
                            <input type="text" class="md-form-control" autofocus id="userId" name="userId" value="{{ $userId }}" />
                            <label>User ID</label>
                        </div>
                        <div class="md-input-wrapper">
                            <input type="password" class="md-form-control" id="userPassword" name="userPassword"/>
                            <label>Password</label>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 ">
                                <div class="rkmd-checkbox checkbox-rotate checkbox-ripple m-b-25">
                                    <label class="input-checkbox checkbox-primary">
                                        <input type="checkbox" id="remember" name="remember" value="Y" {{ $remember }}>
                                        <span class="checkbox"></span>
                                    </label>
                                    <div class="captions">Remember Me</div>
                                </div>
                            </div>                            
                        </div>

                        <div class="row justify-content-center">
                            <div class="col-10">
                                <button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">LOGIN</button>
                            </div>
                        </div>    

                        <!-- </div> -->
                    </form>
                    <!-- end of form -->
                </div>
                <!-- end of login-card -->
            </div>
            <!-- end of col-sm-12 -->
        </div>
        <!-- end of row -->
    </div>
    <!-- end of container-fluid -->
</section>    
@endsection

@section('script')

<script>
    "use strict";
    $(document).ready(function(){
        $(".md-form-control").each(function() {
            $(this).parent().append('<span class="md-line"></span>');
        });
        $(".md-form-control").change(function() {
            if ($(this).val() == "") {
                $(this).removeClass("md-valid");
            } else {
                $(this).addClass("md-valid");
            }
        });
    });
</script>    
@endsection
