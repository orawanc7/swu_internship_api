<div id="edit-info" style="display:none;">
    <form action="{{ route('api/Profile/save') }}" method="post" id="frmSave">
        <div class="row">    
            <div class="col-md-6">
                <table class="table m-b-0">
                    <tbody>
                        <tr>
                            <th scope="row" style="width:30%">Buasri Email</th>
                            <td class="border-0" style="width:70%">{{ $_SESSION['BUASRI_EMAIL'] }}</td>
                        </tr>
                    </tbody>
                </table>                                               
            </div>
            <div class="col-md-6">
                <table class="table m-b-0">
                    <tbody>
                        <tr>
                            <th scope="row" style="width:30%">GAFE Email</th>
                            <td class="border-0" style="width:70%">{{ $_SESSION['GAFE_EMAIL'] }}</td>
                        </tr>
                    </tbody>
                </table>                                               
            </div>
        </div>
        <div class="row">    
            <div class="col-md-6">
                <table class="table m-b-0">
                    <tbody>
                        <tr>
                            <th scope="row" style="width:30%">อีเมล์อื่น</th>
                            <td class="border-0" style="width:70%">
                                <input type="text" name="otherEmail" id="otherEmail" class="form-control" maxlength="50" value="{{ $_SESSION['OTHER_EMAIL'] }}" data-old="{{ $_SESSION['OTHER_EMAIL'] }}">
                            </td>
                        </tr>
                    </tbody>
                </table>                                               
            </div>
            <div class="col-md-6">
                                                            
            </div>
        </div>
        <div class="row">    
            <div class="col-md-6">
                <table class="table m-b-0">
                    <tbody>
                        <tr>
                            <th scope="row" style="width:30%">เบอร์โทรศัพท์</th>
                            <td class="border-0" style="width:70%">
                                <input type="text" name="telephoneNo" id="telephoneNo" class="form-control" maxlength="50" value="{{ $_SESSION['TELEPHONE_NO'] }}" data-old="{{ $_SESSION['TELEPHONE_NO'] }}">
                            </td>
                        </tr>
                    </tbody>
                </table>                                               
            </div>
            <div class="col-md-6">
                <table class="table m-b-0">
                    <tbody>
                        <tr>
                            <th scope="row" style="width:30%">โทรศัพท์มือถือ</th>
                            <td class="border-0" style="width:70%">
                                <input type="text" name="mobileNo" id="mobileNo" class="form-control" maxlength="50" value="{{ $_SESSION['MOBILE_NO'] }}" data-old="{{ $_SESSION['MOBILE_NO'] }}">
                            </td>
                        </tr>
                    </tbody>
                </table>                                                                                                       
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                    <div class="text-center">
                        <button class="btn btn-primary waves-effect waves-light" type="submit"><i class="fa fa-save"></i> บันทึก</button>
                        <button class="btn btn-secondary waves-effect waves-light" type="reset" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>                        
                    </div>
            </div>
        </div>        
    </form>
</div>