<div class="modal fade" id="profile-upload" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Upload New Picture</h4>
            <div>
            <div class="modal-body">            
                <form id="profile-upload-form" method="post" action="{{ route('api/Profile/upload') }}" class="dropzone dz-clickable">
                    <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                </form>

                <div id="profile-edit" class="d-none">
                    <img id="imgProfileEdit" src="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                <button type="button" id="btnUpload" class="btn btn-primary waves-effect waves-light">Change Profile</button>
                <button type="button" id="btnCrop" class="btn btn-primary waves-effect waves-light d-none">Create Icon</button>
            </div>
        </div>
    </div>
</div>