<div id="view-info">
    <div class="row">    
        <div class="col-md-6">
            <table class="table m-b-0">
                <tbody>
                    <tr>
                        <th scope="row" style="width:30%">Buasri Email</th>
                        <td class="border-0" style="width:70%">{{ $_SESSION['BUASRI_EMAIL'] }}</td>
                    </tr>
                </tbody>
            </table>                                               
        </div>
        <div class="col-md-6">
            <table class="table m-b-0">
                <tbody>
                    <tr>
                        <th scope="row" style="width:30%">GAFE Email</th>
                        <td class="border-0" style="width:70%">{{ $_SESSION['GAFE_EMAIL'] }}</td>
                    </tr>
                </tbody>
            </table>                                               
        </div>
    </div>
    <div class="row">    
        <div class="col-md-6">
            <table class="table m-b-0">
                <tbody>
                    <tr>
                        <th scope="row" style="width:30%">อีเมล์อื่น</th>
                        <td class="border-0" style="width:70%" id="view-other-email">{{ $_SESSION['OTHER_EMAIL'] }}</td>
                    </tr>
                </tbody>
            </table>                                               
        </div>
        <div class="col-md-6">
                                                     
        </div>
    </div>
    <div class="row">    
        <div class="col-md-6">
            <table class="table m-b-0">
                <tbody>
                    <tr>
                        <th scope="row" style="width:30%">เบอร์โทรศัพท์</th>
                        <td class="border-0" style="width:70%" id="view-telephone-no">{{ $_SESSION['TELEPHONE_NO'] }}</td>
                    </tr>
                </tbody>
            </table>                                               
        </div>
        <div class="col-md-6">
            <table class="table m-b-0">
                <tbody>
                    <tr>
                        <th scope="row" style="width:30%">โทรศัพท์มือถือ</th>
                        <td class="border-0" style="width:70%" id="view-mobile-no">{{ $_SESSION['MOBILE_NO'] }}</td>
                    </tr>
                </tbody>
            </table>                                                                                                       
        </div>
    </div>
</div>