@extends('layout')
@section('beforestyle')
    <link rel="stylesheet" type="text/css" href="@relative('css/dropzone.min.css')">
    <link rel="stylesheet" type="text/css" href="@relative('css/cropper.min.css')">
@endsection

@section('style')
    <style>
        #imgProfileEdit {
            max-width:100%
        }
    </style>    
@endsection

@section('content')    
<ul id="breadcrumb-triangle">
    @if ($_SESSION['TYPE']=='TEACHER')
        <li><a href="{{route('teacher')}}"><span class="icofont icofont-home"> </span></a></li>                    
    @elseif ($_SESSION['TYPE']=='STUDENT')
        <li><a href="{{route('student')}}"><span class="icofont icofont-home"> </span></a></li>                    
    @elseif ($_SESSION['TYPE']=='OTHER')
        <li><a href="{{route('admin')}}"><span class="icofont icofont-home"> </span></a></li>                    
    @endif    
    <li><a href="#"><span class="icon-screen-desktop"></span> ประวัติส่วนตัว</a></li>
</ul>

<div class="row">
    <div class="col-xl-3 col-lg-4">
        <div class="card faq-left">
            <div class="social-profile">
                <img class="img-fluid width-100" src="{{ profile($_SESSION['ITS_ID']) }}?{{ time() }}" alt="" id="imgProfile">
                <div class="profile-hvr m-t-15">
                    <i class="icofont icofont-ui-edit p-r-10 c-pointer" onclick="Profile.openUpload();"></i>
                </div>
            </div>
            <div class="card-block">
                <h4 class="f-18 f-normal m-b-10 txt-primary">{{ $_SESSION['NAME'] }}</h4>
                <h5 class="f-14">เลขประจำตัว {{ $_SESSION['ID'] }}</h5>                
                <h5 class="f-14">{{ $_SESSION['MAJOR'] }}</h5>                
                <h5 class="f-14">{{ $_SESSION['INDEPT'] }}</h5>              
                <h5 class="f-14">{{ $_SESSION['DEPT'] }}</h5>                
                <ul>
                    <li class="faq-contact-card">
                        <i class="icofont icofont-mobile-phone"></i>
                        <a href="tel:{{ $_SESSION['MOBILE_NO'] }}" id="lnkMobileNo">{{ $_SESSION['MOBILE_NO'] }}</a>
                    </li>
                    <li class="faq-contact-card">                        
                        <i class="icofont icofont-email"></i>
                        <a href="mailto:{{ $_SESSION['BUASRI_EMAIL'] }}@swu.ac.th">{{ $_SESSION['BUASRI_EMAIL'] }}</a>
                    </li>
                    <li class="faq-contact-card">                        
                        <i class="icofont icofont-email"></i>
                        <a href="mailto:{{ $_SESSION['GAFE_EMAIL'] }}">{{ $_SESSION['GAFE_EMAIL'] }}</a>
                    </li>
                </ul>                
            </div>
        </div>
        <!-- end of card-block -->        
        <!-- end of card -->
    </div>
    <!-- end of col-lg-3 -->

    <!-- start col-lg-9 -->
    <div class="col-xl-9 col-lg-8">                                
        <div class="card">
            <div class="card-header"><h5 class="card-header-text">ประวัติส่วนตัว : {{ $_SESSION['NAME'] }}</h5>
                <button id="edit-btn" type="button" class="btn btn-primary waves-effect waves-light f-right">
                    <i class="icofont icofont-edit"></i>
                </button>
            </div>
            <div class="card-block">
                @include('profile-view')             
                @include('profile-edit')             
            </div>
            <!-- end of card-block -->
        </div>
        <!-- end of card-->                                            
    </div>
</div>    

@include('profile-upload')
@endsection

@section('beforescript')
<script src="@relative('js/dropzone.min.js')"></script>
<script src="@relative('js/cropper.min.js')"></script>
@endsection

@section('script')
<script>
Dropzone.autoDiscover = false;    
var X;
var Y;
var Width;
var Height;
var image;
var cropper;

$(document).ready(function () {
    $('#btnCrop').click(function(){
        Profile.createIcon();
    }); 

    $('#frmSave').submit(function (e) { 
        e.preventDefault();
        
        Profile.save($(this));
    });   

    $('#btnCancel').click(function(e) {
        $('#otherEmail').focus();
    });

    $('#edit-info').hide();

    $('#edit-btn').on('click',function(){
        var b=$(this).find( "i" );
        var edit_class=b.attr('class');
        if (edit_class=='icofont icofont-edit') {
            b.removeClass('icofont-edit');
            b.addClass('icofont-close');
            $('#view-info').hide();
            $('#edit-info').show();
            $('#otherEmail').focus();
        }
        else{
            $('#frmSave')[0].reset();
            b.removeClass('icofont-close');
            b.addClass('icofont-edit');
            $('#view-info').show();
            $('#edit-info').hide();
        }
    });
    

    Profile.setupUpload();            
});

var Profile = {
    openUpload: function() {
        $('#profile-upload').modal('show');
    },
    save:function(form) {
        $('#edit-info').block();
        $.ajax({
            type: "post",
            url: form.attr('action'),
            data: form.serialize(),
            dataType: "json",
            success: function (response) {
                if (response) {
                    $('#edit-info').unblock();                    
                    $("#lnkMobileNo").text($('#mobileNo').val());
                    $("#lnkMobileNo").attr("href", "tel:" + $('#mobileNo').val());

                    $("#view-other-email").html($('#otherEmail').val());
                    $("#view-telephone-no").html($('#telephoneNo').val());
                    $("#view-mobile-no").html($('#mobileNo').val());
                    
                }                
            }
        });
    },
    setupUpload: function() {
        $('#profile-upload-form').dropzone({
			acceptedFiles: "image/*",
			maxFiles : 1,
			addRemoveLinks: true,
			autoProcessQueue: false,
			uploadMultiple: false,
			paramName: 'file',
			init: function() {
                var dropZone = this;
                $('#btnUpload').click(function(){
                    dropZone.processQueue();                    
                });             
            },
            complete: function(file) {                
                $("#imgProfile").attr('src', file.dataURL);
                $('#profile-upload-form').hide();
                $('#profile-edit').removeClass('d-none');
                $("#imgProfileEdit").attr('src', file.dataURL);
                
                $('#btnCrop').removeClass('d-none');
                $('#btnUpload').addClass('d-none');

                this.removeAllFiles();         
                
         
                Profile.setupCropper(); 

                
            },
			error: function (file, response) {
				this.removeFile(file);
            },
		}); 
    } , 
    setupCropper: function() {
        image = document.getElementById('imgProfileEdit');
        cropper = new Cropper(image, {
            aspectRatio: 1 / 1,
            maxWidth:45,
            maxHeight:45,
            crop(event) {
                X = event.detail.x;        
                Y = event.detail.y;
                Width = event.detail.width;
                Height = event.detail.height;
            },
        });
    },
    createIcon : function() {
        var f = new FormData();
        f.append('x',X);
        f.append('y',Y);
        f.append('width',Width);
        f.append('height',Height);
                
        $.ajax({
            url: "{{ route('api/Profile/createIcon') }}",
            type: "POST",
            data: f,      
            cache : false,
            processData: false,
            contentType:false,
            success: function (response) {
                $('#btnCrop').addClass('d-none');
                $('#btnUpload').removeClass('d-none');
                $('#profile-upload-form').show();
                $('#profile-edit').addClass('d-none');
                $("#imgProfileEdit").attr('src', null);

                $('#profile-upload').modal('hide');
                
                $("#imgUser").attr("src", 
                    Helper.userUrlIcon($('#its-id').val())
                );

                cropper.destroy();
            }
        });
    }   
}
</script>    
@endsection

