<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Swu-Internship @yield('title')</title>
    <link rel="shortcut icon" href="@relative('images/favicon.ico')" type="image/x-icon">
    <link rel="icon" href="@relative('images/favicon.ico')" type="image/x-icon">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <!--Date Picker Material Icon Css-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- iconfont -->
    <link rel="stylesheet" type="text/css" href="@relative('icon/icofont/css/icofont.css')">

    <!-- simple line icon -->
    <link rel="stylesheet" type="text/css" href="@relative('icon/simple-line-icons/css/simple-line-icons.css')">
    <link rel="stylesheet" href="@relative('css/bootstrap.min.css')">        
    <link rel="stylesheet" href="@relative('css/main.min.css')"> 
    <link rel="stylesheet" href="@relative('css/responsive.css')">    
    <link rel="stylesheet" href="@relative('css/landingpage.min.css')">    
    @yield('style')
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">
    <input type="hidden" name="current-path" id="current-path" value="@relative('')">

    @yield('content')        
    <script>
        var color = false;
    </script>
    <script src="@relative('js/jquery.min.js')"></script>
    <script src="@relative('js/jquery-ui.min.js')"></script>
    <script src="@relative('js/bootstrap.bundle.min.js')"></script>              
    <script src="@relative('js/form.js')?{{ time() }}"></script> 
    @yield('script')
</body>
</html>