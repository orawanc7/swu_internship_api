<div class="card d-none" id="comment-panel">
    <input type="hidden" name="comment-loadurl" value="{{route('api/SchdCommentDoc/listByStatusSend')}}">
    <div class="card-block">     
        <div class="p-b-30">
            <span class="f-14"><a href="javascript:ActivityComment.writeComment();">Write Comments (<span id="comment.total"></span>)</a></span>
            <span class="f-right"><a href="#"></a></span>
        </div>
        
        <div class="media">
            <div class="mr-3">
                <img class="img-fluid rounded-circle " src="{{ icon($_SESSION['ITS_ID']) }}" width="45px">
            </div>
            <div class="media-body">
                <form action="{{ route ('api/SchdCommentDoc/save')  }}" method="post" id="comment-form"> 
                    <input type="hidden" name="comment-itsStatusSendId" value="0">
                    <input type="hidden" name="comment-seqId" value="0">
                    <input type="hidden" name="comment-load" value="0">
                    

                    <textarea class="f-13 post-input msg-send" rows="3" cols="10"
                            required=""
                            placeholder="....." name="post"></textarea>
                    <div class="text-right m-t-20">
                        <button class="btn btn-primary waves-effect waves-light" type="submit"><i class="icofont icofont-edit"></i> เขียน</button>                        
                    </div>                    
                </form>
            </div>
        </div>
        
        <div id="comment-detail" class="p-t-20">
        </div>         
    </div>
</div>