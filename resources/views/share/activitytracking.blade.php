<div class="card">        
    <div class="card-header">      
        <i class="icofont icofont-clock-time"></i> ประวัติการส่งงาน            
    </div>
    <div class="card-block">
        <table class="table table-striped table-bordered" style="width:100%;">
            <thead>
                <tr>                                           
                    <th style="text-align:left" width="10%">ครั้งที่</th>
                    <th style="text-align:center" width="20%">วันที่ส่ง</th>
                    <th style="text-align:center" width="20%">วันที่ตรวจ</th>                                                     
                    <th style="text-align:center" width="10%">สถานะ</th>   
                    <th style="text-align:center" width="30%">ผู้ตรวจ</th>                                                                                
                    <th style="text-align:center" width="10%"></th> 
                </tr>
            </thead>
            <tbody>
                <tr class="text-danger">                        
                    <td align="center">1</td>
                    <td align="center">30 ส.ค. 2561 10:25 น.</td>
                    <td align="center">31 ส.ค. 2561 22:00 น.</td>                            
                    <td align="center">ส่งคืน</td>                            
                    <td align="left">
                        <div class="card-block user-box">
                            <div class="media m-b-20">                            
                                <img class="media-object rounded-circle m-r-20"
                                        src="@relative('images/teacher.png')"
                                        alt="" width="56px">                            
                                <div class="media-body">
                                    <div class="chat-header">ตุลาคม 30, 2560 14:20 น.
                                    </div>
                                    <p class="text-muted">มีปรับเรื่องข้อแนะนำ</p>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td align="center"><button class="btn btn-primary">รายละเอียด</button></td>   
                </tr>                        
            </tbody>
        </table>
    </div>
</div>