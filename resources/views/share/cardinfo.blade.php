<div class="card faq-left">
    <div class="social-profile">
        <img class="img-fluid width-100" src="{{ profile($_SESSION['ITS_ID']) }}" alt="">
        <div class="profile-hvr m-t-15">
            <i class="icofont icofont-ui-edit p-r-10 c-pointer" onclick="alert('xx');"></i>
        </div>
    </div>
    <div class="card-block">
        <h4 class="f-18 f-normal m-b-10 txt-primary">{{ $_SESSION['NAME'] }}</h4>
        <h5 class="f-14">{{ $_SESSION['MAJOR'] }}</h5>                
        <h5 class="f-14">{{ $_SESSION['DEPT'] }}</h5>                
        <ul>
            <li class="faq-contact-card">
                <i class="icofont icofont-mobile-phone"></i>
                <a href="tel:{{ $_SESSION['MOBILE_NO'] }}">{{ $_SESSION['MOBILE_NO'] }}</a>
            </li>
            <li class="faq-contact-card">                        
                <i class="icofont icofont-email"></i>
                <a href="mailto:{{ $_SESSION['BUASRI_EMAIL'] }}@swu.ac.th">{{ $_SESSION['BUASRI_EMAIL'] }}</a>
            </li>
            <li class="faq-contact-card">                        
                <i class="icofont icofont-email"></i>
                <a href="mailto:{{ $_SESSION['GAFE_EMAIL'] }}">{{ $_SESSION['GAFE_EMAIL'] }}</a>
            </li>
        </ul>                
    </div>
</div>