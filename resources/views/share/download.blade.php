<div class="card">
    <div class="card-header	">
        <h5 class="card-header-text">
            เอกสารดาวน์โหลด
        </h5>
    </div>
    <div class="card-block accordion-block">
    </div>
    <div class="card-block">            
        <ul class="basic-list list-icons faq-expi border-bottom">
            <a href="@relative('files/แบบบันทึกการสังเกตุการสอน.pdf')" class="href">
                <li>
                    <i class="h2 icofont icofont-file-pdf text-danger"></i>
                    <h6>แบบบันทึกการสังเกตการสอน</h6>                
                    <h6>Last edited : 30 เม.ย. 2562</h6>            
                </li>                
            </a>
        </ul>
        <ul class="basic-list list-icons faq-expi border-bottom">
            <a href="@relative('files/แบบบันทึกกิจกรรมประจำสัปดาห์ของนิสิต.pdf')" class="href">
                <li>
                    <i class="h2 icofont icofont-file-pdf text-danger"></i>
                    <h6>แบบบันทึกกิจกรรมประจำสัปดาห์ของนิสิต</h6>      
                    <h6>Last edited : 30 เม.ย. 2562</h6>                      
                </li>                
            </a>
        </ul>
        <ul class="basic-list list-icons faq-expi border-bottom">
            <a href="@relative('files/แบบบันทึกการนิเทศการปฏิบัติการสอนและฝึกประสบการณ์วิชาชีพ.pdf')" class="href">
                <li>
                    <i class="h2 icofont icofont-file-pdf text-danger"></i>
                    <h6>แบบบันทึกการนิเทศการปฏิบัติการสอนและฝึกประสบการณ์วิชาชีพ</h6>            
                    <h6>Last edited : 30 เม.ย. 2562</h6>                
                </li>                
            </a>
        </ul>
        <ul class="basic-list list-icons faq-expi border-bottom">
            <a href="@relative('files/แบบประเมินกระบวนการปฏิบัติงานวิจัยในชั้นเรียน.pdf')" class="href">
                <li>
                    <i class="h2 icofont icofont-file-pdf text-danger"></i>
                    <h6>แบบประเมินกระบวนการปฏิบัติงานวิจัยในชั้นเรียน</h6>    
                    <h6>Last edited : 30 เม.ย. 2562</h6>                        
                </li>                
            </a>
        </ul>
        <ul class="basic-list list-icons faq-expi border-bottom">
            <a href="@relative('files/แบบประเมินed591_1.pdf')" class="href">
                <li>
                    <i class="h2 icofont icofont-file-pdf text-danger"></i>
                    <h6>แบบประเมิน ED591 ภาคเรียนที่ 1</h6>    
                    <h6>Last edited : 30 เม.ย. 2562</h6>            
                </li>                
            </a>
        </ul>
        <ul class="basic-list list-icons faq-expi border-bottom">
            <a href="@relative('files/แบบประเมินed591_2.pdf')" class="href">
                <li>
                    <i class="h2 icofont icofont-file-pdf text-danger"></i>
                    <h6>แบบประเมิน ED591 ภาคเรียนที่ 2</h6>
                    <h6>Last edited : 30 เม.ย. 2562</h6>            
                </li>                
            </a>
        </ul>
        <ul class="basic-list list-icons faq-expi border-bottom">
            <a href="@relative('files/หน้าอนุมัติงานวิจัย.pdf')" class="href">
                <li>
                    <i class="h2 icofont icofont-file-pdf text-danger"></i>
                    <h6>หน้าอนุมัติงานวิจัย</h6>
                    <h6>Last edited : 14 พ.ค. 2562</h6>            
                </li>                
            </a>
        </ul>
        <ul class="basic-list list-icons faq-expi border-bottom">
            <a href="@relative('files/แบบฟอร์มเขียนนวัตกรรม.pdf')" class="href">
                <li>
                    <i class="h2 icofont icofont-file-pdf text-danger"></i>
                    <h6>แบบฟอร์มเขียนนวัตกรรม</h6>
                    <h6>Last edited : 14 พ.ค. 2562</h6>            
                </li>                
            </a>
        </ul>
        <ul class="basic-list list-icons faq-expi border-bottom">
            <a href="@relative('files/ใบลา.pdf')" class="href">
                <li>
                    <i class="h2 icofont icofont-file-pdf text-danger"></i>
                    <h6>ใบลา</h6>
                    <h6>Last edited : 14 พ.ค. 2562</h6>            
                </li>                
            </a>
        </ul>
    </div>
</div>

