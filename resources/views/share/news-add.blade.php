<div class="md-modal md-effect-8" id="news-add">
    <div class="md-content">
        <h3><i class="h4 icofont icofont-megaphone-alt txt-white"></i> ประกาศ</h3>
        <div>            
            <form action="{{ route('api/News/saveTeacher') }}" method="post" id="news-form" name="news-form">     
                <input type="hidden"  name="itsPersonId">
                <div class="md-input-wrapper">
                    <input type="text" class="md-form-control md-static" autofocus name="newsTitleNameTh" required/>
                    <label class="mandatory">หัวข้อ</label>
                </div>                                              
                <div class="md-input-wrapper">
                    <textarea class="md-form-control md-static" cols="2" rows="4" name="newsDescTh" required maxlength="200"></textarea>
                    <label class="mandatory">รายละเอียด </label>
                </div>            

                <div class="md-input-wrapper">
                    <div class="form-group row">
                        <div class="col-md-2"><label for="">ผู้รับ</label></div>
                        <div class="col-md-10" id="news-receiver">                            
                            
                        </div>
                    </div>
                </div>
                                                    
                <div class="row">
                    <div class="col-md-6">
                        <button type="button" class="btn btn-secondary waves-effect md-close" id="btnNewsClose"><i class="icofont icofont-close"></i>  ปิด</button>
                    </div>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary waves-effect md-close"><i class="icofont icofont-save"></i>  บันทึก</button>
                    </div>
                </div>     
                                
            </form>      
        </div>
    </div>
</div>
