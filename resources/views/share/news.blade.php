<input type="hidden" name="news-load-sender-receiver-url" value="{{ route('api/News/listBySenderReceiver') }}"> 
<input type="hidden" name="news-load-sender-url" value="{{ route('api/News/listBySender') }}"> 
<input type="hidden" name="news-load-receiver-url" value="{{ route('api/News/listByReceiver') }}"> 
<input type="hidden" name="news-delete-url" value="{{ route('api/News/delete') }}"> 
<input type="hidden" name="news-delete-receiver-url" value="{{ route('api/News/deleteByReceiver') }}"> 

<div class="card">
    <div class="card-header">
        <h5 class="card-header-text"><i class="h4 icofont icofont-megaphone-alt txt-primary"></i> ประกาศแจ้งนิสิต</h5>        
        <div class="f-right">            
            <button type="button" class="btn btn-sm btn-primary waves-effect md-trigger d-none" data-modal="news-add" data-toggle="tooltip" data-placement="top" title="" data-original-title="แจ้งประกาศ"  id="btnOpenNews">
                <i class="icofont icofont-ui-add"></i> เพิ่มประกาศ
            </button>
        </div>
    </div>
    <div class="card-block" id="news">
        <ul class="list-view" >

        </ul>        
        <!--
        <div class="new-users-more text-center p-t-10">
            <a href="#"><h6 class="m-b-0 f-w-400">ดูประกาศทั้งหมด<i class="icofont icofont-bubble-right m-l-10"></i></h6></a>
        </div>
        -->
    </div>
</div>