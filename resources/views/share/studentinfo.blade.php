<input type="hidden" name="studentinfo-get-url" value="{{route('api/SchdStdInfo/get')}}">
<input type="hidden" name="studentinfo-relateperson-url" value="{{route('api/SchdStdInfo/listRelatePerson')}}">
<input type="hidden" name="studentinfo-mentor-url" value="{{route('student/mentor')}}">
<input type="hidden" name="studentinfo-executive-url" value="{{route('student/executive')}}">
<div class="card b-l-primary text-left" id="div-studentinfo">                    
    <div class="card-header">
            <i class="icofont icofont-university txt-primary"></i> สถานที่ฝึกประสบการณ์ <i class="icofont icofont-location-pin txt-primary"></i>
        <span id="student-school-name"></span>
    </div>
    
    <div class="card-block widget-user-list" id="studentinfo-relateperson">
        
    </div>

    <div class="text-right p-3">
        <button type="button" class="btn btn-primary waves-effect waves-light mentor" id="studentinfo-mentor-button" style="display:none;"><i class="icofont icofont-teacher"> บันทึกครู/อาจารย์พี่เลี้ยง</i></button>
    </div>    
</div>