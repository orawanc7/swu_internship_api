<input type="hidden" name="studentschedule-load-url" value="{{route('api/SchdStdTeach/listByItsStudentId')}}">
<div class="card b-l-primary d-none" id="div-student-schedule">
    <div class="card-header">
        <i class="h4 icofont icofont-calendar txt-primary"></i> ตารางสอน           
        <div class="f-right">
            <a href="javascript:;"><i class="icofont icofont-minus"></i></a>            
        </div>
    </div>
    <div class="card-block">        
        <div id="student-schedule" class="schedule">
            <div class='s-legend'>
                <div class='s-cell s-head-info'>
                    <div class='s-name'>วันที่</div>
                </div>
                <div class='s-week-day s-cell'>
                    <div class='s-day'>จันทร์</div>
                </div>
                <div class='s-week-day s-cell'>
                    <div class='s-day'>อังคาร</div>
                </div>
                <div class='s-week-day s-cell'>
                    <div class='s-day'>พุธ</div>
                </div>
                <div class='s-week-day s-cell'>
                    <div class='s-day'>พฤ</div>
                </div>
                <div class='s-week-day s-cell'>
                    <div class='s-day'>ศุกร์</div>
                </div>
                <div class='s-week-day s-cell'>
                    <div class='s-day'>เสาร์</div>
                </div>
                <div class='s-week-day s-cell'>
                    <div class='s-day'>อาทิตย์</div>
                </div>
            </div>
            <div class='s-container s-block'>
                <div class='s-head-info' id="student-schedule-header">
                </div>
            </div>
            <div class='s-rows-container'>
            </div>
        </div>    
    </div>
</div>