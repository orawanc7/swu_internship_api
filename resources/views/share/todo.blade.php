<div class="card">
    <div class="card-header	">
        <h5 class="card-header-text">
                <i class="h4 icofont icofont-tasks txt-primary"></i> To Do List
        </h5>
    </div>
    <div class="card-block">
        <div class="row m-b-20">
            <div class="col-xl-8 col-xs-12">
                <div class="md-group-add-on">
                <span class="md-add-on">
                <i class="icofont icofont-listine-dots"></i>
                </span>
                    <div class="md-input-wrapper">
                        <input type="text" class="md-form-control" name="task-insert" />
                        <label>เพิ่มรายการตรงนี้</label>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-xs-12 text-right">
                <button id="create-task" class="btn btn-primary btn-add-task" type="button"><i class="icofont icofont-ui-add"></i> สร้างรายการ</button>
            </div>
        </div>
        <section id="task-container">
            <ul id="task-list">                
            </ul>
            <div class="m-t-15">
                <button id="clear-all-tasks" class="btn btn-danger m-t-10" type="button"><i class="icofont icofont-eraser-alt"></i>ล้างทุกรายการ</button>
            </div>
        </section>
    </div>
</div>