<aside class="main-sidebar hidden-print ">
    <section class="sidebar" id="sidebar-scroll">  
        <ul class="sidebar-menu">
            <li class="nav-level">SWU-ITS</li>        
            <li><a href="{{route($_SESSION['URL']) }}"><i class="icon-home"></i><span>  หน้าแรก </span></a>            
            </li>            
            <li class="d-block d-md-none"><a href="{{route('profile') }}"><i class="icon-user"></i><span>  ประวัติส่วนตัว</span></a>
            </li>            
            <li class="d-block d-md-none"><a href="{{route('api/logout') }}"><i class="icon-logout"></i><span>  ออกจากระบบ</span></a>
            </li>
        </ul>
    <!--horizontal Menu Ends-->
    </section>
</aside>