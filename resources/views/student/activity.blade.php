@extends('student.layout')
@section('beforestyle')
    <link rel="stylesheet" type="text/css" href="@relative('css/dropzone.min.css')">    
@endsection

@section('content')
<ul id="breadcrumb-triangle">
    <li><a href="{{ route('student') }}"><span class="icofont icofont-home"> </span></a></li>    
    <li><a href="#"><span class="icon-screen-desktop"> {{ $activity['docNameTh']}} ภาค/ปีการศึกษา : {{ $activity['semCd'] }}/{{ $activity['year'] }}</span> </a></li>
</ul>

<input type="hidden" name="activityId" id="activityId" value="{{ $activityId }}">
<input type="hidden" name="itsStudentId" id="itsStudentId" value="{{ $itsStudentId }}">
<input type="hidden" name="itsStatusSendId" id="itsStatusSendId" value="{{ $itsStatusSendId }}">


<div class="row">
    <div class="col-xl-12 col-lg-12 grid-item">
        <div class="row">      
            <div class="col-xl-12 col-lg-12 grid-item">
                @set($scriptName="")
                @if ($activity['docTypeId']=="1") 
                    @include('activity.make.course')  
                    @set($scriptName="Course")
                @elseif ($activity['docTypeId']=="2") 
                    @include('activity.make.teach')
                    @set($scriptName="Teach")  
                @elseif ($activity['docTypeId']=="4") 
                    @include('activity.make.courseplan')
                    @set($scriptName="CoursePlan")  
                @elseif ($activity['docTypeId']=="6")
                    @include('activity.make.research')  
                    @set($scriptName="Research")
                @elseif ($activity['docTypeId']=="7")
                    @include('activity.make.research2')  
                    @set($scriptName="Research")
                @elseif ($activity['docTypeId']=="10")
                    @include('activity.make.portfolio')  
                    @set($scriptName="Portfolio")
                @elseif ($activity['docTypeId']=="15")
                    @include('activity.make.portfolio2')  
                    @set($scriptName="Portfolio")
                @else
                    @include('activity.form')
                    @include("activity.make.form")                          
                    @set($scriptName="Form")
                @endif                
            </div>                        
        </div>
        <div class="row">               
            <div class="col-xl-12 col-lg-12 grid-item">           
                    
            </div>               
        </div>
        <div class="row">               
            <div class="col-xl-12 col-lg-12 grid-item">           
                @include('share.activitycomment')
            </div>               
        </div>
    </div>    
</div>
@endsection    

@section('modal')
    @include('activity.make.upload')
    @include('activity.make.status')    
@endsection

@section('beforescript')
<script src="@relative('js/dropzone.min.js')"></script>
@endsection

@section('script')
<script>    
Dropzone.autoDiscover = false;
var itsUserId = "{{ $itsStudentId  }}";
var itsRoundId = "{{$activity['itsRoundId']}}";
var itsStudentId = "{{ $itsStudentId }}";
var docTypeId = "{{$activity['docTypeId']}}";
var activityId = "{{$activity['activityId']}}";
var docId = "{{$activity['docId']}}";
var docTypeId = "{{$activity['docTypeId']}}";

$(document).ready(function () {
    $('#btnFileUpload').click(function(){        
        $('#upload').modal('show');
    });

    $('input[name=uploadType]').click(function(){
        
        //if ($(this).is(":checked")) {
            var val = $(this).val();     
            
            if (val=="L") {
                $('#link_url').prop('disabled',false);
                $('#link_url').focus();                

                Dropzone.forElement("#upload-form").removeAllFiles(true);                                
        
                $('#btnSaveLink').removeClass('d-none');
                $('#upload-form').hide();
            }  else {
                $('#link_url').val('');
                $('#link_url').prop('disabled',true);                

                $('#btnSaveLink').addClass('d-none');
                $('#upload-form').show();
            }                      
        //}
    });

    $('#btnSaveLink').click(function(){
        $('#upload-link-form').submit();
    });

    $('#upload').on('hidden.bs.modal', function () {
        $('input[name=uploadType][value=F]').trigger('click');
        Dropzone.forElement("#upload-form").removeAllFiles(true);
    });

    $('#div-doctype-upload-list').on('click','a.delete',function(){
        var id = $(this).attr('data-id');
        $.ajax({
            type: "DELETE",
            url: "{{ route('api/SchdUpload/delete') }}" + '/' + id,            
            dataType: "json",
            success: function (response) {
                Activity.getUploadList();
            }
        });
    });

    $('#div-doctype-upload-list').on('click','a.download',function(){
        var id = $(this).attr('data-id');
        location.href=  "{{ route('api/SchdUpload/download') }}" + '/' + id;
    });

    $('#upload-link-form').validate({
        rules:{
            'link_url': {
                required:true,
                url:true
            }
        }
    });

    $('#upload-link-form').submit(function (e) { 
        e.preventDefault();
        
        if ($(this).valid()) {
            $.ajax({
                type: "post",
                url: "{{ route('api/SchdUpload/saveLink') }}",
                data: $(this).serialize(),
                dataType: "json",
                success: function (response) {
                    $('#upload').modal('hide');

                    Activity.getUploadList();
                }
            });
        }
    });    

    $('#upload-form').dropzone({
        acceptedFiles: ".pdf,.doc,.docx,.xls,.xlsx,.ppt,.pptx,image/png,image/jpg,image/jpeg",
        /*timeout: 180000,*/
        maxFiles : 1,
        maxFilesize: 20,
        addRemoveLinks: true,
        autoProcessQueue: false,
        uploadMultiple: true,
        paramName: 'file',
        init: function() {
            this.on("addedfile", function (file) {
                $('#btnUpload').removeClass('d-none');
            });

            this.on("removedfile", function (file) {
                if (this.files.length==0) {
                    $('#btnUpload').addClass('d-none');
                }
            });

            this.on("maxfilesexceeded", function() {
                if (this.files[1]!=null){                    
                    this.removeFile(this.files[1]);
                }
            });                  
            
            this.on("success", function(file, responseText) {
                this.removeAllFiles();
                $('#upload').modal('hide');

                Activity.getUploadList();
            });

            this.on("sending",function(file, xhr, formData) {
                xhr.ontimeout = function(e) {
                    alert('Uploading ' + file.name + ' timed out.');
                };
            });

            var dropZone = this;
            $('#btnUpload').on('click',function() {                
                dropZone.processQueue();                    
            });             
        },                       
        error: function (file, message, xhr) {            
            if (xhr == null) this.removeFile(file); // perhaps not remove on xhr errors
            alert(message);  
        },
    });         
});

var Activity = {
    setUploadStdCourseId : function(stdCourseId) {
        $('input[name=stdCourseId]').val(stdCourseId);
    },
    setUploadRefDocId : function(refDocId) {
        $('input[name=refDocId]').val(refDocId);
        
    },
    getUploadList: function () {        
        $('#div-doctype-upload-list').empty();        

        var docId = $('input[name=docId]').val();        
        var senderId = $('input[name=senderId]').val();
        var stdCourseId = $('input[name=stdCourseId]').val();
        if (stdCourseId=="0") {
            stdCourseId = "";
        }
        var refDocId = $('input[name=refDocId]').val();
        if (refDocId=="0") {
            refDocId = "";
        }
        var v_url = "";
                

        if ((stdCourseId.length>0) && (refDocId.length>0)) {
            v_url = "{{ route('api/SchdUpload/listByDocCourseRefDocSender') }}" + '/' + docId + '/' + stdCourseId + '/' + refDocId + '/' + senderId;
        } else if (stdCourseId.length>0) {
            v_url = "{{ route('api/SchdUpload/listByDocCourseSender') }}" + '/' + docId + '/' + stdCourseId + '/' + senderId;
        } else if (refDocId.length>0) {
            v_url = "{{ route('api/SchdUpload/listByDocRefDocSender') }}" + '/' + docId + '/' + refDocId + '/' + senderId;
        }

                        
        $.ajax({
            type: "get",
            url: v_url,
            dataType: "json",
            success: function (response) {
                if (response.length>0) {                                    
                    $.each(response, function (idx, item) {
                        var fileType = "";                        
                        if (item.uploadType=="F") {
                            var fileExtension = item.fileExtension.toLocaleLowerCase();
                            if (fileExtension=="pdf") {                        
                                fileType = '<i class="h3 icofont icofont-file-pdf text-danger"></i>';
                            } else if ((fileExtension=="docx") || (fileExtension=="doc")) {
                                fileType = '<i class="h3 icofont icofont-file-word text-primary"></i>';
                            } else if ((fileExtension=="xls") || (fileExtension=="xlsx")) {
                                fileType = '<i class="h3 icofont icofont-file-excel text-success"></i>';
                            } else if ((fileExtension=="ppt") || (fileExtension=="pptx")) {
                                fileType = '<i class="h3 icofont icofont-file-powerpoint text-danger"></i>';
                            } else if (fileExtension=="png") {
                                fileType = '<i class="h3 icofont icofont-file-png text-primary"></i>';
                            } else if ((fileExtension=="jpg") || (fileExtension=="jpeg")) {
                                fileType = '<i class="h3 icofont icofont-file-jpg text-primary"></i>';
                            } else {
                                fileType = '<i class="h3 icofont icofont-file-document text-black"></i>';
                            }     
                            $('#div-doctype-upload-list').append(
                                '<div class="row">' +
                                    '<div class="col-sm-12 border-bottom">' +
                                        '<label class="f-left"><a class="download" href="#" data-id="' + item.fileHdrId + '">' + fileType + ' ' + item.fileName + '</a> <b>Version :</b> ' + item.fileVersion + '</label>' +
                                        '<label class="f-right"><a class="delete" href="#" data-id="' + item.fileHdrId + '">' +
                                            '<i class="h3 icofont icofont-ui-delete"></i>' + 
                                        '</a></label>' +                                
                                    '</div>' +                            
                                '</div>'
                            );                     
                        } else {
                            fileType = '<i class="h3 icofont icofont-external-link"></i>';
                            $('#div-doctype-upload-list').append(
                                '<div class="row">' +
                                    '<div class="col-sm-12 border-bottom">' +
                                        '<label class="f-left"><a href="' + item.linkUrl + '" target="_blank">' + fileType + ' ' + item.linkUrl + '</a></label>' +
                                        '<label class="f-right"><a class="delete" href="#" data-id="' + item.fileHdrId + '">' +
                                            '<i class="h3 icofont icofont-ui-delete"></i>' + 
                                        '</a></label>' +                                
                                    '</div>' +                            
                                '</div>'
                            );  
                        }                                                     
                    });

                    $('#doctype-form-uploadlist').removeClass('d-none');
                }
            }
        });
    }
}

</script>
@if ($scriptName=="Form") 
    <script src="{{ route('js/app/FormGenerate.js' )}}?{{ time() }}"></script>
@endif
<script src="{{ route('js/app/DocType' )}}{{ $scriptName }}.js?{{ time() }}"></script>
<script src="{{ route('js/app/DocTypeActivity.js' )}}?{{ time() }}"></script>
@endsection