@extends('student.layout')

@section('content')
<ul id="breadcrumb-triangle">
    <li><a href="{{ route('student') }}"><span class="icofont icofont-home"> </span></a></li>
    <li><a href="#"><span class="icon-screen-desktop"> </span> </a></li>
</ul>

<input type="hidden" name="activityId" id="activityId" value="{{ $activityId }}">
<input type="hidden" name="itsStudentId" id="itsStudentId" value="{{ $itsStudentId }}">
<input type="hidden" name="itsStatusSendId" id="itsStatusSendId" value="{{ $itsStatusSendId }}">

<div class="row">
    <div class="col-xl-12 col-lg-12 grid-item">
        <div class="row">
            <div class="col-xl-12 col-lg-12 grid-item">
                @if ($activity['docTypeFlag']=="S")
                @include('activity.' . $activity['docTypeId'])
                @else
                @include("activity.form")
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 grid-item">

            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 grid-item">
                @include('share.activitycomment')
            </div>
        </div>
    </div>
</div>
@endsection


@section('script')
<script src="@relative('js/app/ActivityComment.js')"></script>
<script src="@relative('js/app/Activity.js')"></script>
<script>
    $(document).ready(function() {
        ActivityComment.setStatusSendId({
            {
                $itsStatusSendId
            }
        });
        ActivityComment.setupComment();

        setInterval(function() {
            ActivityComment.loadComment();
        }, 5000);
    });
</script>
@endsection