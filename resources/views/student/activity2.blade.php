@extends('student.layout')


@section('content')
<ul id="breadcrumb-triangle">
    <li><a href="{{ route('student') }}"><span class="icofont icofont-home"> </span></a></li>    
    <li><a href="#"><span class="icon-screen-desktop"> {{ $activity['docNameTh']}} ภาค/ปีการศึกษา : {{ $activity['semCd'] }}/{{ $activity['year'] }}</span> </a></li>
</ul>

<input type="hidden" name="activityId" id="activityId" value="{{ $activityId }}">
<input type="hidden" name="itsStudentId" id="itsStudentId" value="{{ $itsStudentId }}">
<input type="hidden" name="itsStatusSendId" id="itsStatusSendId" value="{{ $itsStatusSendId }}">

<div class="row">
    <div class="col-xl-12 col-lg-12 grid-item">
        <div class="row">      
            <div class="col-xl-12 col-lg-12 grid-item">
                @set($scriptName="")
                @include("activity.form")      
                @include("activity.check.form")      
                @set($scriptName="CheckForm")
            </div>                        
        </div>
        <div class="row">               
            <div class="col-xl-12 col-lg-12 grid-item">           
                    
            </div>               
        </div>
    </div>    
</div>
@endsection    


@section('script')
<script>    
var itsUserId = "{{ $itsStudentId  }}";
var itsRoundId = "{{$activity['itsRoundId']}}";
var itsStudentId = "{{ $itsStudentId }}";
var docTypeId = "{{$activity['docTypeId']}}";
var activityId = "{{$activity['activityId']}}";
var docId = "{{$activity['docId']}}";
var docTypeId = "{{$activity['docTypeId']}}";

$(document).ready(function () {
   Activity.loadDetail();
});

var Activity = {   
    loadDetail: function () {                
        $('#divDetail').block();

        Activity.getCurrentStatus(function(data) {                         
            $('#divDetail').unblock();
            if (data) {
                            
                if (data.confirmFlag=="Y") {
                    DocType.init(docId,itsStudentId,data.refDocId,function(){
                        Activity.getUploadList(data.refDocId);
                    });                        
                } else {
                    $('#div-form').html('<h1 class="text-danger text-center pt-2">รอผล</h1>');                   
                }                                                      
            } else {
                $('#div-form').html('<h1 class="text-danger text-center pt-2">รอผล</h1>');                   
            }    
            
        });           
        
        $('#div-doctype-upload-list').on('click','a.download',function(){
            var id = $(this).attr('data-id');
            location.href=  "{{ route('api/SchdUpload/download') }}" + '/' + id;
        });
    },
    getCurrentStatus: function (callback) {            
        $.ajax({
            type: "get",
            url: "{{route('api/SchdDocStatus/getByItsStudentIdDocId')}}" + "/" + itsStudentId + '/' + docId ,                
            dataType: "json",
            success: function (response) {                
                callback(response);                                
            }
        });
    },
    getUploadList: function (refDocId) {        
        $('#div-doctype-upload-list').empty();        
        var senderId = $('#doctype-form-user-id').val();

        var v_url = "{{ route('api/SchdUpload/listByDocRefDocSender') }}" + '/' + docId + '/' + refDocId + '/' + senderId;
                
        $.ajax({
            type: "get",
            url: v_url,
            dataType: "json",
            success: function (response) {                
                if (response.length>0) {

                    $.each(response, function (idx, item) {
                        var fileType = "";

                        if (item.uploadType=="F") {
                            if (item.fileExtension=="pdf") {                        
                                fileType = '<i class="h3 icofont icofont-file-pdf text-danger"></i>';
                            } else if ((item.fileExtension=="docx") || (item.fileExtension=="doc")) {
                                fileType = '<i class="h3 icofont icofont-file-word text-primary"></i>';
                            } else if ((item.fileExtension=="xls") || (item.fileExtension=="xlsx")) {
                                fileType = '<i class="h3 icofont icofont-file-excel text-success"></i>';
                            } else if ((item.fileExtension=="ppt") || (item.fileExtension=="pptx")) {
                                fileType = '<i class="h3 icofont icofont-file-powerpoint text-danger"></i>';
                            } else if (item.fileExtension=="png") {
                                fileType = '<i class="h3 icofont icofont-file-png text-primary"></i>';
                            } else if ((item.fileExtension=="jpg") || (item.fileExtension=="jpeg")) {
                                fileType = '<i class="h3 icofont icofont-file-jpg text-primary"></i>';
                            } else {
                                fileType = '<i class="h3 icofont icofont-file-document text-black"></i>';
                            }     
                            $('#div-doctype-upload-list').append(
                                '<div class="row">' +
                                    '<div class="col-sm-12 border-bottom">' +
                                        '<label class="f-left"><a class="download" href="#" data-id="' + item.fileHdrId + '">' + fileType + ' ' + item.fileName + '</a> <b>Version :</b> ' + item.fileVersion + '</label>' +                                        
                                    '</div>' +                            
                                '</div>'
                            );                     
                        } else {
                            fileType = '<i class="h3 icofont icofont-external-link"></i>';
                            $('#div-doctype-upload-list').append(
                                '<div class="row">' +
                                    '<div class="col-sm-12 border-bottom">' +
                                        '<label class="f-left"><a href="' + item.linkUrl + '" target="_blank">' + fileType + ' ' + item.linkUrl + '</a></label>' +
                                        '<label class="f-right"><a class="delete" href="#" data-id="' + item.fileHdrId + '">' +
                                            '<i class="h3 icofont icofont-ui-delete"></i>' + 
                                        '</a></label>' +                                
                                    '</div>' +                            
                                '</div>'
                            );  
                        }                                                     
                    });

                    $('#doctype-form-uploadlist').removeClass('d-none');
                }
            }
        });
    }
}

</script>
@if ($scriptName=="CheckForm") 
    <script src="{{ route('js/app/FormGenerate.js' )}}?{{ time() }}"></script>
@endif
<script src="{{ route('js/app/DocType' )}}{{ $scriptName }}.js?{{ time() }}"></script>
@endsection