@extends('student.layout')

@section('content')
<input type="hidden" name="studentId" id="studentId" value="{{ $schdStudent['studentId'] }}">
<input type="hidden" name="itsStudentId" id="itsStudentId" value="{{ $schdStudent['itsStudentId'] }}">
<input type="hidden" name="leaderFlag" id="leaderFlag" value="{{ $schdStudent['leaderFlag'] }}">

<div class="row">        
    <div class="col-sm-12 p-0">           
        <div class="row">            
            <div class="card-block breadcrumb-icon-block">
                <ul id="breadcrumb-triangle">
                    <li><a href="{{route('teacher')}}"><span class="icofont icofont-home"> </span></a></li>
                    <li><a href="#"><span class="icofont icofont-user"> </span>ข้อมูลนิสิต</a></li>                            
                </ul>
            </div>                     
        </div>        
    </div>
</div>

<div class="row">
    <div class="col-xl-8 col-lg-7 grid-item">
        <div class="row">      
            <div class="col-xl-12 col-lg-12 grid-item">     
                <div class="row">
                    <div class="col-md-2">
                        <img class="rounded-circle " src="{{ route('images/users/' . $schdStudent['studentId'] . '.png') }}" style="width:150px;" alt="User Image"></span>                                                    
                    </div>
                    <div class="col-md-10">                        
                            <div class="card">
                                <div class="card-block">
                                    <h4 class="f-18 f-normal m-b-10 txt-primary">นาย ปิติ เรียนดี</h4>
                                    <h5 class="f-14">เลขประจำตัวนิสิต 57104010079</h5>                            
                                    <h5 class="f-14">สุขศึกษา(กศ.บ.)</p>
                                    <h5 class="f-14">คณะพลศึกษา </p>
                                    <br>
                                    <ul>                                
                                        <li class="faq-contact-card">
                                            <i class="icofont icofont-ui-cell-phone"></i>
                                            0812345678
                                        </li>
                                        <li class="faq-contact-card">
                                            <i class="icofont icofont-email"></i>
                                            <a href="mailto:joe@example.com">57104010079@g.swu.ac.th</a>
                                        </li>
                                    </ul>   
                                </div>
                            </div>                           
                    </div>
                </div>                           
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-5 grid-item">       
        <div class="row">               
            <div class="col-xl-12 col-lg-12 grid-item">           
                @include('student.doctype')
            </div>                
        </div>
    </div>
</div>
@endsection


@section('script')
    <script>
        $(document).ready(function () {
            
            
        });
        
    </script>
@endsection