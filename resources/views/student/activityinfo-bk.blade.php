<div class="card">            
    <div class="card-block">      
        <h6 class="job-card-desc">ชื่อครูพี่เลี้ยง</h6>
        <select name="" id="" class="f-13 post-input msg-send">
            <option value=""><img class="media-object rounded-circle m-r-20"
                src="@relative('images/student.png')"
                alt="" width="56px">ครูลิซ่า</option>
        </select>
        <h6 class="job-card-desc">รายวิชา</h6>
        <select name="" id="" class="f-13 post-input msg-send">
            <option value=""><img class="media-object rounded-circle m-r-20"
                src="@relative('images/student.png')"
                alt="" width="56px">วิชาภาษาอังกฤษ</option>
        </select>
        <h6 class="job-card-desc">ชั้น</h6>
            <input type="text" name="" id="" class="f-13 post-input msg-send">
        </select>
        <h6 class="job-card-desc">เรื่อง</h6>
            <input type="text" name="" id="" class="f-13 post-input msg-send">        
        <h6 class="job-card-desc">วัน-เวลาที่สังเกต</h6>
        <input type="text" name="" id="" class="f-13 post-input msg-send">        
        <h6 class="job-card-desc">สิ่งที่ได้จากการเรียนรู้</h6>        
            <textarea class="f-13 post-input msg-send" rows="10" cols="10"
            required=""></textarea>             
        <h6 class="job-card-desc">สิ่งที่จะนำไปประยุกต์ใช้</h6>
            <textarea class="f-13 post-input msg-send" rows="10" cols="10"
            required=""></textarea>             
        <hr>  
        
        <h6 class="job-card-desc">รายการแนบ (1 ไฟล์)
            <button class="btn btn-primary"><i class="icofont icofont-ui-add"></i> เพิ่มไฟล์หรือลิงค์แนบ</button>
        </h6>        
        <ul class="basic-list list-icons faq-expi">
            <li>
                <i class="h2 icofont icofont-file-pdf text-danger"></i>
                <h6>แบบสังเกตการสอนครูลิซ่า <span class="f-right">ขนาดไฟล์ 2MB</span></h6>
                <h6>Last updated : 1 ก.ย. 2561 13:40 น.</h6>
            </li>                
        </ul>        
    </div>
    <div class="card-footer">        
        <span class="f-right">
            <button type="button"
                    class="btn btn-primary waves-effect waves-light"><i class="icofont icofont-ui-check"></i> ส่งงาน
            </button>
            <button type="button"
                    class="btn btn-danger waves-effect waves-light"><i class="icofont icofont-close"></i> ยกเลิก
            </button>
        </span>
    </div>
</div>
<!-- Job card end -->