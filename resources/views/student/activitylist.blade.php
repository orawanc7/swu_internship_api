@extends('student.layout')
@section('style')
    <link rel="stylesheet" href="@relative('plugins/timeline/css/style.css')">
@endsection
@section('content')
<input type="hidden" name="itsStudentId" id="itsStudentId" value="{{ $itsStudentId }}">
<input type="hidden" name="itsRoundId" id="itsRoundId">

<ul id="breadcrumb-triangle">
    <li><a href="{{route('student')}}"><span class="icofont icofont-home"> </span></a></li>
    <li><a href="#"><span class="icofont icofont-calendar"> </span>กิจกรรม <span id="activityInfo"></span></a></li>
</ul>

<div class="row">
    <div class="col-sm-12 p-0">           
        <div class="card">
            <div class="card-header text-center">                
                <h1 class="card-header-text">ภาคเรียนที่ <span id="semCd"></span> ปีการศึกษา <span id="year"></span></h1>
            </div>
            <div class="card-block" id="div-activity">
                <div class="main-timeline d-none" id="main-timeline">
                    <div class="cd-timeline cd-container" id="activity">
                       
                    </div> <!-- end cd-timeline cd-containter-->
                </div> <!-- end main-timeline -->
            </div> <!-- end card-block -->
        </div>
    </div>
</div>       
@endsection

@section('modal')
    @include('student.register')   
    <div class="md-overlay"></div>
@endsection

@section('script')
<script>
var itsStudentId;
var itsRoundId;
$(document).ready(function () {    
    itsStudentId = $('#itsStudentId').val();

    ActivityList.getStudentInfo();
});

var ActivityList = {      
    getStudentInfo : function() {      
        $('#div-activity').block();      
        $.ajax({
            type: "GET",
            url: "{{route('api/SchdStdInfo/get')}}/" + itsStudentId,            
            dataType: "json",
            success: function (response) {                
                $('#activityInfo').html(response.schoolNameTh);
                $('#semCd').html(response.semCd);
                $('#year').html(response.year);
                $('#itsRoundId').html(response.itsRoundId);
                itsRoundId = response.itsRoundId;
                
                $('#div-activity').unblock();
                ActivityList.loadActivity();
            }
        });
    },
    activity: function(activityId,itsStatusSendId) {
        location.href = "{{ route('student/activity') }}" + "/" + activityId + "/" + itsStudentId + "/" + itsStatusSendId;
    },
    loadActivity: function() {
        $('#div-activity').block();    
        $.ajax({
            type: "GET",
            url: "{{route('api/SchdActivity/listActivityByStudent')}}/" + itsRoundId + "/" + itsStudentId,            
            dataType: "json",
            success: function (response) {       
                $('#main-timeline').removeClass('d-none');
                $.each(response, function (idx, item) {   
                    var startDate = moment(item.startDate.date).format("DD/MM/YYYY");
                    var endDate = moment(item.endDate.date).format("DD/MM/YYYY");
                    var activityDate = "";
                    var doDate = "";
                    var status = "";
                    if (startDate==endDate) {
                        activityDate = startDate;
                    } else {
                        activityDate = startDate + '-' + endDate;
                    }

                    var linkDetail = "";
                    if (item.docId!=null) {
                        

                        if (item.finishStepFlag=="F") {
                            doDate = moment(item.sendDate.date).format("DD/MM/YYYY HH:mm");
                            if (item.confirmFlag=="F") {
                                status = "bg-success";
                            } else {
                                status = "bg-warning";
                            }
                            linkDetail = '<p class="m-t-20 text-right"><a class="btn" name="btn-activity-link" onClick=\'ActivityList.activity(' + item.activityId + ',' + item.itsStatusSendId  + ')\'>รายละเอียด</a></p>';
                        } else {
                            linkDetail = '<p class="m-t-20 text-right"><a class="btn" name="btn-activity-link">รายละเอียด</a></p>';
                        }
                    }

                    if (item.qrCodeFlag) {
                        if (item.seminarId!=null) {
                            var seminarStartDate = moment(item.seminarStartDate.date).format("DD/MM/YYYY HH:mm");
                            var seminarendDate = moment(item.seminarEndDate.date).format("DD/MM/YYYY HH:mm");
                            doDate = seminarStartDate + '-' + seminarendDate;  
                            status = "bg-success";
                        } else {
                            linkDetail = '<p class="m-t-20 text-right"><a class="btn" name="btn-activity-link" onClick=\'ActivityList.register("' + item.activityNameTh  +  '")\'><i class="fa fa-qrcode"></i> เข้าร่วมกิจกรรม</a></p>';
                        }
                    }

                    $('#activity').append(
                    '<div class="cd-timeline-block" name="card-activity" data-activity-id="' + item.activityId + '">' + 
                        '<div class="cd-timeline-icon ' + status +'">' +
                        '</div>' +
                        '<div class="cd-timeline-content card_main">' +                           
                            '<div class="card-block">'  +
                                '<h6>' + item.activityNameTh + '</h6>' + 
                                '<div class="timeline-details">' + 
                                    '<a href="#"> <i class="icofont icofont-ui-calendar"></i><span>' + activityDate + '</span> </a>' +                                     
                                    linkDetail + 
                                '</div>' + 
                            '</div>' +
                            '<span class="cd-date">' + doDate +'</span>' +
                            '<span class="cd-details">' + item.activityNameTh + '</span>' +
                        '</div>' +
                    '</div>' 
                    );
                });

                $('#div-activity').unblock();
            }
        });
    },
    register: function(activityName) {
        $('#register-name').html(activityName);
        $("#register-qr").addClass("md-show");
    },
    registerClose: function() {        
        $("#register-qr").removeClass("md-show");
    }
}
</script>
@endsection