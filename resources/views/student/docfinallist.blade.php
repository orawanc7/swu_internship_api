<div class="card b-l-primary" id="div-docfinallist">
    <div class="card-header">
        <div class="row">
            <div class="col-md-12">
                    <i class="h4 icofont icofont-document-folder txt-primary"></i> เอกสารส่งงานปลายภาค                
            </div>
        </div>                
    </div>
    <div class="card-block">                                           
        <div class="card job-right-header" id="selectedCourse" style="display:none;">
            <form action="{{ route('api/SchdStdCourse/saveSelected') }}" id="form-std-course">
                <input type="hidden" name="itsStudentId" id="itsStudentId">
                <div class="card-block">         
                    <div class="row">
                        <div class="col-6">
                            เลือกรายวิชาส่งงานปลายภาค
                        </div>
                        <div class="col-6 text-right">
                            <button type="submit" class="btn btn-primary waves-effect waves-light ">
                                <i class="fa fa-check "></i><span class="m-l-10">ยืนยันการเลือก</span>
                            </button>                        
                        </div>
                    </div>
                    
                    <div class="form-radio">
                        
                    </div>                
                </div>        
            </form>    
        </div>

        <a class="h5 txt-primary m-1 float-right" id="btnExpandCollapseDocFinal" data-toggle="tooltip" data-placement="top" title="" data-original-title="ย่อ/ขยาย">
            <i class="icofont icofont-expand"></i>
        </a>
        <table class="table table-striped table-bordered" style="width:100%;" id="tbDocFinalList">            
            <thead>
                <tr>
                    <th style="width:45%;text-align:left;vertical-align:top;">                        
                        งาน
                    </th>
                    <th style="width:22%;text-align:center;vertical-align:top;">วันที่</th>                    
                    <th style="width:20%;text-align:center;vertical-align:top;">สถานะ</th>
                    <th style="width:13%;text-align:center;vertical-align:top;"></th>                    
                </tr>
            </thead>
            <tbody>            
            </tbody>
        </table>             
    </div>
</div>
    