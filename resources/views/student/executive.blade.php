@extends('student.layout')

@section('content')
<input type="hidden" name="itsStudentId" id="itsStudentId" value="{{ $itsStudentId }}">
<ul id="breadcrumb-triangle">
    <li><a href="{{ route('student') }}"><span class="icofont icofont-home"> </span></a></li>    
    <li><a href="#"><span class="icon-screen-desktop"> บันทึกข้อมูลผู้บริหารสถานศึกษา</span> ภาคเรียนที่ <span id="semCd"></span> ปีการศึกษา <span id="year"></span></a></li>
</ul>


<div class="card b-l-primary">
    <div class="card-header">
        สถานที่ฝึกประสบการณ์ <i class="icofont icofont-location-pin"></i> <span id="schoolName"></span>
    </div>
    <div class="card-block" id="div-search">
        <div class="row">            
            <label for="personFnameTh" class="col-md-2">ชื่อ-นามสกุล</label>
            <div class="col-md-9">                
                <input type="text" class="form-control typeahead" name="searchName" id="searchName" placeholder="ระบุเฉพาะชื่อ นามสกุล (ระบุอย่างน้อย 4 ตัวอักษร)" aria-label="ระบุเฉพาะชื่อ นามสกุล" autofocus autocomplete="off">                
            </div>            
            <div class="col-lg-1 text-right">
                <button class="btn btn-primary" type="button" id="btnClear"><i class="fa fa-eraser"></i> ล้าง</button>
            </div>
        </div>     
    </div>   
</div>

<form action="{{ route('api/ItsPersonType/save') }}" method="post" id="frmExecutive">
    <div class="card b-l-primary" id="div-executive" style="display:none;">
        <input type="hidden" name="deptCd" id="deptCd">
        <input type="hidden" name="itsPersonId" id="itsPersonId">
        <input type="hidden" name="itsPersonTypeId" id="itsPersonTypeId">        
        <input type="hidden" name="itsRoundId" id="itsRoundId">        
        <input type="hidden" name="personTypeId" value="{{ getenv('PERSON_TYPE_SCHOOL_EXECUTIVE') }}">        
        <div class="card-block">                             
            <div class="form-group row">            
                <label for="prenameIntThCd" class="col-md-2 col-form-label form-control-label mandatory">คำนำหน้าชื่อ</label>
                <div class="col-md-4">
                    <select name="prenameIntThCd" id="prenameIntThCd" class="select2"></select>
                </div>                    
            </div>     
            <div class="form-group row">            
                <label for="personFnameTh" class="col-md-2 col-form-label form-control-label mandatory">ชื่อ</label>
                <div class="col-md-4">
                    <div class="input-data">
                        <input type="text" id="personFnameTh" name="personFnameTh" class="form-control" placeholder="ชื่อ" required maxlength="50" autofocus>
                    </div>
                </div>
                <label for="personLnameTh" class="col-md-2 col-form-label form-control-label mandatory">นามสกุล</label>
                <div class="col-md-4">
                    <div class="input-data">
                        <input type="text" id="personLnameTh" name="personLnameTh" class="form-control" placeholder="นามสกุล" required maxlength="50">
                    </div>
                </div>                        
            </div>     
            <div class="form-group row">            
                <label for="mobileNo" class="col-md-2 col-form-label form-control-label">มือถือ</label>
                <div class="col-md-4">
                    <div class="input-data">
                        <input type="text" id="mobileNo" name="mobileNo" class="form-control" placeholder="มือถือ" maxlength="50">
                    </div>
                </div>
                <label for="otherEmail" class="col-md-2 col-form-label form-control-label">อีเมล</label>
                <div class="col-md-4">
                    <div class="input-data">                    
                        <input type="text" id="otherEmail" name="otherEmail" class="form-control" placeholder="อีเมล" maxlength="50">
                    </div>
                </div>                        
            </div>     
        </div>   
        <div class="card-footer text-right">
            <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> บันทึก</button>
            <button class="btn btn-secondary" type="reset" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
        </div>     
    </div>
</form>    


<div class="card b-l-primary" id="div-executive-list">   
    <div class="card-header">
        <i class="icofont icofont-teacher">รายชื่อผู้บริหารสถานศึกษาทั้งหมด</i>
    </div> 
    <div class="card-body">        
        <div class="table-responsive-sm">
            <table class="table table-sm table-striped table-hover" id="tbExecutive">                           
                <thead>
                        <tr class="bg-primary">                
                            <th style="width:50%">ชื่อ-นามสกุล</th>
                            <th style="width:20%">มือถือ</th>
                            <th style="width:20%">อีเมล</th>                            
                            <th style="width:10%"></th>                
                        </tr>
                </thead>            
                <tbody>
                </tbody>
            </table>
        </div>
    </div>    
</div>
@endsection    

@section('script')
<script>
var itsStudentId;
var nameIdMap = {};
var notFound;
$(document).ready(function () {    
    itsStudentId = $('#itsStudentId').val();    

    $('#prenameIntThCd').select2({
        width:'100%',        
    });       
    
    $('#btnClear').click(function(){
        $('.typeahead').typeahead('val', '');
        $('#searchName').val("");
        $('#searchName').focus();
    });

    $('#btnCancel').click(function(){
        Executive.clearScreen();

        $('#searchName').val("");

        $('#div-search').show();
        $('#div-executive').hide();        

        $('#searchName').focus();
    });

    $('#div-search').on('click','#btnAdd',function() {    
        Executive.setScreen(true);
                
        $('#div-search').hide();
        $('#div-executive').show();        

        $('#prenameIntThCd').focus();
    });

    $('#tbExecutive').on("click", "a.update", function (e) {
            var itsPersonTypeId = $(this).attr('data-id');
            $('#div-search').hide();
            $('#div-executive').show();       
            Executive.getExecutive(itsPersonTypeId);
        });

        $('#tbExecutive').on("click", "a.delete", function () {
            var itsPersonTypeId = $(this).attr('data-id');
            Executive.deleteExecutive(itsPersonTypeId);
        });
    
    $('#searchName').on('typeahead:selected', function(evt, item) {      
        $('#div-executive').show();          
        Executive.getPerson(nameIdMap[item]);        
    });
    
    
    notFound = function (item) {
        var query = item.query;
        var arr = query.split(' ');
        if (arr.length>0) {
            $('#personFnameTh').val(arr[0]);

            if (arr.length>1) {
                $('#personLnameTh').val(arr[1]);
            }
        }
        return '<div class="row"><div class="col-md-6">&nbsp;<i class="fa fa-times"></i> ไม่พบข้อมูลต้องการเพิ่มหรือไม่</div><div class="col-md-6 text-right"><button class="btn btn-primary" id="btnAdd"><i class="fa fa-plus"></i> เพิ่ม</button></div></div></div>&nbsp;';
    }
    
    $('#searchName').typeahead(
        {
        hint: true,
        highlight: true,
        minLength: 4
        },
        {
            source: function ( query, syncResults, asyncResults) {
                var deptCd = $('#deptCd').val();
                $.get( '{{ route('api/Person/getExternalByDeptAndName') }}', { 'deptCd': deptCd, 'name' : query }, function ( data ) {
                    var resultList = [];

                    $.each(data, function (idx, item) { 
                         var aItem = {id: item.itsPersonId , name: item.personFnameTh + " " + item.personLnameTh};
                         resultList.push(aItem);
                    });

                    return asyncResults(getOptionsFromJson(resultList));
                } );
            },
            updater: function (item) {                
                return item;
            },
            templates: {
                notFound: notFound
            }              
        }
          
    );           

    $('#frmExecutive').validate({
        rules : {
            'otherEmail' : {
                'email' : true
            }
        }
    });

    $('#frmExecutive').submit(function (e) {
        e.preventDefault();

        if ($(this).valid()) {
            $('#div-executive').block();
            $('#prenameIntThCd').prop('disabled',false)
            $.ajax({
                type: 'post',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json",
                success: function (response) {
                    $('#div-executive').unblock();
                    $('#div-executive').hide();
                    $('#div-search').show();
                    Executive.clearScreen();                
                    Executive.loadExecutive();
                }
            });
        }        
    });

    Executive.loadPrename();

    Executive.getStudentInfo(function() {
        Executive.loadExecutive();
    });    

    $('#searchName').focus();
});

function getOptionsFromJson(json) {
    $.each(json, function (i, v) {
        nameIdMap[v.name] = v.id;
    });

    return $.map(json, function (n, i) {
        return n.name;
    });
}

var Executive = {          
    loadPrename: function() {
        $.ajax({
            type: "GET",
            url: "{{route('api/WSPrename')}}",
            dataType: "json",
            success: function (response) {
                if (response.data) {
                    var data = response.data;

                    $.each(data, function (idx, item) {
                        $('#prenameIntThCd').append($('<option>', {
                            value: item.prenameCd,
                            text: item.prenameLnameTh
                        }));
                    });
                }
            }
        });
        $.ajax({
            type: "GET",
            url: "{{route('api/SchdStdInfo/get')}}/" + itsStudentId,
            data: "data",
            dataType: "dataType",
            success: function (response) {
                
            }
        });
    },    
    getStudentInfo : function(callback) {            
        $.ajax({
            type: "GET",
            url: "{{route('api/SchdStdInfo/get')}}/" + itsStudentId,            
            dataType: "json",
            success: function (response) {                         
                $('#semCd').html(response.semCd);
                $('#year').html(response.year);
                $('#itsRoundId').val(response.itsRoundId);            
                $('#schoolName').html(response.schoolNameTh);      
                $('#deptCd').val(response.schoolId);      
                          
                if (callback) {
                    callback();
                }
            }
        });
    },
    getPerson : function(itsPersonId) {       
        $('#div-executive').block();

        Executive.clearScreen();
        $.ajax({
            type: "GET",
            url: "{{route('api/Person/get')}}/" + itsPersonId,                        
            dataType: "json",
            success: function (response) {
                if (response) {                    
                    $('#div-search').hide();
                    
                    $('#itsPersonId').val(response.itsPersonId);
                    $('#personFnameTh').val(response.personFnameTh);
                    $('#personLnameTh').val(response.personLnameTh);
                    $('#prenameIntThCd').val(response.prenameCd).trigger('change');
                    $('#mobileNo').val(response.mobileNo);
                    $('#otherEmail').val(response.otherEmail);
                                                            
                    Executive.getPersonType(response.itsPersonId);                    

                    $('#mobileNo').focus();                    
                }
            }
        });
    },
    getPersonType : function(itsPersonId) {        
        
        $.ajax({
            type: "GET",
            url: "{{route('api/ItsPersonType/getExecutiveByItsPersonId')}}/" + itsPersonId,                        
            dataType: "json",
            success: function (response) {                
                if (response) {                                        
                    $('#itsPersonTypeId').val(response.itsPersonTypeId);
                    Executive.getExecutive(response.itsPersonTypeId);
                } else {
                    Executive.setScreen(false);
                    $('#div-executive').unblock();
                }                
            }
        });
    },    
    getExecutive : function(itsPersonTypeId) {        
        $('#div-executive').block();
        $.ajax({
            type: "GET",
            url: "{{route('api/ItsPersonType/get')}}/" + itsPersonTypeId,                        
            dataType: "json",
            success: function (response) {
                if (response) {                                        
                    
                    $('#itsPersonTypeId').val(response.itsPersonTypeId);
                    $('#itsPersonId').val(response.itsPersonId);                    
                    $('#personFnameTh').val(response.personFnameTh);
                    $('#personLnameTh').val(response.personLnameTh);
                    $('#prenameIntThCd').val(response.prenameCd).trigger('change');
                    $('#mobileNo').val(response.mobileNo);
                    $('#otherEmail').val(response.otherEmail);                                                                                
                    $('#mobileNo').focus();                    
                }

                Executive.setScreen(false);
                $('#div-executive').unblock();
            }
        });
    },
    deleteExecutive: function (itsPersonTypeId) {        
        $.ajax({
            type: "delete",
            url: "{{route('api/ItsPersonType/save')}}" + "/" + itsPersonTypeId,
            dataType: "json",
            success: function (response) {
                Executive.clearScreen();
                Executive.loadExecutive();
            }
        });
    },
    loadExecutive: function() {        
        $('#tbExecutive tbody').empty();

        $('#div-executive-list').block();
        $.ajax({
            type: "get",
            url: "{{ route('api/ItsPersonType/listExecutiveByDeptCd') }}" + "/" + $('#deptCd').val(),
            dataType: "json",
            success: function (response) {
                if (response) {
                    var seq = 0;
                    $.each(response, function (idx, item) {
                        $('#tbExecutive tbody').append(
                            '<tr>' +
                            '<td>' + item.prenameSnameTh + item.personFnameTh + " " + item.personLnameTh + '</td>' +
                            '<td>' + ((item.mobileNo==null)?"":item.mobileNo) + '</td>' +
                            '<td>' + ((item.otherEmail==null)?"":item.otherEmail) + '</td>' +
                            '<td class="text-right text-sm-right">' +
                            '<span class="dtr-data">' +
                            '<a class="btn btn-primary waves-effect waves-light update" data-id="' + item.itsPersonTypeId + '"><i class="icofont icofont-ui-edit text-white"></i></a>&nbsp;' +
                            '<a class="btn btn-primary waves-effect waves-light delete" data-id="' + item.itsPersonTypeId + '"><i class="icofont icofont-ui-delete text-white"></i></a> ' +
                            '</span>' +
                            '</td>' +
                            '</tr>'
                        );

                        $("#tbExecutive").rtResponsiveTables();

                    });
                }

                $('#div-executive-list').unblock();
            }
        });
    },    
    setScreen: function(status) {
        if (status) {
            $('#personFnameTh').prop('readOnly',false);
            $('#personLnameTh').prop('readOnly',false);
            $('#prenameIntThCd').prop('disabled',false);
        } else {
            $('#personFnameTh').prop('readOnly',true);
            $('#personLnameTh').prop('readOnly',true);
            $('#prenameIntThCd').prop('disabled',true);

        }
    },
    clearScreen: function () {
        $('#frmExecutive')[0].reset();

        $('#itsPersonId').val("");
        $('#itsPersonTypeId').val("");        

        $('.typeahead').typeahead('val', '');        
    }
};
</script>
@endsection