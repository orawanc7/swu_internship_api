@extends('student.layout')

@section('content')
<input type="hidden" name="roundId" id="roundId" value="{{ $semYear['roundId'] }}">
<input type="hidden" name="studentId" id="studentId" value="{{ $studentId }}">
<input type="hidden" name="itsStudentId" id="itsStudentId" value="{{ $schdStudent['itsStudentId'] }}">
<input type="hidden" name="leaderFlag" id="leaderFlag" value="{{ $schdStudent['leaderFlag'] }}">

<div class="row">
    <div class="col-xl-8 col-lg-7 grid-item">
        <div class="row">      
            <div class="col-xl-12 col-lg-12 grid-item">
                @include('student.doclist')
            </div>                        
            <div class="col-xl-12 col-lg-12 grid-item">
                @include('student.news')
            </div>
            <div class="col-xl-12 col-lg-12 grid-item">
                @include('../news')
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-5 grid-item">       
        <div class="row">               
            <div class="col-xl-12 col-lg-12 grid-item">           
                @include('student.doctype')
            </div>   
            <div class="col-xl-12 col-lg-12 grid-item">           
                @include('student.menu')
            </div>
            <div class="col-xl-12 col-lg-12 grid-item">           
                @include('student.todo')
            </div>
            <div class="col-xl-12 col-lg-12 grid-item">           
                @include('../download')
            </div>            
        </div>
    </div>
</div>
@endsection


@section('script')
    <script>
        $(document).ready(function () {
            $('#btnActivity').click(function (e) { 
                e.preventDefault();
                location.href = "{{ route('student/activity/') }}" + $('#roundId').val();
            });

            loadDocList();

            loadStudentNews();
            
        });

        function loadDocList() {
            $.ajax({
                type: "get",
                url: "{{ route('api/SchdDocStatus') }}",
                data : {
                    'roundId' : $('#roundId').val(),
                    'itsStudentId' : $('#itsStudentId').val()
                },
                dataType: "json",
                success: function (response) {
                    if (response) {
                        $('#doclist tbody').empty();
                        var seq=0;
                        $.each(response, function (index, row) {          
                            var statusControl  =  "";
                            if (row.finishStepFlag==null) {
                                statusControl = "<span class=\"btn btn-mini btn-secondary waves-effect waves-light\">"  + "รอดำเนินการ" + "</span>";
                            } else {
                                if (row.finishStepFlag=='Y') {
                                    statusControl = "<span class=\"btn btn-mini btn-success waves-effect waves-light\">"  + "เสร็จสิ้นแล้ว" + "</span>";
                                } else {
                                    statusControl = "<span class=\"btn btn-mini btn-info waves-effect waves-light\">"  + "ระหว่างการดำเนินการ" + "</span>";
                                }
                            }
                            $('#doclist tbody').append(
                                "<tr>" +
                                    "<td>" + row.docNameTh  + "</td>" +                                                                         
                                    "<td style=\"text-align:center;\">" + statusControl  + "</td>" +
                                "</tr>"
                                );
                        });                    
                    }
                }
            });
        }

        function loadStudentNews() {
            $.ajax({
                type: "get",
                url: "{{ route('api/News/ListByPersonTypeReceiver') }}",
                data : {
                    'studentId' : $('#studentId').val()
                },
                dataType: "json",
                success: function (response) {
                    if (response) {                        
                        var seq=0;
                        $.each(response, function (index, row) {                                                                  
                            $('#studentnews ul').append(
                                '<li>'  + 
                                    '<div class="card">' + 
                                        '<div class="card-block">' + 
                                            '<div class="media">' + 
                                                    '<a class="mr-3" href="#">' + 
                                                        '<img class=" rounded-circle card-list-img" src="' + $('#current-path').val() + "images/users/" + row.sendBy + ".png"  + '">' +                                                        
                                                    '</a>' + 
                                                '<div class="media-body">' + 
                                                    '<div>' +
                                                         '<h6 class="d-inline-block">' + row.newsTitleNameTh + '</h6>' +      
                                                         '<p>' + row.newsDescTh  + '</p>' + 
                                                     '</div>' +
                                                '</div>' + 
                                            '</div>' + 
                                        '</div>' +                                     
                                    '</div>' + 
                                '</li>'
                            );
                        });                    
                    }
                }
            });
        }
    </script>
@endsection