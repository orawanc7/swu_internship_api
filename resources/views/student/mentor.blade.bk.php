@extends('student.layout')

@section('content')
<input type="hidden" name="itsStudentId" id="itsStudentId" value="{{ $itsStudentId }}">
<input type="hidden" name="itsRoundId" id="itsRoundId">
<ul id="breadcrumb-triangle">
    <li><a href="{{ route('student') }}"><span class="icofont icofont-home"> </span></a></li>
    <li><a href="#"><span class="icon-screen-desktop"> บันทึกข้อมูลครู/อาจารย์พี่เลี้ยง</span> ภาคเรียนที่ <span id="semCd"></span> ปีการศึกษา <span id="year"></span></a></li>
</ul>


<div class="card b-l-primary" id="div-studentlist">
    <div class="card-header">
        สถานที่ฝึกประสบการณ์ <i class="icofont icofont-location-pin"></i> <span id="schoolName"></span>
    </div>
    <div class="card-block" id="studentlist">

    </div>
</div>
@endsection

@section('modal')
<div class="modal fade modal-flex" id="mentor-add" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body model-container">
                <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> บันทึก</button>
                <button class="btn btn-secondary" type="reset" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <hr>
                <div class="form-group row">
                    <label for="prenameIntThcd" class="col-md-2 col-form-label form-control-label mandatory">คำนำหน้าชื่อ</label>
                    <div class="col-md-4">
                        <select name="prenameIntThcd" id="prenameIntThcd" class="select2"></select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="personFnameTh" class="col-md-2 col-form-label form-control-label mandatory">ชื่อ</label>
                    <div class="col-md-4">
                        <input type="text" id="personFnameTh" name="personFnameTh" class="form-control" placeholder="ชื่อ" required maxlength="50" autofocus>
                    </div>
                    <label for="personLnameTh" class="col-md-2 col-form-label form-control-label mandatory">นามสกุล</label>
                    <div class="col-md-4">
                        <input type="text" id="personLnameTh" name="personLnameTh" class="form-control" placeholder="นามสกุล" required maxlength="50">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="mobileNo" class="col-md-2 col-form-label form-control-label">มือถือ</label>
                    <div class="col-md-4">
                        <input type="text" id="mobileNo" name="mobileNo" class="form-control" placeholder="มือถือ" maxlength="50">
                    </div>
                    <label for="otherEmail" class="col-md-2 col-form-label form-control-label">อีเมล</label>
                    <div class="col-md-4">
                        <input type="text" id="otherEmail" name="otherEmail" class="form-control" placeholder="นามสกุล" maxlength="50">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    var itsStudentId;
    var itsRoundId;
    $(document).ready(function() {
        itsStudentId = $('#itsStudentId').val();

        $('#prenameIntThcd').select2({
            width: '100%',
            dropdownParent: $('#mentor-add')
        });

        $('#mentor-add').on('shown.bs.modal', function() {
            $('#prenameIntThcd').focus();
        });

        $('#studentlist').on("click", "button.mentor-add", function(e) {
            var itsStudentId = $(this).attr('data-id');

            $('#mentor-add').modal('show');
        });

        Mentor.loadPrename();

        Mentor.getStudentInfo();
    });

    var Mentor = {
        loadPrename: function() {
            $.ajax({
                type: "GET",
                url: "{{route('api/WSPrename')}}",
                dataType: "json",
                success: function(response) {
                    if (response.data) {
                        var data = response.data;

                        $.each(data, function(idx, item) {
                            $('#prenameIntThcd').append($('<option>', {
                                value: item.prenameCd,
                                text: item.prenameLnameTh
                            }));
                        });
                    }
                }
            });
            $.ajax({
                type: "GET",
                url: "{{route('api/SchdStdInfo/get')}}/" + itsStudentId,
                data: "data",
                dataType: "dataType",
                success: function(response) {

                }
            });
        },
        getStudentInfo: function() {
            $('#div-studentlist').block();

            $.ajax({
                type: "GET",
                url: "{{route('api/SchdStdInfo/get')}}/" + itsStudentId,
                dataType: "json",
                success: function(response) {
                    $('#semCd').html(response.semCd);
                    $('#year').html(response.year);
                    $('#itsRoundId').html(response.itsRoundId);
                    $('#schoolName').html(response.schoolNameTh);
                    itsRoundId = response.itsRoundId;

                    $('#div-studentlist').unblock();

                    Mentor.loadStudentSchool();
                }
            });
        },
        loadStudentSchool: function() {
            $('#div-studentlist').block();
            $('#studentlist').empty();
            $.ajax({
                type: "GET",
                url: "{{route('api/SchdStdInfo/listStudentSchoolByItsStudentId')}}/" + itsStudentId,
                dataType: "json",
                success: function(response) {

                    $.each(response, function(idx, item) {
                        $('#studentlist').append(
                            '<div class="job-cards">' +
                            '<div class="media">' +
                            '<h6 class="mr-3 media-middle">' +
                            Helper.userIcon(item.studentId) +
                            '</h6>' +
                            '<div class="media-body">' +
                            '<p>' + item.studentId + " " + item.prenameSnameTh + item.fnameTh + " " + item.lnameTh + '</p>' +
                            '<p class="text-muted">test</p>' +
                            '</div>' +
                            '<div class="media-right">' +
                            '<div class="label-main">' +
                            '<button class="btn btn-primary mentor-add" data-id="' + item.itsStudentId + '"><i class="fa fa-plus"></i></button>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>'
                        );
                    });

                    $('#div-studentlist').unblock();
                }
            });
        }
    };
</script>
@endsection