<nav class="navbar navbar-light bg-white m-b-10 p-2" id="div-studentmenu">
    <ul class="nav navbar-nav task-board-list">
        <li class="nav-item dropdown">            
            <div class="input-group-append">
                <label class="input-group-text bg-white border-0" for="itsStudentId">ภาค/ปีการศึกษา</label>
                <select name="itsStudentId" id="itsStudentId" class="select2"></select>
            </div>
        </li>        
    </ul>

    <ul class="nav navbar-nav f-right">
        <li class="nav-item">            
            <button type="button" class="btn btn-primary waves-effect waves-light m-r-10" data-toggle="tooltip" data-placement="top" title="" data-original-title="กิจกรรม" id="btnActivity">
                <i class="icofont icofont-listine-dots"></i>
            </button>            
        </li>
    </ul>
</nav>