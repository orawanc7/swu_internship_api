<div class="md-modal md-effect-8" id="register-qr">
    <div class="md-content">
        <h3>ลงทะเบียน</h3>        
        <div class="text-center">            
            <form method="post" id="news-form" name="register-form">                
                <div id="register-name"></div>
                <div class="row text-center">
                    <div class="col-md-12">
                        <img src="@relative('images/qr.png')" class="img-fluid">
                    </div>
                </div>                

                <div class="row text-center">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-secondary waves-effect md-close" onclick="ActivityList.registerClose();"><i class="icofont icofont-close"></i>  ปิด</button>
                    </div>                    
                </div>     
            </form>      
        </div>
    </div>
</div>
    