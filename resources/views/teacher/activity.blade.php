@extends('teacher.layout')

@section('content')
<ul id="breadcrumb-triangle">
    <li><a href="{{route('teacher')}}"><span class="icofont icofont-home"> </span></a></li>                    
    <li><a href="#"><span class="icon-screen-desktop"></span> <span id="divDocName"></span></a></li>
</ul>

<input type="hidden" name="activityId" id="activityId" value="{{ $activityId }}">

<div class="row">
    <div class="col-xl-4 col-lg-5 grid-item">
        <div class="row">      
            <div class="col-xl-12 col-lg-12 grid-item">
                @include('teacher.activitystudent')
            </div>                        
        </div>
        <div class="row">      
            <div class="col-xl-12 col-lg-12 grid-item">
                @include('teacher.activitytracking')
            </div>                        
        </div>
    </div>    
    <div class="col-xl-8 col-lg-7 grid-item">                   
        <div class="row">               
            <div class="col-xl-12 col-lg-12 grid-item">           
                <div class="d-none" id="divDetail">                        
                    @include("teacher.activitystudentinfo")                    
                    @include("teacher.activitystatus")
                    @set($scriptName="")
                    @if ($activity['docTypeId']=="1") 
                        @include('activity.check.course')  
                        @set($scriptName="Course")
                    @elseif ($activity['docTypeId']=="2") 
                        @include('activity.check.teach')
                        @set($scriptName="Teach")  
                    @elseif ($activity['docTypeId']=="4") 
                        @include('activity.check.courseplan')
                        @set($scriptName="CoursePlan")  
                    @elseif ($activity['docTypeId']=="6")
                        @include('activity.check.research')  
                        @set($scriptName="Research")
                    @elseif ($activity['docTypeId']=="7")
                        @include('activity.check.research2')  
                        @set($scriptName="Research")
                    @elseif (($activity['docTypeId']=="10") || ($activity['docTypeId']=="15"))
                        @include('activity.check.portfolio')  
                        @set($scriptName="Portfolio")                    
                    @else
                        @include("activity.form")  
                        @include("activity.check.form")  
                        @set($scriptName="Form")
                    @endif

                    @include("activity.check.checkstatus")
                </div>    
            </div>               
        </div>        
    </div>   
</div>
@endsection    

@section('script')
<script src="@relative('js/app/ActivityComment.js')"></script>
@if ($scriptName=="Form") 
    <script src="{{ route('js/app/FormGenerate.js' )}}?{{ time() }}"></script>
@endif
<script src="{{ route('js/app/DocTypeCheck' )}}{{ $scriptName }}.js?{{ time() }}"></script>
<script src="{{ route('js/app/DocTypeActivityCheck.js' )}}?{{ time() }}"></script>
<script>
    var activityId = "{{ $activityId }}";
    $(document).ready(function () {                
        ActivityComment.setupComment();

        Activity.loadStudent();
    
        @if ($itsStatusSendId != null)
            Activity.loadData( {{ $itsStudentId }}, {{ $itsStatusSendId }} );
        @endif 

        $('#tbStudent tbody').on("click", "tr.data-its-send-status-row", function (e) {
            e.preventDefault();
            var itsSendStatusId = $(this).attr('data-its-send-status-id');
            var itsStudentId = $(this).attr('data-its-student-id');
            
            Activity.loadData( itsStudentId, itsSendStatusId);
        });

        
        $('#div-doctype-upload-list').on('click','a.download',function(){
            var id = $(this).attr('data-id');
            location.href=  "{{ route('api/SchdUpload/download') }}" + '/' + id;
        });
        
    });

    var Activity = {
        loadStudent: function() {            
            $('#div-activity-student').block();

            $('#tbStudent tbody').empty();
            $.ajax({
                url: "{{route('api/SchdDocStatus/listAllStudentByActivityTeacher')}}/{{ $activityId }}",
                type: "get",                
                dataType: "json",
                success: function (response) {          
                    var total = 0;
                    var completeTotal = 0;
                    var docName = "";
                    var semYear = "";
                    var studentId = "";

                    var arrStudent ={};
                    var count = 0;
                    $.each(response, function (idx, item) {                            
                        if ((studentId!=item.studentId) && (studentId.length>0)) {
                            arrStudent[studentId] = count;
                            
                            count=0;
                        }
                        studentId = item.studentId;
                        count++;
                    });
                    arrStudent[studentId] = count;
                    
                    studentId = "";
                    $.each(response, function (idx, item) {                            
                        var sendDate = "";

                                                
                        if (item.sendDate!=null) {
                            sendDate = Helper.toScreenDate(item.sendDate.date,true);
                        }                        
                        semYear = item.semCd + "/" + item.year;
                        docName = item.docNameTh;

                        var activityStatus = "";       

                        if (item.itsStatusSendId!=null) {
                            if (item.confirmFlag=="Y") {
                                activityStatus = "<label class=\"label label-success\">อนุมัติ</label>";
                                completeTotal++;
                            } else if (item.confirmFlag=="N") {
                                activityStatus = "<label class=\"label label-danger\">ส่งคืน</label>";
                            } else {
                                activityStatus = "<label class=\"label label-default\">รอตรวจ</label>";
                            }                 
                        } else {
                            activityStatus = "<label class=\"label label-default\">ยังไม่ได้ส่ง</label>";
                        }
                                     
                        
                        if (studentId!=item.studentId) {
                            var rowspan  = arrStudent[item.studentId]+1;                            
                            $('#tbStudent tbody').append(                            
                                "<tr>" +                                                            
                                        "<td rowspan='" + rowspan + "'>" + Helper.userIcon(item.studentId)  + "</td>" +
                                        "<td colspan=\"2\">" + item.studentId + "<br>" + item.prenameSnameTh +  item.fnameTh + " " + item.lnameTh + "</td>" +
                                "</tr>"
                            );   
                        }                        

                        if (item.itsStatusSendId!=null) {
                            $('#tbStudent tbody').append(                            
                                "<tr class=\"data-its-send-status-row\" data-its-send-status-id='" + item.itsStatusSendId +  "' data-its-student-id='" +  item.itsStudentId +"'>" +                                        
                                        "<td style=\"text-align:left;vertical-align:top;\">"+ activityStatus + ((item.itsCourseCd!=null)?item.itsCourseCd:"")  + "</td>" +
                                        "<td style=\"text-align:right;vertical-align:top;\"  class=\"small\">" + sendDate + "</td>" +                                        
                                "</tr>"
                            );
                        } else {
                            $('#tbStudent tbody').append(                            
                                "<tr>" +                                        
                                        "<td style=\"vertical-align:middle;\" colspan=\"2\">" + activityStatus + "</td>" +                                                                            
                                "</tr>"
                            );         
                        }
                                               
                        total++;                

                        studentId = item.studentId;
                    });
                    //Update Total
                    $('#divTotal').html(total);
                    $('#divCompleteTotal').html(completeTotal);
                    $('#divSemYear').html(semYear);
                    $('#divDocName').html(docName);

                    $('#div-activity-student').unblock();
                }
            });
        },
        loadData: function (itsStudentId, itsStatusSendId) {
            if (itsStatusSendId!=null) {
                $('input[name=itsStatusSendId]').val(itsStatusSendId);   
                                

                Activity.loadTracking(itsStatusSendId);
                Activity.loadDetail(itsStudentId,itsStatusSendId);

                ActivityComment.clear();
                ActivityComment.setStatusSendId(itsStatusSendId);
                ActivityComment.loadComment();
                

                //load auto
                setInterval(function(){
                    ActivityComment.loadComment();
                }, 5000);
            }
        },
        loadTracking: function (itsStatusSendId) {
            $('#divTracking').removeClass('d-none');
        },
        loadDetail: function (itsStudentId,itsStatusSendId) {
            $('#divDetail').removeClass('d-none');
            
            $('#divDetail').block();
            Activity.getStudentInfo(itsStudentId,function(data) {
                
                if (data) {                    
                    Activity.getCurrentStatus(itsStatusSendId,function(data) {                    
                        if (data) {
                            $('#divDetail').unblock();

                            var stdCourseId = "";
                            var refDocId = "";
                            if (data.stdCourseId) stdCourseId = data.stdCourseId.toString();
                            if (data.refDocId) refDocId = data.refDocId.toString();

                            DocTypeActivity.getCurrent(itsStatusSendId, $('#its-id').val(),activityId,itsStudentId,data.stdCourseId,data.refDocId);                            
                            
                            @if (($activity['docTypeId']=="1") || ($activity['docTypeId']=="2"))                                  
                                DocType.init(data.stdCourseId,function(){                                                                          
                                    Activity.getUploadList(stdCourseId,refDocId,itsStudentId);
                                });
                            @elseif ($activity['docTypeId']=="4")
                                DocType.init(data.stdCourseId,data.refDocId,function(){
                                    Activity.getUploadList(stdCourseId,refDocId,itsStudentId);
                                });
                            @elseif (($activity['docTypeId']=="6") || ($activity['docTypeId']=="7"))
                                DocType.init(data.refDocId,function(){
                                    Activity.getUploadList('',refDocId,itsStudentId);
                                });
                            @elseif (($activity['docTypeId']=="10") || ($activity['docTypeId']=="15"))
                                DocType.init(data.refDocId,function(){
                                    Activity.getUploadList('',refDocId,itsStudentId);
                                });
                            @else                                 
                                DocType.init({{ $activity['docId'] }},itsStudentId,data.refDocId,function(){
                                    Activity.getUploadList('',refDocId,itsStudentId);
                                });
                            @endif

                            $('#receiverRemark').focus();
                        }                        
                    });
                }
            });                        
        },
        getStudentInfo: function (itsStudentId,callback) {
            $.ajax({
                type: "get",
                url: "{{route('api/SchdStdInfo/get')}}" + "/" + itsStudentId,                
                dataType: "json",
                success: function (response) {
                    if (response) {                                         
                        $('div[name=studentImage]').html(Helper.userIcon(response.studentId));
                        $('span[name=studentId]').html(response.studentId);
                        $('span[name=studentName]').html(response.prenameSnameTh + response.fnameTh + " " + response.lnameTh);
                        $('span[name=dept]').html(response.deptLnameTh);
                        $('span[name=major]').html(response.majorLnameTh);          
                        $('a[name=documentAll]').attr('href',"{{ route ('teacher/student/') }}" + itsStudentId);                        
                    }                    
                    callback(response.itsStudentId);
                }
            });
        },
        getCurrentStatus: function (itsStatusSendId,callback) {
            
            $.ajax({
                type: "get",
                url: "{{route('api/SchdDocStatus/get')}}" + "/" + itsStatusSendId,                
                dataType: "json",
                success: function (response) {
                    
                    $('span[name=statusSendDate]').html(Helper.toScreenDate(response.sendDate.date,true));                    
                    callback(response);
                }
            });
        },

        getUploadList: function (stdCourseId,refDocId,senderId) {

            $('#div-doctype-upload-list').empty();

            var docId = "{{ $activity['docId'] }}";                    
            var v_url = "";            
           
            if ((stdCourseId.length>0) && (refDocId.length>0)) {                
                v_url = "{{ route('api/SchdUpload/listByDocCourseRefDocSender') }}" + '/' + docId + '/' + stdCourseId + '/' + refDocId + '/' + senderId;
            } else if (stdCourseId.length>0) {                
                v_url = "{{ route('api/SchdUpload/listByDocCourseSender') }}" + '/' + docId + '/' + stdCourseId + '/' + senderId;
            } else if (refDocId.length>0) {                
                v_url = "{{ route('api/SchdUpload/listByDocRefDocSender') }}" + '/' + docId + '/' + refDocId + '/' + senderId;
            }
            
            
            $.ajax({
                type: "get",
                url: v_url,
                dataType: "json",
                success: function (response) {
                    $.each(response, function (idx, item) {
                        
                        var fileType = "";
                        if (item.uploadType=="F") {
                            if (item.fileExtension=="pdf") {                        
                                fileType = '<i class="h3 icofont icofont-file-pdf text-danger"></i>';
                            } else if ((item.fileExtension=="docx") || (item.fileExtension=="doc")) {
                                fileType = '<i class="h3 icofont icofont-file-word text-primary"></i>';
                            } else if ((item.fileExtension=="xls") || (item.fileExtension=="xlsx")) {
                                fileType = '<i class="h3 icofont icofont-file-excel text-success"></i>';
                            } else if ((item.fileExtension=="ppt") || (item.fileExtension=="pptx")) {
                                fileType = '<i class="h3 icofont icofont-file-powerpoint text-danger"></i>';
                            } else if (item.fileExtension=="png") {
                                fileType = '<i class="h3 icofont icofont-file-png text-primary"></i>';
                            } else if ((item.fileExtension=="jpg") || (item.fileExtension=="jpeg")) {
                                fileType = '<i class="h3 icofont icofont-file-jpg text-primary"></i>';
                            } else {
                                fileType = '<i class="h3 icofont icofont-file-document text-black"></i>';
                            }     
                            $('#div-doctype-upload-list').append(
                                '<div class="row">' +
                                    '<div class="col-sm-12 border-bottom">' +
                                        '<label class="f-left"><a class="download" href="#" data-id="' + item.fileHdrId + '">' + fileType + ' ' + item.fileName + '</a> <b>Version :</b> ' + item.fileVersion + '</label>' +                                        
                                    '</div>' +                            
                                '</div>'
                            );                     
                        } else {
                            fileType = '<i class="h3 icofont icofont-external-link"></i>';
                            $('#div-doctype-upload-list').append(
                                '<div class="row">' +
                                    '<div class="col-sm-12 border-bottom">' +
                                        '<label class="f-left"><a href="' + item.linkUrl + '" target="_blank">' + fileType + ' ' + item.linkUrl + '</a></label>' +                                        
                                    '</div>' +                            
                                '</div>'
                            );  
                        }                     
                    });
                }
            });
        }
    };
</script>    
@endsection