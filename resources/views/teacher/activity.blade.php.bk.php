@extends('teacher.layout')

@section('style')
    <link rel="stylesheet" href="@relative('plugins/timeline/css/style.css')">
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12 p-0">           
        <div class="row">
            <div class="card-block breadcrumb-icon-block">
                <ul id="breadcrumb-triangle">
                    <li><a href="{{route('teacher')}}"><span class="icofont icofont-home"> </span></a></li>
                    <li><a href="#"><span class="icofont icofont-calendar"> </span> การส่งงาน/ตรวจรับงาน</a></li>                            
                </ul>
            </div>                     
        </div>        
    </div>
</div>
<div class="row">
    <div class="col-sm-12 p-0">           
        <div class="card">
            <div class="card-header text-center">
                <h1 class="card-header-text">ภาค 1 ปีการศึกษา 2561</h1>
            </div>
            <div class="card-block p-t-0">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="main-timeline timeline-line crm-timeline p-0">
                            <div class="cd-timeline cd-container m-b-0 p-b-0">
                                <!-- 2 -->
                                <div class="cd-timeline-block">
                                        <div class="cd-timeline-icon bg-warning">
                                            <i class="icofont icofont-ui-file"></i>
                                        </div> <!-- cd-timeline-img -->
    
                                        <div class="cd-timeline-content card_main">
                                            <div class="card-block">
                                                <h6>บันทึกแบบสังเกตสอน</h6>
                                                <div class="timeline-details">
                                                    <i class="icofont icofont-ui-close text-warning"></i><span class="text-warning">ส่งแล้ว : 1/2</span>
                                                    <p class="m-t-20 text-right"><a href="{{ route('teacher/activity/detail') }}" class="btn btn-warning" >รายละเอียด</a></p>
                                                </div>
                                            </div>
                                            <span class="cd-date">15-26 พฤษภาคม 2561</span>    
                                        </div> <!-- cd-timeline-content -->
                                    </div> <!-- cd-timeline-block -->

                                <!-- 1 -->
                                <div class="cd-timeline-block m-0">
                                    <div class="cd-timeline-icon bg-success">
                                        <i class="icofont icofont-ui-check"></i>
                                    </div> <!-- cd-timeline-img -->

                                    <div class="cd-timeline-content card_main">
                                        <div class="card-block">
                                            <h6>โครงสร้างรายวิชา</h6>
                                            <div class="timeline-details">
                                                <i class="icofont icofont-ui-check text-success"></i><span class="text-success">ส่งแล้ว : 2/2</span>
                                            <p class="m-t-20 text-right"><a href="{{ route('teacher/activity/detail') }}" class="btn btn-success" >รายละเอียด</a></p>
                                            </div>
                                        </div>
                                        <span class="cd-date">15-26 พฤษภาคม 2561</span>                                
                                    </div> <!-- cd-timeline-content -->
                                </div> <!-- cd-timeline-block -->
                                                                
                            </div> <!-- cd-timeline -->
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>        
@endsection    