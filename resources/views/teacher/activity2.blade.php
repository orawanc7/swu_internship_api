@extends('teacher.layout')
@section('beforestyle')
    <link rel="stylesheet" type="text/css" href="@relative('css/dropzone.min.css')">    
@endsection
@section('content')
<ul id="breadcrumb-triangle">
    <li><a href="{{route('teacher')}}"><span class="icofont icofont-home"> </span></a></li>                    
    <li><a href="#"><span class="icon-screen-desktop"></span> <span id="divDocName"></span></a></li>
</ul>

<input type="hidden" name="activityId" id="activityId" value="{{ $activityId }}">

<div class="row">
    <div class="col-xl-4 col-lg-5 grid-item">
        <div class="row">      
            <div class="col-xl-12 col-lg-12 grid-item">
                @include('teacher.activitystudent2')
            </div>                        
        </div>        
    </div>    
    <div class="col-xl-8 col-lg-7 grid-item">                   
        <div class="row">               
            <div class="col-xl-12 col-lg-12 grid-item">           
                <div class="d-none" id="divDetail">                        
                    @include("teacher.activitystudentinfo")
                    
                    @include("activity.form")  
                    @include("activity.make.form2")  
                    @if ($activity['docId']=="59") 
                        @set($scriptName="FormNew")
                    @else
                    @set($scriptName="Form2")
                    @endif                   
                </div>    
            </div>               
        </div>        
    </div>   
</div>
<input type="hidden" id="doctype-activity-get-docstatus-url" value="{{ route('api/SchdDocStatus/getByItsStudentIdDocId') }}">  


<form action="{{route('api/SchdDocSend/saveCheck')}}" method="POST" id="frmActivityStatus">    
    <input type="hidden" name="itsStatusSendId">
    <input type="hidden" name="itsDocId">    
    <input type="hidden" name="receiverRemark">    
    <input type="hidden" name="confirmFlag" value="Y">    
</form>    

<div class="md-overlay"></div>
@endsection    

@section('modal')
    @include('activity.make.upload') 
    
    <div class="md-modal md-effect-8" id="confirmMessage">
        <div class="md-content">
            <h3>ยืนยันข้อมูล</h3>
            <div>                            
                    <div class="md-input-wrapper">
                        เมื่อกดปุ่ม "บันทึก" แล้ว จะไม่สามารถเปลี่ยนแปลงรายการได้อีก<br>
                        กลับไปแก้ไขรายการกดปุ่ม "ปิด"
                    </div>                                                                  
                    <div class="row">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-secondary waves-effect md-close"><i class="icofont icofont-close"></i>  ปิด</button>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary waves-effect md-close" id="btnConfirmSend"><i class="icofont icofont-save"></i>  บันทึก</button>
                        </div>
                    </div>                  
                </form>      
            </div>
        </div>
    </div>
@endsection

@section('beforescript')
<script src="@relative('js/dropzone.min.js')"></script>
@endsection

@section('script')
<script src="@relative('js/app/ActivityComment.js')"></script>
@if ($scriptName=="Form2") 
    <script src="{{ route('js/app/FormGenerate.js' )}}?{{ time() }}"></script>
@endif
<script src="{{ route('js/app/DocType' )}}{{ $scriptName }}.js?{{ time() }}"></script>
<script src="{{ route('js/app/DocTypeActivity2.js' )}}?{{ time() }}"></script>
<script>
    Dropzone.autoDiscover = false;
    var personType = "{{ getenv('PERSON_TYPE_TEACHER') }}";
    var itsUserId = $('#its-id').val();
    var itsRoundId = "{{$activity['itsRoundId']}}";
    var itsStudentId = "{{ $itsStudentId }}";
    var docTypeId = "{{$activity['docTypeId']}}";
    var activityId = "{{$activity['activityId']}}";
    var docId = "{{$activity['docId']}}";
    var docTypeId = "{{$activity['docTypeId']}}";    
    
    $(document).ready(function () {        
        ActivityComment.setupComment();

        Activity.loadStudent();

        DocTypeActivity.setSender(
            personType,
            itsUserId
        );

        DocTypeForm.setCallback (function () {
            Activity.loadStudent();
        });            
    
        @if ($itsStatusSendId != null)
            Activity.loadData( {{ $itsStudentId }}, {{ $itsStatusSendId }} );
        @endif 

        $('#tbStudent tbody').on("click", "tr.data-its-send-status-row", function (e) {
            var itsSendStatusId = $(this).attr('data-its-send-status-id');
            var itsStudentId = $(this).attr('data-its-student-id');
            var refDocId = $(this).attr('data-ref-doc-id');                   
            
            Activity.loadData( itsStudentId, itsSendStatusId);
        });

        $('#tbStudent tbody').on("click", "tr.data-its-student-row", function (e) {
            var itsStudentId = $(this).attr('data-its-student-id');
                                        
            Activity.loadData( itsStudentId, 0);
        });

        $('#tbStudent tbody').on("mouseover","tr.data-its-send-status-row", function (e) {            
            $(this).css('cursor','pointer');
        });

        $('#tbStudent tbody').on("mouseover","tr.data-its-student-row", function (e) {            
            $(this).css('cursor','pointer');
        });
        

        $('#div-doctype-upload-list').on('click','a.download',function(){
            var id = $(this).attr('data-id');
            location.href=  "{{ route('api/SchdUpload/download') }}" + '/' + id;
        });


        $('#btnFileUpload').click(function(){        
        $('#upload').modal('show');
    });

    $('input[name=uploadType]').click(function(){
        
        //if ($(this).is(":checked")) {
            var val = $(this).val();     
            
            if (val=="L") {
                $('#link_url').prop('disabled',false);
                $('#link_url').focus();                

                Dropzone.forElement("#upload-form").removeAllFiles(true);                                
        
                $('#btnSaveLink').removeClass('d-none');
                $('#upload-form').hide();
            }  else {
                $('#link_url').val('');
                $('#link_url').prop('disabled',true);                

                $('#btnSaveLink').addClass('d-none');
                $('#upload-form').show();
            }                      
        //}
    });

    $('#btnSaveLink').click(function(){
        $('#upload-link-form').submit();
    });

    $('#upload').on('hidden.bs.modal', function () {
        $('input[name=uploadType][value=F]').trigger('click');
        Dropzone.forElement("#upload-form").removeAllFiles(true);
    });

    $('#div-doctype-upload-list').on('click','a.delete',function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        $.ajax({
            type: "DELETE",
            url: "{{ route('api/SchdUpload/delete') }}" + '/' + id,            
            dataType: "json",
            success: function (response) {
                Activity.getUploadList();
            }
        });
    });

    $('#div-doctype-upload-list').on('click','a.download',function(){
        var id = $(this).attr('data-id');
        location.href=  "{{ route('api/SchdUpload/download') }}" + '/' + id;
    });

    $('#upload-link-form').validate({
        rules:{
            'link_url': {
                required:true,
                url:true
            }
        }
    });

    $('#upload-link-form').submit(function (e) { 
        e.preventDefault();
        
        if ($(this).valid()) {
            $.ajax({
                type: "post",
                url: "{{ route('api/SchdUpload/saveLink') }}",
                data: $(this).serialize(),
                dataType: "json",
                success: function (response) {
                    $('#upload').modal('hide');

                    Activity.getUploadList();
                }
            });
        }
    });    

    $('#upload-form').dropzone({
        acceptedFiles: ".pdf,.doc,.docx,.xls,.xlsx,.ppt,.pptx,image/png,image/jpg,image/jpeg",
        maxFiles : 1,
        maxFilesize: 20,
        addRemoveLinks: true,
        autoProcessQueue: false,
        uploadMultiple: true,
        paramName: 'file',
        init: function() {
            this.on("addedfile", function (file) {
                $('#btnUpload').removeClass('d-none');
            });

            this.on("removedfile", function (file) {
                if (this.files.length==0) {
                    $('#btnUpload').addClass('d-none');
                }
            });

            this.on("maxfilesexceeded", function() {
                if (this.files[1]!=null){                    
                    this.removeFile(this.files[1]);
                }
            });                  
            
            this.on("success", function(file, responseText) {
                this.removeAllFiles();
                $('#upload').modal('hide');

                Activity.getUploadList();
            });

            var dropZone = this;
            $('#btnUpload').on('click',function() {                
                dropZone.processQueue();                    
            });             
        },               
        error: function (file, message, xhr) {
            if (xhr == null) this.removeFile(file); // perhaps not remove on xhr errors
            alert(message);  
        },
    });         
        
    });

    var Activity = {
        loadStudent: function() {               
            $('#div-activity-student').block();

            $('#tbStudent tbody').empty();
            $.ajax({
                url: "{{route('api/SchdDocStatus/listAllStudentByActivityTeacher')}}/{{ $activityId }}",
                type: "get",                
                dataType: "json",
                success: function (response) {                          
                    var total = 0;
                    var completeTotal = 0;
                    var docName = "";
                    var semYear = "";
                    var studentId = "";

                    var arrStudent ={};
                    var count = 0;
                    $.each(response, function (idx, item) {                            
                        if ((studentId!=item.studentId) && (studentId.length>0)) {
                            arrStudent[studentId] = count;
                            
                            count=0;
                        }
                        studentId = item.studentId;
                        count++;
                    });
                    arrStudent[studentId] = count;
                    
                    studentId = "";
                    $.each(response, function (idx, item) {                            
                        var sendDate = "";

                                                
                        if (item.sendDate!=null) {
                            sendDate = Helper.toScreenDate(item.sendDate.date,true);
                        }                        
                        semYear = item.semCd + "/" + item.year;
                        docName = item.docNameTh;

                        var activityStatus = "";       

                        if (item.itsStatusSendId!=null) {
                            if (item.confirmFlag=="Y") {
                                activityStatus = "<label class=\"label label-success\">ยืนยันแล้ว</label>";
                                completeTotal++;
                            } else if (item.confirmFlag=="N") {
                                activityStatus = "<label class=\"label label-danger\">ส่งคืน</label>";
                            } else {
                                activityStatus = "<label class=\"label label-default\">รอยืนยัน</label>";
                            }                 
                        } else {
                            activityStatus = "<label class=\"label label-default\">รอบันทึกข้อมูล</label>";
                        }
                                     
                        
                        if (studentId!=item.studentId) {
                            var rowspan  = arrStudent[item.studentId]+1;                            
                            $('#tbStudent tbody').append(                            
                                "<tr>" +                                                            
                                        "<td rowspan='" + rowspan + "'>" + Helper.userIcon(item.studentId)  + "</td>" +
                                        "<td colspan=\"2\">" + item.studentId + "<br>" + item.prenameSnameTh +  item.fnameTh + " " + item.lnameTh + "</td>" +
                                "</tr>"
                            );   
                        }                        

                        if (item.itsStatusSendId!=null) {
                            $('#tbStudent tbody').append(                            
                                "<tr class=\"data-its-send-status-row\" data-its-send-status-id='" + item.itsStatusSendId +  "' data-its-student-id='" +  item.itsStudentId +"' data-ref-doc-id='" +  item.refDocId +"'>" +                                        
                                        "<td style=\"text-align:left;vertical-align:top;\">"+ activityStatus + ((item.itsCourseCd!=null)?item.itsCourseCd:"")  + "</td>" +
                                        "<td style=\"text-align:right;vertical-align:top;\"  class=\"small\">" + sendDate + "</td>" +                                        
                                "</tr>"
                            );
                        } else {
                            //เลือกนิสิต <i class=\"fa fa-hand-point-left\"></i>
                            $('#tbStudent tbody').append(                            
                                "<tr class=\"data-its-student-row\" data-its-student-id='" +  item.itsStudentId +"'>" +                                        
                                        "<td style=\"vertical-align:middle;\" colspan=\"2\">" + activityStatus + "</td>" +
                                "</tr>"
                            );         
                        }
                                               
                        total++;                

                        studentId = item.studentId;
                    });
                    //Update Total
                    $('#divTotal').html(total);
                    $('#divCompleteTotal').html(completeTotal);
                    $('#divSemYear').html(semYear);
                    $('#divDocName').html(docName);

                    $('#div-activity-student').unblock();
                }
            });
        },
        loadData: function (itsStudentId, itsStatusSendId) {            
            $('input[name=itsStatusSendId]').val(itsStatusSendId);   
                            
            
            Activity.loadDetail(itsStudentId,itsStatusSendId);

            ActivityComment.clear();
            ActivityComment.setStatusSendId(itsStatusSendId);
            ActivityComment.loadComment();
            

            //load auto
            /*
            setInterval(function(){
                ActivityComment.loadComment();
            }, 5000);            
            */
        },
        loadDetail: function (itsStudentId,itsStatusSendId) {
            $('#divDetail').removeClass('d-none');
                        
            $('#divDetail').block();
            Activity.getStudentInfo(itsStudentId,function(data) {
                
                if (data) {                    
                    Activity.getCurrentStatus(itsStatusSendId,function(data) {              
                        if (data) {                                          
                            DocTypeForm.getCurrent(itsStudentId,data.refDocId);
                        } else {
                            DocTypeForm.getCurrent(itsStudentId,null);
                        }
                        $('#divDetail').unblock();
                    });
                }
            });                        
        },
        getStudentInfo: function (itsStudentId,callback) {
            $.ajax({
                type: "get",
                url: "{{route('api/SchdStdInfo/get')}}" + "/" + itsStudentId,                
                dataType: "json",
                success: function (response) {
                    if (response) {                                         
                        $('div[name=studentImage]').html(Helper.userIcon(response.studentId));
                        $('span[name=studentId]').html(response.studentId);
                        $('span[name=studentName]').html(response.prenameSnameTh + response.fnameTh + " " + response.lnameTh);
                        $('span[name=dept]').html(response.deptLnameTh);
                        $('span[name=major]').html(response.majorLnameTh);          
                        $('[name=documentAll]').attr('href',"{{ route ('teacher/student/') }}" + itsStudentId);
                    }                    
                    callback(response.itsStudentId);
                }
            });
        },
        getCurrentStatus: function (itsStatusSendId,callback) {
            if (itsStatusSendId>0) {
                $.ajax({
                    type: "get",
                    url: "{{route('api/SchdDocStatus/get')}}" + "/" + itsStatusSendId,                
                    dataType: "json",
                    success: function (response) {                   
                        if (response)   {
                            $('span[name=statusSendDate]').html(Helper.toScreenDate(response.sendDate.date,true));                    
                        }
                        callback(response);
                    }
                });
            } else {
                callback(null);
            }
        },

        setUploadStdCourseId : function(stdCourseId) {
            $('input[name=stdCourseId]').val(stdCourseId);
        },
        setUploadRefDocId : function(refDocId) {
            $('input[name=refDocId]').val(refDocId);            
        },
        getUploadList: function () {            
            $('#div-doctype-upload-list').empty();        

            var docId = $('input[name=docId]').val();        
            var senderId = $('input[name=senderId]').val();
            var refDocId = $('input[name=refDocId]').val();
            
            var v_url = "{{ route('api/SchdUpload/listByDocRefDocSender') }}" + '/' + docId + '/' + refDocId + '/' + senderId;            
                        
            $.ajax({
                type: "get",
                url: v_url,
                dataType: "json",
                success: function (response) {
                    $.each(response, function (idx, item) {
                        
                        var fileType = "";
                        if (item.uploadType=="F") {
                            if (item.fileExtension=="pdf") {                        
                                fileType = '<i class="h3 icofont icofont-file-pdf text-danger"></i>';
                            } else if ((item.fileExtension=="docx") || (item.fileExtension=="doc")) {
                                fileType = '<i class="h3 icofont icofont-file-word text-primary"></i>';
                            } else if ((item.fileExtension=="xls") || (item.fileExtension=="xlsx")) {
                                fileType = '<i class="h3 icofont icofont-file-excel text-success"></i>';
                            } else if ((item.fileExtension=="ppt") || (item.fileExtension=="pptx")) {
                                fileType = '<i class="h3 icofont icofont-file-powerpoint text-danger"></i>';
                            } else if (item.fileExtension=="png") {
                                fileType = '<i class="h3 icofont icofont-file-png text-primary"></i>';
                            } else if ((item.fileExtension=="jpg") || (item.fileExtension=="jpeg")) {
                                fileType = '<i class="h3 icofont icofont-file-jpg text-primary"></i>';
                            } else {
                                fileType = '<i class="h3 icofont icofont-file-document text-black"></i>';
                            }     
                            $('#div-doctype-upload-list').append(
                                '<div class="row">' +
                                    '<div class="col-sm-12 border-bottom">' +
                                        '<label class="f-left"><a class="download" href="#" data-id="' + item.fileHdrId + '">' + fileType + ' ' + item.fileName + '</a> <b>Version :</b> ' + item.fileVersion + '</label>' +                                        
                                        '<label class="f-right"><a class="delete" href="#" data-id="' + item.fileHdrId + '">' +
                                            '<i class="h3 icofont icofont-ui-delete"></i>' + 
                                        '</a></label>' +  
                                    '</div>' +                            
                                '</div>'
                            );                     
                        } else {
                            fileType = '<i class="h3 icofont icofont-external-link"></i>';
                            $('#div-doctype-upload-list').append(
                                '<div class="row">' +
                                    '<div class="col-sm-12 border-bottom">' +
                                        '<label class="f-left"><a href="' + item.linkUrl + '" target="_blank">' + fileType + ' ' + item.linkUrl + '</a></label>' +                                        
                                        '<label class="f-right"><a class="delete" href="#" data-id="' + item.fileHdrId + '">' +
                                            '<i class="h3 icofont icofont-ui-delete"></i>' + 
                                        '</a></label>' +
                                    '</div>' +                            
                                '</div>'
                            );  
                        }                     
                    });
                }
            });
        }
    };
</script>    
@endsection