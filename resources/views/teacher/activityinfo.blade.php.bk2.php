<!-- Job card start -->
<div class="card d-none" id="divDetail">
    <div class="card-header">
        <div class="media">
            <a class="media-left media-middle" href="#">
                <div name="divStudentImage"></div>
            </a>
            <div class="media-body media-middle">
                <div class="company-name">
                    <p><div name="divStudentName"></div></p>
                    <span class="text-muted f-14"><div name="divSendDate"></div></span>
                </div>
                <div class="job-badge">
                    
                </div>
            </div>
        </div>
    </div>
    <div class="card-block">      
        
    </div>
    <div class="card-footer">
        <textarea class="f-13 post-input msg-send" rows="3" cols="10"
            required=""
            placeholder="..."></textarea>
        <span class="f-right">
            <button type="button"
                    class="btn btn-primary waves-effect waves-light"><i class="icofont icofont-ui-check"></i> รับงาน
            </button>
            <button type="button"
                    class="btn btn-danger waves-effect waves-light"><i class="icofont icofont-close"></i> ส่งคืน
            </button>
        </span>
    </div>
</div>
<!-- Job card end -->