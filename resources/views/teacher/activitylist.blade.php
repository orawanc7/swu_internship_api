@extends('teacher.layout')
@section('style')
    <link rel="stylesheet" href="@relative('plugins/timeline/css/style.css')">
@endsection
@section('content')
<input type="hidden" name="itsRoundId" id="itsRoundId" value="{{ $itsRoundId }}">

<ul id="breadcrumb-triangle">
    <li><a href="{{route('teacher')}}"><span class="icofont icofont-home"> </span></a></li>
    <li><a href="#"><span class="icofont icofont-calendar"> </span>กิจกรรม</a></li>
</ul>

<div class="row">
    <div class="col-sm-12 p-0">           
        <div class="card">
            <div class="card-header text-center">                                
                <h1 class="card-header-text">ภาคเรียนที่ <span id="semCd">{{ $round['semCd'] }}</span> ปีการศึกษา <span id="year">{{ $round['year'] }}</span></h1>
            </div>
            <div class="card-block" id="div-activity">
                <div class="main-timeline d-none" id="main-timeline">
                    <div class="cd-timeline cd-container" id="activity">
                       
                    </div> <!-- end cd-timeline cd-containter-->
                </div> <!-- end main-timeline -->
            </div> <!-- end card-block -->
        </div>
    </div>
</div>       
@endsection

@section('modal')
    @include('teacher.register')   
    <div class="md-overlay"></div>
@endsection

@section('script')
<script>
var itsPersonId;
var itsRoundId;
$(document).ready(function () {        
    itsPersonId = $('#its-id').val();
    itsRoundId = $('#itsRoundId').val();
    ActivityList.loadActivity();
});

var ActivityList = {          
    activity: function(activityId,itsStatusSendId) {
        location.href = "{{ route('student/activity') }}" + "/" + activityId + "/" + itsStudentId + "/" + itsStatusSendId;
    },
    loadActivity: function() {
        
        $('#div-activity').block();    
        $.ajax({
            type: "GET",
            url: "{{route('api/SchdActivity/listActivityByTeacher')}}/" + itsRoundId + "/" + itsPersonId,            
            dataType: "json",
            success: function (response) {       
                $('#main-timeline').removeClass('d-none');
                $.each(response, function (idx, item) {   
                    var startDate = moment(item.startDate.date).format("DD/MM/YYYY");
                    var endDate = moment(item.endDate.date).format("DD/MM/YYYY");
                    var activityDate = "";
                    var doDate = "";
                    var status = "";
                    if (startDate==endDate) {
                        activityDate = startDate;
                    } else {
                        activityDate = startDate + '-' + endDate;
                    }

                    var linkDetail = "";                   

                    if (item.qrCodeFlag) {
                        if (item.seminarId!=null) {
                            var seminarStartDate = moment(item.seminarStartDate.date).format("DD/MM/YYYY HH:mm");
                            var seminarendDate = moment(item.seminarEndDate.date).format("DD/MM/YYYY HH:mm");
                            doDate = seminarStartDate + '-' + seminarendDate;  
                            status = "bg-success";
                        } else {
                            linkDetail = '<p class="m-t-20 text-right"><a class="btn" name="btn-activity-link" onClick=\'ActivityList.register("' + item.activityNameTh  +  '")\'><i class="fa fa-qrcode"></i> เข้าร่วมกิจกรรม</a></p>';
                        }
                    }

                    $('#activity').append(
                    '<div class="cd-timeline-block" name="card-activity" data-activity-id="' + item.activityId + '">' + 
                        '<div class="cd-timeline-icon ' + status +'">' +
                        '</div>' +
                        '<div class="cd-timeline-content card_main">' +                           
                            '<div class="card-block">'  +
                                '<h6>' + item.activityNameTh + '</h6>' + 
                                '<div class="timeline-details">' + 
                                    '<a href="#"> <i class="icofont icofont-ui-calendar"></i><span>' + activityDate + '</span> </a>' +                                     
                                    linkDetail + 
                                '</div>' + 
                            '</div>' +
                            '<span class="cd-date">' + doDate +'</span>' +
                            '<span class="cd-details">' + item.activityNameTh + '</span>' +
                        '</div>' +
                    '</div>' 
                    );
                });

                $('#div-activity').unblock();
            }
        });
    },
    register: function(activityName) {
        $('#register-name').html(activityName);
        $("#register-qr").addClass("md-show");
    },
    registerClose: function() {        
        $("#register-qr").removeClass("md-show");
    }
}
</script>
@endsection