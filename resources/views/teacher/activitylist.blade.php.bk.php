<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-10">
                การส่ง/ตรวจรับงาน
            </div>
            <div class="col-md-2">
                <select name="itsRoundId" id="itsRoundId"></select>
            </div>
        </div>                
    </div>
    <div class="card-block">
        <table class="table table-striped table-bordered" style="width:100%;" id="tbActivity">
            <thead>
                <tr>
                    <th style="width:8%;text-align:center;">ลำดับที่</th>
                    <th style="width:20%;text-align:center;">วันที่กิจกรรม</th>
                    <th style="width:42%;text-align:left;">งาน</th>
                    <th style="width:10%;text-align:center;">ส่งแล้ว/ทั้งหมด</th>
                    <th style="width:15%;text-align:center;">สถานะ</th>
                    <th style="width:15%;text-align:center;"></th>
                </tr>
            </thead>
            <tbody>                                  
                <tr class="text-success">
                        <td align="center">1.</td>
                        <td align="center">15 พ.ค. - 26 พ.ค. 2561</td>
                        <td>โครงสร้างรายวิชา หน่วยการเรียนรู้ และแผนการเรียนรู้</td>
                        <td align="center">2/2</td>
                        <td align="center">เสร็จสิ้นแล้ว</td>
                        <td align="center"></td>
                </tr>          
                <tr>
                        <td align="center">2.</td>
                        <td align="center">15 พ.ค. - 26 พ.ค. 2561</td>
                        <td>บันทึกแบบสังเกตสอน</td>
                        <td align="center">1/2</td>
                        <td align="center">กำลังดำเนินการ</td>
                        <td align="center"><a href="{{route('teacher/activity')}}" class="btn btn-primary">ตรวจงาน</a></td>
                </tr>        
            </tbody>
        </table>        
    </div>
</div>
