<div class="card">        
    <div class="card-header">      
        ส่งแล้ว 1/2                
    </div>
    <div class="card-block">
        <table class="table table-striped table-bordered" style="width:100%;">
            <thead>
                <tr>                                                                
                    <th width="20%"></th>
                    <th align="left" width="35%">ชื่อ-สกุล</th>
                    <th align="center" width="20%">สถานะ</th>
                    <th align="center" width="25%">เวลาที่ส่ง</th>                                                        
                </tr>
            </thead>
            <tbody>
                <tr>                    
                    <td align="center"><img class="img-fluid rounded-circle " src="@relative('images/users/57104010079.png')" width="56px"></td>
                    <td>นายปิติ เรียนดี</td>
                    <td align="center" class="text-primary">
                        <span class="btn btn-mini btn-primary waves-effect waves-light">รอตรวจ</span>
                    </td>                            
                    <td align="center">1 ก.ย. 2561 13:45 น.</td>
                </tr>                        
                <tr>                    
                    <td align="center">
                        <img class="img-fluid rounded-circle " src="@relative('images/users/57104010152.png')" width="56px">                                             
                    </td>
                    <td>นางสาวมานี รักการเรียน</td>
                    <td align="center" class="text-success">
                        <span class="btn btn-mini btn-success waves-effect waves-light">รับงาน</span>
                    </td>                            
                    <td align="center">1 ก.ย. 2561 01:32 น.</td>
                </tr>                        
            </tbody>
        </table>
    </div>
</div>