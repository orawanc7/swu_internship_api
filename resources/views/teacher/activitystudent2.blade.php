<div class="card" id="div-activity-student">        
    <div class="card-header">      
        <div class="row">
            <div class="col-md-6">
                ภาคปีการศึกษา <span id="divSemYear"></span>
            </div>
            <div class="col-md-6 text-right">
                ทำรายการแล้ว <span id="divCompleteTotal">0</span>/<span id="divTotal">0</span>
            </div>
        </div>        
    </div>
    <div class="card-block">
        <div class="table-responsive-sm">
            <table class="table dt-responsive nowrap no-footer dtr-inline" style="width:100%;" id="tbStudent">
                <thead>
                    <tr>                                                                
                        <th style="text-align:center;" width="15%"></th>                    
                        <th style="text-align:left;" width="40%">สถานะ</th>
                        <th style="text-align:right;" width="45%">เวลาที่ทำรายการ</th>                                                        
                    </tr>
                </thead>
                <tbody>                
                </tbody>
            </table>
        </div>
    </div>
</div>