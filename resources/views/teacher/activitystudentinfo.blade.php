<div class="card b-l-primary">    
    <div class="card-body">      
        <div class="row">
            <div class="col-md-1" name="studentImage">

            </div>
            <div class="col-md-11">
                <h5 class="f-14">เลขประจำตัว : <span name="studentId"></span></h5>                
                <h5 class="f-14">ชื่อ-นามสกุล : <span name="studentName"></span></h5>                
                <h5 class="f-14">คณะ : <span name="dept"></span></h5>                
                <h5 class="f-14">สาขาวิชา <span name="major"></span></h5>                  
                <h5 class="f-14"><i class="h4 icofont icofont-document-folder txt-primary"></i> <span><a href="#" name="documentAll">เอกสารทั้งหมด</a></span></h5>    
            </div>
        </div>      
    </div>
</div>