<div class="card d-none" id="divTracking">        
        <div class="card-header">      
            <i class="icofont icofont-clock-time"></i> ประวัติ : <div name="divStudentName"></div>
            <div class="f-right">
                <div name="divStudentImage"></div>
            </div>                           
        </div>
        <div class="card-block">
            <table class="table table-striped table-bordered" style="width:100%;">
                <thead>
                    <tr>                                           
                        <th align="left" width="15%">ที่</th>
                        <th align="center" width="25%">วันที่ส่ง</th>
                        <th align="center" width="25%">วันที่ตรวจ</th>                                                     
                        <th align="center" width="15%">สถานะ</th>   
                        <th align="center" width="20%">ผู้ตรวจ</th>                                                                                
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>