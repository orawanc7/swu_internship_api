@foreach ($personTypes as $personType)
<div class="card">        
    <div class="card-header" name="teacher-student" data-person-type-id="{{$personType['itsPersonTypeId']}}">          
        {{$personType['personTypeNameTh']}}         
        <div class="f-right">
            <a href="">
                <i class="icofont icofont-ui-previous"></i>
            </a>
            ปีการศึกษา 2561
            <a href="">
                <i class="icofont icofont-ui-next"></i>
            </a>  
        </div>                   
    </div>
    <div class="card-block">
        <ul class="nav nav-tabs  tabs" role="tablist" id="tabsStudent">
        @foreach ($personType['rounds'] as $round)        
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#sem{{ $round['itsRoundId'] }}" role="tab" data-round-id="{{ $round['itsRoundId']}}" data-person-type-id="{{$personType['itsPersonTypeId']}}">ภาคเรียนที่ {{ $round['semCd'] }}</a>
            </li>                              
        @endforeach
        </ul>
        <!-- Tab panes -->
        <div class="tab-content tabs">
        @foreach ($personType['rounds'] as $round)          
        <div class="tab-pane" id="sem{{ $round['itsRoundId'] }}" role="tabpanel">
            <table class="table table-striped table-bordered" style="width:100%;" id="{{ $round['itsRoundId']}}-{{$personType['itsPersonTypeId']}}">
                <thead>
                    <tr>                                             
                        <th style="text-align:center;width:15%">เลขประจำตัว</th>
                        <th style="width:25%">ชื่อ-สกุล</th>
                        <th style="text-align:center;width:15%">คณะ-สาขา</th>
                        <th style="text-align:center;width:10%">วิชา</th>                            
                        <th style="text-align:center;width:25%">สถานที่ฝึกประสบการณ์</th>
                        <th style="text-align:center;width:10%"></th>
                    </tr>
                </thead>
                <tbody>                        
                </tbody>
            </table>
        </div>                    
        @endforeach
        </div>
    </div>
</div>
@endforeach