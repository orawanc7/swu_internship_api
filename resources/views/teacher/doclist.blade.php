<div class="card">
    <div class="card-header" id="div-doclist">
        <div class="row">
            <div class="col-md-10">
                <i class="h4 icofont icofont-check-circled txt-primary"></i> การส่ง/ตรวจรับงาน <a id="btnRefreshDocListStatus"><i class="icofont icofont-refresh"></i></a>
            </div>
            <div class="col-md-2">                
                <select name="itsRoundId" id="itsRoundId"></select>                
            </div>
        </div>                
    </div>
    <div class="card-block">     
            @include('teacher.docsummary') 
        <a class="h5 txt-primary m-1 float-right" id="btnExpandCollapseActivity" data-toggle="tooltip" data-placement="top" title="" data-original-title="ย่อ/ขยาย">
            <i class="icofont icofont-expand"></i>
        </a>
        <table class="table table-striped table-bordered d-none" style="width:100%;" id="tbActivity">            
            <thead>
                <tr>
                    <th style="width:45%;valign:top;" class="bg-primary text-left">                        
                        งาน
                    </th>
                    <th style="width:25%;valign:top;" class="bg-primary text-left text-sm-center">วันที่กิจกรรม</th>                    
                    <th style="width:30%;valign:top;" class="bg-primary text-left text-sm-center">ยังไม่ส่ง/รอตรวจ/ส่งคืน/อนุมัติ/ทั้งหมด</th>                    
                </tr>
            </thead>            
            <tbody>            
            </tbody>            
        </table>    
        <div class="row">
            <div class="col-12 text-right">
                <label class="label border border-dark text-dark">ยังไม่ส่ง</label>/
                <label class="label label-default">รอตรวจ</label>/
                <label class="label label-danger">ส่งคืน</label>/
                <label class="label label-success">อนุมัติ</label>/
                <label class="label bg-dark text-white">ทั้งหมด</label>
            </div>            
        </div>        
    </div>
    <div class="card-footer">
        <button type="button" class="btn btn-primary waves-effect waves-light m-1 float-right float-sm-none" data-toggle="tooltip" data-placement="top" title="" data-original-title="กิจกรรมประจำภาคการศึกษา" id="btnActivity">
            <i class="icofont icofont-listine-dots"> กิจกรรมประจำภาคการศึกษา</i> 
        </button>  
    </div>
</div>
