@extends('teacher.layout')

@section('style')
    <style>
        .student-table-row{
            cursor:pointer;
        }    
        
        #tbStudent1 tbody td:before {            
            width: 40% !important;
        }

        #tbStudent2 tbody td:before {            
            width: 40% !important;
        }
        
        #tbActivity tbody td:before {            
            width: 1% !important;
        }
        
    </style>    
@endsection

@section('content')

<div class="row">
    <div class="col-xl-8 col-lg-7 grid-item">
        <div class="row">      
            <div class="col-xl-12 col-lg-12 grid-item">
                @include('teacher.doclist')
            </div>            
            <div class="col-xl-12 col-lg-12 grid-item">
                @include('teacher.studentlist')
            </div>
            <div class="col-xl-12 col-lg-12 grid-item">
                @include('share.news')
            </div>
            <div class="col-xl-12 col-lg-12 grid-item">                
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-5 grid-item">       
        <div class="row">                           
            <div class="col-xl-12 col-lg-12 grid-item">           
                @include('share.todo')
            </div>
            <div class="col-xl-12 col-lg-12 grid-item">           
                @include('share.download')
            </div>
            <div class="col-xl-12 col-lg-12 grid-item">           
                
            </div>            
        </div>
    </div>
</div>

@endsection

@section('modal')
    @include('share.news-add')   
    <div class="md-overlay"></div>
@endsection


@section('script')  
<script src="@relative('js/app/Todo.js')"></script>  
<script src="@relative('js/app/Download.js')?{{time()}}"></script>
<script src="@relative('js/app/News.js')?{{time()}}"></script>
<script>      
    var students;
    var currentTable = 'tbStudent1';    
    $(document).ready(function () {      
        $('#year').select2({});
        $('#itsRoundId').select2({});

        $('#year').change(function (e) { 
            e.preventDefault();
            Teacher.renderStudent($(this).val());
        });

        $('#itsRoundId').change(function (e) { 
            e.preventDefault();
        
            Teacher.setCurrentRoundId($(this).val());

            var itsRoundDesc = $( "#itsRoundId option:selected" ).text();
            if (itsRoundDesc) {                
                var arr = itsRoundDesc.split('/');
                if (arr) {
                    Teacher.renderStudent(arr[1],arr[0]);                    
                }                
            }
            
            Teacher.loadActivity($(this).val());
        });

        $('#btnActivity').click(function(){
            var value = $('#itsRoundId').val();          

            location.href = "{{ route('teacher/activitylist') }}" + "/" + value;
        });
        
        $("#tbActivity tbody").on("mousedown", "tr", function() {
            $(".selected").not(this).removeClass("selected");
            $(this).toggleClass("selected");
        });
        

        $('#btnExpandCollapseActivity').click(function (e) { 
            e.preventDefault();            
            $("i", this).toggleClass("icofont icofont-expand icofont icofont-collapse");

            if ($("i", this)[0].className == "icofont icofont-collapse") {
                $("#tbActivity").treetable('expandAll');
            } else {
                $("#tbActivity").treetable('collapseAll');
            }            
        });       
                

        News.setSender( $('#its-id').val());

        $('#btnOpenNews').click(function() {                
            News.setCallback(function() {
                News.loadBySender( $('#its-id').val());
            });            

            setTimeout(function(){
                $("input[name='newsTitleNameTh']").focus();
            },200);        
        });

        $("#tbActivity tbody").on("click", "tr", function(e) {
            if ($(this).hasClass('branch')) {                
                var target = e.target;
                if (target.tagName.toLowerCase() != "a" && target !== undefined) {
                    var id = $(this).attr('data-tt-id');
                    if ($(this).hasClass('collapsed')) {
                        $(this).parents('table').treetable("expandNode", id) ;                
                        return false;
                    } else if ($(this).hasClass('expanded')) {
                        $(this).parents('table').treetable("collapseNode", id) ;     
                        return false;
                    }
                }                            
            }
            return true;
        });

        $('#btnRefreshDocListStatus').click(function() {
            Teacher.loadActivity($('#itsRoundId').val());
        });
        

        $('#div-doclist').block();
        $('#div-studentlist').block();        


        $('#tbActivity').basictable({breakpoint: 768});

        Teacher.loadStudent();
    });       

    var Teacher = {
        setCurrentRoundId: function(itsRoundId) {        
            $('#current-its-round-id').val(itsRoundId);
            $.ajax({
                type: "post",
                url: "{{route('api/setCurrentRoundId')}}",
                data: {
                    itsRoundId : itsRoundId
                },
                dataType: "json",
                success: function (response) {
                    
                }
            });
        },
        createReceiver:function() {            
            News.emptyReceiver();
            var arrReceiverId = [];
            $('#' + currentTable + ' tbody > tr').each(function(idx,item){
                var id = $(this).attr('data-id');
                var receiverId = $(this).attr('data-its-id');
                var name = $(this).attr('data-name');

                News.addReceiver(id,id,name);

                arrReceiverId.push(id);
            });    
               
        },
        loadStudent: function() {            
            $.ajax({
                url: "{{route('api/SchdPersonStd/listStudentByTeacher')}}",
                type: "get",                
                dataType: "json",
                success: function (response) {                    
                    students = response;                    
                    
                    var year = ""; var itsRoundId="";
                    var currentRoundId = $('#current-its-round-id').val();       
                    var currentSelected = "";             
                    var currentYear = "";
                    var currentSem = "";

                    if (students.length>0) {                        
                        var v_selected = "";
                        $.each(students, function (idx, item) {                            
                            if (item.year!=year) {
                                $('#year').append($('<option>', { 
                                    value: item.year,
                                    text : item.year 
                                }));

                                if (idx==0) {
                                    currentYear = item.year;
                                    currentSem = item.semCd;
                                    currentSelected = item.itsRoundId;
                                }
                            }

                            if (item.itsRoundId!=itsRoundId) {
                                $('#itsRoundId').append($('<option>', { 
                                    value: item.itsRoundId,
                                    text : item.semCd + "/" + item.year
                                }));
                                                  
                                if (currentRoundId==item.itsRoundId) {
                                    currentYear = item.year;
                                    currentSem = item.semCd;
                                    currentSelected = item.itsRoundId;
                                }
                            }

                            year = item.year;
                            itsRoundId = item.itsRoundId;                            
                        }); 

                        if (currentRoundId) {
                            $("#itsRoundId option[value='" + currentRoundId + "']").attr('selected',true);

                            $("#year option[value='" + currentYear + "']").attr('selected',true);
                        }

                        Teacher.renderStudent(currentYear,currentSem) ;
                        Teacher.loadActivity(currentSelected);
                        Teacher.createReceiver();
                        News.loadBySender( $('#its-id').val());  
                    }
                }
            });
        },     
        loadActivity:function(itsRoundId) {            
            $('#div-doclist').block();
            
            $('#div-doc-status-none').html(0);
            $('#div-doc-status-wait').html(0);
            $('#div-doc-status-return').html(0);
            $('#div-doc-status-confirm').html(0);
            if (!$('#tbActivity').hasClass('d-none')) {
                $('#tbActivity').addClass('d-none');
            }            
            $("#tbActivity").treetable('destroy');            
            $('#tbActivity tbody').empty();
            

            $('#btnExpandCollapseActivity').attr('disabled',true);
            
            $.ajax({
                url: "{{route('api/SchdActivity/listDocByItsRoundIdTeacher')}}/" + itsRoundId + "/" + $('#its-id').val(),
                type: "get",                
                dataType: "json",
                success: function (response) {
                    var docTypeId = ""; 
                    var arrDocTypeId = [];           
                    var status_none_count = 0;
                    var status_wait_count = 0;
                    var status_return_count = 0;
                    var status_confirm_count = 0;
                    $.each(response, function (idx, item) {                                                
                        var startDate = Helper.toScreenDate(item.startDate.date,false);
                        var endDate = Helper.toScreenDate(item.endDate.date,false);                        
                        if($.inArray(item.docTypeId, arrDocTypeId) === -1) {                            
                            $('#tbActivity tbody').append(                            
                                "<tr data-tt-id='" + item.docTypeId + "'>" +                                        
                                        "<td colspan=\"3\" style=\"cursor:pointer\"><span class='folder'></span>" +  item.docTypeTh + "</span></td>" +
                                "</tr>"
                            );                            
                            arrDocTypeId.push(item.docTypeId);
                        }

                        var activityDetailUrl = "{{ route ('teacher/activity') }}/" + item.activityId;

                        var element = $('#tbActivity tr[data-tt-parent-id="' + item.docTypeId + '"]')                        
                        var countDetail = "";

                        countDetail += "<label class=\"label border border-dark text-dark\">"+ item.noneCount + "</label>/";
                        countDetail += "<label class=\"label label-default\">"+ item.waitCount + "</label>/";
                        countDetail += "<label class=\"label label-danger\">"+ item.returnCount + "</label>/";
                        countDetail += "<label class=\"label label-success\">"+ item.confirmCount + "</label>/";
                        countDetail += "<label class=\"label bg-dark text-white\">"+ item.allCount + "</label>";      
                        
                        var color = "btn-outline-primary";
                        if (item.confirmCount==item.allCount) {
                            color = "btn-success"
                        }

                        if (element.length>0) {                            
                            element.last()
                            .after(                            
                                "<tr data-tt-id='" + item.activityId + "' data-tt-parent-id='" + item.docTypeId + "' data-status-none-count='" + item.noneCount + "' data-status-wait-count='" + item.waitCount + "' data-status-return-count='" + item.returnCount + "' data-status-confirm-count='" + item.confirmCount + "' data-status-all-count='" + item.allCount + "'>" +                                        
                                        "<td><span class='file'></span>" +  item.docNameTh + "</span></td>" +
                                        "<td class=\"justify-content-center text-center\"><span style=\"white-space: nowrap;\">"+ startDate + "-" + endDate + "</span></td>" +                                                    
                                        "<td class=\"justify-content-center text-center\"><a href=\"" + activityDetailUrl + "\"><button class=\"w-100 btn btn-sm " + color + "\">" + countDetail + "</button></a></td>" + 
                                "</tr>"
                                );
                        } else {                            
                            $('#tbActivity tbody').append(                            
                                "<tr data-tt-id='" + item.activityId + "' data-tt-parent-id='" + item.docTypeId + "' data-status-none-count='" + item.noneCount + "' data-status-wait-count='" + item.waitCount + "' data-status-return-count='" + item.returnCount + "' data-status-confirm-count='" + item.confirmCount + "' data-status-all-count='" + item.allCount + "'>" +                                        
                                        "<td><span class='file'></span>" +  item.docNameTh + "</span></td>" +
                                        "<td class=\"justify-content-center text-center\"><span style=\"white-space: nowrap;\">"+ startDate + "-" + endDate + "</span></td>" +            
                                        "<td class=\"justify-content-center text-center\"><a href=\"" + activityDetailUrl + "\"><button class=\"w-100 btn btn-sm " + color + "\">" + countDetail + "</button></a></td>" + 
                                "</tr>"
                            );                        
                        }
                        //docTypeId = item.docTypeId;

                        status_none_count += item.noneCount;
                        status_wait_count += item.waitCount;
                        status_return_count += item.returnCount;
                        status_confirm_count += item.confirmCount;                        
                    });
                    
                    $('#tbActivity').removeClass('d-none');
                    $("#tbActivity").treetable({ expandable: true });

                    $('#btnExpandCollapseActivity').removeAttr('disabled');                    

                    $('#div-doc-status-none').html(status_none_count);
                    $('#div-doc-status-wait').html(status_wait_count);
                    $('#div-doc-status-return').html(status_return_count);
                    $('#div-doc-status-confirm').html(status_confirm_count);

                    $('#div-doclist').unblock();                    
                }
            });
               
        },
        renderStudent:function(year,semCd) {
            $('#div-studentlist').block();
            $('#tbStudent1 tbody').empty();
            $('#tbStudent2 tbody').empty();
            $('#tbStudent1 tfoot').empty();
            $('#tbStudent2 tfoot').empty();

            var itsRoundId1;
            var itsRoundId2;
            $.each(students, function (idx, item) {                
                if (item.year == year) {
                    var leaderFlagDiv = "";
                    if (item.leaderFlag=="Y") {
                        leaderFlagDiv = "<div class=\"student-leader-status\"></div>";
                    }

                    if (item.semCd==1) {
                        itsRoundId1 = item.itsRoundId;
                    } else if (item.semCd==2) {
                        itsRoundId2 = item.itsRoundId;
                    }
                                        
                    $('#tbStudent' + item.semCd + ' tbody').append(
                        "<tr class=\"student-table-row\" data-id=\"" + item.studentId + "\" data-name=\"" +  item.prenameSnameTh + item.fnameTh + " " + item.lnameTh + "\" data-its-id=\"" + item.itsStudentId + "\" data-its-round-id=\"" + item.itsRoundId + "\">" +
                            "<td class=\"text-left text-md-center\">" + Helper.userIcon(item.studentId) + leaderFlagDiv + "</td>" +
                            "<td class=\"text-left text-md-center\">" + item.studentId + "</td>" +                                                                         
                            "<td class=\"text-left text-md-center\">" + item.prenameSnameTh + item.fnameTh + " " + item.lnameTh  + "</td>" +
                            "<td class=\"text-left text-md-center small\">" +item.deptLnameTh + "<div class=\"small\">" + item.majorLnameTh + "</div>" + "</td>" +
                            "<td class=\"text-left text-md-center\">" + item.courseCd + "</td>" +                                    
                            "<td class=\"text-left text-md-center\">" + item.schoolNameTh + "</td>" +                            
                        "</tr>"
                    );                                       
                }
            });  
                   
            var url1 = "{{route('api/SchdPersonStd/downloadStudentList')}}" + "/" + itsRoundId1;
            var url2 = "{{route('api/SchdPersonStd/downloadStudentList')}}" + "/" + itsRoundId2;
            /*
            $('#tbStudent1 tfoot').append(
                '<tr>' + 
                    '<td colspan="3">' +  '<i class="fa fa-people"></i> รวมจำนวนนิสิต ' + $('#tbStudent1 tbody tr').length + ' คน </td>' +
                    '<td colspan="3" class="text-right"><a href="' + url1 + '"><i class="fa fa-download"></i> ดาวน์โหลดรายชื่อนิสิต</a></td>'  +
                '</tr>'
            );
            $('#tbStudent2').append(
                '<tr>' + 
                    '<td colspan="3">' +  '<i class="fa fa-people"></i> รวมจำนวนนิสิต ' + $('#tbStudent2 tbody tr').length + ' คน</td>' +
                    '<td colspan="3" class="text-right"><a href="' + url2 + '"><i class="fa fa-download"></i> ดาวน์โหลดรายชื่อนิสิต</a></td>'  +
                '</tr>'
            );
            */

            $('#student-total1').html('<i class="fa fa-users"></i> รวมจำนวนนิสิต ' + $('#tbStudent1 tbody tr').length + ' คน' );
            $('#student-download1').html('<a href="' + url1 + '"><i class="fa fa-download"></i> ดาวน์โหลดรายชื่อนิสิต</a>');

            $('#student-total2').html('<i class="fa fa-users"></i> รวมจำนวนนิสิต ' + $('#tbStudent2 tbody tr').length + ' คน' );
            $('#student-download2').html('<a href="' + url2 + '"><i class="fa fa-download"></i> ดาวน์โหลดรายชื่อนิสิต</a>');
                        
            
            // $("#tbStudent1").rtResponsiveTables({                
            // });
            // $("#tbStudent2").rtResponsiveTables({
                
            // });
            $('#tbStudent1').basictable({breakpoint: 768});
            $('#tbStudent2').basictable({breakpoint: 768});                
            
            $(".student-table-row").click(function() {
                var itsStudentId = $(this).attr('data-its-id')
                location.href = "{{route('teacher/student')}}/" + itsStudentId;                
            });

            if (semCd!=undefined) {
                $('#navStudent a[href="#sem' + semCd + '"]').tab('show');
            }

            $('#div-studentlist').unblock();            
        },
        showDocByStatus:function(status) {
            $("#tbActivity").treetable('collapseAll');     
            $("#tbActivity tr").removeClass('text-danger font-weight-bold');
            
        
            var status_desc = "";
            if(status=="Y") {
                status_desc = "confirm";
            } else if (status=="N") {
                status_desc = "none";
            } else if (status=="W") {
                status_desc = "wait";
            } else if (status=="R") {
                status_desc = "return";
            }

            $('div[id*="div-doc-status-card"]').each(function(idx,item) {
                $(this).removeClass('b-l-primary');
            });

            $('[id="div-doc-status-card-' + status_desc + '"]').addClass("b-l-primary");
            
            

            var element = $("#tbActivity tr").filter(function() {
                if ($(this).attr("data-status-none-count")) {                    

                    if(status=="Y") {
                        return  $(this).attr("data-status-confirm-count") > 0;
                    } else if (status=="N") {
                        return  $(this).attr("data-status-none-count") > 0;
                    } else if (status=="W") {
                        return  $(this).attr("data-status-wait-count") > 0;
                    } else if (status=="R") {
                        return  $(this).attr("data-status-return-count") > 0;
                    }                    
                }                
            });

            
            if (element.length>0) {
                $.each(element, function (idx, item) {       
                    var parent_id = $(this).attr('data-tt-parent-id');
                    $(this).addClass('text-danger font-weight-bold');

                    if (parent_id) {                                                            
                        $("#tbActivity").treetable("expandNode", parent_id);
                    }
                });
            }                  
        }
    };
</script>
@endsection