@extends('teacher.layout')

@section('content')
<input type="hidden" name="itsPersonId" id="itsPersonId" value="{{ $itsPersonId }}">

<div class="row">
    <div class="col-xl-8 col-lg-7 grid-item">
        <div class="row">      
            <div class="col-xl-12 col-lg-12 grid-item">
                @include('teacher.activitylist')
            </div>            
            <div class="col-xl-12 col-lg-12 grid-item">
                
            </div>
            <div class="col-xl-12 col-lg-12 grid-item">
                @include('teacher.news')
            </div>
            <div class="col-xl-12 col-lg-12 grid-item">
                @include('../news')
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-5 grid-item">       
        <div class="row">               
            <div class="col-xl-12 col-lg-12 grid-item">           
                @include('teacher.today')
            </div>   
            <div class="col-xl-12 col-lg-12 grid-item">           
                @include('teacher.menu')
            </div>
            <div class="col-xl-12 col-lg-12 grid-item">           
                @include('teacher.todo')
            </div>
            <div class="col-xl-12 col-lg-12 grid-item">           
                @include('../download')
            </div>            
        </div>
    </div>
</div>

@include('teacher.news-modal')

<div class="md-overlay"></div>
@endsection

@section('script')    
    <script>        
        $(document).ready(function () {            
            $('#tabsStudent a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var itsRoundId = $(e.target).attr('data-round-id');
                var itsPersonTypeId = $(e.target).attr('data-person-type-id');
                
                loadStudentList(itsRoundId,itsPersonTypeId);
            });

            $('#tabsStudent li:first-child a').tab('show');
            
            loadTeacherNews();
            saveStudentNews();
        });

        function loadTeacherNews() {
            $('#studentnews ul').empty();
            $.ajax({
                type: "get",
                url: "{{ route('api/News/ListByTeacher') }}",
                data : {
                    'itsPersonId' : $('#itsPersonId').val()
                },
                dataType: "json",
                success: function (response) {
                    if (response) {                        
                        var seq=0;
                        $.each(response, function (index, row) {                                                                  
                            var receiver = "";
                            $.each(row.receiver, function (indexInArray, valueOfElement) {                                 
                                receiver += '<img class=" rounded-circle card-list-img" src="' + $('#current-path').val() + "images/users/" + valueOfElement.receiverId + ".png"  + '"> '
                            });

                            $('#studentnews ul').append(
                                '<li>'  + 
                                    '<div class="card">' + 
                                        '<div class="card-block">' + 
                                            '<div class="media">' +                                                     
                                                '<div class="media-body">' + 
                                                    '<div>' +
                                                         '<h6 class="d-inline-block">' + row.newsTitleNameTh + '</h6>' +      
                                                         '<p>' + row.newsDescTh  + '</p>' + 
                                                         receiver + 
                                                     '</div>' +
                                                '</div>' + 
                                            '</div>' + 
                                        '</div>' +                                     
                                    '</div>' + 
                                '</li>'
                            );
                        });                    
                    }
                }
            });
        }

        function saveStudentNews () {
            $('#frmStudentNews').submit(function (e) { 
                e.preventDefault();

                var thisform = $(this);
                
                $.ajax({
                    type: "post",
                    url: $(this).attr('action'),
                    data: $(this).serialize(),                    
                    success: function (response) {                        
                        thisform[0].reset();
                        loadTeacherNews();
                    }
                });
                
            });
        }        

        function loadStudentList(itsRoundId,itsPersonTypeId) {            
            var tabId = itsRoundId + "-" + itsPersonTypeId;
            var activitydocpath = "{{route('student/activitydoc/')}}";
            $.ajax({
                type: "get",
                url: "{{route('api/SchdPersonStd/ListStudentByTeacher')}}",
                data: {
                    "itsRoundId": itsRoundId,
                    "itsPersonTypeId": itsPersonTypeId
                },
                dataType: "json",
                success: function (response) {         
                    if (response) {
                        $('#' + tabId +' tbody').empty();
                        $('#news-receiver ul').empty();
                        var seq=0;
                        $.each(response, function (index, row) {                                      
                            var imagePath = $('#current-path').val() + "images/users/" + row.studentId + ".png";
                            var leader = "";
                            if (row.leaderFlag=="Y") {
                                leader = "<i class=\"icofont icofont-king-crown text-danger\" style=\"font-size:25px;\"></i>";
                            }
                            $('#' + tabId +' tbody').append(
                                "<tr>" +
                                    "<td style=\"text-align:center;\">" + row.studentId + "</td>" +                                                                         
                                    "<td style=\"text-align:left;\">" + row.fnameTh + " " + row.lnameTh  + "</td>" +
                                    "<td style=\"text-align:center;\">" + row.deptLnameTh + " " + row.majorLnameTh + "</td>" +
                                    "<td style=\"text-align:center;\">" + row.courseCd + "</td>" +                                    
                                    "<td style=\"text-align:center;\">ร.ร. สวนกุหลาบ</td>" +
                                    "<td style=\"text-align:center;\"><div style=\"position:relative;\"><a href=\"" +activitydocpath + row.itsStudentId + "\"><img class=\"img-fluid rounded-circle\" src=\"" + imagePath + "\" width=\"56px\"><div style=\"position:absolute;left:53px;top:0\">" + leader +"</div></div></a></td>" +
                                "</tr>"
                                );
                                
                                $('#news-receiver ul').append(
                                    "<li class=\"friendlist-box\">" + 
                                        "<img class=\"img-fluid rounded-circle p-absolute d-block text-center\" src=\"" + imagePath + "\" width=\"56px\">" +
                                        "<div class=\"checkbox-color checkbox-primary\">" + 
                                            "<input id=\"receiverId"  + row.itsStudentId + "\" name=\"receiverId[]\" type=\"checkbox\" value=\"" + row.studentId + "\">" + 
                                            "<label for=\"receiverId"  + row.itsStudentId + "\">" +
                                                "<h6>" + row.fnameTh + " " + row.lnameTh + " " + leader +  "</h6>" + 
                                            "</label>" +
                                        "</div>" + 
                                        "<p>" + row.deptLnameTh + " " + row.majorLnameTh + "</p>" +
                                    "</li>"
                                );                                
                        });                    
                    }           
                }
            });
        }
    </script>
@endsection