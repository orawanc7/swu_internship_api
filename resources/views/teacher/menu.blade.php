<div class="card">
    <div class="card-block button-list">
        <button type="button" class="btn btn-lg btn-primary btn-icon" data-toggle="tooltip" data-placement="top" title="" data-original-title=".icofont-home ">                
            <i class="icofont icofont-social-facebook-messenger"></i>
            <label class="badge bg-danger badge-top-right text-white">9</label>                
        </button>                    

        <button type="button" class="btn btn-lg btn-warning btn-icon" data-toggle="tooltip" data-placement="top" title="" data-original-title=".icofont-home ">
            <i class="icofont icofont-megaphone"></i>
            <label class="badge bg-danger badge-top-right text-white">9</label>
        </button>

    </div>
</div>