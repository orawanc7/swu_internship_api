<div class="card">
    <div class="card-header">
        <h5 class="card-header-text"><i class="h4 icofont icofont-news txt-primary"></i> ข่าวประกาศแจ้งนิสิต</h5>        
        <div class="f-right">            
            <button type="button" class="btn btn-sm btn-primary waves-effect md-trigger" data-modal="news-add" data-toggle="tooltip" data-placement="top" title="" data-original-title="แจ้งประกาศ"  id="btnOpenNews">
                <i class="icofont icofont-ui-add"></i> เพิ่มข่าว
            </button>
        </div>
    </div>
    <div class="card-block" id="studentnews">                                               
        <ul class="list-view" >

        </ul>
        <div class="new-users-more text-center p-t-10">
            <a href="#"><h6 class="m-b-0 f-w-400">ดูข่าวทั้งหมด<i class="icofont icofont-bubble-right m-l-10"></i></h6></a>
        </div>
    </div>
</div>