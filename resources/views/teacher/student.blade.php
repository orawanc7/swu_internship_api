@extends('teacher.layout')

@section('style')
    <link rel="stylesheet" href="@relative('css/scheduler.css')">
    <style>        
        #tbDocList tbody td:before {            
            width: 1% !important;
        }

        #tbDocFinalList tbody td:before {            
            width: 1% !important;
        }

    </style>    
@endsection

@section('content')
<ul id="breadcrumb-triangle">
    <li><a href="{{route('teacher')}}"><span class="icofont icofont-home"> </span></a></li>                    
    <li><a href="#"><span class="icon-screen-desktop"></span> ข้อมูลนิสิต ภาค/ปีการศึกษา : {{ $itsStudent['semCd'] }}/{{ $itsStudent['year'] }}</a></li>
</ul>

<div class="row">
    <div class="col-xl-4 col-lg-5 grid-item">               
        <div class="row">               
            <div class="col-xl-12 col-lg-12 grid-item">           
                @include('teacher.studentinfo')
            </div>            
            <div class="col-xl-12 col-lg-12 grid-item">           
                @include('share.news')
            </div>
        </div>
    </div>
    <div class="col-xl-8 col-md-7 pull-xl-3 pull-md-4 filter-bar">                                
        <div class="row">      
            <div class="col-xl-12 col-lg-12 grid-item">
                @include('share.studentinfo') 
            </div>                                    
        </div>
        <div class="row">      
            <div class="col-xl-12 col-lg-12 grid-item">
                
            </div>                                    
        </div>
        <div class="row">      
            <div class="col-xl-12 col-lg-12 grid-item">
                @include('teacher.studentdoclist')
            </div>                                    
            <div class="col-xl-12 col-lg-12 grid-item">
                @include('teacher.studentdocfinallist')
            </div>  
        </div>
    </div>    
</div>
@endsection        

@section('modal')
    @include('share.news-add')   
    <div class="md-overlay"></div>
@endsection

@section('script')
<script src="@relative('js/app/StudentInfo.js')"></script>
<script src="@relative('js/app/News.js')?{{time()}}"></script>
<script>
$(document).ready(function () {
    $("#tbDocList tbody").on("mousedown", "tr", function() {
        $(".selected").not(this).removeClass("selected");
        $(this).toggleClass("selected");
    });
        

    $('#btnExpandCollapseDoc').click(function (e) { 
        e.preventDefault();            
        $("i", this).toggleClass("icofont icofont-expand icofont icofont-collapse");

        if ($("i", this)[0].className == "icofont icofont-collapse") {
            $("#tbDocList").treetable('expandAll');
        } else {
            $("#tbDocList").treetable('collapseAll');
        }            
    });
    $('#btnExpandCollapseDocFinal').click(function (e) { 
        e.preventDefault();            
        $("i", this).toggleClass("icofont icofont-expand icofont icofont-collapse");

        if ($("i", this)[0].className == "icofont icofont-collapse") {
            $("#tbDocFinalList").treetable('expandAll');
        } else {
            $("#tbDocFinalList").treetable('collapseAll');
        }            
    });

    StudentInfo.loadinfo({{ $itsStudent['itsStudentId']}});    
    Student.loadDoc({{ $itsStudent['itsRoundId']}},{{ $itsStudent['itsStudentId']}}); 
    Student.loadCourse({{ $itsStudent['itsStudentId']}});     
    
    News.setCallback(function() {
        News.loadBySenderReceiver( $('#its-id').val(), "{{ $itsStudent['studentId'] }}");
    });
    News.setSender( $('#its-id').val());
    News.addReceiver("{{ $itsStudent['studentId'] }}","{{ $itsStudent['studentId'] }}","{{ $itsStudent['prenameSnameTh'] }}{{ $itsStudent['fnameTh'] }} {{ $itsStudent['lnameTh'] }}");    
    News.loadBySenderReceiver( $('#its-id').val(), "{{ $itsStudent['studentId'] }}");
    

    $('#btnOpenNews').click(function() {                
        setTimeout(function(){
            $("input[name='newsTitleNameTh']").focus();
        },200);        
    });

    
    $('#tbDocList').basictable({
        breakpoint: 768
    });         
    $('#tbDocFinalList').basictable({
        breakpoint: 768
    });         
    
    
    $("#tbDocList,#tbDocFinalList tbody").on("click", "tr", function(e) {
        if ($(this).hasClass('branch')) {                
            var target = e.target;
            if (target.tagName.toLowerCase() != "a" && target !== undefined) {
                var id = $(this).attr('data-tt-id');
                if ($(this).hasClass('collapsed')) {
                    $(this).parents('table').treetable("expandNode", id) ;                
                    return false;
                } else if ($(this).hasClass('expanded')) {
                    $(this).parents('table').treetable("collapseNode", id) ;     
                    return false;
                }
            }                            
        }
        return true;
    });


    $('#div-student-doclist').block();    
});

var Student = {      
    loadDoc:function(itsRoundId,itsStudentId) {      
        
        $('#div-student-doclist').block();

        $("#tbDocList").treetable('destroy');            
        $('#tbDocList tbody').empty();

        $('#btnExpandCollapseDoc').attr('disabled',true);
        
        $.ajax({
            url: "{{route('api/SchdActivity/listDocByStudent')}}/" + itsRoundId + "/" + itsStudentId,
            type: "get",                
            dataType: "json",
            success: function (response) {
                var docTypeId = "";            
                var status_none_count = 0;
                var status_wait_count = 0;
                var status_return_count = 0;
                var status_confirm_count = 0;
                    
                $.each(response, function (idx, item) {                                                
                    var startDate = Helper.toScreenDate(item.startDate.date,false);
                    var endDate = Helper.toScreenDate(item.endDate.date,false);
                    var sendDate = "";
                    var courseCd = "";
                    var status = "";
                    var status_id = "";
                    
                    if (item.itsStatusSendId!=null) {
                        
                        sendDate= Helper.toScreenDate(item.sendDate.date,true);

                        
                        if (item.confirmFlag=='Y') {
                            status = "<div class=\"pb-1\"><label class=\"label label-success\">อนุมัติ</label></div>";
                            status_id = "Y";
                            status_confirm_count++;
                        } else if (item.confirmFlag=="N") {
                            status = "<div class=\"pb-1\"><label class=\"label label-danger\">ส่งคืน</label></div>";
                            status_id = "R";
                            status_return_count++;
                        } else {                            
                            if (item.senderTypeId!=1) {
                                status = "<div class=\"pb-1\"><label class=\"label label-default\">รอผล</label></div>";
                            } else {
                                status = "<div class=\"pb-1\"><label class=\"label label-default\">รอตรวจ</label></div>";
                            }
                            status_id = "W";
                            status_wait_count++;
                        }

                        if (item.stdCourseId!=null) {
                            status += "<div class=\"small\">" + item.itsCourseCd + "</div>"; 
                        }

                        status += "<div class=\"small\"><span style=\"white-space: nowrap;\">" + "<i class=\"icofont icofont-time\"></i> "  + sendDate + "</span></div>"; 
                    } else {
                        if (item.senderTypeId!=1) {
                            status = "<div class=\"pb-1\"><label class=\"label label-default\">รอผล</label></div>";
                        } else {
                            status_id = "N";
                            status_none_count++;                            
                        }
                    }

                    if (docTypeId!=item.docTypeId) {
                        $('#tbDocList tbody').append(                            
                            "<tr data-tt-id='" + item.docTypeId + "'>" +                                        
                                    "<td colspan=\"4\" style=\"cursor:pointer\"><span class='folder'></span>" +  item.docTypeTh + "</span></td>" +
                            "</tr>"
                        );
                    }                    
                    
                    var withStatusSendUrl = (item.itsStatusSendId==null)?"":("/" + item.itsStatusSendId);
                    var activityDetailUrl = "{{ route ('teacher/activity') }}/" + item.activityId + "/" + itsStudentId + withStatusSendUrl;

                    if ((withStatusSendUrl.length>0) || (item.senderTypeId!=1)) {                        
                        if (withStatusSendUrl.length>0) {
                            $('#tbDocList tbody').append(                            
                                "<tr data-tt-id='" + item.activityId + "' data-tt-parent-id='" + item.docTypeId + "' data-status-id='" + status_id + "'>" +                                        
                                        "<td><span class='file'></span>" +  item.docNameTh + "</span></td>" +
                                        "<td class=\"justify-content-center small\"><span style=\"white-space: nowrap;\">"+ startDate + "-" + endDate + "</span></td>" +            
                                        "<td class=\"justify-content-center text-center\">" +  status + "</td>" + 
                                        "<td class=\"justify-content-center text-center\"><a href=\"" + activityDetailUrl + "\"><button class=\"btn btn-sm btn-outline-primary\"><i class=\"icofont icofont-law-document\"></i> รายละเอียด</button></a></td>" + 
                                "</tr>"
                            );     
                        } else {
                            $('#tbDocList tbody').append(                            
                                "<tr data-tt-id='" + item.activityId + "' data-tt-parent-id='" + item.docTypeId + "' data-status-id='" + status_id + "'>" +                                        
                                        "<td><span class='file'></span>" +  item.docNameTh + "</span></td>" +
                                        "<td class=\"justify-content-center small\"><span style=\"white-space: nowrap;\">"+ startDate + "-" + endDate + "</span></td>" +            
                                        "<td class=\"justify-content-center text-center\">" +  status + "</td>" + 
                                        "<td class=\"justify-content-center text-center\"><a href=\"" + activityDetailUrl + "/0" + "\"><button class=\"btn btn-sm btn-outline-primary\"><i class=\"icofont icofont-law-document\"></i> รายละเอียด</button></a></td>" + 
                                "</tr>"
                            );                                  
                        }
                    } else {                        
                        $('#tbDocList tbody').append(                            
                            "<tr data-tt-id='" + item.activityId + "' data-tt-parent-id='" + item.docTypeId + "' data-status-id='" + status_id + "'>" +                                        
                                    "<td><span class='file'></span>" +  item.docNameTh + "</span></td>" +
                                    "<td class=\"justify-content-center small\"><span style=\"white-space: nowrap;\">"+ startDate + "-" + endDate + "</span></td>" +            
                                    "<td class=\"justify-content-center text-center\">" +  status + "</td>" + 
                                    "<td class=\"justify-content-center text-center\"></td>" + 
                            "</tr>"
                        );      
                    }
                                      

                    docTypeId = item.docTypeId;
                });
                
                $('#div-doc-status-none').html(status_none_count);
                $('#div-doc-status-wait').html(status_wait_count);
                $('#div-doc-status-return').html(status_return_count);
                $('#div-doc-status-confirm').html(status_confirm_count);
                
                                        
                $("#tbDocList").treetable({ expandable: true });

                $('#btnExpandCollapseDoc').removeAttr('disabled');
                   

                $('#div-student-doclist').unblock();
            }
        });               
    },
    loadCourse: function (itsStudentId) {        
        $('#selectedCourse').hide();
        $.ajax({
            type: "get",
            url: "{{ route ('api/SchdStdCourse/listByItsStudentId') }}" + "/" + itsStudentId,
            dataType: "json",
            success: function (response) {                
                if (response) {
                    
                    var elm = $('#selectedCourse').find('.form-radio');
                    elm.empty();                    

                    $.each(response, function (idx, item) {                                    
                        var checked = "";
                        if (item.selectedFlag=="Y")  {
                            checked = " checked=\"checked\"";
                        
                            elm.append(
                                '<div class="radio radiofill radio-inline">' +
                                    '<label>' +
                                        '<input type="radio" name="stdCourseId" ' + checked + ' value="' + item.stdCourseId + '" readOnly>' +
                                        '<i class="helper"></i> ' + item.itsCourseCd + " " + item.itsCourseName + " " + item.levelClass +                                     
                                    '</label>' +
                                '</div>'
                            );                        
                        }
                    });

                    if (response.length) {                        
                        $('#selectedCourse').show();

                        Student.loadDocFinal({{ $itsStudent['itsRoundId']}},{{ $itsStudent['itsStudentId']}}); 
                    }
                }
            }
        });
    },
    loadDocFinal:function(itsRoundId,itsStudentId) {      
        var stdCourseId = $('input[name="stdCourseId"]').val();        

        $('#div-student-docfinallist').block();

        $("#tbDocFinalList").treetable('destroy');            
        $('#tbDocFinalList tbody').empty();

        $('#btnExpandCollapseDocFinal').attr('disabled',true);
        
        $.ajax({
            url: "{{route('api/SchdActivity/listDocFinalByStudent')}}/" + itsRoundId + "/" + itsStudentId,
            type: "get",                
            dataType: "json",
            success: function (response) {
                var docTypeId = "";            
                var status_none_count = 0;
                var status_wait_count = 0;
                var status_return_count = 0;
                var status_confirm_count = 0;
                    
                $.each(response, function (idx, item) {                                                
                    var startDate = Helper.toScreenDate(item.startDate.date,false);
                    var endDate = Helper.toScreenDate(item.endDate.date,false);
                    var sendDate = "";
                    var courseCd = "";
                    var status = "";
                    var status_id = "";
                    var add_flag = false;
                    
                    if (item.itsStatusSendId!=null) {
                        
                        if (item.stdCourseId==null) {
                                add_flag = true;
                        } else {
                            if (stdCourseId==item.stdCourseId) {
                                add_flag = true;
                            }
                        }

                        sendDate= Helper.toScreenDate(item.sendDate.date,true);

                        
                        if (item.confirmFlag=='Y') {
                            status = "<div class=\"pb-1\"><label class=\"label label-success\">อนุมัติ</label></div>";
                            status_id = "Y";
                            status_confirm_count++;
                        } else if (item.confirmFlag=="N") {
                            status = "<div class=\"pb-1\"><label class=\"label label-danger\">ส่งคืน</label></div>";
                            status_id = "R";
                            status_return_count++;
                        } else {                            
                            if (item.senderTypeId!=1) {
                                status = "<div class=\"pb-1\"><label class=\"label label-default\">รอผล</label></div>";
                            } else {
                                status = "<div class=\"pb-1\"><label class=\"label label-default\">รอตรวจ</label></div>";
                            }
                            status_id = "W";
                            status_wait_count++;
                        }

                        if (item.stdCourseId!=null) {
                            status += "<div class=\"small\">" + item.itsCourseCd + "</div>"; 
                        }

                        status += "<div class=\"small\"><span style=\"white-space: nowrap;\">" + "<i class=\"icofont icofont-time\"></i> "  + sendDate + "</span></div>"; 
                    } else {
                        if (item.senderTypeId!=1) {
                            status = "<div class=\"pb-1\"><label class=\"label label-default\">รอผล</label></div>";
                        } else {
                            status_id = "N";
                            status_none_count++;                            
                        }
                    }

                    if (docTypeId!=item.docTypeId) {
                        $('#tbDocFinalList tbody').append(                            
                            "<tr data-tt-id='" + item.docTypeId + "'>" +                                        
                                    "<td colspan=\"4\" style=\"cursor:pointer\"><span class='folder'></span>" +  item.docTypeTh + "</span></td>" +                                                                            
                            "</tr>"
                        );
                    }                    
                    
                    if (add_flag) {
                        var withStatusSendUrl = (item.itsStatusSendId==null)?"":("/" + item.itsStatusSendId);
                        var activityDetailUrl = "{{ route ('teacher/activity') }}/" + item.activityId + "/" + itsStudentId + withStatusSendUrl;

                        if ((withStatusSendUrl.length>0) || (item.senderTypeId!=1)) {                        
                            if (item.senderTypeId!=1) {
                                $('#tbDocFinalList tbody').append(                            
                                    "<tr data-tt-id='" + item.activityId + "' data-tt-parent-id='" + item.docTypeId + "' data-status-id='" + status_id + "'>" +                                        
                                            "<td><span class='file'></span>" +  item.docNameTh + "</span></td>" +
                                            "<td class=\"justify-content-center small\"><span style=\"white-space: nowrap;\">"+ startDate + "-" + endDate + "</span></td>" +            
                                            "<td class=\"justify-content-center text-center\">" +  status + "</td>" + 
                                            "<td class=\"justify-content-center\"><a href=\"" + activityDetailUrl + "/0" + "\"><button class=\"btn btn-sm btn-outline-primary\"><i class=\"icofont icofont-law-document\"></i> รายละเอียด</button></a></td>" + 
                                    "</tr>"
                                );      
                            } else {
                                $('#tbDocFinalList tbody').append(                            
                                    "<tr data-tt-id='" + item.activityId + "' data-tt-parent-id='" + item.docTypeId + "' data-status-id='" + status_id + "'>" +                                        
                                            "<td><span class='file'></span>" +  item.docNameTh + "</span></td>" +
                                            "<td class=\"justify-content-center small\"><span style=\"white-space: nowrap;\">"+ startDate + "-" + endDate + "</span></td>" +            
                                            "<td class=\"justify-content-center text-center\">" +  status + "</td>" + 
                                            "<td class=\"justify-content-center\"><a href=\"" + activityDetailUrl + "\"><button class=\"btn btn-sm btn-outline-primary\"><i class=\"icofont icofont-law-document\"></i> รายละเอียด</button></a></td>" + 
                                    "</tr>"
                                );     
                            }
                        } else {                        
                            $('#tbDocFinalList tbody').append(                            
                                "<tr data-tt-id='" + item.activityId + "' data-tt-parent-id='" + item.docTypeId + "' data-status-id='" + status_id + "'>" +                                        
                                        "<td><span class='file'></span>" +  item.docNameTh + "</span></td>" +
                                        "<td class=\"justify-content-center small\"><span style=\"white-space: nowrap;\">"+ startDate + "-" + endDate + "</span></td>" +            
                                        "<td class=\"justify-content-center text-center\">" +  status + "</td>" + 
                                        "<td class=\"justify-content-center\"></td>" + 
                                "</tr>"
                            );      
                        }
                    }

                    docTypeId = item.docTypeId;
                });
                                                        
                $("#tbDocFinalList").treetable({ expandable: true });

                $('#btnExpandCollapseDocFinal').removeAttr('disabled');

                $('#div-student-docfinallist').unblock();
            }
        });               
    },
    showDocByStatus:function(status) {
        $("#tbDocList").treetable('collapseAll');     
        $("#tbDocList tr").removeClass('text-danger font-weight-bold');
        
       
        var status_desc = "";
        if(status=="Y") {
            status_desc = "confirm";
        } else if (status=="N") {
            status_desc = "none";
        } else if (status=="W") {
            status_desc = "wait";
        } else if (status=="R") {
            status_desc = "return";
        }

        $('div[id*="div-doc-status-card"]').each(function(idx,item) {
            $(this).removeClass('b-l-primary');
        });

        $('[id="div-doc-status-card-' + status_desc + '"]').addClass("b-l-primary");
        
        var element = $('#tbDocList tr[data-status-id="' + status + '"]')

        if (element.length>0) {
            $.each(element, function (idx, item) {       
                var parent_id = $(this).attr('data-tt-parent-id');
                $(this).addClass('text-danger font-weight-bold');

                if (parent_id) {                                                            
                    $("#tbDocList").treetable("expandNode", parent_id);
                }
            });
        }        
    }
}           
</script>    
@endsection