<div class="card b-l-primary" id="div-student-docfinallist">
    <div class="card-header">
        <i class="h4 icofont icofont-document-folder txt-primary"></i> เอกสารส่งงานปลายภาค                
        <div class="f-right">
            <a href="javascript:;"><i class="icofont icofont-minus"></i></a>            
        </div>                            
    </div>
    <div class="card-block">
        <div class="card job-right-header" id="selectedCourse">
            <div class="card-block">         
                <div class="row">
                    <div class="col-12">
                        วิชาที่ส่งงานปลายภาค
                    </div>                    
                </div>
                
                <div class="form-radio">
                    
                </div>                
            </div>                    
        </div>
        <a class="h5 txt-primary m-1 float-right" id="btnExpandCollapseDocFinal" data-toggle="tooltip" data-placement="top" title="" data-original-title="ย่อ/ขยาย">
            <i class="icofont icofont-expand"></i>
        </a>
        <table class="table table-striped table-bordered" style="width:100%;" id="tbDocFinalList">            
            <thead>
                <tr>
                    <th style="width:45%;" class="bg-primary">                        
                        งาน
                    </th>
                    <th style="width:22%;" class="bg-primary">วันที่</th>                    
                    <th style="width:20%;" class="bg-primary">สถานะ</th>
                    <th style="width:13%;" class="bg-primary"></th>                    
                </tr>
            </thead>
            <tbody>            
            </tbody>
        </table>             
    </div>
</div>
    