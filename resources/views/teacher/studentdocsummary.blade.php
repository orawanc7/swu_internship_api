<div class="row">    
    <div class="col-lg-3 d-none">
        <a href="javascript:Student.showDocByStatus('N');">
            <div class="card dashboard-card-sm" id="div-doc-status-card-none">
                <div class="card-block">
                    <div class="media d-flex">
                        <div class="mr-2">
                            <div class="new-orders">
                                <i class="icofont icofont-not-allowed bg-warning"></i>
                            </div>
                        </div>
                        <div class="media-body">       
                            <span class="counter-txt">                 
                                <span class="counter" id="div-doc-status-none">0</span>                      
                            </span>
                            <h6 class="small">ยังไม่ได้ส่ง</h6>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>    
    <div class="col-lg-4">
        <a href="javascript:Student.showDocByStatus('W');">
            <div class="card dashboard-card-sm" id="div-doc-status-card-wait">
                <div class="card-block">
                    <div class="media d-flex">
                        <div class="mr-2">
                            <div class="new-orders">
                                <i class="icofont icofont-hour-glass bg-secondary"></i>
                            </div>
                        </div>
                        <div class="media-body">       
                            <span class="counter-txt">                 
                                <span class="counter" id="div-doc-status-wait">0</span>                      
                            </span>
                            <h6 class="small">รอตรวจ</h6>
                        </div>
                </div>
            </div>
        </a>
      </div>
    </div>
    <div class="col-lg-4">
        <a href="javascript:Student.showDocByStatus('R');">
            <div class="card dashboard-card-sm" id="div-doc-status-card-return">
                <div class="card-block">
                    <div class="media d-flex order-counter">
                        <div class="mr-2">
                            <div class="new-orders">
                                <i class="icofont icofont-undo bg-danger"></i>
                            </div>
                        </div>
                        <div class="media-body">
                            <span class="counter-txt">
                                <span class="counter" id="div-doc-status-return">0</span>
                            </span>
                            <h6 class="small">ส่งคืน</h6>
                        </div>
                    </div>                
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-4">
        <a href="javascript:Student.showDocByStatus('Y');">
            <div class="card dashboard-card-sm" id="div-doc-status-card-confirm">
                <div class="card-block">
                    <div class="media d-flex order-counter">
                        <div class="mr-2">
                            <div class="new-orders">
                                <i class="icofont icofont-check bg-success"></i>
                            </div>
                        </div>
                        <div class="media-body">
                            <span class="counter-txt">
                                <span class="counter" id="div-doc-status-confirm">0</span>
                            </span>
                            <h6 class="small">อนุมัติ</h6>
                        </div>
                    </div>                
                </div>
            </div>
        </a>
    </div>
</div>