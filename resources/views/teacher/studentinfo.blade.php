<div class="card faq-left">
    <div class="social-profile">               
        <img class="img-fluid width-100" src="{{ profile($itsStudent['studentId']) }}">        
    </div>
    <div class="card-block">
        <h4 class="f-18 f-normal m-b-10 txt-primary">                                    
            {{ $itsStudent['prenameSnameTh'] . $itsStudent['fnameTh'] . " " . $itsStudent['lnameTh'] }}            
        </h4>        
        <h5 class="f-14">เลขประจำตัว {{ $itsStudent['studentId'] }}</h5>                
        <h5 class="f-14">คณะ {{ $itsStudent['deptLnameTh'] }}</h5>                
        <h5 class="f-14">สาขาวิชา {{ $itsStudent['majorLnameTh'] }}</h5>                
        <h5 class="f-14">รหัสวิชา {{ $itsStudent['courseCd'] }}</h5>                                   
        <ul>           
            @if ($itsStudent['leaderFlag']=="Y")
            <li class="faq-contact-card">
                <i class="icofont icofont-king-crown text-danger"></i>หัวหน้านิสิต                                
            </li>
            @endif
            <li class="faq-contact-card">
                <i class="icofont icofont-mobile-phone"></i>
                <a href="tel:{{ $itsStudent['mobileNo'] }}">{{ $itsStudent['mobileNo'] }}</a>
            </li>
            <li class="faq-contact-card">                        
                <i class="icofont icofont-email"></i>
                <a href="mailto:{{ $itsStudent['buasriId'] }}@swu.ac.th">{{ $itsStudent['buasriId'] }}</a>
            </li>
            <li class="faq-contact-card">                        
                <i class="icofont icofont-email"></i>
                <a href="mailto:{{ $itsStudent['gafeEmail'] }}">{{ $itsStudent['gafeEmail'] }}</a>
            </li>
            <li class="faq-contact-card">                        
                <i class="icofont icofont-email"></i>
                <a href="mailto:{{ $itsStudent['otherEmail'] }}">{{ $itsStudent['otherEmail'] }}</a>
            </li>
        </ul>                
    </div>
</div>