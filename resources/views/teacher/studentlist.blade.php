<div class="card" id="div-studentlist">
    <div class="card-header">
        <div class="row">
            <div class="col-md-10">                    
                <i class="h4 icofont icofont-group-students txt-primary"></i> นิสิตในความดูแล
            </div>
            <div class="col-md-2">
                <select name="year" id="year"></select>
            </div>
        </div>        
    </div>
    <div class="card-block">        
        <ul class="nav nav-tabs" role="tablist" id="navStudent">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#sem1">ภาคเรียนที่ 1</a>
            </li>                
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#sem2">ภาคเรียนที่ 2</a>
            </li>                
        </ul>

        <div class="tab-content" id="tab-studentlist">
            <div class="tab-pane fade show active" id="sem1" role="tabpanel">                
<!-- Sem 1 -->
<div class="row">
    <div class="col-md-12">        
        <table class="table table-striped table-bordered table-hover" style="width:100%;" id="tbStudent1" data-rtContainerBreakPoint="599">
            <thead>
                <tr>                                             
                    <th style="width:10%" class="bg-primary text-left text-md-center"></th>
                    <th style="width:15%" class="bg-primary text-left text-md-center">เลขประจำตัว</th>
                    <th style="width:25%" class="bg-primary text-left text-md-cente">ชื่อ-สกุล</th>
                    <th style="width:15%" class="bg-primary text-left text-md-center">คณะ-สาขา</th>
                    <th style="width:10%" class="bg-primary text-left text-md-center">วิชา</th>                            
                    <th style="width:25%" class="bg-primary text-left text-md-center">สถานที่ฝึกประสบการณ์</th>                    
                </tr>
            </thead>
            <tbody>                        
            </tbody>            
        </table>
        <div class="row">
            <div class="col-12 col-sm-6" id="student-total1">
                
            </div>
            <div class="col-12 col-sm-6 text-right" id="student-download1">
                
            </div>
        </div>
    </div>        
</div>
<!-- Sem 1 -->
            </div>
            <div class="tab-pane fade" id="sem2" role="tabpanel">
<!-- Sem 2 -->
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-hover" style="width:100%;" id="tbStudent2" data-rtContainerBreakPoint="599">
            <thead>
                <tr class="bg-primary">                                             
                    <th style="width:10%" class="bg-primary text-left text-md-center"></th>
                    <th style="width:15%" class="bg-primary text-left text-md-center">เลขประจำตัว</th>
                    <th style="width:25%" class="bg-primary text-left text-md-cente"ชื่อ-สกุล</th>
                    <th style="width:15%" class="bg-primary text-left text-md-center">คณะ-สาขา</th>
                    <th style="width:10%" class="bg-primary text-left text-md-center">วิชา</th>                            
                    <th style="width:25%" class="bg-primary text-left text-md-center">สถานที่ฝึกประสบการณ์</th>                                      
                </tr>
            </thead>
            <tbody>                        
            </tbody>                
        </table>
        <div class="row">
            <div class="col-12 col-sm-6" id="student-total2">
        
            </div>
            <div class="col-12 col-sm-6 text-right" id="student-download2">
                
            </div>
        </div>
    </div>        
</div>
<!-- Sem 2 -->
            </div>            
        </div>
        <br>        
    </div>
</div>