<div class="card card-success">
    <div class="panel-heading bg-success">
        <i class="icofont icofont-ui-calendar"></i> กิจกรรมวันนี้ 1 กันยายน 2561
    </div>
    <div class="card-block">        
        <ul class="basic-list list-icons faq-expi">
            <li>                
                <i class="icofont icofont-clock-time m-r-5"></i>
                <h6>บันทึกแบบสังเกตสอน</h6>
                <h6>15-26 พฤษภาคม 2561</h6>
            </li>                
        </ul>
        <div class="new-users-more text-center p-t-10">
            <a href="{{route('teacher/activity')}}"><h6 class="m-b-0 f-w-400">ดูกิจกรรมทั้งหมด<i class="icofont icofont-bubble-right m-l-10"></i></h6></a>
        </div>
    </div>    
</div>